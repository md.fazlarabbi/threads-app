# **Moin**


## What is **Moin** ?

**Moin** is a decentralized messenger application which enables the user to communicate with 
others. The basic characteristics are decentralized, respect of personal data,
full encryption of messages and data, open source, free of charge, free of 
advertising and legally impeccable.

It is intended as a PoC (proof of concept) to demonstrate the performance of distributed 
applications especially in the mobile world.
**Moin** based on the decentralized IPFS technology which represent the backbone of this application.
 Further information is described in the Technology chapter.


## Table of Contents
- [Install](#install)
  - [System Requirements](#system-requirements)
- [Dependencies](#dependencies)
- [Features](#features)
  - [Environment](#environment)
  - [Login](#login)
  - [Use Cases](#use-cases)
  - [User Connection](#user-connection)
  - [Server Mode](#server-mode)
  - [Transparency](#transparency)
- [Technology](#technology)
- [Legal Notices](#legal-notices)
- [Data Privacy](#data-privacy)
- [Security](#security)
- [Privacy Policy](#privacy-policy)
    - [Data Protection](#data-protection)
    - [Android Permissions](#android-permissions)
    - [Amendment of the Data Protection Provisions](#amendment-of-the-data-protection-provisions)
- [Issues](#issues)
  - [Security Issues](#security-issues)
- [Todos](#todos)
- [Contributing](#contributing)


## Install
The **Moin** application can be installed via Google Play Store:

https://play.google.com/store/apps/details?id=groups.app.basic
### System Requirements
An Android runtime environment with SDK 24 is required. (Up till now only Android phones are supported)
## Dependencies
This section describes briefly the main dependencies of the application
* ipfs-go IPFS API (https://github.com/ipfs/go-ipfs) 

## Features
This section describes the intended features of this application in regards of a user perspective.
The features will be marked whether they have been established or when they are intended to be available.

### Environment
**Moin** is intended to run on Android devices (including Chrome OS)

Support OS :
- [x] Android OS SDK >= 24
- [x] Chrome OS devices with Android support

Supported devices :
- [x] Smartphone
- [ ] Tablet 
- [x] Chrome OS devices with Android support

Android App-Stores :
- [x] Goolge Play Store
- [ ]  Amanzon Underground (Version 1.1)
- [ ]  F-Droid (Version 1.1)

### Login
A login into the decentralized platform it not required. Yet the user has the possibility to provide
a name (alias) to communicate with other people. 

In the future, identification systems like Facebook, Google, etc might be used to receive also an thumbnail
of the current user. But it is not planned to publish the picture to the public.
- [ ] Google, Facebook, others... (Version 2.0) 


### Use Cases
This section described briefly what kind of operations are possible with this application.
- [x] The user can connect to others (see section **User Connection**)
- [x] The user can send decrypted messages to others.
- [x] The user can upload decrypted and not encrypted data using the IPFS ecosystem, so that notified users
can download it (usually from the providers device).
- [x] The user can create a group (session) where he can invite others to participate. The creator
of the group makes him the **owner**.
- [x] The user can create a group where the encryption mode for the data and messages is set. Depending
      of the choice all messages and data will be in the group (session) encrypted.
- [x] The user can leave a group.
- [x] The user can reject to take part of a group.
- [x] The user can block other users, which will result in not receiving any messages from that person.
- [x] The user can re-block users
- [x] The user can delete a group (All data will be removed automatically)
- [x] The creator of a group can set a thumbnail for the group
- [x] The creator of a group can remove others users from the group 
- [x] The user has the possibility to run his **own** Moin service (see section **Server Mode**)
- [ ] A user can make video/calls via WebRTC (Version 0.7)
- [ ] A user can send a contact of his own to other users, so that e.g. an **owner** of a group can add him to the session (Version 0.6)


### User Connection
Two kind of user connections are supported. The direct connection via a QR code and an indirect
connection mode where the the QR code will be send via email for example.
In both cases a user has to provide the QR code (which is the Peer ID of the underlying IPFS node)
 and the other party has to scan that code.
- [x] Direct User Connection via QR code
- [x] Indirect User Connection via Email, etc.

### Server Mode
The user has the possibility to run the application in **Server Mode**. 
In **Server Mode** 
- the application is run as a foreground service and will not be closed by the Android system
- the application will be configured differently then the in normal execution mode
    - [x] All messages and data will be a automatically downloaded 
    - [x] IPFS Configuration Changes
        - Routing as client is disabled
        - NAT port map is enabled
        - Bitswap also provides data (Strategic Providing is false)
        - AutoNATService is enabled
        - RelayHop is enabled
         
- the application requires through the changed configuration more energy, so that it make sense to charge it during operation time
- the application requires that network is always available and that the service of the application is reachable from outside
    - [x] The network visibility of the app from will be presented to the user as a permanent notification
    - [x] The port of the server will be presented to the user in form of a permanent notification. 
    In case the user is not reachable from the outside, the user can still do a port forwarding, 
    when he is behind his own router.
    - [x] The user should be informed how the port forwarding is done. In case there is still no
    network visibility, the user should be informed that the **Server Mode** is not possible.
- the application run best, when the user is behind his own router

What are the uses cases to run the application in **Server Mode**?
Assume you want to have your **own** communication and data server to share messages and data with others or with yourself.
Additionally assume you have an **old** smartphone or PC (where you can run Android apps). 
Now you have the possibility to run the **Moin** app in **Server Mode** on that machine.
With your **normal** smartphone, you can now connect to the other device via the **Moin** app.
So for example when you create a session on your daily smartphone, you just add the **old** smartphone to the group.
Of course in the **old** smartphone you have to accept that you will join the session.
From now on you can send data to the **old** smartphone. In the moment the 
**old** smartphone receives the notification it will try to download the data from your daily phone.
In this scenario the **old** smartphone behaves like a backup system.
Even whe you delete all the data on your daily phone, the data are still stored and available on
the server phone or PC.
But the real value of the server or service is that it is available for all members within the group.
Assume that more then you are in the group. So when you are offline and another
person just sends a message within the group, at least the permanent service will download
the data and will provide it to the others in case the sender itself is offline.
There are some other advantages regarding connections within the group, but this would go
to far to describe, it is just to get an idea.




### Transparency
The transparency of the application is very important. It should provide a trust in using this 
application. Though for the most users some transparencies might be too technical.
- [ ] The user should have the possibility to view his public and private key which are required 
for the encrypted communication between users.
- [x] The user should can see the "Privacy Policy" declaration of the application
- [x] The user should can see the "Data Privacy" declaration of the application
- [x] The user should can see the "Security" declaration of the application
- [x] The user should have the possibility to participate to the project. This is achieved by a
link "Issue Report" to the gitlab repository. The nature of the project will be revealed to the user.
- [ ] The user should have the possibility to export all __his__ data to an external device (e.g. SD card) (Version 0.7)

## Technology
The technology section describes briefly the underlying architecture of this application.
In general this application uses the decentralized technology of IPFS.
The IPFS technology is used for establishing a data exchange mechanism between users (peers)
and it used for establishing a direct connection between users.

So how does the mechanism work in general 
- When a user starts his decentralized platform, several steps are done
    - the system creates an __account identification__ for the user (Peer ID)
    - the system creates a **public and private key** for the user 
- When a user wants to communicate with others, several steps are done
    - the user has to provide his __account identification__ to the other via a QR code mechanism
    - the other user scans the QR code, and has to establish a direct communication to the user 
    via IPFS technology
    - when the direct communication succeeds the user knows the public key of the other person
    This **public key** of the other user is important to establish an encrypted communication later
- When a user wants to start a communication to another known user, several steps are done
    - the user has to create a new group and has to invite the person in a follow up step
    - the creation of a group includes the generation of a new encryption key (**symetric_key**), 
    which is only valid for the group and the encryption mode was enabled for the group
    - to invite a person, the user has to send a notification to the other person
    The notification contains the basic information of the session, like **name**,
    **image** and the most important information the **symetric_key**, which is necessary for 
    encrypting the data within the group
     (**Note**: Notification sending is done by using the FCM technology by Google)
    - the message itself is encrypted with the **public key** of the other person
    - the receiver can now uses its **private key** to decrypt the message
    - the receiver can now deceide whether he wants to join the session and send its
    reply to the sender via a notification provided by FCM
    - This encryption mechanism is well known as **hybrid encryption**
- When a user send a message to a group he has joined, several steps are done
    - the message of the user will be encrypted with the **symetric_key** of the group. (Each
    user within a group, can now decrypt the message, because they have the key also)
    - the encrypted message is stored within your internal IPFS node
    - each user within the group has to be notified, that a new message is ready for download
    - the message contains the CID (__content id__) of the encrypted message and the PID (the users __account identification__)
- When a user receives a message from a group, several steps are done
    - the received message contains the CID, which can now be downloaded by the IPFS ecosystem
    - when the message is finally downloaded, the message has to be encrypted and presented to the user
    - the received message contains the PID of the sender, so that a direct connection between
    the two peers can be established
    - additionally to the direct communication to the sender, also a direct connection to each
    member of the group will be established, so that a faster download of data can be established

## Legal Notices
In general the idea is that the user is responsible for everything, because the provider of this
app does not run any server. It just provides a platform which can be used. Also no financial 
interests are involved. 

## Data Privacy
This section describes the data privacy from the users perspective.

__Who can see my personal information__
-  User Name (Alias)
Any participant of the application who are connected with the user either directly or via a group
-  IPFS Content Data
In the current version it is assumed that all have access to your content data through IPFS. 
Though it is very unlikely that they can access your data, because for a real access the CID 
of the information is required which itself it just published to the members of a session.
- Encrypted Messages and Data
Only the indented participants have access to your messages and data, ensured by a hybrid
encryption mechanism.


## Security
The **Moin** application is designed to provide full encryption of all data which are shared
between users within a session. The owner of a group has the possibility to enable the encryption during
a session (group) creation. The encryption mode is default set to false, because of increased
performance requirements.


## Privacy Policy
This section describes the privacy policy of the **Moin** application.

### Data Protection
As an application provider, we take the protection of all personal data very seriously.
All personal information is treated confidentially and in accordance with the legal requirements,
regulations, as explained in this privacy policy.
            
This application is designed in a way, so that the user only shares its name. 
Which is used to identified each other for the communication. 
Of course the user can choose any alias name.

Never will data collected by us and passed to third parties.
The users behaviour is also not analyzed and tracked in any way by this application.
            
The user is self responsible for what kind of data is shared via the IPFS network.
This kind of IPFS data information is also not tracked by this application.
            
### Android Permissions
This section describes briefly why specific Android permissions are required.
- __Camera__ 
    - The camera permission is required to read QR codes from others users.
    - The camera permission is required so that the user can share images via the local IPFS node.
- __Record Audio__ The audio record permission is required so that the user can share audio content with other users. 
- __Foreground Service__ The foreground service permission is required to run the IPFS node over a
longer period of time. Its useful for providing your IPFS content to others users over a longer period
of time.
- __Internet__ Precondition for running the local IPFS node.
- __Access Network State__ The access to the network state is required, so that the best configuration 
for the local IPFS node can be configured.
                    
### Amendment of the Data Protection Provisions
Our data protection declaration can be adapted at irregular intervals,
so that they comply with current legal requirements or in order to make changes
of our services, e.g. in the insertion of new offers. For
Your next visit will then automatically be subject to the new data protection declaration.

## Issues
This section describes known issues so far.
- lots ....

### Security Issues
This application depends fully on the IPFS technology. Therefore security issues which
apply for those technologies are also valid for this application.
For further information see : https://github.com/ipfs/go-ipfs
## Todos
This section contains a disordered list of items which has to be clarified and solved.
- WebRTC support (Version 0.7)
- Find the right configuration for IPFS in the mobile environment
    - should be a different configuration when user is connected via a real public IP address
    - different configuration when connected via WLAN of via LTE ?
    - what happens when connection is too slow
    - what about memory and battery live (profile **lowpower**)
- Legal aspects
    - what about article 13 of european law, how affect it this app
    - present legal documentation to the user ("Data Privacy", "Security" and "Privacy Policy") 
- Reuse account
    - Should the user have the possibility to use the same account on different machines
    - In general it is possible, but very low hanging fruit
- Battery and memory consumption should be evaluated
- General performance
    - Does it make sense to restrict the app of minimum LTE connection 
    - better Offline support ?
- DRM (Digital Right Management)
    - Does it make sense to think about it? Probably not in this app (depends of the legal notices outcome)
+- Develop Open Protocol
    - Other messenger application can connect to this application
    - Communicate to other messenger application via their protocol

## Contributing
It is very welcome to contribute to this application. Any advice (legal, technical, etc.) and
also contribution to the code (UI-API improvements) or feature request are very welcome.
Since this project is fully open source and non profitable, any help is appreciated.

