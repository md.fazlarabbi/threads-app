package threads.ipfs;

public class CloseableProgress implements Progress {
    private boolean done = false;
    private long size = 0L;

    @Override
    public void setProgress(int percent) {

    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public boolean isClosed() {
        return false;
    }
}
