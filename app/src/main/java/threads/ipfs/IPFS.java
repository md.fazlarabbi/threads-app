package threads.ipfs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.ServerSocket;
import java.security.Key;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import moin.Listener;
import moin.LoaderStream;
import moin.LsInfoClose;
import moin.Node;
import moin.Reader;
import moin.Writer;
import threads.LogUtils;

public class IPFS implements Listener {


    @NonNull
    public static final List<String> Bootstrap = new ArrayList<>(Arrays.asList(
            "/ip4/147.75.80.110/tcp/4001/p2p/QmbFgm5zan8P6eWWmeyfncR5feYEMPbht5b1FW1C37aQ7y", // default relay  libp2p
            "/ip4/147.75.195.153/tcp/4001/p2p/QmW9m57aiBDHAkKj9nmFSEn7ZqrcF1fZS4bipsTCHburei",// default relay  libp2p
            "/ip4/147.75.70.221/tcp/4001/p2p/Qme8g49gm3q4Acp7xWBKg3nAa9fxZ1YmyDJdyGgoG6LsXh",// default relay  libp2p

            "/ip4/104.131.131.82/tcp/4001/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"// mars.i.ipfs.io

    ));
    private static final String PREF_KEY = "prefKey";
    private static final String PID_KEY = "pidKey";


    private static final String HIGH_WATER_KEY = "highWaterKey";
    private static final String LOW_WATER_KEY = "lowWaterKey";
    private static final String GRACE_PERIOD_KEY = "gracePeriodKey";
    private static final String PUBLIC_KEY = "publicKey";
    private static final String AGENT_KEY = "agentKey";
    private static final String PRIVATE_KEY = "privateKey";
    private static final String P2P_CIRCUIT = "p2p-circuit";
    private static final long TIMEOUT = 30000L;
    private static final String TAG = IPFS.class.getSimpleName();
    private static IPFS INSTANCE = null;

    private final File baseDir;
    private final File cacheDir;
    private final Node node;
    private final Object locker = new Object();
    private final Gson gson = new Gson();
    @Nullable
    private ReachableListener mReachableListener = null;
    @NonNull
    private Reachable mReachable = Reachable.UNKNOWN;
    @Nullable
    private DaemonListener mDaemonListener;


    private IPFS(@NonNull Context context) throws Exception {
        this.baseDir = context.getFilesDir();
        this.cacheDir = context.getCacheDir();

        PID host = getPID(context);

        boolean init = host == null;

        node = new Node(this, baseDir.getAbsolutePath());

        if (init) {
            node.identity();

            setPeerID(context, PID.create(node.getPeerID()));
            setPublicKey(context, node.getPublicKey());
            setPrivateKey(context, node.getPrivateKey());

        } else {
            node.setPeerID(host.getPid());
            node.setPrivateKey(IPFS.getPrivateKey(context));
            node.setPublicKey(IPFS.getPublicKey(context));
        }

        /* addNoAnnounce
         "/ip4/10.0.0.0/ipcidr/8",
                "/ip4/100.64.0.0/ipcidr/10",
                "/ip4/169.254.0.0/ipcidr/16",
                "/ip4/172.16.0.0/ipcidr/12",
                "/ip4/192.0.0.0/ipcidr/24",
                "/ip4/192.0.0.0/ipcidr/29",
                "/ip4/192.0.0.8/ipcidr/32",
                "/ip4/192.0.0.170/ipcidr/32",
                "/ip4/192.0.0.171/ipcidr/32",
                "/ip4/192.0.2.0/ipcidr/24",
                "/ip4/192.168.0.0/ipcidr/16",
                "/ip4/198.18.0.0/ipcidr/15",
                "/ip4/198.51.100.0/ipcidr/24",
                "/ip4/203.0.113.0/ipcidr/24",
                "/ip4/240.0.0.0/ipcidr/4"
         */


        node.setAgent(IPFS.getStoredAgent(context));

        node.setPort(nextFreePort());

        node.setGracePeriod(getGracePeriod(context));
        node.setHighWater(getHighWater(context));
        node.setLowWater(getLowWater(context));

        node.openDatabase();
    }

    public static void copy(InputStream source, OutputStream sink) throws IOException {
        byte[] buf = new byte[4096];
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
        }
    }

    private static void setPublicKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PUBLIC_KEY, key);
        editor.apply();
    }

    private static void setStoredAgent(@NonNull Context context, @NonNull String agent) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(AGENT_KEY, agent);
        editor.apply();
    }

    private static String getStoredAgent(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(AGENT_KEY, "/go-ipfs/0.5.0/lite");

    }

    private static void setPrivateKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRIVATE_KEY, key);
        editor.apply();
    }

    @NonNull
    public static String getPublicKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(PUBLIC_KEY, "");

    }

    @NonNull
    private static String getPrivateKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(PRIVATE_KEY, "");

    }

    private static void setPeerID(@NonNull Context context, @NonNull PID pid) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PID_KEY, pid.getPid());
        editor.apply();
    }

    @Nullable
    public static PID getPID(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        String pid = sharedPref.getString(PID_KEY, "");
        if (pid.isEmpty()) {
            return null;
        }
        return PID.create(pid);
    }

    public static void setLowWater(@NonNull Context context, int lowWater) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(LOW_WATER_KEY, lowWater);
        editor.apply();
    }

    private static int getLowWater(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(LOW_WATER_KEY, 20);
    }


    @NonNull
    private static String getGracePeriod(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(GRACE_PERIOD_KEY, "30s");
    }

    public static void setGracePeriod(@NonNull Context context, @NonNull String gracePeriod) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(GRACE_PERIOD_KEY, gracePeriod);
        editor.apply();

    }

    public static void setHighWater(@NonNull Context context, int highWater) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(HIGH_WATER_KEY, highWater);
        editor.apply();
    }

    private static int getHighWater(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(HIGH_WATER_KEY, 40);
    }

    @NonNull
    public static IPFS getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            synchronized (IPFS.class) {
                if (INSTANCE == null) {
                    try {
                        INSTANCE = new IPFS(context);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return INSTANCE;
    }

    public static String getDeviceName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            if (model.startsWith(manufacturer)) {
                return capitalize(model);
            }
            return capitalize(manufacturer) + " " + model;
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return "";
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase = phrase.concat("" + Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase = phrase.concat("" + c);
        }
        return phrase;
    }


    private static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(4001, 65535);
            }
        }
    }

    public static void logCacheDir(@NonNull Context context) {
        try {
            File[] files = context.getCacheDir().listFiles();
            if (files != null) {
                for (File file : files) {
                    LogUtils.info(TAG, "" + file.length() + " " + file.getAbsolutePath());
                    if (file.isDirectory()) {
                        File[] children = file.listFiles();
                        if (children != null) {
                            for (File child : children) {
                                LogUtils.info(TAG, "" + child.length() + " " + child.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static void logBaseDir(@NonNull Context context) {
        try {
            File[] files = context.getFilesDir().listFiles();
            if (files != null) {
                for (File file : files) {
                    LogUtils.info(TAG, "" + file.length() + " " + file.getAbsolutePath());
                    if (file.isDirectory()) {
                        File[] children = file.listFiles();
                        if (children != null) {
                            for (File child : children) {
                                LogUtils.info(TAG, "" + child.length() + " " + child.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    private static void cleanDir(@NonNull File dir) {
        try {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        cleanDir(file);
                        boolean result = file.delete();
                        if (!result) {
                            LogUtils.info(TAG, "File not deleted.");
                        }
                    } else {
                        boolean result = file.delete();
                        if (!result) {
                            LogUtils.info(TAG, "File not deleted.");
                        }
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    static void cleanCacheDir(@NonNull Context context) {

        try {
            File[] files = context.getCacheDir().listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        cleanDir(file);
                        boolean result = file.delete();
                        if (!result) {
                            LogUtils.info(TAG, "File not deleted.");
                        }
                    } else {
                        boolean result = file.delete();
                        if (!result) {
                            LogUtils.info(TAG, "File not deleted.");
                        }
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    public long getTotalSpace() {
        return this.baseDir.getTotalSpace();
    }

    public long getFreeSpace() {
        return this.baseDir.getFreeSpace();
    }

    public void setReachableListener(@Nullable ReachableListener listener) {
        this.mReachableListener = listener;
    }

    @NonNull
    public Reachable getReachable() {
        return mReachable;
    }

    private void setReachable(@NonNull Reachable reachable) {
        mReachable = reachable;
        if (mReachableListener != null) {
            mReachableListener.status(mReachable);
        }
    }

    void bootstrap() {

        checkDaemon();

        for (String address : Bootstrap) {

            boolean result = swarmConnect(address, 10);
            LogUtils.debug(TAG, result + " \n Bootstrap : " + address);


        }

        List<String> multiAddresses = DnsAddrResolver.getMultiAddresses();
        for (String address : multiAddresses) {

            boolean result = swarmConnect(address, 10);
            LogUtils.warning(TAG, result + " \n Bootstrap : " + address);


        }

    }

    private void checkDaemon() {
        if (!isDaemonRunning()) {
            startDaemon();
        }
    }

    private void storeToOutputStream(@NonNull OutputStream os, @NonNull Progress progress,
                                     @NonNull CID cid, int blockSize) throws Exception {

        long totalRead = 0;
        AtomicInteger atomicProgress = new AtomicInteger(0);
        Reader reader = getReader(cid);
        try {

            reader.load(blockSize);
            long read = reader.getRead();
            while (read > 0) {

                if (progress.isClosed()) {
                    throw new RuntimeException("Progress closed");
                }

                // calculate progress
                totalRead += read;
                if (progress.getSize() > 0) {
                    int percent = (int) ((totalRead * 100.0f) / progress.getSize());
                    if (atomicProgress.getAndSet(percent) < percent) {
                        progress.setProgress(percent);
                    }
                }


                byte[] bytes = reader.getData();
                os.write(bytes, 0, bytes.length);

                reader.load(blockSize);
                read = reader.getRead();
            }
        } finally {
            reader.close();
        }

    }

    @Nullable
    public PeerInfo id(@NonNull Peer peer, int timeout) {

        checkDaemon();
        return id(peer.getPid(), timeout);
    }

    @Nullable
    public PeerInfo id(@NonNull PID pid, int timeout) {

        checkDaemon();
        try {
            String json = node.idWithTimeout(pid.getPid(), timeout);
            Map map = gson.fromJson(json, Map.class);
            return PeerInfo.create(map);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        return null;

    }

    @Nullable
    public PeerInfo id() {

        checkDaemon();
        try {
            String json = node.id();
            Map map = gson.fromJson(json, Map.class);
            return PeerInfo.create(map);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    boolean checkRelay(@NonNull Peer peer, int timeout) {

        checkDaemon();

        AtomicBoolean success = new AtomicBoolean(false);
        //noinspection CatchMayIgnoreException
        try {

            PID pid = PID.create("QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u"); // DUMMY

            String address = relayAddress(peer, pid);
            node.swarmConnect(address, timeout);

        } catch (Throwable e) {
            String line = e.getLocalizedMessage();
            if (line != null) {
                if (line.contains("HOP_NO_CONN_TO_DST")) {
                    success.set(true);
                }
            }
        }
        return success.get();
    }

    @Nullable
    public PeerInfo pidInfo(@NonNull PID pid) {

        checkDaemon();
        try {
            String json = node.pidInfo(pid.getPid());
            Map map = gson.fromJson(json, Map.class);
            return PeerInfo.create(map);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        return null;

    }

    @NonNull
    private String relayAddress(@NonNull Peer relay, @NonNull PID pid) {

        return relayAddress(relay.getMultiAddress(), relay.getPid(), pid);
    }

    @NonNull
    private String relayAddress(@NonNull String address, @NonNull PID relay, @NonNull PID pid) {

        return address + "/p2p/" + relay.getPid() +
                "/" + P2P_CIRCUIT + "/p2p/" + pid.getPid();
    }

    private boolean specificRelay(@NonNull PID relay, @NonNull PID pid, int timeout) {

        checkDaemon();

        if (swarmConnect(relay, timeout)) {
            Peer peer = swarmPeer(relay);
            if (peer != null) {
                return swarmConnect(relayAddress(peer, pid), timeout);
            }
        }


        return false;
    }

    boolean relay(@NonNull PID relay, @NonNull PID pid, int timeout) {
        checkDaemon();

        return specificRelay(relay, pid, timeout);

    }

    public boolean swarmConnect(@NonNull PID pid, int timeout) {
        checkDaemon();
        return swarmConnect("/p2p/" +
                pid.getPid(), timeout);
    }

    public boolean swarmConnect(@NonNull Peer peer, int timeout) {
        checkDaemon();
        String ma = peer.getMultiAddress() + "/p2p/" + peer.getPid();
        return swarmConnect(ma, timeout);

    }

    public boolean swarmConnect(@NonNull String multiAddress, int timeout) {
        checkDaemon();
        try {
            return node.swarmConnect(multiAddress, timeout);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return false;
    }

    public boolean isConnected(@NonNull PID pid) {

        if (!isDaemonRunning()) {
            return false;
        }

        try {
            return node.isConnected(pid.getPid());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return false;
    }

    @Nullable
    public Peer swarmPeer(@NonNull PID pid) {
        checkDaemon();
        try {
            String json = node.swarmPeer(pid.getPid());
            if (json != null && !json.isEmpty()) {
                Map map = gson.fromJson(json, Map.class);
                if (map != null) {
                    return Peer.create(map);
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @NonNull
    public List<Peer> swarmPeers() {
        checkDaemon();
        List<Peer> peers = swarm_peers();
        peers.sort(Peer::compareTo);
        return peers;
    }

    @NonNull
    private List<Peer> swarm_peers() {

        List<Peer> peers = new ArrayList<>();
        if (isDaemonRunning()) {
            try {
                String json = node.swarmPeers();
                if (json != null && !json.isEmpty()) {
                    Map map = gson.fromJson(json, Map.class);
                    if (map != null) {

                        Object object = map.get("Peers");
                        if (object instanceof List) {
                            List list = (List) object;
                            if (!list.isEmpty()) {
                                for (Object entry : list) {
                                    if (entry instanceof Map) {
                                        Map peer = (Map) entry;
                                        peers.add(Peer.create(peer));
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
        return peers;
    }

// --Commented out by Inspection START (4/28/2020 7:02 PM):
//    private boolean swarmDisconnect(@NonNull PID pid) {
//        checkDaemon();
//        try {
//            return node.swarmDisconnect("/p2p/" + pid.getPid());
//        } catch (Throwable e) {
//            LogUtils.error(TAG, e);
//        }
//
//        return false;
//    }
// --Commented out by Inspection STOP (4/28/2020 7:02 PM)

    public void rm(@NonNull CID cid) {
        try {
            node.rm(cid.getCid());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    public long getSwarmPort() {
        return node.getPort();
    }

    public void setDaemonListener(@NonNull DaemonListener daemonListener) {
        this.mDaemonListener = daemonListener;
    }

    private synchronized void startDaemon() {
        if (!node.getRunning()) {
            synchronized (locker) {
                if (!node.getRunning()) {
                    AtomicBoolean failure = new AtomicBoolean(false);
                    ExecutorService executor = Executors.newSingleThreadExecutor();
                    AtomicReference<String> exception = new AtomicReference<>("");
                    executor.submit(() -> {
                        try {
                            long port = node.getPort();
                            if (!isLocalPortFree((int) port)) {
                                node.setPort(nextFreePort());
                            }
                            node.daemon();

                        } catch (Throwable e) {
                            failure.set(true);
                            exception.set("" + e.getLocalizedMessage());
                            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                        }
                    });
                    long time = 0L;
                    while (!node.getRunning() && time <= TIMEOUT && !failure.get()) {
                        time = time + 100L;
                        try {
                            Thread.sleep(100);
                        } catch (Throwable e) {
                            throw new RuntimeException(exception.get());
                        }
                    }
                    if (failure.get()) {
                        throw new RuntimeException(exception.get());
                    }

                    if (mDaemonListener != null) {
                        mDaemonListener.swarmPort(node.getPort());
                    }
                }
            }
        }
    }

    @Nullable
    public CID storeData(@NonNull byte[] data, @NonNull String key) {

        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return storeInputStream(inputStream, key, new CloseableProgress());
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
        return null;
    }


    @Nullable
    public CID storeText(@NonNull String content, @NonNull String key) {

        try (InputStream inputStream = new ByteArrayInputStream(content.getBytes())) {
            return storeInputStream(inputStream, key, new CloseableProgress());
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
        return null;
    }

    @Nullable
    List<LinkInfo> ls(@NonNull CID cid, @NonNull Closeable closeable) {
        checkDaemon();
        List<LinkInfo> infoList = new ArrayList<>();
        try {

            node.ls(cid.getCid(), new LsInfoClose() {
                @Override
                public boolean close() {
                    return closeable.isClosed();
                }

                @Override
                public void lsLink(String json) {
                    try {
                        Map map = gson.fromJson(json, Map.class);
                        LinkInfo info = LinkInfo.create(map);
                        infoList.add(info);
                    } catch (Throwable e) {
                        LogUtils.error(TAG, e);
                    }
                }
            });

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }
        return infoList;
    }

    @Nullable
    public CID storeFile(@NonNull File target, @NonNull String key) {

        try (InputStream io = new FileInputStream(target)) {
            return storeInputStream(io, key, new CloseableProgress());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @Nullable
    CID storeFile(@NonNull File target) {

        try (InputStream io = new FileInputStream(target)) {
            return storeInputStream(io);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @NonNull
    public String getPrivateKey() throws Exception {
        return node.getRawPrivateKey();
    }

    @NonNull
    public Reader getReader(@NonNull CID cid) throws Exception {
        return node.getReader(cid.getCid());
    }

    @NonNull
    private Writer getWriter() throws Exception {
        return node.getWriter();
    }

    private boolean loadToOutputStream(@NonNull OutputStream outputStream, @NonNull CID cid,
                                       @NonNull String key, @NonNull CloseableProgress progress) {

        try (InputStream inputStream = loadStream(cid, progress, key)) {
            if (inputStream == null) {
                return false;
            }
            copy(inputStream, outputStream);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return false;
        }
        return progress.isDone();

    }

    private void getToOutputStream(@NonNull OutputStream outputStream, @NonNull CID cid,
                                   @NonNull String key) throws Exception {
        try (InputStream inputStream = getInputStream(cid, key)) {
            copy(inputStream, outputStream);
        }
    }

    public boolean loadToFile(@NonNull File file, @NonNull CID cid, @NonNull String key,
                              @NonNull CloseableProgress progress) {
        checkDaemon();

        if (key.isEmpty()) {
            return loadToFile(file, cid, progress);
        } else {
            try (FileOutputStream outputStream = new FileOutputStream(file)) {
                return loadToOutputStream(outputStream, cid, key, progress);
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
                return false;
            }
        }

    }

    boolean loadToFile(@NonNull File file, @NonNull CID cid, @NonNull CloseableProgress progress) {
        checkDaemon();

        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            return loadToOutputStream(outputStream, cid, "", progress);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return false;
        }
    }

    private void storeToOutputStream(@NonNull OutputStream os, @NonNull CID cid, int blockSize) throws Exception {

        Reader reader = getReader(cid);
        try {

            reader.load(blockSize);
            long read = reader.getRead();
            while (read > 0) {
                byte[] bytes = reader.getData();

                os.write(bytes, 0, bytes.length);

                reader.load(blockSize);
                read = reader.getRead();
            }
        } finally {
            reader.close();
        }

    }

    @NonNull
    private InputStream loadStream(@NonNull CID cid, @NonNull CloseableProgress progress) throws Exception {


        PipedOutputStream pos = new PipedOutputStream();
        PipedInputStream pis = new PipedInputStream(pos);

        List<LinkInfo> info = ls(cid, progress);
        if (info == null) {
            throw new RuntimeException("closed " + progress.isClosed());
        }


        final AtomicInteger atomicProgress = new AtomicInteger(0);

        progress.setProgress(0);
        final AtomicBoolean close = new AtomicBoolean(false);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {


            try {
                node.getStream(cid.getCid(), new LoaderStream() {
                    int totalRead = 0;
                    long size = 0;

                    @Override
                    public boolean close() {
                        return close.get() || progress.isClosed();
                    }


                    @Override
                    public long read(byte[] bytes) {
                        try {
                            pos.write(bytes);

                            int bytesRead = bytes.length;
                            if (bytesRead > 0) {

                                totalRead += bytesRead;
                                if (size > 0) {
                                    int percent = (int) ((totalRead * 100.0f) / size);
                                    if (atomicProgress.getAndSet(percent) < percent) {
                                        progress.setProgress(percent);
                                    }
                                }
                            }
                            progress.setDone(totalRead == size);

                        } catch (Throwable e) {
                            close.set(true);
                            progress.setDone(false);
                            // Ignore exception might be on pipe is closed
                            LogUtils.error(TAG, e);
                        }
                        return -1;
                    }

                    @Override
                    public void size(long size) {
                        progress.setSize(size);
                        this.size = size;
                        if (size == 0) { // special case (empty file)
                            progress.setDone(true);
                        }
                    }

                });
            } catch (Throwable e) {
                progress.setDone(false);
                LogUtils.error(TAG, e);
            } finally {
                try {
                    pos.close();
                } catch (Throwable ec) {
                    LogUtils.error(TAG, ec);
                }
            }
        });


        return pis;


    }

    public void storeToFile(@NonNull File file, @NonNull CID cid, @NonNull String key, int blockSize) throws Exception {

        if (key.isEmpty()) {
            storeToFile(file, cid, blockSize);
        } else {
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                storeToOutputStream(fileOutputStream, cid, key, blockSize);
            }
        }
    }

    public void storeToOutputStream(@NonNull OutputStream os, @NonNull Progress progress,
                                    @NonNull CID cid, @NonNull String key, int blockSize)
            throws Exception {

        LogUtils.info(TAG, "storeToOutputStream : " + key);
        if (key.isEmpty()) {
            storeToOutputStream(os, progress, cid, blockSize);
        } else {
            Key aesKey = Encryption.getKey(key);
            @SuppressLint("GetInstance") Cipher cipher = Cipher.getInstance(Encryption.AES);
            cipher.init(Cipher.DECRYPT_MODE, aesKey);

            try (CipherOutputStream outputStream = new CipherOutputStream(os, cipher)) {
                storeToOutputStream(outputStream, progress, cid, blockSize);
            }

        }
    }

    public void storeToOutputStream(@NonNull OutputStream os, @NonNull CID cid,
                                    @NonNull String key, int blockSize) throws Exception {

        LogUtils.info(TAG, "storeToOutputStream : " + key);
        if (key.isEmpty()) {
            storeToOutputStream(os, cid, blockSize);
        } else {
            Key aesKey = Encryption.getKey(key);
            @SuppressLint("GetInstance") Cipher cipher = Cipher.getInstance(Encryption.AES);
            cipher.init(Cipher.DECRYPT_MODE, aesKey);

            try (CipherOutputStream outputStream = new CipherOutputStream(os, cipher)) {
                storeToOutputStream(outputStream, cid, blockSize);
            }

        }
    }

    void storeToFile(@NonNull File file, @NonNull CID cid, int blockSize) throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            storeToOutputStream(fileOutputStream, cid, blockSize);
        }

    }

    @NonNull
    public File getCacheDir() {
        return this.cacheDir;
    }

    @NonNull
    public File createCacheFile() throws IOException {
        return File.createTempFile("temp", ".cid", getCacheDir());
    }

    @NonNull
    public File createCacheFile(@NonNull CID cid) throws IOException {

        File file = new File(getCacheDir(), cid.getCid());
        if (file.exists()) {
            boolean result = file.delete();
            if (!result) {
                LogUtils.info(TAG, "Deleting failed");
            }
        }
        boolean succes = file.createNewFile();
        if (!succes) {
            LogUtils.info(TAG, "Failed create a new file");
        }
        return file;
    }

    @Nullable
    private CID storeInputStream(@NonNull InputStream inputStream, @NonNull Progress progress) {


        String res = "";
        try {
            Writer writer = getWriter();
            res = writer.stream(new WriterStream(writer, inputStream, progress));
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        if (!progress.isClosed()) {
            if (!res.isEmpty()) {
                return CID.create(res);
            }
        }
        return null;
    }

    @Nullable
    private CID storeInputStream(@NonNull InputStream inputStream) {


        String res = "";
        try {
            Writer writer = getWriter();
            res = writer.stream(new WriterStream(writer, inputStream, new CloseableProgress()));
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        if (!res.isEmpty()) {
            return CID.create(res);
        }
        return null;
    }

    @Nullable
    public CID storeInputStream(@NonNull InputStream inputStream, @NonNull String key,
                                @NonNull Progress progress) throws Exception {

        LogUtils.info(TAG, "storeInputStream : " + key);
        if (!key.isEmpty()) {
            Key aesKey = Encryption.getKey(key);
            @SuppressLint("GetInstance") Cipher cipher = Cipher.getInstance(Encryption.AES);
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            try (CipherInputStream cipherStream = new CipherInputStream(inputStream, cipher)) {
                return storeInputStream(cipherStream, progress);
            }
        } else {
            return storeInputStream(inputStream, progress);
        }

    }


    @Nullable
    public String loadText(@NonNull CID cid, @NonNull String key, @NonNull CloseableProgress progress) {
        checkDaemon();
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            boolean success = loadToOutputStream(outputStream, cid, key, progress);
            if (success) {
                return new String(outputStream.toByteArray());
            } else {
                return null;
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }
    }


    @Nullable
    public String getText(@NonNull CID cid, @NonNull String key) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            getToOutputStream(outputStream, cid, key);
            return new String(outputStream.toByteArray());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }
    }

    @Nullable
    public byte[] getData(@NonNull CID cid, @NonNull String key) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            getToOutputStream(outputStream, cid, key);
            return outputStream.toByteArray();
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }
    }


    @Nullable
    public byte[] loadData(@NonNull CID cid, @NonNull String key, @NonNull CloseableProgress progress) {
        checkDaemon();
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            boolean success = loadToOutputStream(outputStream, cid, key, progress);
            if (success) {
                return outputStream.toByteArray();
            } else {
                return null;
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }

    }

    @Nullable
    byte[] loadData(@NonNull CID cid, @NonNull CloseableProgress progress) {
        checkDaemon();
        return loadData(cid, "", progress);
    }

    @Nullable
    private InputStream loadStream(@NonNull CID cid, @NonNull CloseableProgress progress, @NonNull String key) {
        try {
            LogUtils.info(TAG, "loadStream : " + key);
            if (key.isEmpty()) {
                return loadStream(cid, progress);
            } else {
                Key aesKey = Encryption.getKey(key);
                @SuppressLint("GetInstance") Cipher cipher = Cipher.getInstance(Encryption.AES);
                cipher.init(Cipher.DECRYPT_MODE, aesKey);
                return new CipherInputStream(loadStream(cid, progress), cipher);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @NonNull
    private InputStream getInputStream(@NonNull CID cid, @NonNull String key) throws Exception {

        LogUtils.info(TAG, "getInputStream : " + key);
        if (key.isEmpty()) {
            return getInputStream(cid, 4096);
        } else {
            Key aesKey = Encryption.getKey(key);
            @SuppressLint("GetInstance") Cipher cipher = Cipher.getInstance(Encryption.AES);
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            return new CipherInputStream(getInputStream(cid, 4096), cipher);
        }
    }

// --Commented out by Inspection START (4/28/2020 6:59 PM):
//    public boolean swarmDisconnect(@NonNull Peer peer) {
//        checkDaemon();
//        return swarmDisconnect(peer.getPid());
//    }
// --Commented out by Inspection STOP (4/28/2020 6:59 PM)

    public void gc() {
        try {
            node.repoGC();
        } catch (Throwable e) {
            LogUtils.info(TAG, "" + e.getMessage());
        }

    }

    /*
    @Override
    public void connected(String pid) {
        LogUtils.error(TAG, "connected : " + pid);
    }*/

    @Override
    public void error(String message) {
        if (message != null && !message.isEmpty()) {
            LogUtils.error(TAG, "" + message);
        }
    }

    @Override
    public void info(String message) {
        if (message != null && !message.isEmpty()) {
            LogUtils.info(TAG, "" + message);
        }
    }


    @Override
    public void reachablePrivate() {
        setReachable(Reachable.PRIVATE);
    }

    @Override
    public void reachablePublic() {
        setReachable(Reachable.PUBLIC);
    }

    @Override
    public void reachableUnknown() {
        setReachable(Reachable.UNKNOWN);
    }

    @Override
    public void verbose(String s) {
        LogUtils.verbose(TAG, "" + s);
    }

    @NonNull
    public InputStream getInputStream(@NonNull CID cid, int blockSize) throws Exception {
        Reader reader = getReader(cid);
        return new ReaderInputStream(reader, blockSize);

    }

    public boolean isDaemonRunning() {
        return node.getRunning();
    }

// --Commented out by Inspection START (4/28/2020 6:55 PM):
//    public String getAgent() {
//        return node.getAgent();
//    }
// --Commented out by Inspection STOP (4/28/2020 6:55 PM)

    public void setAgent(@NonNull Context context, @NonNull String agent) {
        node.setAgent(agent);
        IPFS.setStoredAgent(context, agent);
    }

    public boolean isValidCID(String multihash) {
        try {
            this.node.cidCheck(multihash);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    public boolean isValidPID(String multihash) {
        try {
            this.node.pidCheck(multihash);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }


    @SuppressWarnings("WeakerAccess")
    public interface DaemonListener {
        void swarmPort(long port);
    }

    @SuppressWarnings("WeakerAccess")
    public interface ReachableListener {
        void status(@NonNull Reachable reachable);
    }

    private static class ReaderInputStream extends InputStream implements AutoCloseable {
        private final Reader mReader;
        private final int mBlockSize;
        private int position = 0;
        private byte[] data = null;

        ReaderInputStream(@NonNull Reader reader, int blockSize) {
            mReader = reader;
            mBlockSize = blockSize;
        }

        @Override
        public int read() throws IOException {

            try {
                if (data == null) {
                    invalidate();
                    preLoad();
                }
                if (data == null) {
                    return -1;
                }
                if (position < data.length) {
                    byte value = data[position];
                    position++;
                    return (value & 0xff);
                } else {
                    invalidate();
                    if (preLoad()) {
                        byte value = data[position];
                        position++;
                        return (value & 0xff);
                    } else {
                        return -1;
                    }
                }


            } catch (Throwable e) {
                throw new IOException(e);
            }
        }

        private void invalidate() {
            position = 0;
            data = null;
        }


        private boolean preLoad() throws Exception {
            mReader.load(mBlockSize);
            int read = (int) mReader.getRead();
            if (read > 0) {
                data = new byte[read];
                byte[] values = mReader.getData();
                System.arraycopy(values, 0, data, 0, read);
                return true;
            }
            return false;
        }

        public void close() {
            try {
                mReader.close();
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }

    private static class WriterStream implements moin.WriterStream {
        private final InputStream mInputStream;
        private final Writer mWriter;
        private final Progress mProgress;
        private final AtomicInteger atomicProgress = new AtomicInteger(0);
        private long totalRead = 0;

        WriterStream(@NonNull Writer writer, @NonNull InputStream inputStream, @NonNull Progress progress) {
            this.mWriter = writer;
            this.mInputStream = inputStream;
            this.mProgress = progress;
        }


        @Override
        public void load(long size) {
            try {
                if (mProgress.isClosed()) {
                    mWriter.setWritten(-1);
                    return;
                }
                byte[] data = new byte[(int) size];

                int read = mInputStream.read(data);
                mWriter.setWritten(read);
                mWriter.setData(data);

                long streamSize = mProgress.getSize();
                if (streamSize > 0) {
                    totalRead += read;
                    int percent = (int) ((totalRead * 100.0f) / streamSize);
                    if (atomicProgress.getAndSet(percent) < percent) {
                        mProgress.setProgress(percent);
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }
}
