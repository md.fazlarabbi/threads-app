package threads.ipfs;

public enum Reachable {
    UNKNOWN, PUBLIC, PRIVATE
}
