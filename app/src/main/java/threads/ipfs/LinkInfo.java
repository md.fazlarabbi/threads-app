package threads.ipfs;

import androidx.annotation.NonNull;

import java.util.Map;
import java.util.Objects;

import threads.LogUtils;

public class LinkInfo {
    private static final String TAG = LinkInfo.class.getSimpleName();
    private static final String NAME = "Name";
    private static final String TYPE = "Type";
    private static final String HASH = "Hash";
    private static final String SIZE = "Size";

    @NonNull
    private final String hash;
    @NonNull
    private final String name;
    private final long size;
    private final int type;


    private LinkInfo(@NonNull String hash,
                     @NonNull String name,
                     long size,
                     int type) {
        this.hash = hash;
        this.name = name;
        this.size = size;
        this.type = type;
    }

    public static LinkInfo create(@NonNull Map map) {
        Objects.requireNonNull(map);
        String name = (String) map.get(NAME);
        Objects.requireNonNull(name);
        String hash = (String) map.get(HASH);
        Objects.requireNonNull(hash);
        long size = -1;
        int type = -1;
        try {
            Object object = map.get(SIZE);
            if (object instanceof Long) {
                size = (Long) object;
            } else if (object instanceof Integer) {
                size = (Integer) object;
            } else if (object instanceof Double) {
                size = ((Double) object).longValue();
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            // ignore exception (can be not defined or "-")
        }
        try {
            Object object = map.get(TYPE);
            if (object instanceof Long) {
                type = ((Long) object).intValue();
            } else if (object instanceof Integer) {
                type = (Integer) object;
            } else if (object instanceof Double) {
                type = ((Double) object).intValue();
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        return new LinkInfo(hash, name, size, type);
    }

    @NonNull
    public CID getCid() {
        return CID.create(hash);
    }


// --Commented out by Inspection START (4/28/2020 6:58 PM):
//    public long getSize() {
//        return size;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:58 PM)

// --Commented out by Inspection START (4/28/2020 6:58 PM):
//    @NonNull
//    public String getName() {
//        return name;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:58 PM)


// --Commented out by Inspection START (4/28/2020 6:58 PM):
//    public int getType() {
//        return type;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:58 PM)


// --Commented out by Inspection START (4/28/2020 6:58 PM):
//    public boolean isDirectory() {
//        return type == 1;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:58 PM)


    @NonNull
    @Override
    public String toString() {
        return "LinkInfo{" +
                "hash='" + hash + '\'' +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", type=" + type +
                '}';
    }

// --Commented out by Inspection START (4/28/2020 6:58 PM):
//    public boolean isFile() {
//        return type == 2;
//
//    }
// --Commented out by Inspection STOP (4/28/2020 6:58 PM)


// --Commented out by Inspection START (4/28/2020 6:58 PM):
//    public boolean isRaw() {
//        return type == 0;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:58 PM)
}
