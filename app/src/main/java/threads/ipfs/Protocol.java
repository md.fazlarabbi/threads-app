package threads.ipfs;

import androidx.annotation.NonNull;

import java.util.Objects;

public class Protocol {
    @NonNull
    private final String name;

    private Protocol(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public static Protocol create(@NonNull String name) {
        Objects.requireNonNull(name);
        return new Protocol(name);
    }

// --Commented out by Inspection START (4/28/2020 7:02 PM):
//    @NonNull
//    public String getName() {
//        return name;
//    }
// --Commented out by Inspection STOP (4/28/2020 7:02 PM)

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Protocol protocol = (Protocol) o;
        return Objects.equals(name, protocol.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    boolean isCircuitRelay() {
        return name.startsWith("/libp2p/circuit/relay/");
    }

// --Commented out by Inspection START (4/28/2020 7:02 PM):
//    public boolean isAutonat() {
//        return name.startsWith("/libp2p/autonat/");
//    }
// --Commented out by Inspection STOP (4/28/2020 7:02 PM)

    boolean isFloodSub() {
        return name.startsWith("/floodsub/");
    }

    boolean isMeshSub() {
        return name.startsWith("/meshsub/");
    }

    @Override
    @NonNull
    public String toString() {
        return "Protocol{" +
                "name='" + name + '\'' +
                '}';
    }
}
