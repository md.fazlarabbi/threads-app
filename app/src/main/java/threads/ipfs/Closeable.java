package threads.ipfs;

public interface Closeable {
    boolean isClosed();
}
