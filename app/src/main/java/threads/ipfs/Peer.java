package threads.ipfs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Peer implements Comparable<Peer> {
    @NonNull
    private final String multiAddress;
    @NonNull
    private final PID pid;
    private final long latency;
    @Nullable
    private final String muxer;
    private final int direction;

    @NonNull
    private final List<Protocol> streams = new ArrayList<>();

    private Peer(@NonNull PID pid,
                 @NonNull String multiAddress,
                 int direction,
                 long latency,
                 @Nullable String muxer,
                 @NonNull List<Protocol> streams) {
        Objects.requireNonNull(multiAddress);
        Objects.requireNonNull(pid);
        Objects.requireNonNull(streams);
        this.multiAddress = multiAddress;
        this.pid = pid;
        this.direction = direction;
        this.latency = latency;
        this.muxer = muxer;
        this.streams.addAll(streams);
    }


    @NonNull
    public static Peer create(@NonNull Map map) {
        Objects.requireNonNull(map);
        Double latency = (Double) map.get("Latency");
        Objects.requireNonNull(latency);
        String addr = (String) map.get("Addr");
        Objects.requireNonNull(addr);
        String peerID = (String) map.get("Peer");
        Objects.requireNonNull(peerID);
        Object direction = map.get("Direction");
        int direct = 1;
        try {
            if (direction != null) {
                direct = ((Double) (direction)).intValue();
            }
        } catch (Throwable e) {
            // ignore exception
        }
        String muxer = (String) map.get("Muxer");
        Object streams = map.get("Streams");
        List<Protocol> protocols = new ArrayList<>();
        if (streams instanceof List) {
            for (Object stream : (List) streams) {
                if (stream instanceof Map) {
                    Map streamMap = (Map) stream;
                    Object streamMapProtocol = streamMap.get("Protocol");
                    if (streamMapProtocol instanceof String) {
                        String protocol = (String) streamMapProtocol;
                        if (!protocol.isEmpty()) {
                            Protocol proto = Protocol.create(protocol);
                            if (!protocols.contains(proto)) {
                                protocols.add(proto);
                            }
                        }
                    }
                }
            }
        }


        long latencyValue = Long.MAX_VALUE;
        if (latency > 0) {
            latencyValue = latency.longValue() / 1000000; // now have in ms
        }
        PID pid = PID.create(peerID);

        return new Peer(pid, addr, direct, latencyValue, muxer, protocols);
    }

    public boolean isRelay() {

        if (!this.getStreams().isEmpty()) {
            return this.hasCircuitRelayProtocol();
        }

        return false;
    }

    boolean isFloodSub() {


        if (!this.getStreams().isEmpty()) {
            return this.hasFloodSubProtocol();
        }

        return false;
    }


    boolean isMeshSub() {

        if (!this.getStreams().isEmpty()) {
            return this.hasMeshSubProtocol();
        }

        return false;
    }

// --Commented out by Inspection START (4/28/2020 6:56 PM):
//    public boolean isAutonat() {
//
//        if (!this.getStreams().isEmpty()) {
//            return this.hasAutonatProtocol();
//        }
//
//        return false;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:56 PM)

    private boolean hasCircuitRelayProtocol() {

        for (Protocol protocol : streams) {
            if (protocol.isCircuitRelay()) {
                return true;
            }
        }

        return false;
    }

    private boolean hasFloodSubProtocol() {

        for (Protocol protocol : streams) {
            if (protocol.isFloodSub()) {
                return true;
            }
        }

        return false;
    }

    private boolean hasMeshSubProtocol() {

        for (Protocol protocol : streams) {
            if (protocol.isMeshSub()) {
                return true;
            }
        }

        return false;
    }

// --Commented out by Inspection START (4/28/2020 7:00 PM):
//    private boolean hasAutonatProtocol() {
//
//        for (Protocol protocol : streams) {
//            if (protocol.isAutonat()) {
//                return true;
//            }
//        }
//
//        return false;
//    }
// --Commented out by Inspection STOP (4/28/2020 7:00 PM)

// --Commented out by Inspection START (4/28/2020 6:56 PM):
//    public int getDirection() {
//        return direction;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:56 PM)

    @NonNull
    public String getMultiAddress() {
        return multiAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peer peer = (Peer) o;
        return Objects.equals(pid, peer.pid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pid);
    }

    @NonNull
    public PID getPid() {
        return pid;
    }

// --Commented out by Inspection START (4/28/2020 6:56 PM):
//    public long getLatency() {
//        return latency;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:56 PM)

// --Commented out by Inspection START (4/28/2020 6:56 PM):
//    @Nullable
//    public String getMuxer() {
//        return muxer;
//    }
// --Commented out by Inspection STOP (4/28/2020 6:56 PM)

    @NonNull
    private List<Protocol> getStreams() {
        return streams;
    }


    @Override
    @NonNull
    public String toString() {
        return "Peer{" +
                "multiAddress=" + multiAddress +
                ", pid=" + pid.getPid() +
                ", direction=" + direction +
                ", latency=" + latency +
                ", muxer='" + muxer + '\'' +
                ", streams=" + streams.toString() +
                '}';
    }

    @Override
    public int compareTo(@NonNull Peer peer) {
        return Double.compare(this.latency, peer.latency);
    }


}
