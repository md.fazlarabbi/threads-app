package threads.ipfs;

public class TimeoutProgress extends CloseableProgress {
    private final long timeout;
    private final long start;

    public TimeoutProgress(long timeout) {
        this.timeout = timeout;
        this.start = System.currentTimeMillis();
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    public void setProgress(int percent) {

    }

    @Override
    public boolean isClosed() {
        return (System.currentTimeMillis() - start) > (timeout * 1000);
    }
}
