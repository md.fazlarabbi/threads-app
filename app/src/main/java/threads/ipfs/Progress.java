package threads.ipfs;

public interface Progress extends Closeable {
    long getSize();

    void setProgress(@SuppressWarnings("unused") int progress);
}
