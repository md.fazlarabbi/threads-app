package threads.app.services;

import android.content.Context;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Members;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.utils.GroupRequestChannel;
import threads.app.work.ConnectionWorker;
import threads.app.work.DownloadMessageWorker;
import threads.app.work.DownloadNoteWorker;
import threads.app.work.GeneralNotifyWorker;
import threads.app.work.GroupConnectWorker;
import threads.app.work.GroupImageWorker;
import threads.app.work.GroupJoinedWorker;
import threads.app.work.GroupNotifyWorker;
import threads.app.work.UserDownloadWorker;
import threads.app.work.UserWaitWorker;
import threads.ipfs.CID;
import threads.ipfs.Encryption;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupRequestService {
    private static final String TAG = GroupRequestService.class.getSimpleName();


    public static void handleNote(@NonNull Context context, @NonNull Map<String, String> content) {

        long start = System.currentTimeMillis();

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {

                THREADS threads = THREADS.getInstance(context);

                // NOT ENCRYPTED
                String pid = content.get(Content.PID);
                Objects.requireNonNull(pid);

                if (threads.isUserBlocked(pid)) {
                    return;
                }

                String token = content.get(Content.FCM);
                Objects.requireNonNull(token);
                String pkey = content.get(Content.PKEY);
                Objects.requireNonNull(pkey);

                // ENCRYPTED
                String alias = content.get(Content.ALIAS);
                Objects.requireNonNull(alias);
                alias = new String(Base64.decode(alias, Base64.DEFAULT));

                UserService.createUser(context, pid, alias, pkey, token, true);


                String uuid = content.get(Content.UUID);
                Objects.requireNonNull(uuid);

                List<Group> list = threads.getGroupsByUuid(uuid);
                if (!list.isEmpty()) {
                    Group group = list.get(0);

                    if (!group.hasMember(pid)) {
                        group.addMember(pid);
                        threads.updateGroup(group);
                    }

                    String sesKey = group.getSesKey();

                    // NOT ENCRYPTED
                    String estType = content.get(Content.EST);
                    Objects.requireNonNull(estType);
                    NoteType noteType = NoteType.toNoteType(Integer.valueOf(estType));


                    // NOT ENCRYPTED
                    String date = content.get(Content.DATE);
                    Objects.requireNonNull(date);
                    long timestamp = Long.parseLong(date);


                    // NOT ENCRYPTED
                    CID cid = null;
                    String cidValue = content.get(Content.CID);
                    if (cidValue != null) {
                        cid = CID.create(cidValue);
                    }

                    // ENCRYPTED
                    String mimeType = content.get(Content.MIME_TYPE);
                    Objects.requireNonNull(mimeType);
                    mimeType = Encryption.decrypt(mimeType, sesKey);

                    // NOT ENCRYPTED
                    String fileSize = content.get(Content.FILESIZE);
                    Objects.requireNonNull(fileSize);

                    // ENCRYPTED
                    String fileName = content.get(Content.FILENAME);
                    Objects.requireNonNull(fileName);
                    fileName = Encryption.decrypt(fileName, sesKey);


                    Note note = Note.createNote(
                            group.getIdx(),
                            PID.create(pid),
                            alias,
                            noteType,
                            mimeType,
                            timestamp);
                    note.setUnread(true);
                    note.setSize(Long.parseLong(fileSize));
                    note.setName(fileName);
                    note.setContent(cid);


                    if (note.getNoteType() == NoteType.VIDEO ||
                            note.getNoteType() == NoteType.IMAGE ||
                            note.getNoteType() == NoteType.AUDIO) {
                        handleDataNote(context, threads, note);
                    } else if (note.getNoteType() == NoteType.LINK) {
                        handleLinkNote(context, threads, note);
                    } else {
                        handleMessageNote(context, threads, note);
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });
    }


    private static void handleMessageNote(@NonNull Context context,
                                          @NonNull THREADS threads,
                                          @NonNull Note note) {
        String message = context.getString(R.string.message_from_try, note.getAlias());
        note.setText(message);
        note.setLeaching(true);
        PID owner = note.getOwner();
        long group = note.getGroup();

        long idx = threads.storeNote(note);

        OneTimeWorkRequest work = DownloadMessageWorker.getWork(idx);
        threads.setNoteWork(idx, work.getId());

        IPFS ipfs = IPFS.getInstance(context);
        if (ipfs.isDaemonRunning()) {
            if (ipfs.isConnected(owner)) {


                // download message note
                WorkManager.getInstance(context).beginWith(work)
                        .then(GroupNotifyWorker.getWork(idx))
                        .then(GeneralNotifyWorker.getWork(idx))
                        .then(UserDownloadWorker.getWork(owner))
                        .enqueue();
                return;
            }
        }

        // download message note
        WorkManager.getInstance(context)
                .beginWith(ConnectionWorker.getWork())
                .then(GroupConnectWorker.getWork(owner, group))
                .then(work)
                .then(GroupNotifyWorker.getWork(idx))
                .then(GeneralNotifyWorker.getWork(idx))
                .then(UserDownloadWorker.getWork(owner))
                .enqueue();

    }


    private static void handleDataNote(@NonNull Context context,
                                       @NonNull THREADS threads,
                                       @NonNull Note note) {
        note.setLeaching(true);
        long size = note.getSize();
        PID owner = note.getOwner();
        long group = note.getGroup();
        long idx = threads.storeNote(note);


        OneTimeWorkRequest work = DownloadNoteWorker.getWork(idx,
                DownloadNoteWorker.isTask(size));
        threads.setNoteWork(idx, work.getId());

        IPFS ipfs = IPFS.getInstance(context);
        if (ipfs.isDaemonRunning()) {
            if (ipfs.isConnected(owner)) {
                WorkManager.getInstance(context)
                        .beginWith(work)
                        .then(GroupNotifyWorker.getWork(idx))
                        .then(GeneralNotifyWorker.getWork(idx))
                        .then(UserDownloadWorker.getWork(owner))
                        .enqueue();
                return;
            }
        }
        WorkManager.getInstance(context)
                .beginWith(ConnectionWorker.getWork())
                .then(GroupConnectWorker.getWork(owner, group))
                .then(work)
                .then(GroupNotifyWorker.getWork(idx))
                .then(GeneralNotifyWorker.getWork(idx))
                .then(UserDownloadWorker.getWork(owner))
                .enqueue();

    }


    private static void handleLinkNote(@NonNull Context context,
                                       @NonNull THREADS threads,
                                       @NonNull Note note) {

        note.setLeaching(false);
        String filename = note.getName();
        note.setText(filename);

        long size = note.getSize();
        PID owner = note.getOwner();
        long group = note.getGroup();

        long idx = threads.storeNote(note);

        OneTimeWorkRequest work = DownloadNoteWorker.getWork(idx,
                DownloadNoteWorker.isTask(size));
        threads.setNoteWork(idx, work.getId());

        IPFS ipfs = IPFS.getInstance(context);
        if (ipfs.isDaemonRunning()) {
            if (ipfs.isConnected(owner)) {
                if (DaemonService.isRunning.get()) {
                    WorkManager.getInstance(context)
                            .beginWith(work)
                            .then(GroupNotifyWorker.getWork(idx))
                            .then(GeneralNotifyWorker.getWork(idx))
                            .then(UserDownloadWorker.getWork(owner)).enqueue();
                } else {
                    WorkManager.getInstance(context)
                            .beginWith(GroupNotifyWorker.getWork(idx))
                            .then(GeneralNotifyWorker.getWork(idx)).enqueue();
                }
                return;
            }
        }

        if (DaemonService.isRunning.get()) {
            WorkManager.getInstance(context)
                    .beginWith(ConnectionWorker.getWork())
                    .then(GroupConnectWorker.getWork(owner, group))
                    .then(work)
                    .then(GroupNotifyWorker.getWork(idx))
                    .then(GeneralNotifyWorker.getWork(idx))
                    .then(UserDownloadWorker.getWork(owner)).enqueue();
        } else {
            WorkManager.getInstance(context)
                    .beginWith(GroupNotifyWorker.getWork(idx))
                    .then(GeneralNotifyWorker.getWork(idx)).enqueue();
        }

    }


    public static void handleGroupRemoved(@NonNull Context context, @NonNull String pid,
                                          @NonNull String address) {


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            try {
                THREADS threads = THREADS.getInstance(context);
                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);
                if (threads.isUserBlocked(pid)) {
                    return;
                }

                List<Group> list = threads.getGroupsByUuid(address);

                if (!list.isEmpty()) {
                    Group group = list.get(0);

                    if (Objects.equals(pid, host.getPid())) {
                        group.removeMembers();
                        threads.updateGroup(group);
                        String message = context.getString(R.string.user_remove_from_thread,
                                group.getName());
                        String alias = InitApplication.getAlias(context);
                        Note note = threads.createInfoNote(group.getIdx(), group.getOwner(),
                                alias, message, new Date());

                        long idx = threads.storeNote(note);

                        WorkManager.getInstance(context).beginWith(GroupNotifyWorker.getWork(idx))
                                .then(GeneralNotifyWorker.getWork(idx)).enqueue();

                    } else {
                        if (group.hasMember(pid)) {
                            group.removeMember(pid);
                            threads.updateGroup(group);

                            User user = threads.getUserByPid(pid);
                            if (user != null) {

                                String message = context.getString(R.string.user_remove_thread,
                                        user.getAlias(), group.getName());
                                Note note = threads.createInfoNote(group.getIdx(), group.getOwner(),
                                        user.getAlias(), message, new Date());

                                long idx = threads.storeNote(note);

                                WorkManager.getInstance(context).beginWith(GroupNotifyWorker.getWork(idx))
                                        .then(GeneralNotifyWorker.getWork(idx)).enqueue();
                            }
                        }
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });

    }

    public static void handleGroupJoined(@NonNull Context context,
                                         @NonNull String pid,
                                         @NonNull String alias,
                                         @NonNull String pkey,
                                         @NonNull String token,
                                         @NonNull String address) {


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            try {
                THREADS threads = THREADS.getInstance(context);

                if (threads.isUserBlocked(pid)) {
                    return;
                }

                List<Group> list = threads.getGroupsByUuid(address);

                if (!list.isEmpty()) {
                    Group group = list.get(0);

                    boolean added = group.addMember(pid);
                    if (added) {
                        threads.updateGroup(group);

                        UserService.createUser(context, pid,
                                alias, pkey, token, true);


                        String message = context.getString(R.string.user_joins_thread,
                                group.getName());
                        Note note = threads.createInfoNote(group.getIdx(), PID.create(pid),
                                alias, message, new Date());

                        long idx = threads.storeNote(note);

                        WorkManager.getInstance(context).beginWith(GroupNotifyWorker.getWork(idx))
                                .then(GeneralNotifyWorker.getWork(idx)).enqueue();

                    }


                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });

    }

    public static void handleGroupJoin(@NonNull Context context, @NonNull String pid,
                                       @NonNull String address) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            try {
                THREADS threads = THREADS.getInstance(context);
                List<Group> list = threads.getGroupsByUuid(address);

                User user = threads.getUserByPid(pid);
                Objects.requireNonNull(user);
                String alias = user.getAlias();

                if (!list.isEmpty()) {
                    Group group = list.get(0);
                    Members members = (Members) group.getMembers().clone();
                    Objects.requireNonNull(members);

                    group.addMember(pid);
                    threads.updateGroup(group);


                    String message = context.getString(R.string.user_joins_thread, group.getName());
                    Note note = threads.createInfoNote(group.getIdx(), PID.create(pid),
                            alias, message, new Date());

                    long idx = threads.storeNote(note);


                    WorkManager.getInstance(context).beginWith(GroupNotifyWorker.getWork(idx))
                            .then(GeneralNotifyWorker.getWork(idx)).enqueue();


                    for (String member : members) {
                        User userMember = threads.getUserByPid(member);
                        if (userMember != null && userMember.hasToken() && !userMember.isBlocked()) {
                            String token = userMember.getToken();
                            Objects.requireNonNull(token);
                            GroupJoinedWorker.joined(context, token, pid, group.getUuid());
                        }
                    }

                    if (user.hasToken() && !user.isBlocked()) {
                        String token = user.getToken();
                        Objects.requireNonNull(token);
                        for (String member : members) {
                            GroupJoinedWorker.joined(context, token, member, group.getUuid());
                        }
                    }

                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });

    }

    public static void handleWakeup(@NonNull Context context, @NonNull String pid) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            try {
                THREADS threads = THREADS.getInstance(context);

                if (!threads.isUserBlocked(pid)) {
                    WorkManager.getInstance(context).beginWith(ConnectionWorker.getWork())
                            .then(UserWaitWorker.getWork(pid))
                            .enqueue();
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });
    }

    public static void handleGroupReject(@NonNull Context context, @NonNull String pid,
                                         @NonNull String address) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            try {
                THREADS threads = THREADS.getInstance(context);
                List<Group> list = threads.getGroupsByUuid(address);

                User user = threads.getUserByPid(pid);
                Objects.requireNonNull(user);
                String alias = user.getAlias();

                if (!list.isEmpty()) {
                    Group group = list.get(0);

                    String message = context.getString(
                            R.string.user_reject_thread, group.getName());
                    Note note = threads.createInfoNote(group.getIdx(), PID.create(pid),
                            alias, message, new Date());

                    long idx = threads.storeNote(note);


                    WorkManager.getInstance(context).beginWith(GroupNotifyWorker.getWork(idx))
                            .then(GeneralNotifyWorker.getWork(idx)).enqueue();
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });
    }

    public static void handleGroupLeave(@NonNull Context context, @NonNull String pid,
                                        @NonNull String alias, @NonNull String address) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            try {
                THREADS threads = THREADS.getInstance(context);
                List<Group> list = threads.getGroupsByUuid(address);

                if (!list.isEmpty()) {

                    Group group = list.get(0);
                    boolean removed = group.removeMember(pid);
                    if (!removed) {
                        LogUtils.error(TAG, "User was not a member of the group " + group.getName());
                    }
                    threads.updateGroup(group);


                    String message = context.getString(R.string.user_leaves_thread, group.getName());
                    Note note = threads.createInfoNote(group.getIdx(), PID.create(pid),
                            alias, message, new Date());

                    long idx = threads.storeNote(note);


                    WorkManager.getInstance(context).beginWith(GroupNotifyWorker.getWork(idx))
                            .then(GeneralNotifyWorker.getWork(idx)).enqueue();
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });
    }

    private synchronized static void handleSyncGroupRequest(@NonNull Context context,
                                                            @NonNull String pid,
                                                            @NonNull String alias,
                                                            @NonNull String pkey,
                                                            @NonNull String token,
                                                            @NonNull String address,
                                                            @NonNull String name,
                                                            @NonNull String sesKey,
                                                            @Nullable String image) {
        THREADS threads = THREADS.getInstance(context);

        List<Group> list = threads.getGroupsByUuid(address);
        PID owner = PID.create(pid);

        if (!threads.isUserBlocked(owner)) {

            UserService.createUser(context, pid, alias, pkey, token, false);

            if (list.isEmpty()) {

                Group group = threads.createGroup(address, owner, sesKey);
                group.addMember(pid);
                group.setRequest(true);
                group.setName(name);

                String content = context.getString(R.string.thread_creator, alias);
                group.setContent(content);

                long idx = threads.storeGroup(group);


                if (image != null) {
                    GroupImageWorker.loadImage(context, image, idx);
                }
                GroupRequestChannel.createGroupRequestNotification(context, idx, owner, alias);
            } else {
                Group group = list.get(0);
                threads.setGroupRequest(group.getIdx());

                GroupRequestChannel.cancelGroupRequestNotification(context, group.getIdx());
                GroupRequestChannel.createGroupRequestNotification(
                        context, group.getIdx(), owner, alias);
            }
        }
    }

    public static void handleGroupRequest(@NonNull Context context,
                                          @NonNull String pid,
                                          @NonNull String alias,
                                          @NonNull String pkey,
                                          @NonNull String token,
                                          @NonNull String address,
                                          @NonNull String name,
                                          @NonNull String sesKey,
                                          @Nullable String image) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            try {
                handleSyncGroupRequest(context, pid, alias, pkey,
                        token, address, name, sesKey, image);
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, "Time : " + (System.currentTimeMillis() - start));
            }
        });

    }


}
