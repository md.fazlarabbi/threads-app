package threads.app.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.EVENTS;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.work.ConnectionWorker;
import threads.app.work.GroupConnectWorker;
import threads.app.work.GroupNoteWorker;
import threads.app.work.UploadNoteWorker;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class UploadService {

    private static final String TAG = UploadService.class.getSimpleName();
    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    @Nullable
    private static CID getThumbnail(@NonNull Context context, @NonNull Uri uri, @NonNull String mimeType) {

        CID cid = null;
        byte[] bytes = null;


        try {
            bytes = getPreviewImage(context, uri, mimeType);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        if (bytes != null) {
            try {
                cid = IPFS.getInstance(context).storeData(bytes, "");
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }

        return cid;
    }

    @Nullable
    private static byte[] getPreviewImage(@NonNull Context context,
                                          @NonNull Uri uri,
                                          @NonNull String mimeType) throws Exception {

        Bitmap bitmap = getPreview(context, uri, mimeType);
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.WEBP, 100, stream);
            byte[] image = stream.toByteArray();
            bitmap.recycle();
            return image;
        }
        return null;
    }

    @Nullable
    private static Bitmap getPreview(@NonNull Context context, @NonNull Uri uri, @NonNull String mimeType) throws Exception {

        if (mimeType.startsWith("video")) {


            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(context, uri);

            Bitmap bitmap = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {

                try {
                    bitmap = mediaMetadataRetriever.getPrimaryImage();
                } catch (Throwable e) {
                    LogUtils.error(TAG, e);
                }
            }
            try {
                if (bitmap == null) {
                    final WeakReference<Bitmap> weakBmp = new WeakReference<>(mediaMetadataRetriever.getFrameAtTime());
                    bitmap = weakBmp.get();
                }
            } catch (Throwable e) {
                bitmap = mediaMetadataRetriever.getFrameAtTime();
            }
            mediaMetadataRetriever.release();
            return bitmap;

        } else if (mimeType.startsWith("application/pdf")) {
            return getPDFBitmap(context, uri);
        } else if (mimeType.startsWith("image")) {

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            return ThumbnailUtils.extractThumbnail(bitmap, 128, 128);
        }
        return null;
    }

    @NonNull
    private static Bitmap getPDFBitmap(@NonNull Context context, @NonNull Uri uri) throws Exception {

        ParcelFileDescriptor fileDescriptor = context.getContentResolver().openFileDescriptor(
                uri, "r");
        Objects.requireNonNull(fileDescriptor);
        PdfRenderer pdfRenderer = new PdfRenderer(fileDescriptor);

        PdfRenderer.Page rendererPage = pdfRenderer.openPage(0);
        int rendererPageWidth = rendererPage.getWidth();
        int rendererPageHeight = rendererPage.getHeight();


        Bitmap bitmap = Bitmap.createBitmap(
                rendererPageWidth,
                rendererPageHeight,
                Bitmap.Config.ARGB_8888);

        rendererPage.render(bitmap, null, null,
                PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);


        rendererPage.close();

        pdfRenderer.close();
        fileDescriptor.close();
        return bitmap;
    }

    public static void upload(@NonNull Context context, @NonNull Uri uri, long groupIdx, boolean connectGroup) {

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);

        EXECUTOR.submit(() -> {
            try {
                THREADS threads = THREADS.getInstance(context);


                ThumbnailService.FileDetails fileDetails =
                        ThumbnailService.getFileDetails(context, uri);
                Objects.requireNonNull(fileDetails);

                String name = fileDetails.getFileName();
                long size = fileDetails.getFileSize();
                String mimeType = fileDetails.getMimeType();


                Group group = threads.getGroupByIdx(groupIdx);
                Objects.requireNonNull(group);


                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);
                String hostAlias = InitApplication.getAlias(context);

                CID thumbnail = getThumbnail(context, uri, mimeType);
                Note note = threads.createLinkNote(host, hostAlias, group, null, name, mimeType, size);
                note.setInit(true);
                String message = fileDetails.getFileName();
                note.setText(message);
                note.setThumbnail(thumbnail);
                note.setPublished(false);
                note.setUri(uri.toString());
                long idx = threads.storeNote(note);

                OneTimeWorkRequest request = UploadNoteWorker.getWork(idx);
                threads.setNoteWork(idx, request.getId());

                if (connectGroup) {
                    WorkManager.getInstance(context).beginWith(request)
                            .then(GroupNoteWorker.getSharedWork())
                            .then(ConnectionWorker.getWork())
                            .then(GroupConnectWorker.getWork(host, groupIdx))
                            .enqueue();
                } else {
                    WorkManager.getInstance(context).beginWith(request)
                            .then(GroupNoteWorker.getSharedWork())
                            .enqueue();
                }

                WorkManager.getInstance(context).enqueue(request);

            } catch (Throwable e) {
                EVENTS.getInstance(context).error(
                        context.getString(R.string.file_not_found));

            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }
}
