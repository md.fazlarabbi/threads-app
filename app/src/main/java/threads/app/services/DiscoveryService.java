package threads.app.services;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import threads.LogUtils;

public class DiscoveryService implements NsdManager.DiscoveryListener {
    private static final String TAG = DiscoveryService.class.getSimpleName();
    private static DiscoveryService INSTANCE = null;
    private OnServiceFoundListener mListener = null;

    public static DiscoveryService getInstance() {
        if (INSTANCE == null) {

            synchronized (DiscoveryService.class) {

                if (INSTANCE == null) {
                    INSTANCE = new DiscoveryService();
                }
            }

        }
        return INSTANCE;
    }

    public void setOnServiceFoundListener(@NonNull OnServiceFoundListener listener) {
        mListener = listener;
    }

    @Override
    public void onStartDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.error(TAG, "onStartDiscoveryFailed");
    }

    @Override
    public void onStopDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.error(TAG, "onStopDiscoveryFailed");
    }

    @Override
    public void onDiscoveryStarted(String serviceType) {
        LogUtils.error(TAG, "onDiscoveryStarted");
    }

    @Override
    public void onDiscoveryStopped(String serviceType) {
        LogUtils.error(TAG, "onDiscoveryStopped");
    }

    @Override
    public void onServiceFound(NsdServiceInfo serviceInfo) {

        if (mListener != null) {
            mListener.resolveService(serviceInfo);
        }
    }

    @Override
    public void onServiceLost(NsdServiceInfo serviceInfo) {
        LogUtils.error(TAG, "onServiceLost");
    }

    public interface OnServiceFoundListener {

        void resolveService(@NonNull NsdServiceInfo serviceInfo);
    }
}
