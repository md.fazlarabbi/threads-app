package threads.app.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.exifinterface.media.ExifInterface;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.EVENTS;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.app.utils.Network;
import threads.app.work.ConnectionWorker;
import threads.app.work.GroupConnectWorker;
import threads.app.work.GroupNoteWorker;
import threads.codec.MetadataEditor;
import threads.codec.boxes.MetaValue;
import threads.ipfs.CID;
import threads.ipfs.Encryption;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class NoteService {
    private static final String TAG = NoteService.class.getSimpleName();

    public static void storeText(@NonNull Context context, long groupIdx, @NonNull String text) {


        final THREADS threads = THREADS.getInstance(context);
        final IPFS ipfs = IPFS.getInstance(context);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {
                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);
                String hostAlias = InitApplication.getAlias(context);


                Group group = threads.getGroupByIdx(groupIdx);
                Objects.requireNonNull(group);

                CID cid = ipfs.storeText(text, group.getSesKey());
                Objects.requireNonNull(cid, "CID not stored");
                Note note = threads.createMessageNote(host, hostAlias, group, cid);
                note.setText(text);
                note.setSeeding(true);
                note.setPublished(false);
                long idx = threads.storeNote(note);

                GroupService.updateGroupInfo(context, groupIdx, idx);

                if (group.hasMembers()) {

                    WorkManager.getInstance(context).beginUniqueWork(
                            GroupNoteWorker.getUniqueId(idx), ExistingWorkPolicy.KEEP,
                            GroupNoteWorker.getWork(idx))
                            .then(ConnectionWorker.getWork())
                            .then(GroupConnectWorker.getWork(note.getOwner(), note.getGroup()))
                            .enqueue();

                    if (!Network.isConnected(context)) {
                        EVENTS.getInstance(context).warning(
                                context.getString(R.string.offline_mode));
                    }
                }


            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        });

    }


    public static void storeAudio(@NonNull Context context, long groupIdx, @NonNull File file) {


        final IPFS ipfs = IPFS.getInstance(context);
        final THREADS threads = THREADS.getInstance(context);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {

                Group group = threads.getGroupByIdx(groupIdx);
                Objects.requireNonNull(group);


                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);
                String hostAlias = InitApplication.getAlias(context);


                String filename = file.getName();
                String mimeType = "audio/m4a";
                long size = file.length();

                CID cid = ipfs.storeFile(file, group.getSesKey());
                Objects.requireNonNull(cid);
                Note note = threads.createAudioNote(groupIdx, host, hostAlias,
                        mimeType, filename, cid, size);
                note.setText("");
                note.setSeeding(true);
                note.setPublished(false);
                long idx = threads.storeNote(note);

                GroupService.updateGroupInfo(context, group.getIdx(), idx);

                if (group.hasMembers()) {
                    WorkManager.getInstance(context).beginUniqueWork(
                            GroupNoteWorker.getUniqueId(idx), ExistingWorkPolicy.KEEP,
                            GroupNoteWorker.getWork(idx))
                            .then(ConnectionWorker.getWork())
                            .then(GroupConnectWorker.getWork(note.getOwner(), note.getGroup()))
                            .enqueue();

                    if (!Network.isConnected(context)) {
                        EVENTS.getInstance(context).warning(
                                context.getString(R.string.offline_mode));
                    }
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        });


    }

    private static void loadImage(@NonNull Context context, @NonNull Uri uri,
                                  @NonNull String text, long idx) {


        try {
            IPFS ipfs = IPFS.getInstance(context);
            THREADS threads = THREADS.getInstance(context);

            Note note = threads.getNoteByIdx(idx);
            Objects.requireNonNull(note);
            Group group = threads.getGroup(note);
            Objects.requireNonNull(group);


            File temp = ipfs.createCacheFile();
            Bitmap bitmap = handleSamplingAndRotationBitmap(context, uri);

            FileOutputStream stream = new FileOutputStream(temp);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            bitmap.recycle();
            stream.close();


            if (!text.isEmpty()) {
                String sesKey = group.getSesKey();
                String encText;
                if (!sesKey.isEmpty()) {
                    encText = Encryption.encrypt(text, sesKey);
                } else {
                    encText = Base64.encodeToString(text.getBytes(), Base64.DEFAULT);
                }
                ExifInterface exif = new ExifInterface(temp.getAbsolutePath());
                exif.setAttribute(ExifInterface.TAG_IMAGE_DESCRIPTION, encText);
                exif.saveAttributes();
            }


            CID cid = ipfs.storeFile(temp, group.getSesKey());
            Objects.requireNonNull(cid);

            long size = temp.length();

            boolean result = temp.delete();
            if (!result) {
                LogUtils.error(TAG, "File is not deleted");
            }

            threads.setNoteSize(idx, size);
            threads.setNoteContent(idx, cid);
            threads.setNoteSeeding(idx);


            GroupService.updateGroupInfo(context, group.getIdx(), idx);


            if (group.hasMembers()) {

                WorkManager.getInstance(context).beginUniqueWork(
                        GroupNoteWorker.getUniqueId(idx), ExistingWorkPolicy.KEEP,
                        GroupNoteWorker.getWork(idx))
                        .then(ConnectionWorker.getWork())
                        .then(GroupConnectWorker.getWork(note.getOwner(), note.getGroup()))
                        .enqueue();

                if (!Network.isConnected(context)) {
                    EVENTS.getInstance(context).warning(
                            context.getString(R.string.offline_mode));
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }


    }

    private static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try (InputStream imageStream = context.getContentResolver().openInputStream(selectedImage)) {
            BitmapFactory.decodeStream(imageStream, null, options);
        }

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap img;
        try (InputStream imageStream = context.getContentResolver().openInputStream(selectedImage)) {
            img = BitmapFactory.decodeStream(imageStream, null, options);
        }
        Objects.requireNonNull(img);
        return rotateImageIfRequired(context, img, selectedImage);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = Math.min(heightRatio, widthRatio);

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(@NonNull Context context,
                                                @NonNull Bitmap img,
                                                @NonNull Uri selectedImage) throws IOException {

        try (InputStream inputStream = context.getContentResolver().openInputStream(selectedImage)) {
            Objects.requireNonNull(inputStream);
            ExifInterface ei = new ExifInterface(inputStream);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotateImage(img, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotateImage(img, 180);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotateImage(img, 270);
                default:
                    return img;
            }
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public static void storeImage(@NonNull Context context, @NonNull Uri uri, @NonNull CID image,
                                  @NonNull String text, @NonNull String fileName,
                                  @NonNull String mimeType, long groupIdx) {

        THREADS threads = THREADS.getInstance(context);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {

                Group group = threads.getGroupByIdx(groupIdx);
                Objects.requireNonNull(group);


                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);
                String hostAlias = InitApplication.getAlias(context);

                Note note = threads.createDataNote(groupIdx, host, hostAlias,
                        NoteType.IMAGE, mimeType, null);

                note.setName(fileName);
                note.setText(text);
                note.setThumbnail(image);
                note.setPublished(false);

                long idx = threads.storeNote(note);


                loadImage(context, uri, text, idx);


            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        });
    }


    public static void storeVideo(@NonNull Context context, @NonNull Uri uri, @NonNull CID image,
                                  @NonNull String text, @NonNull String fileName,
                                  @NonNull String mimeType, long groupIdx) {

        final IPFS ipfs = IPFS.getInstance(context);
        final THREADS threads = THREADS.getInstance(context);


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {


            try {
                Group group = threads.getGroupByIdx(groupIdx);
                Objects.requireNonNull(group);


                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);
                String hostAlias = InitApplication.getAlias(context);

                Note note = threads.createDataNote(groupIdx,
                        host, hostAlias, NoteType.VIDEO, mimeType, null);

                note.setName(fileName);
                note.setText(text);
                note.setThumbnail(image);
                note.setPublished(false);

                long idx = threads.storeNote(note);


                GroupService.updateGroupInfo(context, groupIdx, idx);

                try {
                    File temp = ipfs.createCacheFile();
                    FileOutputStream outputStream = new FileOutputStream(temp);
                    try (InputStream inputStream = context.getContentResolver().openInputStream(uri)) {
                        Objects.requireNonNull(inputStream);
                        IPFS.copy(inputStream, outputStream);
                    }
                    outputStream.close();


                    if (!text.isEmpty()) {
                        MetadataEditor mediaMeta = MetadataEditor.createFrom(temp);
                        Map<String, MetaValue> keyedMeta = mediaMeta.getKeyedMeta();
                        String encText = Encryption.encrypt(text, group.getSesKey());
                        keyedMeta.put(Content.SYNOPSIS, MetaValue.createString(encText));
                        mediaMeta.save(false);
                    }


                    CID cid = ipfs.storeFile(temp, group.getSesKey());
                    Objects.requireNonNull(cid);

                    threads.setNoteContent(idx, cid);
                    threads.setNoteSize(idx, temp.length());
                    threads.setNoteSeeding(idx);


                    boolean result = temp.delete();
                    if (!result) {
                        LogUtils.error(TAG, "File is not deleted");
                    }

                    if (group.hasMembers()) {

                        WorkManager.getInstance(context).beginUniqueWork(
                                GroupNoteWorker.getUniqueId(idx), ExistingWorkPolicy.KEEP,
                                GroupNoteWorker.getWork(idx))
                                .then(ConnectionWorker.getWork())
                                .then(GroupConnectWorker.getWork(note.getOwner(), note.getGroup()))
                                .enqueue();

                        if (!Network.isConnected(context)) {
                            EVENTS.getInstance(context).warning(
                                    context.getString(R.string.offline_mode));
                        }
                    }

                } catch (Throwable e) {
                    LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                    EVENTS.getInstance(context).postError(context.getString(R.string.file_not_found));
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }


        });
    }
}
