package threads.app.services;

import android.content.Context;
import android.content.res.Configuration;

import androidx.annotation.NonNull;
import androidx.annotation.RawRes;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import threads.LogUtils;
import threads.ipfs.IPFS;


public class MoinService {
    public static final int NUM_LETTERS = 30;

    private static final String TAG = MoinService.class.getSimpleName();

    public static boolean isNightNode(@NonNull Context context) {
        int nightModeFlags =
                context.getResources().getConfiguration().uiMode &
                        Configuration.UI_MODE_NIGHT_MASK;
        switch (nightModeFlags) {
            case Configuration.UI_MODE_NIGHT_YES:
                return true;
            case Configuration.UI_MODE_NIGHT_UNDEFINED:
            case Configuration.UI_MODE_NIGHT_NO:
                return false;
        }
        return false;
    }


    @NonNull
    public static String loadRawData(@NonNull Context context, @RawRes int id) {

        try (InputStream inputStream = context.getResources().openRawResource(id)) {
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

                IPFS.copy(inputStream, outputStream);
                return new String(outputStream.toByteArray());

            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            return "";
        }
    }
}
