package threads.app.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import java.util.concurrent.atomic.AtomicBoolean;

import threads.LogUtils;
import threads.app.MainActivity;
import threads.app.R;
import threads.app.utils.Network;
import threads.app.work.ConnectionWorker;
import threads.ipfs.IPFS;
import threads.ipfs.Reachable;


public class DaemonService extends Service {

    public static final AtomicBoolean isRunning = new AtomicBoolean(false);
    private static final String HIGH_CHANNEL_ID = "HIGH_CHANNEL_ID";
    private static final int NOTIFICATION_ID = 998;
    private static final String TAG = DaemonService.class.getSimpleName();
    private static final String START_DAEMON = "START_DAEMON";
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                IPFS ipfs = IPFS.getInstance(getApplicationContext());
                int port = (int) ipfs.getSwarmPort();
                if (!Network.isConnected(context)) {
                    buildNotification(port, Reachable.PRIVATE);
                } else {
                    buildNotification(port, ipfs.getReachable());
                }
                ConnectionWorker.connect(getApplicationContext());
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        }
    };

    public static void invoke(@NonNull Context context, boolean startDaemon) {

        try {
            Intent intent = new Intent(context, DaemonService.class);
            intent.putExtra(START_DAEMON, startDaemon);
            ContextCompat.startForegroundService(context, intent);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private static void createChannel(@NonNull Context context) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {
                CharSequence name = context.getString(R.string.daemon_channel_name);
                String description = context.getString(R.string.daemon_channel_description);
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(HIGH_CHANNEL_ID, name, importance);
                mChannel.setDescription(description);

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                        Context.NOTIFICATION_SERVICE);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(mChannel);
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getBooleanExtra(START_DAEMON, false)) {

            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            ipfs.setReachableListener((reachable) ->
                    buildNotification((int) ipfs.getSwarmPort(), ipfs.getReachable())
            );
            startForeground(NOTIFICATION_ID, buildNotification((int)
                    ipfs.getSwarmPort(), ipfs.getReachable()));
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(broadcastReceiver, intentFilter);
            isRunning.set(true);
        } else {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            ipfs.setReachableListener(null);
            Toast.makeText(getApplicationContext(), R.string.daemon_shutdown,
                    Toast.LENGTH_LONG).show();
            try {
                isRunning.set(false);
                stopForeground(true);
                unregisterReceiver(broadcastReceiver);
            } finally {
                stopSelf();
            }
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        try {
            createChannel(getApplicationContext());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        super.onCreate();
    }


    private Notification buildNotification(int port, @NonNull Reachable reachable) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
                HIGH_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.access_point_network);
        builder.setPriority(NotificationManager.IMPORTANCE_MAX);
        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.notification);
        RemoteViews notificationLayoutExpanded = new RemoteViews(getPackageName(), R.layout.notification_expanded);

        if (reachable == Reachable.PUBLIC) {
            notificationLayout.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_on);
            notificationLayoutExpanded.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_on);
            String info = getString(R.string.reachable_public);
            notificationLayoutExpanded.setTextViewText(R.id.notification_info, info);
        } else if (reachable == Reachable.PRIVATE) {
            notificationLayout.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_off);
            notificationLayoutExpanded.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_off);
            String info = getString(R.string.reachable_private);
            if (!Network.isConnected(getApplicationContext())) {
                info = getString(R.string.reachable_network_down);
            } else {
                if (Network.isConnectedMobile(getApplicationContext())) {
                    info = getString(R.string.reachable_private_mobile);
                }
            }
            notificationLayoutExpanded.setTextViewText(R.id.notification_info, info);
        } else {
            notificationLayout.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_unknown);
            notificationLayoutExpanded.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_unknown);
            String info = getString(R.string.reachable_unknown);
            notificationLayoutExpanded.setTextViewText(R.id.notification_info, info);
        }

        notificationLayout.setTextViewText(R.id.notification_text_content,
                getString(R.string.daemon_port_title, String.valueOf(port)));
        notificationLayoutExpanded.setTextViewText(R.id.notification_text_content,
                getString(R.string.daemon_port_title, String.valueOf(port)));
        builder.setStyle(new NotificationCompat.DecoratedCustomViewStyle());
        builder.setCustomContentView(notificationLayout);
        builder.setCustomBigContentView(notificationLayoutExpanded);


        Intent stopIntent = new Intent(getApplicationContext(), DaemonService.class);
        stopIntent.putExtra(START_DAEMON, false);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent stopPendingIntent = PendingIntent.getService(
                getApplicationContext(), requestID, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.action_turn_off, stopPendingIntent);
        notificationLayout.setOnClickPendingIntent(R.id.action_turn_off, stopPendingIntent);

        Intent defaultIntent = new Intent(getApplicationContext(), MainActivity.class);
        requestID = (int) System.currentTimeMillis();
        PendingIntent defaultPendingIntent = PendingIntent.getActivity(
                getApplicationContext(), requestID, defaultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(defaultPendingIntent);

        Notification notification = builder.build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_ID, notification);
        }
        return notification;
    }


}
