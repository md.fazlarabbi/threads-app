package threads.app.services;

import android.provider.DocumentsContract;

import androidx.annotation.NonNull;

import threads.app.R;
import threads.app.utils.MimeType;

public class MimeTypeService {

    public static String getCompactString(@NonNull String title) {

        return title.replace("\n", " ");
    }

    public static int getMediaResource(@NonNull String mimeType) {

        if (!mimeType.isEmpty()) {
            if (mimeType.equals(MimeType.TORRENT_MIME_TYPE)) {
                return R.drawable.arrow_up_down_bold;
            }
            if (mimeType.equals(MimeType.OCTET_MIME_TYPE)) {
                return R.drawable.file_star;
            }
            if (mimeType.equals(MimeType.JSON_MIME_TYPE)) {
                return R.drawable.json;
            }
            if (mimeType.equals(MimeType.PLAIN_MIME_TYPE)) {
                return R.drawable.file;
            }
            if (mimeType.startsWith("text")) {
                return R.drawable.file;
            }
            if (mimeType.equals(MimeType.PDF_MIME_TYPE)) {
                return R.drawable.pdf;
            }
            if (mimeType.equals(DocumentsContract.Document.MIME_TYPE_DIR)) {
                return R.drawable.folder;
            }
            if (mimeType.startsWith(MimeType.VIDEO)) {
                return R.drawable.video;
            }
            if (mimeType.startsWith(MimeType.IMAGE)) {
                return R.drawable.camera;
            }
            if (mimeType.startsWith(MimeType.AUDIO)) {
                return R.drawable.audio;
            }
            if (mimeType.startsWith("application")) {
                return R.drawable.application;
            }

            return R.drawable.settings;
        }

        return R.drawable.help;

    }

}
