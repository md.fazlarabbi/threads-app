package threads.app.services;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.pdf.PdfRenderer;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Size;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Objects;

import threads.LogUtils;
import threads.app.utils.MimeType;
import threads.ipfs.CID;
import threads.ipfs.IPFS;

public class ThumbnailService {

    private static final String TAG = ThumbnailService.class.getSimpleName();
    private static final int THUMBNAIL_SIZE = 128;


    @NonNull
    public static Bitmap getNameImage(@NonNull String name) {

        Canvas canvas = new Canvas();
        String letter = name.substring(0, 1);
        int color = ColorGenerator.MATERIAL.getColor(name);
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(letter, color);
        Bitmap bitmap = Bitmap.createBitmap(64, 64, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, 64, 64);

        drawable.draw(canvas);
        return bitmap;
    }

    @NonNull
    private static Bitmap getPDFBitmap(@NonNull Context context, @NonNull Uri uri) throws Exception {

        ParcelFileDescriptor fileDescriptor = context.getContentResolver().openFileDescriptor(
                uri, "r");
        Objects.requireNonNull(fileDescriptor);
        PdfRenderer pdfRenderer = new PdfRenderer(fileDescriptor);

        PdfRenderer.Page rendererPage = pdfRenderer.openPage(0);
        int rendererPageWidth = rendererPage.getWidth();
        int rendererPageHeight = rendererPage.getHeight();


        Bitmap bitmap = Bitmap.createBitmap(
                rendererPageWidth,
                rendererPageHeight,
                Bitmap.Config.ARGB_8888);
        rendererPage.render(bitmap, null, null,
                PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);


        rendererPage.close();

        pdfRenderer.close();
        fileDescriptor.close();
        return bitmap;
    }

    @Nullable
    public static FileDetails getFileDetails(@NonNull Context context, @NonNull Uri uri) {

        String mimeType = context.getContentResolver().getType(uri);
        if (mimeType == null) {
            mimeType = MimeType.OCTET_MIME_TYPE;
        }
        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri, new String[]{
                DocumentsContract.Document.COLUMN_DISPLAY_NAME,
                DocumentsContract.Document.COLUMN_SIZE}, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();

            String fileName = cursor.getString(0);
            if (fileName == null) {
                fileName = "";
            }
            long fileSize = cursor.getLong(1);
            return FileDetails.create(fileName, mimeType, fileSize);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

        return null;
    }


    @Nullable
    public static CID getThumbnail(@NonNull Context context,
                                   @NonNull File file,
                                   @NonNull String mimeType) {


        try {
            Bitmap bitmap = getPreview(context, file, mimeType);
            byte[] bytes = null;


            if (bitmap != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.WEBP, 100, stream);
                bytes = stream.toByteArray();
                bitmap.recycle();
            }

            if (bytes != null) {
                IPFS ipfs = IPFS.getInstance(context);

                return ipfs.storeData(bytes, "");
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
        return null;

    }

    @Nullable
    private static Bitmap getPreview(@NonNull Context context, @NonNull File file,
                                     @NonNull String mimeType) throws Exception {


        if (mimeType.startsWith("video")) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(context, Uri.fromFile(file));
            Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime();
            mediaMetadataRetriever.release();
            return bitmap;
        } else if (mimeType.startsWith("application/pdf")) {
            return getPDFBitmap(context, Uri.fromFile(file));
        } else if (mimeType.startsWith(MimeType.AUDIO)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                return ThumbnailUtils.createAudioThumbnail(file,
                        new Size(THUMBNAIL_SIZE, THUMBNAIL_SIZE), null);
            }
        } else if (mimeType.startsWith("image")) {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(
                    context.getContentResolver(), Uri.fromFile(file));
            return ThumbnailUtils.extractThumbnail(bitmap, THUMBNAIL_SIZE, THUMBNAIL_SIZE);

        }
        return null;

    }

    public static class FileDetails {

        @NonNull
        private final String fileName;
        @NonNull
        private final String mimeType;
        private final long fileSize;

        private FileDetails(@NonNull String fileName, @NonNull String mimeType, long fileSize) {
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.fileSize = fileSize;
        }

        static FileDetails create(@NonNull String fileName, @NonNull String mimeType, long fileSize) {
            return new FileDetails(fileName, mimeType, fileSize);
        }

        @NonNull
        public String getFileName() {
            return fileName;
        }

        @NonNull
        public String getMimeType() {
            return mimeType;
        }

        public long getFileSize() {
            return fileSize;
        }

    }
}
