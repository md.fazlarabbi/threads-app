package threads.app.services;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Objects;

import threads.LogUtils;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.work.IdentifyWorker;
import threads.ipfs.IPFS;
import threads.ipfs.PID;
import threads.ipfs.Peer;
import threads.ipfs.PeerInfo;

public class ConnectService {
    private static final String TAG = ConnectService.class.getSimpleName();

    public static boolean connect(@NonNull Context context, @NonNull String pid,
                                  int timeout, boolean checkIdentity) {

        boolean success = false;
        try {
            THREADS threads = THREADS.getInstance(context);

            if (!threads.isUserBlocked(pid)) {
                success = connect(context, PID.create(pid), timeout);
            }

            threads.setUserDialing(pid, false);

            if (success) {
                threads.incrementRating(pid);
            } else {
                threads.decrementRating(pid);
            }

            IPFS ipfs = IPFS.getInstance(context);
            if (success) {

                Peer peerInfo = ipfs.swarmPeer(PID.create(pid));

                String multiAddress = "";
                if (peerInfo != null) {
                    multiAddress = peerInfo.getMultiAddress();

                    boolean isRelay = peerInfo.isRelay();

                    if (isRelay) {
                        threads.incrementRating(pid);
                    }
                }

                if (!multiAddress.isEmpty() && !multiAddress.contains("p2p-circuit")) {

                    if (!Objects.equals(threads.getUserAddress(pid), multiAddress)) {
                        threads.setUserAddress(pid, multiAddress);
                    }
                }

            }

            if (checkIdentity) {
                boolean evaluate = false;
                PeerInfo pInfo = ipfs.pidInfo(PID.create(pid));
                if (pInfo != null) {
                    String pKey = pInfo.getPublicKey();
                    if (pKey != null) {
                        if (!Objects.equals(pKey, threads.getUserPublicKey(pid))) {
                            evaluate = true;
                        }
                    }
                    String agent = pInfo.getAgentVersion();
                    if (!agent.isEmpty()) {
                        if (!Objects.equals(agent, threads.getUserAgent(pid))) {
                            evaluate = true;
                        }
                    }
                }
                if (evaluate) {
                    IdentifyWorker.check(context, pid);
                }

            }


        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return success;
    }

    private static boolean connect(@NonNull Context context, @NonNull PID pid, int timeout) {

        IPFS ipfs = IPFS.getInstance(context);

        if (ipfs.isConnected(pid)) {
            return true;
        }

        // now check old address
        THREADS peers = THREADS.getInstance(context);
        User user = peers.getUserByPid(pid.getPid());
        Objects.requireNonNull(user);


        String address = user.getAddress();
        if (!address.isEmpty() && !address.contains("p2p-circuit")) {
            String multiAddress = address.concat("/p2p/" + pid.getPid());

            if (ipfs.swarmConnect(multiAddress, timeout)) {
                return true;
            }
        }


        if (ipfs.swarmConnect(pid, timeout)) {
            return true;
        }


        PeerInfo pInfo = ipfs.id(pid, timeout);
        if (pInfo != null) {
            return ipfs.isConnected(pid);
        }

        return false;
    }
}
