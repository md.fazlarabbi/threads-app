package threads.app.services;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.BuildConfig;
import threads.app.InitApplication;
import threads.app.core.Content;
import threads.app.work.ConnectionWorker;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class IdentityService {


    private static final String TAG = IdentityService.class.getSimpleName();


    public static void identity(@NonNull Context context, boolean bootstrap) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();
            LogUtils.info(TAG, " start ...");
            try {
                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);
                Map<String, String> params = new HashMap<>();

                String alias = InitApplication.getAlias(context);
                params.put(Content.ALIAS, alias);

                String token = InitApplication.getToken(context);
                if (!token.isEmpty()) {
                    params.put(Content.FCM, token);
                } else {
                    throw new RuntimeException("Token is not yet defined");
                }

                String image = InitApplication.getImage(context);
                if (!image.isEmpty()) {
                    params.put(Content.IMG, image);
                }

                Gson gson = new Gson();
                String data = gson.toJson(params);

                IPFS ipfs = IPFS.getInstance(context);

                CID cid = ipfs.storeText(data, BuildConfig.ApiAesKey);
                Objects.requireNonNull(cid);
                ipfs.setAgent(context, cid.getCid());

                InitApplication.setLogin(context, true);

                if (bootstrap) {
                    ConnectionWorker.connect(context);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, " finish running [" + (System.currentTimeMillis() - start) + "]...");
            }
        });
    }

}

