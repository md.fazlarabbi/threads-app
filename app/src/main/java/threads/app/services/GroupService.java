package threads.app.services;

import android.app.NotificationManager;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.EVENTS;
import threads.app.core.Group;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.utils.MimeType;
import threads.app.work.GroupJoinWorker;
import threads.app.work.GroupRejectWorker;
import threads.ipfs.PID;

public class GroupService {
    private static final String TAG = GroupService.class.getSimpleName();


    public static void deleteGroups(@NonNull Context context, long... idxs) {

        THREADS threads = THREADS.getInstance(context);
        EVENTS events = EVENTS.getInstance(context);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {

                threads.setGroupsDeleting(idxs);


                GroupDelete groupDelete = new GroupDelete();
                for (long group : idxs) {
                    groupDelete.addGroup(group);
                }

                Gson gson = new Gson();
                String content = gson.toJson(groupDelete);
                events.delete(content);

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        });

    }

    public static void reject(@NonNull Context context, long idx) {

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancel((int) idx);
        }


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            THREADS threads = THREADS.getInstance(context);
            Group group = threads.getGroupByIdx(idx);
            Objects.requireNonNull(group);
            try {

                if (!threads.hasNotes(group)) {
                    threads.setGroupDeleting(group);
                } else {
                    threads.resetGroupRequest(idx);
                }

                PID owner = group.getOwner();
                String uuid = group.getUuid();

                User user = threads.getUserByPid(owner.getPid());
                Objects.requireNonNull(user);

                GroupRejectWorker.reject(context, owner.getPid(), uuid);

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {

                if (!threads.hasNotes(group)) {
                    threads.removeGroup(group);
                }
            }

        });

    }

    public static void accept(@NonNull Context context, long idx) {

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancel((int) idx);
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {
                THREADS threads = THREADS.getInstance(context);
                Group group = threads.getGroupByIdx(idx);
                Objects.requireNonNull(group);
                PID owner = group.getOwner();
                group.setRequest(false);

                // only security here
                if (!group.hasMember(owner.getPid())) {
                    group.addMember(owner.getPid());
                }

                threads.updateGroup(group);

                User user = threads.getUserByPid(owner.getPid());
                Objects.requireNonNull(user);

                if (!user.isVisible()) {
                    threads.setUserVisible(owner.getPid(), true);
                }

                GroupJoinWorker.join(context, owner.getPid(), group.getUuid());

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });


    }

    public static void updateGroupInfo(@NonNull Context context, long groupIdx, long idx) {

        THREADS threads = THREADS.getInstance(context);
        String mimeType = threads.getNoteMimeType(idx);
        String text = threads.getNoteText(idx);
        if (text == null || text.isEmpty()) {
            if (mimeType.startsWith(MimeType.AUDIO)) {
                text = context.getString(R.string.audio);
            } else if (mimeType.startsWith(MimeType.VIDEO)) {
                text = context.getString(R.string.video);
            } else if (mimeType.startsWith(MimeType.IMAGE)) {
                text = context.getString(R.string.image);
            } else {
                text = "";
            }
        }
        threads.setGroupMimeType(groupIdx, mimeType);
        threads.setGroupContent(groupIdx, text);
        threads.setGroupDate(groupIdx, new Date());
    }
}
