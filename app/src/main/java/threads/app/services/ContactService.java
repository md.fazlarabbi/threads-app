package threads.app.services;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkManager;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.work.ConnectionWorker;
import threads.app.work.IdentifyWorker;
import threads.app.work.UserConnectWorker;
import threads.ipfs.IPFS;
import threads.ipfs.PID;


public class ContactService {

    private static final String TAG = ContactService.class.getSimpleName();

    public static void contact(@NonNull Context context, @NonNull String peerID) {


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long start = System.currentTimeMillis();

            try {
                PID host = IPFS.getPID(context);
                THREADS threads = THREADS.getInstance(context);
                PID pid = PID.create(peerID);

                if (!Objects.equals(host, pid)) {
                    User user = threads.getUserByPid(peerID);
                    if (user == null) {
                        String alias = pid.getPid();
                        user = threads.createUser(pid, null, alias, null);
                        user.setDialing(true);
                        user.setRating(0);
                        threads.storeUser(user);
                    } else {

                        threads.setUserVisible(user.getPid(), true);
                        threads.setUserBlocked(user.getPid(), false);
                    }
                    WorkManager.getInstance(context).beginUniqueWork(
                            UserConnectWorker.getUniqueId(pid.getPid()), ExistingWorkPolicy.KEEP,
                            ConnectionWorker.getWork())
                            .then(UserConnectWorker.getWork(pid.getPid(), 30, false))
                            .then(IdentifyWorker.getWork(pid.getPid()))
                            .enqueue();
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                LogUtils.info(TAG, " finish running [" + (System.currentTimeMillis() - start) + "]...");
            }
        });

    }

}