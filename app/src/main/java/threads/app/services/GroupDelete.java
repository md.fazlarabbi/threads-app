package threads.app.services;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;


public class GroupDelete {
    @NonNull
    private final List<Long> groups = new ArrayList<>();


    public void addGroup(long group) {
        groups.add(group);
    }

    @NonNull
    public List<Long> getGroups() {
        return groups;
    }
}
