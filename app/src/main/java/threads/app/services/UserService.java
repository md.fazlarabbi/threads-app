package threads.app.services;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.ipfs.PID;

public class UserService {
    private static final String TAG = UserService.class.getSimpleName();


    public static void deleteUser(@NonNull Context context, @NonNull String... pids) {

        try {


            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> {
                try {
                    for (String pid : pids) {
                        THREADS threads = THREADS.getInstance(context);
                        threads.setUserBlocked(pid, true);
                        threads.setUserVisible(pid, false);
                    }

                } catch (Throwable e) {
                    LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                }
            });
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    static void createUser(@NonNull Context context, @NonNull String pid,
                           @NonNull String alias, @NonNull String pkey,
                           @NonNull String token, boolean initVisible) {

        THREADS threads = THREADS.getInstance(context);


        PID owner = PID.create(pid);


        // check if user is defined
        User user = threads.getUserByPid(pid);
        if (user == null) {
            user = threads.createUser(owner, pkey, alias, null);
            user.setToken(token);
            user.setValid(true);
            user.setVisible(initVisible); // important will be true, when user accepts
            threads.storeUser(user);
        }

    }


}
