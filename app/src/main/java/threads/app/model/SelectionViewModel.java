package threads.app.model;


import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SelectionViewModel extends ViewModel {

    @NonNull
    private final MutableLiveData<Long> group = new MutableLiveData<>(0L);

    @NonNull
    private final MutableLiveData<PreviewData> previewData = new MutableLiveData<>(null);

    @NonNull
    public MutableLiveData<Long> getGroup() {
        return group;
    }

    public void setGroup(long group) {
        getGroup().postValue(group);
    }


    @NonNull
    public MutableLiveData<PreviewData> getPreviewData() {
        return previewData;
    }


    public void setPreviewData(PreviewData data) {
        getPreviewData().postValue(data);
    }

}
