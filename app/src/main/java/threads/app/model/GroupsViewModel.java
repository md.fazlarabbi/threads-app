package threads.app.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import java.util.ArrayList;
import java.util.List;

import threads.app.core.Group;
import threads.app.core.THREADS;
import threads.app.core.ThreadsDatabase;

public class GroupsViewModel extends AndroidViewModel {
    @NonNull
    private final MediatorLiveData<List<Group>> liveDataMerger = new MediatorLiveData<>();

    public GroupsViewModel(@NonNull Application application) {
        super(application);
        ThreadsDatabase threadsDatabase = THREADS.getInstance(
                application.getApplicationContext()).getThreadsDatabase();

        LiveData<List<Group>> liveDataGroups = threadsDatabase.groupDao().getLiveDataGroups();


        liveDataMerger.addSource(liveDataGroups, (threads) -> {

            if (threads != null) {
                List<Group> adds = new ArrayList<>();
                for (Group group : threads) {
                    if (!group.isDeleting()) {
                        adds.add(group);
                    }
                }
                liveDataMerger.setValue(adds);
            }

        });
    }

    @NonNull
    public LiveData<List<Group>> getGroups() {
        return liveDataMerger;
    }


}
