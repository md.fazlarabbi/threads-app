package threads.app.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import threads.app.core.THREADS;
import threads.app.core.ThreadsDatabase;
import threads.app.core.User;

public class UsersViewModel extends AndroidViewModel {

    @NonNull
    private final ThreadsDatabase threadsDatabase;

    public UsersViewModel(@NonNull Application application) {
        super(application);
        threadsDatabase = THREADS.getInstance(
                application.getApplicationContext()).getThreadsDatabase();
    }

    @NonNull
    public LiveData<List<User>> getVisibleUsers() {
        return threadsDatabase.userDao().getLiveDataUsers();
    }
}