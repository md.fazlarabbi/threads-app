package threads.app.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import threads.app.core.THREADS;
import threads.app.core.ThreadsDatabase;
import threads.app.core.User;

public class AccountsViewModel extends AndroidViewModel {
    private final ThreadsDatabase threadsDatabase;

    public AccountsViewModel(@NonNull Application application) {
        super(application);
        threadsDatabase = THREADS.getInstance(
                application.getApplicationContext()).getThreadsDatabase();
    }


    public LiveData<List<User>> getLiteUsers() {
        return threadsDatabase.userDao().getLiveDataLiteUsers();
    }
}
