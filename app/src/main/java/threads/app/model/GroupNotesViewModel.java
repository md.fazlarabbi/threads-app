package threads.app.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.core.ThreadsDatabase;

public class GroupNotesViewModel extends AndroidViewModel {

    @NonNull
    private final ThreadsDatabase threadsDatabase;

    public GroupNotesViewModel(@NonNull Application application) {
        super(application);
        threadsDatabase = THREADS.getInstance(
                application.getApplicationContext()).getThreadsDatabase();
    }

    @NonNull
    public LiveData<List<Note>> getNotes(long thread) {
        return threadsDatabase.noteDao().getLiveDataNotesByGroup(thread);
    }
}

