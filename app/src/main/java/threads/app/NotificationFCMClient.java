package threads.app;

import android.util.Base64;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.services.GroupRequestService;
import threads.app.services.IdentityService;
import threads.app.utils.FcmContent;
import threads.ipfs.Encryption;
import threads.ipfs.IPFS;


public class NotificationFCMClient extends FirebaseMessagingService {
    private static final String TAG = NotificationFCMClient.class.getSimpleName();


    public NotificationFCMClient() {
        super();
    }


    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);

        LogUtils.info(TAG, "TOKEN : " + token);

        if (!Objects.equals(token, InitApplication.getToken(getApplicationContext()))) {
            InitApplication.setToken(getApplicationContext(), token);
            IdentityService.identity(getApplicationContext(), false);
        }
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {

        try {
            Map<String, String> data = remoteMessage.getData();
            long timestamp = remoteMessage.getSentTime();

            LogUtils.error(TAG, "FCM Message : " + data.toString());

            if (data.containsKey(Content.PID)) {

                String pid = data.get(Content.PID);
                Objects.requireNonNull(pid);
                if (data.containsKey(Content.EST)) {
                    String est = data.get(Content.EST);
                    Objects.requireNonNull(est);
                    if (FcmContent.REQUEST.name().equals(est)) {
                        IPFS ipfs = IPFS.getInstance(getApplicationContext());
                        String privateKey = ipfs.getPrivateKey();

                        String alias = data.get(Content.ALIAS);
                        Objects.requireNonNull(alias);
                        alias = new String(Base64.decode(alias, Base64.DEFAULT));
                        String sesKey = data.get(Content.SKEY);

                        String name = data.get(Content.NAME);
                        Objects.requireNonNull(name);
                        name = new String(Base64.decode(name, Base64.DEFAULT));
                        String uuid = data.get(Content.UUID);
                        Objects.requireNonNull(uuid);
                        String pkey = data.get(Content.PKEY);
                        Objects.requireNonNull(pkey);
                        String fcm = data.get(Content.FCM);
                        Objects.requireNonNull(fcm);

                        String cid = data.get(Content.CID); // icon for group
                        String skey = "";
                        if (sesKey != null && !sesKey.isEmpty()) {
                            skey = Encryption.decryptRSA(sesKey, privateKey);
                        }

                        GroupRequestService.handleGroupRequest(getApplicationContext(), pid,
                                alias, pkey, fcm, uuid, name, skey, cid);

                    } else if (FcmContent.LEAVE.name().equals(est)) {
                        String alias = data.get(Content.ALIAS);
                        Objects.requireNonNull(alias);
                        alias = new String(Base64.decode(alias, Base64.DEFAULT));
                        String uuid = data.get(Content.UUID);
                        Objects.requireNonNull(uuid);

                        GroupRequestService.handleGroupLeave(getApplicationContext(), pid,
                                alias, uuid);
                    } else if (FcmContent.REJECT.name().equals(est)) {
                        String uuid = data.get(Content.UUID);
                        Objects.requireNonNull(uuid);

                        GroupRequestService.handleGroupReject(getApplicationContext(), pid, uuid);
                    } else if (FcmContent.WAKEUP.name().equals(est)) {

                        // handle only messages which are less then 3 minutes ago
                        if (System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(3)
                                < timestamp) {
                            GroupRequestService.handleWakeup(getApplicationContext(), pid);
                        }
                    } else if (FcmContent.JOIN.name().equals(est)) {
                        String uuid = data.get(Content.UUID);
                        Objects.requireNonNull(uuid);
                        GroupRequestService.handleGroupJoin(getApplicationContext(), pid, uuid);
                    } else if (FcmContent.JOINED.name().equals(est)) {
                        String uuid = data.get(Content.UUID);
                        Objects.requireNonNull(uuid);
                        String alias = data.get(Content.ALIAS);
                        Objects.requireNonNull(alias);
                        alias = new String(Base64.decode(alias, Base64.DEFAULT));
                        String pkey = data.get(Content.PKEY);
                        Objects.requireNonNull(pkey);
                        String fcm = data.get(Content.FCM);
                        Objects.requireNonNull(fcm);

                        GroupRequestService.handleGroupJoined(getApplicationContext(), pid,
                                alias, pkey, fcm, uuid);
                    } else if (FcmContent.REMOVED.name().equals(est)) {
                        String uuid = data.get(Content.UUID);
                        Objects.requireNonNull(uuid);

                        GroupRequestService.handleGroupRemoved(getApplicationContext(), pid, uuid);
                    } else {
                        GroupRequestService.handleNote(getApplicationContext(), data);
                    }
                }
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }
}

