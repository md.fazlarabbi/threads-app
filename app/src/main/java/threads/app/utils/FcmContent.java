package threads.app.utils;

public enum FcmContent {
    LEAVE, REQUEST, REJECT, JOIN, JOINED, WAKEUP, REMOVED
}
