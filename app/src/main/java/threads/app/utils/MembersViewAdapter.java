package threads.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.User;
import threads.app.services.ThumbnailService;
import threads.ipfs.IPFS;


public class MembersViewAdapter extends RecyclerView.Adapter<MembersViewAdapter.ViewHolder> implements AccountsAdapter {
    private static final String TAG = MembersViewAdapter.class.getSimpleName();

    private final List<User> accounts = new ArrayList<>();
    private final Context context;

    public MembersViewAdapter(@NonNull Context context) {
        this.context = context;
    }

    public void setAccounts(@NonNull List<User> accounts) {
        this.accounts.clear();
        this.accounts.addAll(accounts);
        this.notifyDataSetChanged();
    }

    @Override
    public List<User> getSelectedAccounts() {
        return new ArrayList<>();
    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    @NonNull
    public MembersViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                            int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contacts, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = accounts.get(position);

        try {
            holder.account_name.setText(user.getAlias());
            holder.account_rating.setText(String.valueOf(user.getRating()));

            if (user.getImage() != null) {
                IPFS ipfs = IPFS.getInstance(context);
                IPFSData data = IPFSData.create(ipfs, user.getImage());
                Glide.with(context).
                        load(data).
                        apply(RequestOptions.circleCropTransform()).
                        into(holder.account_icon);
            } else {
                if (user.hasToken()) {
                    Bitmap bitmap = ThumbnailService.getNameImage(user.getAlias());
                    holder.account_icon.setImageBitmap(bitmap);
                } else {
                    holder.account_icon.setImageResource(R.drawable.network_storage);
                }
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e.getLocalizedMessage(), e);
        }


    }


    @Override
    public int getItemCount() {
        return accounts.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView account_name;
        final ImageView account_icon;
        final TextView account_rating;

        ViewHolder(View v) {
            super(v);
            account_name = itemView.findViewById(R.id.account_name);
            account_rating = itemView.findViewById(R.id.account_rating);
            account_icon = itemView.findViewById(R.id.account_icon);
        }

    }
}
