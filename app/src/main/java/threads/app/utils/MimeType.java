package threads.app.utils;

public class MimeType {

    public static final String HTML_MIME_TYPE = "text/html";
    public static final String PDF_MIME_TYPE = "application/pdf";
    public static final String OCTET_MIME_TYPE = "application/octet-stream";
    public static final String JSON_MIME_TYPE = "application/json";
    public static final String PLAIN_MIME_TYPE = "text/plain";
    public static final String TORRENT_MIME_TYPE = "application/x-bittorrent";
    public static final String AUDIO = "audio";
    public static final String JPEG_MIME_TYPE = "image/jpeg";
    public static final String VIDEO = "video";
    public static final String IMAGE = "image";
}
