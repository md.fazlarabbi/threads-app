package threads.app.utils;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.Group;
import threads.app.services.MimeTypeService;
import threads.ipfs.IPFS;


public class GroupViewAdapter extends RecyclerView.Adapter<GroupViewAdapter.ViewHolder> implements GroupItemPosition {

    private static final String TAG = GroupViewAdapter.class.getSimpleName();
    private final Context mContext;
    private final GroupViewAdapterListener mListener;
    private final List<Group> groups = new ArrayList<>();
    @Nullable
    private SelectionTracker<Long> mSelectionTracker;


    public GroupViewAdapter(@NonNull Context context,
                            @NonNull GroupViewAdapterListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {

        Group group = groups.get(position);

        if (group.isRequest()) {
            return 2;
        }
        return 0;
    }

    @Override
    @NonNull
    public GroupViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        View v;
        switch (viewType) {
            case 0:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.group, parent, false);
                return new ThreadViewHolder(this, v);
            case 2:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.group_request, parent, false);
                return new ThreadRequestViewHolder(this, v);

        }
        throw new RuntimeException("Not supported view type.");
    }

    long getIdx(int position) {
        return groups.get(position).getIdx();
    }

    public void setSelectionTracker(SelectionTracker<Long> selectionTracker) {
        this.mSelectionTracker = selectionTracker;
    }


    private boolean hasSelection() {
        if (mSelectionTracker != null) {
            return mSelectionTracker.hasSelection();
        }
        return false;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Group group = groups.get(position);

        if (holder instanceof ThreadViewHolder) {
            ThreadViewHolder threadViewHolder = (ThreadViewHolder) holder;
            boolean isSelected = false;
            if (mSelectionTracker != null) {
                if (mSelectionTracker.isSelected(group.getIdx())) {
                    isSelected = true;
                }
            }

            threadViewHolder.bind(isSelected, group);

            try {


                String date = getDate(new Date(group.getDate()));
                threadViewHolder.session_date.setText(date);
                int number = group.getNumber();
                if (number > 0) {
                    int textColor = mContext.getColor(R.color.colorBackground);
                    int color = mContext.getColor(R.color.colorAccent);
                    TextDrawable left = TextDrawable.builder().beginConfig()
                            .textColor(textColor).bold().height(dpToPx()).width(dpToPx()).endConfig()
                            .buildRound("" + number, color);
                    threadViewHolder.session_date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            left, null, null, null);
                    threadViewHolder.session_date.setCompoundDrawablePadding(8);
                } else {
                    threadViewHolder.session_date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            0, 0, 0, 0);
                    threadViewHolder.session_date.setCompoundDrawablePadding(0);
                }


                String title = MimeTypeService.getCompactString(group.getName());
                threadViewHolder.content_title.setText(title);

                String message = MimeTypeService.getCompactString(group.getContent());
                threadViewHolder.content_subtitle.setText(message);


                int start = 0;
                int end = 0;
                String mimeType = group.getMimeType();
                if (!MimeType.PLAIN_MIME_TYPE.equals(mimeType)) {
                    start = MimeTypeService.getMediaResource(mimeType);
                }
                if (group.isEncrypted()) {
                    end = R.drawable.lock;
                }


                threadViewHolder.content_subtitle.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        start, 0, end, 0);
                threadViewHolder.content_subtitle.setCompoundDrawablePadding(8);


                if (hasSelection()) {
                    if (isSelected) {
                        threadViewHolder.main_image.setClickable(false);
                        int color = mContext.getColor(R.color.colorAccent);
                        TextDrawable drawable = TextDrawable.builder()
                                .buildRound("\u2713", color);
                        threadViewHolder.main_image.setImageDrawable(drawable);
                    } else {
                        if (group.getImage() != null) {
                            threadViewHolder.main_image.setClickable(true);
                            IPFS ipfs = IPFS.getInstance(mContext);
                            IPFSData data = IPFSData.create(ipfs, group.getImage());

                            Glide.with(mContext).
                                    load(data).
                                    apply(RequestOptions.circleCropTransform()).
                                    into(threadViewHolder.main_image);

                            threadViewHolder.main_image.setOnClickListener((v) ->
                                    mListener.invokeViewAction(group));
                        } else {
                            threadViewHolder.main_image.setClickable(false);
                            threadViewHolder.main_image.setImageResource(R.drawable.group);
                        }
                    }
                } else {

                    if (group.getImage() != null) {
                        threadViewHolder.main_image.setClickable(true);
                        IPFS ipfs = IPFS.getInstance(mContext);
                        IPFSData data = IPFSData.create(ipfs, group.getImage());

                        Glide.with(mContext).
                                load(data).
                                apply(RequestOptions.circleCropTransform()).
                                into(threadViewHolder.main_image);

                        threadViewHolder.main_image.setOnClickListener((v) ->
                                mListener.invokeViewAction(group));
                    } else {
                        threadViewHolder.main_image.setClickable(false);
                        threadViewHolder.main_image.setImageResource(R.drawable.group);
                    }
                    threadViewHolder.view.setOnClickListener((v) -> {
                        try {
                            mListener.onClick(group);
                        } catch (Throwable e) {
                            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                        }

                    });
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        }

        if (holder instanceof ThreadRequestViewHolder) {
            ThreadRequestViewHolder threadRequestViewHolder = (ThreadRequestViewHolder) holder;

            threadRequestViewHolder.accept.setOnClickListener((v) -> mListener.onAcceptClick(group));
            threadRequestViewHolder.reject.setOnClickListener((v) -> mListener.onRejectClick(group));
        }


    }


    @NonNull
    private String getDate(@NonNull Date date) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 0);
        Date lastYear = c.getTime();

        if (date.before(today)) {
            if (date.before(lastYear)) {
                return android.text.format.DateFormat.format("dd.MM.yyyy", date).toString();
            } else {
                return android.text.format.DateFormat.format("dd.MMMM", date).toString();
            }
        } else {
            return android.text.format.DateFormat.format("HH:mm", date).toString();
        }
    }

    private int dpToPx() {
        return (int) (25 * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

    public void updateData(@NonNull List<Group> messageGroups) {

        final GroupDiffCallback diffCallback = new GroupDiffCallback(this.groups, messageGroups);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.groups.clear();
        this.groups.addAll(messageGroups);
        diffResult.dispatchUpdatesTo(this);


    }

    @Override
    public synchronized int getPosition(long idx) {
        for (int i = 0; i < groups.size(); i++) {
            if (groups.get(i).getIdx() == idx) {
                return i;
            }
        }
        return 0;
    }


    public interface GroupViewAdapterListener {

        void onClick(@NonNull Group group);

        void onRejectClick(@NonNull Group group);

        void onAcceptClick(@NonNull Group group);

        void invokeViewAction(@NonNull Group group);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final TextView content_title;
        final TextView content_subtitle;

        ViewHolder(View v) {
            super(v);
            v.setLongClickable(true);
            v.setClickable(true);
            view = v;
            content_title = v.findViewById(R.id.content_title);
            content_subtitle = v.findViewById(R.id.content_subtitle);
        }
    }


    static class ThreadViewHolder extends ViewHolder {
        final ImageView main_image;
        final TextView session_date;
        final GroupItemDetails groupItemDetails;

        ThreadViewHolder(GroupItemPosition pos, View v) {
            super(v);
            session_date = v.findViewById(R.id.session_date);
            main_image = v.findViewById(R.id.main_image);
            groupItemDetails = new GroupItemDetails(pos);
        }

        void bind(boolean isSelected, Group thread) {

            groupItemDetails.idx = thread.getIdx();

            itemView.setActivated(isSelected);


        }

        ItemDetailsLookup.ItemDetails<Long> getThreadsItemDetails() {

            return groupItemDetails;
        }
    }


    static class ThreadRequestViewHolder extends ThreadViewHolder {
        final TextView accept;
        final TextView reject;

        ThreadRequestViewHolder(GroupItemPosition pos, View v) {
            super(pos, v);

            accept = itemView.findViewById(R.id.accept);
            reject = itemView.findViewById(R.id.reject);

        }
    }


}
