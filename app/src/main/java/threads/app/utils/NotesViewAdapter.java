package threads.app.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.provider.FileDocumentsProvider;
import threads.app.services.MimeTypeService;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class NotesViewAdapter extends RecyclerView.Adapter<NotesViewAdapter.ViewHolder> {
    private static final String TAG = NotesViewAdapter.class.getSimpleName();
    private final Context mContext;
    private final List<Note> notes = new ArrayList<>();
    private final NotesViewAdapterListener mListener;
    private final IPFS ipfs;
    private final PID host;
    private final NotificationManager mNotificationManager;
    private final MediaPlayer mMediaPlayer = new MediaPlayer();
    private long mLastClickTime = 0;
    private WeakReference<AudioHolder> mAudioPlayer = null;

    public NotesViewAdapter(@NonNull Context context, @NonNull NotesViewAdapterListener listener) {
        this.mContext = context;
        this.mListener = listener;
        this.ipfs = IPFS.getInstance(context);
        this.host = IPFS.getPID(context);
        this.mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

    }

    private static String left(final String str) {
        if (str == null) {
            return null;
        }
        if (str.length() <= 30) {
            return str;
        }
        return str.substring(0, 30);
    }

    @NonNull
    @Override
    public NotesViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        switch (viewType) {
            case 8:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_info, parent, false);
                return new InfoHolder(v);
            case 7:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_data_own, parent, false);
                return new OwnDataHolder(v);
            case 6:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_message_own, parent, false);
                return new OwnMessageHolder(v);
            case 5:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_data, parent, false);
                return new DataHolder(v);
            case 4:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_audio, parent, false);
                return new AudioHolder(v);
            case 3:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_info_own, parent, false);
                return new OwnInfoHolder(v);
            case 2:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_message, parent, false);
                return new MessageHolder(v);
            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_link, parent, false);
                return new LinkHolder(v);
        }
        throw new RuntimeException("View Type not supported !!!");
    }

    @Override
    public void onBindViewHolder(@NonNull NotesViewAdapter.ViewHolder holder, final int position) {
        Note note = notes.get(position);

        try {

            if (holder instanceof AudioHolder) {
                AudioHolder audioHolder = (AudioHolder) holder;
                handleAudioNote(audioHolder, note);
            }
            if (holder instanceof OwnInfoHolder) {
                OwnInfoHolder infoHolder = (OwnInfoHolder) holder;
                handleOwnInfoNote(infoHolder, note);
            }
            if (holder instanceof InfoHolder) {
                InfoHolder infoHolder = (InfoHolder) holder;
                handleInfoNote(infoHolder, note);
            }
            if (holder instanceof MessageHolder) {
                MessageHolder messageHolder = (MessageHolder) holder;
                handleMessageNote(messageHolder, note);
            }
            if (holder instanceof OwnMessageHolder) {
                OwnMessageHolder messageHolder = (OwnMessageHolder) holder;
                handleOwnMessageNote(messageHolder, note);
            }
            if (holder instanceof LinkHolder) {
                LinkHolder linkHolder = (LinkHolder) holder;
                handleLinkNote(linkHolder, note);
            }
            if (holder instanceof DataHolder) {
                DataHolder dataHolder = (DataHolder) holder;
                handleDataNote(dataHolder, note);
            }
            if (holder instanceof OwnDataHolder) {
                OwnDataHolder dataHolder = (OwnDataHolder) holder;
                handleOwnDataNote(dataHolder, note);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        } finally {
            if (mNotificationManager != null) {
                mNotificationManager.cancel((int) note.getIdx());
            }
        }

    }

    @NonNull
    private String getDate(@NonNull Date date) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 0);
        Date lastYear = c.getTime();

        if (date.before(today)) {
            if (date.before(lastYear)) {
                return android.text.format.DateFormat.format("dd.MM.yyyy", date).toString();
            } else {
                return android.text.format.DateFormat.format("dd.MMMM", date).toString();
            }
        } else {
            return android.text.format.DateFormat.format("HH:mm", date).toString();
        }
    }

    private void handleDate(@NonNull ViewHolder holder, @NonNull Note note) {
        Date date = new Date(note.getDate());
        holder.date.setText(getDate(date));
    }

    private void handleOwnMessageNote(@NonNull OwnMessageHolder messageHolder, @NonNull Note note) {

        handleDate(messageHolder, note);
        if (note.isSeeding() && note.isPublished()) {
            messageHolder.date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0, 0, R.drawable.check, 0);
        }

        TextView textView = messageHolder.message_body;
        textView.setText(note.getText());
    }

    private void handleInfoNote(@NonNull InfoHolder infoHolder, @NonNull Note note) {


        String author = note.getAlias();
        infoHolder.user.setText(author);
        int color = ColorGenerator.MATERIAL.getColor(author);
        infoHolder.user.setTextColor(color);


        handleDate(infoHolder, note);


        TextView textView = infoHolder.message_body;

        textView.setText(note.getText());

    }

    private void handleMessageNote(@NonNull MessageHolder messageHolder, @NonNull Note note) {


        String author = note.getAlias();
        messageHolder.user.setText(author);
        int color = ColorGenerator.MATERIAL.getColor(author);
        messageHolder.user.setTextColor(color);


        handleDate(messageHolder, note);


        TextView textView = messageHolder.message_body;

        textView.setText(note.getText());


        if (note.isLeaching()) {
            messageHolder.general_action.setImageResource(R.drawable.pause);
            messageHolder.general_action.setVisibility(View.VISIBLE);
            messageHolder.general_action.setOnClickListener((v) ->
                    mListener.invokePauseAction(note)
            );
        } else if (note.isSeeding()) {
            messageHolder.general_action.setVisibility(View.GONE);
        } else {
            if (!note.isExpired()) {
                messageHolder.general_action.setVisibility(View.VISIBLE);
                messageHolder.general_action.setImageResource(R.drawable.download);
                messageHolder.general_action.setOnClickListener((v) -> {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    mListener.invokeDownloadAction(note);
                });
            } else {

                messageHolder.general_action.setVisibility(View.VISIBLE);
                messageHolder.general_action.setImageResource(R.drawable.error_timer_off);
                messageHolder.general_action.setOnClickListener((v) -> {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    Toast.makeText(mContext, R.string.expired, Toast.LENGTH_LONG).show();
                });
            }

        }

    }

    private void handleOwnInfoNote(@NonNull OwnInfoHolder infoHolder, @NonNull Note note) {

        TextView textView = infoHolder.message_body;
        textView.setText(note.getText());
    }

    @Override
    public int getItemViewType(int position) {
        Note note = getItemAtPosition(position);
        Objects.requireNonNull(note);

        if (note.getNoteType() == NoteType.INFO) {
            if (Objects.equals(note.getOwner(), host)) {
                return 3;
            } else {
                return 8;
            }
        } else if (note.getNoteType() == NoteType.MESSAGE) {
            if (Objects.equals(note.getOwner(), host)) {
                return 6;
            } else {
                return 2;
            }
        } else if (note.getNoteType() == NoteType.AUDIO) {
            return 4;
        } else if (note.getNoteType() == NoteType.VIDEO ||
                note.getNoteType() == NoteType.IMAGE) {
            if (Objects.equals(note.getOwner(), host)) {
                return 7;
            } else {
                return 5;
            }
        } else {
            return 1;
        }
    }

    private Note getItemAtPosition(int position) {
        try {
            return notes.get(position);
        } catch (Throwable e) {
            // ignore exception
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void updateNotes(@NonNull List<Note> notes) {

        final NoteDiffCallback diffCallback = new NoteDiffCallback(this.notes, notes);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.notes.clear();
        this.notes.addAll(notes);
        diffResult.dispatchUpdatesTo(this);
    }

    private void handleOwnDataNote(@NonNull OwnDataHolder holder, @NonNull Note note) {


        if (note.getNoteType() == NoteType.VIDEO) {
            holder.video_view.setImageResource(R.drawable.video_white);
        } else {
            holder.video_view.setImageResource(R.drawable.camera_white);
        }

        handleDate(holder, note);

        if (note.isSeeding() && note.isPublished()) {
            holder.date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0, 0, R.drawable.check, 0);
        }

        if (!note.getText().isEmpty()) {
            holder.message_view.setVisibility(View.VISIBLE);
            holder.message_view.setText(note.getText());
        } else {
            holder.message_view.setVisibility(View.INVISIBLE);
        }

        if (note.isSeeding() && note.isPublished()) {
            holder.date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0, 0, R.drawable.check, 0);
        }

        String mimeType = note.getMimeType();
        int res = MimeTypeService.getMediaResource(mimeType);
        if (note.getThumbnail() != null) {
            //noinspection unchecked
            MultiTransformation transform = new MultiTransformation(
                    new CenterCrop(), new RoundedCorners(10));
            Objects.requireNonNull(transform);

            IPFSData data = IPFSData.create(ipfs, note.getThumbnail());
            //noinspection unchecked
            Glide.with(mContext).load(data).apply(
                    new RequestOptions()
                            .transform(transform))
                    .into(holder.image_view);
        } else {
            holder.image_view.setImageResource(res);
        }


        if (note.isSeeding()) {
            if (!mimeType.isEmpty()) {
                holder.image_view.setOnClickListener((v) -> mListener.invokeAction(note));
            }
        }
    }

    private void handleLinkNote(@NonNull LinkHolder holder, @NonNull Note note) {


        String author = left(note.getAlias());
        holder.user.setText(author);
        int color = ColorGenerator.MATERIAL.getColor(author);
        holder.user.setTextColor(color);


        handleDate(holder, note);
        if (host.equals(note.getOwner())) {
            if (note.isSeeding() && note.isPublished()) {
                holder.date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.check, 0, 0, 0);
            }
        }
        TextView textView = holder.message_body;

        String mimeType = note.getMimeType();
        int res = MimeTypeService.getMediaResource(mimeType);
        if (note.getThumbnail() != null) {
            //noinspection unchecked
            MultiTransformation transform = new MultiTransformation(
                    new CenterCrop(), new RoundedCorners(10));
            Objects.requireNonNull(transform);

            IPFSData data = IPFSData.create(ipfs, note.getThumbnail());
            //noinspection unchecked
            Glide.with(mContext).load(data).apply(
                    new RequestOptions()
                            .transform(transform))
                    .into(holder.image_view);
        } else {
            holder.image_view.setImageResource(res);
        }

        if (note.isLeaching()) {
            if (holder.progress_bar.getVisibility() != View.VISIBLE) {
                holder.progress_bar.setVisibility(View.VISIBLE);
            }
            int progress = note.getProgress();
            if (progress >= 0 && progress < 101) {
                holder.progress_bar.setProgress(progress);
            }
        } else {
            if (holder.progress_bar.getVisibility() != View.INVISIBLE) {
                holder.progress_bar.setVisibility(View.INVISIBLE);
            }
        }


        String fileInfo = getFileInfo(mContext, note);
        textView.setText(fileInfo);


        if (!host.equals(note.getOwner())) {
            if (note.isLeaching()) {
                holder.general_action.setImageResource(R.drawable.pause);
                holder.general_action.setVisibility(View.VISIBLE);
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokePauseAction(note)
                );
            } else if (note.isSeeding()) {
                holder.general_action.setImageResource(R.drawable.copy_to);
                holder.general_action.setVisibility(View.VISIBLE);
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokeCopyTo(note)
                );
            } else {
                if (!note.isExpired()) {
                    holder.general_action.setVisibility(View.VISIBLE);
                    holder.general_action.setImageResource(R.drawable.download);
                    holder.general_action.setOnClickListener((v) ->
                            mListener.invokeDownloadAction(note)
                    );
                } else {
                    holder.general_action.setVisibility(View.VISIBLE);
                    holder.general_action.setImageResource(R.drawable.error_timer_off);
                    holder.general_action.setOnClickListener((v) -> {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        Toast.makeText(mContext, R.string.expired, Toast.LENGTH_LONG).show();
                    });
                }
            }
        } else {
            if (note.isError()) {
                holder.general_action.setImageResource(R.drawable.delete);
                if (holder.general_action.getVisibility() != View.VISIBLE) {
                    holder.general_action.setVisibility(View.VISIBLE);
                }
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokeDeleteAction(note)
                );
            } else if (note.isInit()) {
                holder.general_action.setVisibility(View.INVISIBLE);
            } else if (note.isLeaching()) {
                holder.general_action.setImageResource(R.drawable.pause);
                if (holder.general_action.getVisibility() != View.VISIBLE) {
                    holder.general_action.setVisibility(View.VISIBLE);
                }
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokePauseAction(note)
                );
            } else if (!note.isSeeding() && !note.isLeaching()) {
                holder.general_action.setImageResource(R.drawable.upload);
                if (holder.general_action.getVisibility() != View.VISIBLE) {
                    holder.general_action.setVisibility(View.VISIBLE);
                }
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokeUploadAction(note)
                );
            } else {
                if (holder.general_action.getVisibility() != View.GONE) {
                    holder.general_action.setVisibility(View.GONE);
                }
            }
        }


        if (note.isSeeding()) {
            if (!mimeType.isEmpty()) {
                holder.view.setOnClickListener((v) -> mListener.invokeAction(note));
            }
        }

    }

    private void handleDataNote(@NonNull DataHolder holder, @NonNull Note note) {

        String author = left(note.getAlias());
        holder.user.setText(author);
        int color = ColorGenerator.MATERIAL.getColor(author);
        holder.user.setTextColor(color);

        if (!note.getText().isEmpty()) {
            holder.message_view.setVisibility(View.VISIBLE);
            holder.message_view.setText(note.getText());
        } else {
            holder.message_view.setVisibility(View.INVISIBLE);
        }

        if (note.getNoteType() == NoteType.VIDEO) {
            holder.video_view.setImageResource(R.drawable.video_white);
        } else {
            holder.video_view.setImageResource(R.drawable.camera_white);
        }

        handleDate(holder, note);

        holder.date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                0, 0, 0, 0);


        String mimeType = note.getMimeType();
        int res = MimeTypeService.getMediaResource(mimeType);
        if (note.getThumbnail() != null) {
            //noinspection unchecked
            MultiTransformation transform = new MultiTransformation(
                    new CenterCrop(), new RoundedCorners(10));
            Objects.requireNonNull(transform);

            IPFSData data = IPFSData.create(ipfs, note.getThumbnail());
            //noinspection unchecked
            Glide.with(mContext).load(data).apply(
                    new RequestOptions()
                            .transform(transform))
                    .into(holder.image_view);
        } else {
            holder.image_view.setImageResource(res);
        }

        int progress = note.getProgress();
        if (progress > 0 && progress < 101) {
            if (holder.progress_bar.isIndeterminate()) {
                holder.progress_bar.setIndeterminate(false);
            }
            holder.progress_bar.setProgress(progress);
        } else {
            holder.progress_bar.setIndeterminate(true);
        }


        if (note.isLeaching()) {
            holder.general_action.setImageResource(R.drawable.pause);
            holder.general_action.setVisibility(View.VISIBLE);
            holder.general_action.setOnClickListener((v) ->
                    mListener.invokePauseAction(note)
            );
        } else if (note.isSeeding()) {
            holder.general_action.setImageResource(R.drawable.copy_to);
            holder.general_action.setVisibility(View.VISIBLE);
            holder.general_action.setOnClickListener((v) ->
                    mListener.invokeCopyTo(note)
            );
        } else {
            if (!note.isExpired()) {
                holder.general_action.setVisibility(View.VISIBLE);
                holder.general_action.setImageResource(R.drawable.download);
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokeDownloadAction(note)
                );
            } else {
                holder.general_action.setVisibility(View.VISIBLE);
                holder.general_action.setImageResource(R.drawable.error_timer_off);
                holder.general_action.setOnClickListener((v) -> {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    Toast.makeText(mContext, R.string.expired, Toast.LENGTH_LONG).show();
                });
            }
        }


        if (note.isSeeding()) {
            if (!mimeType.isEmpty()) {
                holder.image_view.setOnClickListener((v) -> mListener.invokeAction(note));
            }
        }

        if (note.isLeaching()) {
            holder.progress_bar.setVisibility(View.VISIBLE);
        } else {
            holder.progress_bar.setVisibility(View.INVISIBLE);
        }
    }

    private void handleAudioNote(@NonNull AudioHolder holder, @NonNull Note note) {

        String author = left(note.getAlias());
        holder.user.setText(author);
        int color = ColorGenerator.MATERIAL.getColor(author);
        holder.user.setTextColor(color);

        handleDate(holder, note);

        if (host.equals(note.getOwner())) {
            if (note.isSeeding() && note.isPublished()) {
                holder.date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        0, 0, R.drawable.check, 0);
            }
        } else {
            holder.date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0, 0, 0, 0);
        }


        if (!host.equals(note.getOwner())) {
            if (note.isLeaching()) {
                holder.general_action.setImageResource(R.drawable.pause);
                holder.general_action.setVisibility(View.VISIBLE);
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokePauseAction(note)
                );
            } else if (note.isSeeding()) {
                holder.general_action.setVisibility(View.VISIBLE);
                handleAudio(holder, note);
            } else {
                if (!note.isExpired()) {
                    holder.general_action.setVisibility(View.VISIBLE);
                    holder.general_action.setImageResource(R.drawable.download);
                    holder.general_action.setOnClickListener((v) ->
                            mListener.invokeDownloadAction(note)
                    );
                } else {
                    holder.general_action.setVisibility(View.VISIBLE);
                    holder.general_action.setImageResource(R.drawable.error_timer_off);
                    holder.general_action.setOnClickListener((v) -> {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        Toast.makeText(mContext, R.string.expired, Toast.LENGTH_LONG).show();
                    });
                }
            }
        } else {
            if (note.isSeeding()) {
                holder.general_action.setVisibility(View.VISIBLE);
                handleAudio(holder, note);
            }
        }

    }

    private void handleAudio(@NonNull AudioHolder holder, @NonNull Note note) {
        Uri uri = FileDocumentsProvider.getUriForNote(note);

        holder.general_action.setOnClickListener((v) -> {


            try {

                if (holder.isPlaying()) {
                    mMediaPlayer.pause();
                    holder.stop();
                } else {
                    stopAudioHolder();
                    holder.play();

                    mMediaPlayer.stop();
                    mMediaPlayer.reset();
                    mMediaPlayer.setDataSource(mContext, uri);
                    mMediaPlayer.setOnCompletionListener(mp -> holder.done());
                    mAudioPlayer = new WeakReference<>(holder);
                    mMediaPlayer.prepare();
                    mMediaPlayer.seekTo(holder.seekTo.get());
                    mMediaPlayer.start();

                    ExecutorService executor = Executors.newSingleThreadExecutor();
                    executor.submit(() -> {
                        try {
                            do {
                                Thread.sleep(50);
                                if (holder.isPlaying()) {
                                    int progress = mMediaPlayer.getCurrentPosition();
                                    holder.mSeekBar.setProgress(progress);
                                    holder.seekTo.set(progress);
                                }

                            } while (holder.isPlaying());


                        } catch (Throwable e) {
                            // ignore exception
                        }
                    });
                }


                final int duration = mMediaPlayer.getDuration();
                holder.mSeekBar.setProgress(holder.seekTo.get());
                holder.mSeekBar.setMax(duration);
                holder.mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            mMediaPlayer.seekTo(progress);
                            LogUtils.info(TAG, "Progress : " + progress);
                            holder.seekTo.set(progress);
                        }
                    }
                });


            } catch (IOException e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });
    }


    public void onDestroy() {
        releaseAudioHolder();
    }

    private void releaseAudioHolder() {
        try {
            if (mAudioPlayer != null) {
                AudioHolder audioHolder = mAudioPlayer.get();
                mMediaPlayer.stop();
                audioHolder.stop();
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        } finally {
            mMediaPlayer.release();
        }
    }


    public void stopAudioHolder() {
        try {
            if (mAudioPlayer != null) {
                AudioHolder audioHolder = mAudioPlayer.get();
                audioHolder.stop();
            }
            mMediaPlayer.stop();
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private String getFileInfo(Context context, Note note) {
        String fileName = note.getName();

        String fileSize;
        long size = note.getSize();

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return context.getString(R.string.link_format, fileName, fileSize);
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return context.getString(R.string.link_format_kb, fileName, fileSize);
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return context.getString(R.string.link_format_mb, fileName, fileSize);
        }
    }

    public interface NotesViewAdapterListener {

        void invokeCopyTo(@NonNull Note note);

        void invokeDownloadAction(@NonNull Note note);

        void invokeAction(@NonNull Note note);

        void invokePauseAction(@NonNull Note note);

        void invokeUploadAction(@NonNull Note note);

        void invokeDeleteAction(@NonNull Note note);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView date;
        final View view;

        ViewHolder(View v) {
            super(v);
            view = v;
            date = v.findViewById(R.id.date);

        }

    }


    static class DataHolder extends NotesViewAdapter.ViewHolder {

        final TextView user;
        final ImageView general_action;
        final ImageView image_view;
        final ImageView video_view;
        final ProgressBar progress_bar;
        final TextView message_view;

        DataHolder(View v) {
            super(v);
            message_view = v.findViewById(R.id.message);
            general_action = v.findViewById(R.id.general_action);
            user = v.findViewById(R.id.user);
            image_view = v.findViewById(R.id.image_view);
            video_view = v.findViewById(R.id.image_video);
            progress_bar = v.findViewById(R.id.progress_bar);

        }
    }

    static class OwnDataHolder extends NotesViewAdapter.ViewHolder {

        final ImageView image_view;
        final ImageView video_view;
        final TextView message_view;

        OwnDataHolder(View v) {
            super(v);
            message_view = v.findViewById(R.id.message);
            image_view = v.findViewById(R.id.image_view);
            video_view = v.findViewById(R.id.image_video);
        }
    }

    static class LinkHolder extends NotesViewAdapter.ViewHolder {

        final TextView user;
        final TextView message_body;
        final ImageView general_action;
        final ImageView image_view;

        final ProgressBar progress_bar;

        LinkHolder(View v) {
            super(v);


            general_action = v.findViewById(R.id.general_action);
            user = v.findViewById(R.id.user);
            message_body = v.findViewById(R.id.message_body);
            image_view = v.findViewById(R.id.image_view);
            progress_bar = v.findViewById(R.id.progress_bar);

        }
    }

    static class AudioHolder extends NotesViewAdapter.ViewHolder {

        final TextView user;
        final ImageView general_action;
        final SeekBar mSeekBar;
        private final AtomicBoolean play = new AtomicBoolean(false);
        private final AtomicInteger seekTo = new AtomicInteger(0);

        AudioHolder(View v) {
            super(v);

            general_action = v.findViewById(R.id.general_action);
            user = v.findViewById(R.id.user);
            mSeekBar = v.findViewById(R.id.seek_bar);

        }

        void stop() {
            general_action.setImageResource(R.drawable.play_circle);
            play.set(false);
        }

        void play() {
            general_action.setImageResource(R.drawable.pause_circle);
            play.set(true);
        }

        boolean isPlaying() {
            return play.get();
        }

        void done() {
            stop();
            seekTo.set(0);
        }
    }

    static class MessageHolder extends NotesViewAdapter.ViewHolder {

        final TextView user;
        final TextView message_body;
        final ImageView general_action;


        MessageHolder(View v) {
            super(v);


            message_body = v.findViewById(R.id.message_body);
            general_action = v.findViewById(R.id.general_action);
            user = v.findViewById(R.id.user);

        }
    }

    static class InfoHolder extends NotesViewAdapter.ViewHolder {

        final TextView user;
        final TextView message_body;


        InfoHolder(View v) {
            super(v);


            message_body = v.findViewById(R.id.message_body);
            user = v.findViewById(R.id.user);

        }
    }

    static class OwnMessageHolder extends NotesViewAdapter.ViewHolder {

        final TextView message_body;

        OwnMessageHolder(View v) {
            super(v);
            message_body = v.findViewById(R.id.message_body);
        }
    }

    static class OwnInfoHolder extends NotesViewAdapter.ViewHolder {
        final TextView message_body;

        OwnInfoHolder(View v) {
            super(v);
            message_body = v.findViewById(R.id.message_body);
        }
    }


}
