package threads.app.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemKeyProvider;

public class GroupItemKeyProvider extends ItemKeyProvider<Long> {

    private final GroupViewAdapter mAdapter;


    public GroupItemKeyProvider(@NonNull GroupViewAdapter adapter) {
        super(SCOPE_CACHED);
        mAdapter = adapter;
    }

    @Nullable
    @Override
    public Long getKey(int position) {
        return mAdapter.getIdx(position);
    }

    @Override
    public int getPosition(@NonNull Long key) {
        return mAdapter.getPosition(key);
    }

}