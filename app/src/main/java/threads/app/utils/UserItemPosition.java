package threads.app.utils;

interface UserItemPosition {
    int getPosition(String pid);
}
