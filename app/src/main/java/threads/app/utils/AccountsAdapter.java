package threads.app.utils;

import androidx.annotation.NonNull;

import java.util.List;

import threads.app.core.User;

public interface AccountsAdapter {
    void setAccounts(@NonNull List<User> accounts);

    List<User> getSelectedAccounts();
}
