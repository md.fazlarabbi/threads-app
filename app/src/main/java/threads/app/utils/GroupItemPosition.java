package threads.app.utils;

interface GroupItemPosition {
    int getPosition(long idx);
}
