package threads.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.User;
import threads.app.services.ThumbnailService;
import threads.ipfs.IPFS;

public class UserViewAdapter extends
        RecyclerView.Adapter<UserViewAdapter.ViewHolder> implements UserItemPosition {

    private static final String TAG = UserViewAdapter.class.getSimpleName();
    private final List<User> users = new ArrayList<>();
    private final Context mContext;
    private final UsersViewAdapterListener listener;

    @Nullable
    private SelectionTracker<String> mSelectionTracker;

    public UserViewAdapter(@NonNull Context context,
                           @NonNull UserViewAdapter.UsersViewAdapterListener listener) {

        this.mContext = context;
        this.listener = listener;
    }


    public void setSelectionTracker(SelectionTracker<String> selectionTracker) {
        this.mSelectionTracker = selectionTracker;
    }

    private boolean hasSelection() {
        if (mSelectionTracker != null) {
            return mSelectionTracker.hasSelection();
        }
        return false;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    @NonNull
    public UserViewAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                             int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.users, parent, false);
        return new UserViewAdapter.UserViewHolder(this, v);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final User user = users.get(position);

        if (holder instanceof UserViewAdapter.UserViewHolder) {
            UserViewAdapter.UserViewHolder userViewHolder = (UserViewAdapter.UserViewHolder) holder;

            boolean isSelected = false;
            if (mSelectionTracker != null) {
                if (mSelectionTracker.isSelected(user.getPid())) {
                    isSelected = true;
                }
            }

            userViewHolder.bind(isSelected, user);


            try {
                if (hasSelection()) {
                    if (isSelected) {
                        userViewHolder.user_image.setClickable(false);
                        int color = mContext.getColor(R.color.colorAccent);
                        TextDrawable drawable = TextDrawable.builder()
                                .buildRound("\u2713", color);
                        userViewHolder.user_image.setImageDrawable(drawable);
                    } else {
                        if (user.getImage() != null) {
                            userViewHolder.user_image.setClickable(false);
                            IPFS ipfs = IPFS.getInstance(mContext);
                            IPFSData data = IPFSData.create(ipfs, user.getImage());
                            Glide.with(mContext).
                                    load(data).
                                    apply(RequestOptions.circleCropTransform()).
                                    into(userViewHolder.user_image);
                            userViewHolder.user_image.setOnClickListener((v) ->
                                    listener.invokeViewAction(user));
                        } else {
                            userViewHolder.user_image.setClickable(false);
                            if (user.hasToken()) {
                                Bitmap bitmap = ThumbnailService.getNameImage(user.getAlias());
                                userViewHolder.user_image.setImageBitmap(bitmap);
                            } else {
                                userViewHolder.user_image.setImageResource(R.drawable.network_storage);
                            }
                        }
                    }

                } else {
                    if (user.getImage() != null) {
                        userViewHolder.user_image.setClickable(true);
                        IPFS ipfs = IPFS.getInstance(mContext);
                        IPFSData data = IPFSData.create(ipfs, user.getImage());
                        Glide.with(mContext).
                                load(data).
                                apply(RequestOptions.circleCropTransform()).
                                into(userViewHolder.user_image);
                        userViewHolder.user_image.setOnClickListener((v) ->
                                listener.invokeViewAction(user));
                    } else {
                        userViewHolder.user_image.setClickable(false);
                        if (user.hasToken()) {
                            Bitmap bitmap = ThumbnailService.getNameImage(user.getAlias());
                            userViewHolder.user_image.setImageBitmap(bitmap);
                        } else {
                            userViewHolder.user_image.setImageResource(R.drawable.network_storage);
                        }
                    }


                    if (user.isDialing()) {
                        userViewHolder.user_action.setImageResource(R.drawable.pause);
                        userViewHolder.user_action.setVisibility(View.VISIBLE);
                        userViewHolder.user_action.setOnClickListener((v) ->
                                listener.invokeAbortDialing(user)
                        );
                    } else {
                        userViewHolder.user_action.setImageResource(R.drawable.dots);
                        userViewHolder.user_action.setVisibility(View.VISIBLE);
                        userViewHolder.user_action.setOnClickListener((v) ->
                                listener.invokeGeneralAction(user, v)
                        );
                    }
                }

                if (user.isBlocked()) {
                    userViewHolder.user_alias.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            R.drawable.blocked, 0, 0, 0);
                }

                if (user.isConnected()) {
                    int color = ContextCompat.getColor(mContext, R.color.colorAccent);
                    userViewHolder.user_date.setTextColor(color);
                    userViewHolder.user_date.setText(mContext.getString(R.string.online));
                } else {
                    long timestamp = user.getTimestamp();
                    if (timestamp > 0L) {
                        String date = getDate(new Date(timestamp));
                        userViewHolder.user_date.setText(date);
                    }
                }
                userViewHolder.user_alias.setText(user.getAlias());

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        }

    }

    @NonNull
    private String getDate(@NonNull Date date) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 0);
        Date lastYear = c.getTime();

        if (date.before(today)) {
            if (date.before(lastYear)) {
                return android.text.format.DateFormat.format("dd.MM.yyyy", date).toString();
            } else {
                return android.text.format.DateFormat.format("dd.MMMM", date).toString();
            }
        } else {
            return android.text.format.DateFormat.format("HH:mm", date).toString();
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    public void updateData(@NonNull List<User> users) {

        final UserDiffCallback diffCallback = new UserDiffCallback(this.users, users);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.users.clear();
        this.users.addAll(users);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public synchronized int getPosition(String pid) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getPid().equals(pid)) {
                return i;
            }
        }
        return 0;
    }

    String getPid(int position) {
        return users.get(position).getPid();
    }


    public interface UsersViewAdapterListener {

        void invokeViewAction(@NonNull User user);

        void invokeGeneralAction(@NonNull User user, @NonNull View view);

        void invokeAbortDialing(@NonNull User user);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(View v) {
            super(v);
        }
    }


    static class UserViewHolder extends ViewHolder {

        final TextView user_date;
        final TextView user_alias;
        final ImageView user_action;
        final ImageView user_image;
        final UserItemDetails userItemDetails;


        UserViewHolder(UserItemPosition pos, View v) {
            super(v);
            user_image = v.findViewById(R.id.user_image);
            user_date = v.findViewById(R.id.user_date);
            user_alias = v.findViewById(R.id.user_alias);
            user_action = v.findViewById(R.id.user_action);
            userItemDetails = new UserItemDetails(pos);
        }

        void bind(boolean isSelected, User user) {

            userItemDetails.pid = user.getPid();

            itemView.setActivated(isSelected);


        }

        ItemDetailsLookup.ItemDetails<String> getUserItemDetails() {

            return userItemDetails;
        }
    }
}
