package threads.app.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import threads.LogUtils;
import threads.app.MainActivity;
import threads.app.R;
import threads.app.RequestBroadcastReceiver;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.services.ThumbnailService;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupRequestChannel {


    private static final String TAG = GroupRequestChannel.class.getSimpleName();
    private static final String GROUP_REQUEST_CHANNEL_ID = "GROUP_REQUEST_CHANNEL_ID";

    @SuppressWarnings("SuspiciousNameCombination")
    private static Bitmap getCircleBitmap(Bitmap bitmap) {
        Bitmap output;
        Rect srcRect, dstRect;
        float r;
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();

        if (width > height) {
            output = Bitmap.createBitmap(height, height, Bitmap.Config.ARGB_8888);
            int left = (width - height) / 2;
            int right = left + height;
            srcRect = new Rect(left, 0, right, height);
            dstRect = new Rect(0, 0, height, height);
            //noinspection IntegerDivisionInFloatingPointContext
            r = height / 2;
        } else {
            output = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            int top = (height - width) / 2;
            int bottom = top + width;
            srcRect = new Rect(0, top, width, bottom);
            dstRect = new Rect(0, 0, width, width);
            //noinspection IntegerDivisionInFloatingPointContext
            r = width / 2;
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, srcRect, dstRect, paint);

        bitmap.recycle();

        return output;
    }

    public static void createGroupRequestNotification(@NonNull Context context, long idx,
                                                      @NonNull PID owner,
                                                      @NonNull String alias) {


        THREADS threads = THREADS.getInstance(context);
        IPFS ipfs = IPFS.getInstance(context);
        Bitmap bitmap = null;
        CID image = threads.getUserImage(owner.getPid());
        if (image != null) {
            try {
                bitmap = getCircleBitmap(
                        BitmapFactory.decodeStream(
                                ipfs.getInputStream(image, 4096)));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        if (bitmap == null) {
            bitmap = ThumbnailService.getNameImage(alias);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                GROUP_REQUEST_CHANNEL_ID);

        builder.setContentTitle(getCompactString(alias));
        builder.setContentText(context.getString(R.string.session_request));
        builder.setSmallIcon(R.drawable.forum_outline);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setLargeIcon(bitmap);


        Intent intent = new Intent(context, MainActivity.class);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(context, requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);


        builder.setContentIntent(pendingIntent);
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        builder.setAutoCancel(true);

        builder.setCategory(NotificationCompat.CATEGORY_MESSAGE);
        builder.setFullScreenIntent(pendingIntent, true);


        Intent rejectIntent = new Intent(context, RequestBroadcastReceiver.class);
        rejectIntent.setAction(RequestBroadcastReceiver.ACTION_REJECT);
        rejectIntent.putExtra(Content.IDX, idx);
        requestID = (int) System.currentTimeMillis();
        PendingIntent rejectPendingIntent = PendingIntent.getBroadcast(context, requestID,
                rejectIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.addAction(R.drawable.group, context.getString(R.string.reject),
                rejectPendingIntent);


        Intent acceptIntent = new Intent(context, RequestBroadcastReceiver.class);
        acceptIntent.setAction(RequestBroadcastReceiver.ACTION_ACCEPT);
        acceptIntent.putExtra(Content.IDX, idx);
        requestID = (int) System.currentTimeMillis();
        PendingIntent acceptPendingIntent = PendingIntent.getBroadcast(context, requestID,
                acceptIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.addAction(R.drawable.group, context.getString(R.string.accept),
                acceptPendingIntent);

        builder.setColor(ContextCompat.getColor(context, R.color.colorAccent));
        Notification notification = builder.build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify((int) idx, notification);
        }
    }


    public static void cancelGroupRequestNotification(@NonNull Context context, long idx) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancel((int) idx);
        }
    }

    private static String getCompactString(@NonNull String title) {
        return title.replace("\n", " ");
    }

    public static void createThreadRequestChannel(@NonNull Context context) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {
                CharSequence name = context.getString(R.string.thread_creator_channel_name);
                String description = context.getString(R.string.thread_creator_channel_description);
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(
                        GROUP_REQUEST_CHANNEL_ID, name, importance);
                mChannel.setDescription(description);

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                        Context.NOTIFICATION_SERVICE);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(mChannel);
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        }
    }
}
