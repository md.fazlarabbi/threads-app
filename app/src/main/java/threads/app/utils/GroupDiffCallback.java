package threads.app.utils;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import threads.app.core.Group;

@SuppressWarnings("WeakerAccess")
public class GroupDiffCallback extends DiffUtil.Callback {
    private final List<Group> mOldList;
    private final List<Group> mNewList;

    public GroupDiffCallback(List<Group> messages, List<Group> messageGroups) {
        this.mOldList = messages;
        this.mNewList = messageGroups;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areItemsTheSame(mNewList.get(
                newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).sameContent(mNewList.get(newItemPosition));
    }


}
