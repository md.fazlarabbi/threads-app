package threads.app.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import threads.ipfs.CID;
import threads.ipfs.PID;


@androidx.room.Entity
public class Note {

    @NonNull
    @TypeConverters(Converter.class)
    @ColumnInfo(name = "owner")
    private final PID owner;

    @ColumnInfo(name = "group")
    private final long group;

    @NonNull
    @TypeConverters(NoteType.class)
    @ColumnInfo(name = "noteType")
    private final NoteType noteType;

    @NonNull
    @ColumnInfo(name = "mimeType")
    private final String mimeType;

    @ColumnInfo(name = "date")
    private final long date;

    @NonNull
    @ColumnInfo(name = "text")
    private String text;

    @SuppressWarnings("CanBeFinal")
    @NonNull
    @ColumnInfo(name = "alias")
    private String alias;

    @PrimaryKey(autoGenerate = true)
    private long idx;

    @Nullable
    @TypeConverters(Converter.class)
    @ColumnInfo(name = "content")
    private CID content;

    @Nullable
    @ColumnInfo(name = "thumbnail")
    @TypeConverters(Converter.class)
    private CID thumbnail;

    @ColumnInfo(name = "size")
    private long size;
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    @Nullable
    @ColumnInfo(name = "uri")
    private String uri;
    @ColumnInfo(name = "seeding")
    private boolean seeding;
    @ColumnInfo(name = "deleting")
    private boolean deleting;
    @ColumnInfo(name = "published")
    private boolean published;
    @ColumnInfo(name = "leaching")
    private boolean leaching;
    @ColumnInfo(name = "error")
    private boolean error;
    @ColumnInfo(name = "expire")
    private long expire;
    @ColumnInfo(name = "progress")
    private int progress;
    @Nullable
    @ColumnInfo(name = "work")
    private String work;
    @ColumnInfo(name = "init")
    private boolean init;
    @ColumnInfo(name = "unread")
    private boolean unread;

    Note(long group, @NonNull PID owner, @NonNull String alias,
         @NonNull String mimeType, @NonNull NoteType noteType, long date) {
        this.group = group;
        this.owner = owner;
        this.alias = alias;
        this.mimeType = mimeType;
        this.noteType = noteType;
        this.published = false;
        this.leaching = false;
        this.expire = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(7);
        this.name = "";
        this.size = 0;
        this.progress = 0;
        this.seeding = false;
        this.text = "";
        this.date = date;
        this.error = false;
        this.init = false;
        this.unread = false;
    }

    public static Note createNote(
            long group,
            @NonNull PID senderPid,
            @NonNull String senderAlias,
            @NonNull NoteType noteType,
            @NonNull String mimeType,
            long date) {


        return new Note(group,
                senderPid,
                senderAlias,
                mimeType,
                noteType,
                date);
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    boolean isDeleting() {
        return deleting;
    }

    void setDeleting(boolean deleting) {
        this.deleting = deleting;
    }

    @Nullable
    public String getWork() {
        return work;
    }

    public void setWork(@Nullable String work) {
        this.work = work;
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }

    @Nullable
    public String getUri() {
        return uri;
    }

    public void setUri(@Nullable String uri) {
        this.uri = uri;
    }

    public long getDate() {
        return date;
    }


    public boolean isLeaching() {
        return leaching;
    }

    public void setLeaching(boolean leaching) {
        this.leaching = leaching;
    }

    long getExpire() {
        return expire;
    }

    void setExpire(long expire) {
        this.expire = expire;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @Nullable
    public CID getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(@Nullable CID thumbnail) {
        this.thumbnail = thumbnail;
    }

    @NonNull
    public PID getOwner() {
        return owner;
    }

    @NonNull
    public String getMimeType() {
        return mimeType;
    }

    public long getIdx() {
        return idx;
    }

    void setIdx(long idx) {
        this.idx = idx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note that = (Note) o;
        return Objects.equals(idx, that.idx);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx);
    }

    @NonNull
    public String getAlias() {
        return alias;
    }


    @NonNull
    public NoteType getNoteType() {
        return noteType;
    }

    public boolean areItemsTheSame(@NonNull Note note) {

        return idx == note.getIdx();
    }

    public long getGroup() {
        return group;
    }


    public boolean sameContent(@NonNull Note o) {

        if (this == o) return true;
        return noteType == o.getNoteType() &&
                published == o.isPublished() &&
                leaching == o.isLeaching() &&
                seeding == o.isSeeding() &&
                date == o.getDate() &&
                Objects.equals(progress, o.getProgress()) &&
                Objects.equals(content, o.getContent()) &&
                Objects.equals(alias, o.getAlias()) &&
                Objects.equals(thumbnail, o.getThumbnail());
    }

    @Override
    @NonNull
    public String toString() {
        return "Note{" +
                "alias='" + alias + '\'' +
                ", owner='" + owner + '\'' +
                ", groups='" + group + '\'' +
                ", noteType=" + noteType +
                ", mimeType='" + mimeType + '\'' +
                ", idx=" + idx +
                ", content='" + content + '}';
    }

    @Nullable
    public CID getContent() {
        return content;
    }

    public void setContent(@Nullable CID content) {
        this.content = content;
    }


    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isExpired() {
        return getExpire() < System.currentTimeMillis();
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public boolean isSeeding() {
        return seeding;
    }

    public void setSeeding(boolean seeding) {
        this.seeding = seeding;
    }

    @NonNull
    public String getText() {
        return text;
    }

    public void setText(@NonNull String text) {
        this.text = text;
    }

    public boolean hasThumbnail() {
        return getThumbnail() != null;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public UUID getWorkUUID() {
        if (work != null) {
            return UUID.fromString(work);
        }
        return null;
    }
}
