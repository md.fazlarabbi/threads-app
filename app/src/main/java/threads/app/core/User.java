package threads.app.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Objects;

import threads.ipfs.CID;
import threads.ipfs.PID;


@androidx.room.Entity
public class User {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "pid")
    private final String pid;
    @Nullable
    @ColumnInfo(name = "publicKey")
    private String publicKey;
    @NonNull
    @ColumnInfo(name = "alias")
    private String alias;
    @Nullable
    @ColumnInfo(name = "token")
    private String token;
    @Nullable
    @ColumnInfo(name = "agent")
    private String agent;
    @ColumnInfo(name = "connected")
    private boolean connected;
    @Nullable
    @ColumnInfo(name = "image")
    @TypeConverters(Converter.class)
    private CID image;
    @ColumnInfo(name = "blocked")
    private boolean blocked;
    @ColumnInfo(name = "dialing")
    private boolean dialing;
    @ColumnInfo(name = "valid")
    private boolean valid;
    @ColumnInfo(name = "visible")
    private boolean visible;
    @NonNull
    @ColumnInfo(name = "address")
    private String address;
    @ColumnInfo(name = "timestamp")
    private long timestamp;
    @ColumnInfo(name = "rating")
    private long rating;

    User(@NonNull String alias,
         @Nullable String publicKey,
         @NonNull String pid,
         @Nullable CID image) {
        this.alias = alias;
        this.publicKey = publicKey;
        this.pid = pid;
        this.image = image;
        this.blocked = false;
        this.dialing = false;
        this.connected = false;
        this.visible = true;
        this.timestamp = 0;
        this.address = "";
        this.rating = 0L;
    }

    @NonNull
    static User createUser(@NonNull String alias, @Nullable String publicKey,
                           @NonNull PID pid, @Nullable CID image) {
        return new User(alias, publicKey, pid.getPid(), image);
    }

    @Nullable
    public String getAgent() {
        return agent;
    }

    public void setAgent(@Nullable String agent) {
        this.agent = agent;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public boolean isDialing() {
        return dialing;
    }

    public void setDialing(boolean dialing) {
        this.dialing = dialing;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    @NonNull
    public String getPid() {
        return pid;
    }


    @NonNull
    public String getAlias() {
        return alias;
    }

    public void setAlias(@NonNull String alias) {
        this.alias = alias;
    }

    @Nullable
    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(@Nullable String publicKey) {
        this.publicKey = publicKey;
    }

    @Nullable
    public CID getImage() {
        return image;
    }


    public void setImage(@Nullable CID image) {
        this.image = image;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(pid, user.pid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pid);
    }

    public boolean areItemsTheSame(@NonNull User user) {
        return this.pid.equals(user.pid);

    }

    public boolean sameContent(@NonNull User user) {
        if (this == user) return true;
        return Objects.equals(connected, user.isConnected()) &&
                Objects.equals(dialing, user.isDialing()) &&
                Objects.equals(alias, user.getAlias()) &&
                Objects.equals(blocked, user.isBlocked()) &&
                Objects.equals(image, user.getImage()) &&
                Objects.equals(publicKey, user.getPublicKey());
    }

    public PID getPID() {
        return PID.create(pid);
    }

    @Nullable
    public String getToken() {
        return token;
    }

    public void setToken(@Nullable String token) {
        this.token = token;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    public boolean hasPublicKey() {
        return getPublicKey() != null;
    }

    public boolean hasToken() {
        return getToken() != null;
    }
}
