package threads.app.core;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;


public class THREADS extends ThreadsAPI {

    private static final Migration MIGRATION_78_79 = new Migration(78, 79) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Note "
                    + " ADD COLUMN deleting INTEGER DEFAULT 0 NOT NULL");
            database.execSQL("ALTER TABLE Note "
                    + " ADD COLUMN init INTEGER DEFAULT 0 NOT NULL");
            database.execSQL("ALTER TABLE Note "
                    + " ADD COLUMN work TEXT");
        }
    };
    private static final Migration MIGRATION_79_80 = new Migration(79, 80) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE User "
                    + " ADD COLUMN agent TEXT");
        }
    };
    private static final Migration MIGRATION_80_81 = new Migration(80, 81) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Note "
                    + " ADD COLUMN unread INTEGER DEFAULT 0 NOT NULL");
        }
    };
    private static THREADS INSTANCE = null;


    private THREADS(final THREADS.Builder builder) {
        super(builder.threadsDatabase);
    }

    @NonNull
    private static THREADS createThreads(@NonNull ThreadsDatabase threadsDatabase) {

        return new THREADS.Builder()
                .threadsDatabase(threadsDatabase)
                .build();
    }

    public static THREADS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (THREADS.class) {
                if (INSTANCE == null) {
                    ThreadsDatabase threadsDatabase = Room.databaseBuilder(context,
                            ThreadsDatabase.class,
                            ThreadsDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            addMigrations(MIGRATION_78_79, MIGRATION_79_80, MIGRATION_80_81).
                            fallbackToDestructiveMigration().
                            build();

                    INSTANCE = THREADS.createThreads(threadsDatabase);
                }
            }
        }
        return INSTANCE;
    }

    private static class Builder {
        ThreadsDatabase threadsDatabase = null;


        THREADS build() {

            return new THREADS(this);
        }

        Builder threadsDatabase(@NonNull ThreadsDatabase threadsDatabase) {

            this.threadsDatabase = threadsDatabase;
            return this;
        }

    }
}
