package threads.app.core;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {User.class, Group.class, Note.class}, version = 81, exportSchema = false)
public abstract class ThreadsDatabase extends RoomDatabase {


    public abstract UserDao userDao();

    public abstract GroupDao groupDao();

    public abstract NoteDao noteDao();


}
