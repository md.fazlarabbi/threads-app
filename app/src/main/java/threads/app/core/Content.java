package threads.app.core;

import java.util.Hashtable;

public class Content extends Hashtable<String, String> {
    public static final String EST = "est";
    public static final String DATE = "date";
    public static final String ALIAS = "alias";
    public static final String PKEY = "pkey";
    public static final String SKEY = "skey";
    public static final String PID = "pid";
    public static final String UUID = "uuid";
    public static final String CID = "cid";
    public static final String FCM = "fcm";
    public static final String MIME_TYPE = "type";
    public static final String FILENAME = "fn";
    public static final String FILESIZE = "fs";
    public static final String NAME = "name";
    public static final String IDX = "idx";
    public static final String TASK = "notify";
    public static final String HOST = "host";
    public static final String PORT = "port";
    public static final String INET6 = "inet6";
    public static final String URI = "uri";
    public static final String SYNOPSIS = "synopsis";
    public static final String IDXS = "idxs";
    public static final String TIME = "time";
    public static final String IMG = "img";
    public static final String CHECK = "check";
}
