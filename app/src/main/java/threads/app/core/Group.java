package threads.app.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Objects;

import threads.app.utils.MimeType;
import threads.ipfs.CID;
import threads.ipfs.PID;

@androidx.room.Entity
public class Group {

    @NonNull
    @ColumnInfo(name = "uuid")
    private final String uuid;
    @NonNull
    @TypeConverters(Converter.class)
    @ColumnInfo(name = "owner")
    private final PID owner;
    @NonNull
    @ColumnInfo(name = "sesKey")
    private final String sesKey;
    @ColumnInfo(name = "date")
    private long date;
    @PrimaryKey(autoGenerate = true)
    private long idx;
    @Nullable
    @ColumnInfo(name = "image")
    @TypeConverters(Converter.class)
    private CID image;
    @ColumnInfo(name = "number")
    private int number = 0;
    @NonNull
    @TypeConverters(Members.class)
    @ColumnInfo(name = "members")
    private Members members = new Members();

    @ColumnInfo(name = "request")
    private boolean request;

    @ColumnInfo(name = "deleting")
    private boolean deleting;

    @NonNull
    @ColumnInfo(name = "mimeType")
    private String mimeType;
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    @NonNull
    @ColumnInfo(name = "content")
    private String content;

    Group(@NonNull String uuid,
          @NonNull PID owner,
          @NonNull String sesKey,
          long date) {
        this.uuid = uuid;
        this.owner = owner;
        this.sesKey = sesKey;
        this.date = date;
        this.mimeType = MimeType.PLAIN_MIME_TYPE;
        this.request = false;
        this.deleting = false;
        this.name = "";
        this.content = "";
    }

    static Group createGroup(@NonNull String uuid, @NonNull PID owner,
                             @NonNull String sesKey, long date) {

        return new Group(uuid, owner, sesKey, date);
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }


    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isRequest() {
        return request;
    }

    public void setRequest(boolean request) {
        this.request = request;
    }


    public long getIdx() {
        return idx;
    }

    void setIdx(long idx) {
        this.idx = idx;
    }

    @NonNull
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(@NonNull String mimeType) {
        this.mimeType = mimeType;
    }

    @NonNull
    public PID getOwner() {
        return owner;
    }

    @NonNull
    public Members getMembers() {
        return members;
    }

    public void setMembers(@NonNull Members members) {
        this.members = members;
    }

    public boolean addMember(@NonNull String pid) {

        return this.members.add(pid);
    }

    public boolean removeMember(@NonNull String pid) {

        return this.members.remove(pid);
    }


    public boolean sameContent(@NonNull Group o) {

        if (this == o) return true;
        return number == o.getNumber() &&
                deleting == o.isDeleting() &&
                request == o.isRequest() &&
                Objects.equals(name, o.getName()) &&
                Objects.equals(content, o.getContent()) &&
                Objects.equals(uuid, o.getUuid()) &&
                Objects.equals(image, o.getImage()) &&
                Objects.equals(date, o.getDate());
    }

    @Nullable
    public CID getImage() {
        return image;
    }

    public void setImage(@Nullable CID image) {
        this.image = image;
    }

    public boolean areItemsTheSame(@NonNull Group group) {

        return idx == group.getIdx();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return getIdx() == group.getIdx();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdx());
    }


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @NonNull
    public String getSesKey() {
        return sesKey;
    }

    @NonNull
    public String getUuid() {
        return uuid;
    }

    public boolean isDeleting() {
        return deleting;
    }

    public void setDeleting(boolean deleting) {
        this.deleting = deleting;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }

    public boolean hasMember(@NonNull String pid) {

        return members.contains(pid);
    }

    public boolean hasMembers() {
        return !members.isEmpty();
    }

    public boolean isEncrypted() {
        return !getSesKey().isEmpty();
    }

    public void removeMembers() {
        members.clear();
    }
}
