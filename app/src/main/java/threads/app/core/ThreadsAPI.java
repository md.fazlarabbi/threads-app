package threads.app.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import threads.app.utils.MimeType;
import threads.ipfs.CID;
import threads.ipfs.PID;


public class ThreadsAPI {

    private final ThreadsDatabase threadsDatabase;

    ThreadsAPI(@NonNull ThreadsDatabase threadsDatabase) {

        this.threadsDatabase = threadsDatabase;
    }

    @NonNull
    public ThreadsDatabase getThreadsDatabase() {
        return threadsDatabase;
    }


    public void setNotePublished(long idx) {
        getThreadsDatabase().noteDao().setPublished(idx);
    }

    public void setNoteProgress(long idx, int progress) {
        getThreadsDatabase().noteDao().setProgress(idx, progress);
    }


    public void resetGroupsDeleting(long... idxs) {
        getThreadsDatabase().groupDao().resetGroupsDeleting(idxs);
    }


    public void setNoteOwnerAlias(@NonNull PID pid, @NonNull String alias) {

        getThreadsDatabase().noteDao().setOwnerAlias(pid, alias);
    }

    public void setGroupMimeType(long idx, @NonNull String mimeType) {

        getThreadsDatabase().groupDao().setMimeType(idx, mimeType);
    }

    public void setGroupContent(long idx, @NonNull String content) {

        getThreadsDatabase().groupDao().setContent(idx, content);
    }


    @NonNull
    public List<Note> getNotes(@NonNull Group group) {

        return getNotesByGroup(group.getIdx());
    }

    @NonNull
    private List<Note> getNotesByGroup(long group) {
        return getThreadsDatabase().noteDao().getNotesByGroup(group);
    }

    public boolean isUserBlocked(@NonNull PID user) {

        return isUserBlocked(user.getPid());
    }

    public boolean isUserBlocked(@NonNull String pid) {

        return getThreadsDatabase().userDao().isBlocked(pid);
    }

    public void setUserValid(@NonNull String pid) {
        getThreadsDatabase().userDao().setValid(pid);
    }

    public boolean isUserValid(@NonNull String pid) {

        return getThreadsDatabase().userDao().isValid(pid);
    }


    @NonNull
    public Group createGroup(@NonNull String address, @NonNull PID owner, @NonNull String sesKey) {

        return createGroup(address, owner, sesKey, new Date());
    }

    @NonNull
    public Note createMessageNote(@NonNull PID sender,
                                  @NonNull String alias,
                                  @NonNull Group group,
                                  @NonNull CID cid) {

        return createNote(
                group.getIdx(),
                sender,
                alias,
                NoteType.MESSAGE,
                cid,
                MimeType.PLAIN_MIME_TYPE,
                new Date());

    }


    @NonNull
    public Note createLinkNote(@NonNull PID sender,
                               @NonNull String alias,
                               @NonNull Group group,
                               @Nullable CID cid,
                               @NonNull String fileName,
                               @NonNull String mimeType,
                               long fileSize) {

        Note note = createNote(
                group.getIdx(),
                sender,
                alias,
                NoteType.LINK,
                cid,
                mimeType,
                new Date());
        note.setName(fileName);
        note.setSize(fileSize);

        return note;
    }


    @NonNull
    public Note createDataNote(long groupIdx,
                               @NonNull PID sender,
                               @NonNull String alias,
                               @NonNull NoteType noteType,
                               @NonNull String mimeType,
                               @Nullable CID cid) {


        return createNote(groupIdx, sender, alias,
                noteType, cid, mimeType, new Date());

    }


    @NonNull
    public Note createAudioNote(long groupIdx, @NonNull PID owner, @NonNull String alias,
                                @NonNull String mimeType, @NonNull String name,
                                @NonNull CID audio, long size) {

        Note note = createNote(groupIdx, owner, alias,
                NoteType.AUDIO, audio, mimeType, new Date());
        note.setSize(size);
        note.setName(name);
        note.setText(name);
        return note;
    }


    @NonNull
    public Note createInfoNote(long groupIdx, @NonNull PID owner, @NonNull String alias,
                               @NonNull String info, @NonNull Date date) {

        Note note = createNote(
                groupIdx,
                owner,
                alias,
                NoteType.INFO,
                null,
                MimeType.PLAIN_MIME_TYPE,
                date);
        note.setText(info);
        return note;
    }


    public void storeUser(@NonNull User user) {

        getThreadsDatabase().userDao().insertUsers(user);
    }


    public void incrementRating(String pid) {
        getThreadsDatabase().userDao().incrementRating(pid);
    }


    public void decrementRating(String pid) {
        getThreadsDatabase().userDao().decrementRating(pid);
    }


    @NonNull
    public User createUser(@NonNull PID pid,
                           @Nullable String publicKey,
                           @NonNull String name,
                           @Nullable CID image) {

        return User.createUser(name, publicKey, pid, image);
    }


    public boolean isReferenced(@NonNull CID cid) {

        int counter = getThreadsDatabase().groupDao().references(cid);
        counter += getThreadsDatabase().noteDao().references(cid);
        return counter > 0;
    }


    public List<Note> getExpiredNotes() {
        return getThreadsDatabase().noteDao().getExpiredNotes(System.currentTimeMillis());
    }


    public void resetGroupsNumber(long idx) {
        getThreadsDatabase().groupDao().resetNumber(idx);
    }

    @Nullable
    public User getUserByPid(@NonNull String pid) {

        return getThreadsDatabase().userDao().getUserByPid(pid);
    }

    @Nullable
    public String getUserAddress(@NonNull String pid) {
        return getThreadsDatabase().userDao().getAddress(pid);
    }

    public void setUserAddress(@NonNull String pid, @NonNull String address) {
        getThreadsDatabase().userDao().setAddress(pid, address);
    }


    public void setGroupDate(long idx, @NonNull Date date) {

        getThreadsDatabase().groupDao().setDate(idx, date.getTime());
    }

    public long storeGroup(@NonNull Group group) {

        return getThreadsDatabase().groupDao().insertGroup(group);
    }


    public long storeNote(@NonNull Note note) {

        return getThreadsDatabase().noteDao().insertNote(note);
    }


    public void setGroupDeleting(@NonNull Group group) {
        getThreadsDatabase().noteDao().setGroupDeleting(group.getIdx());
        getThreadsDatabase().groupDao().setDeleting(group.getIdx());
    }

    public void setNoteContent(long idx, @NonNull CID cid) {

        getThreadsDatabase().noteDao().setContent(idx, cid);

    }


    public void setUserConnected(@NonNull String pid, boolean connected) {

        getThreadsDatabase().userDao().setConnected(pid, connected);

        if (connected) {
            getThreadsDatabase().userDao().setTimestamp(pid, System.currentTimeMillis());
        }
    }


    public void setUserDialing(@NonNull String pid, boolean dialing) {

        getThreadsDatabase().userDao().setDialing(pid, dialing);
    }

    public void setUserBlocked(@NonNull String pid, boolean blocked) {

        getThreadsDatabase().userDao().setBlocked(pid, blocked);
    }

    public void setUserVisible(@NonNull String pid, boolean visible) {

        getThreadsDatabase().userDao().setVisible(pid, visible);
    }


// --Commented out by Inspection START (4/28/2020 7:48 PM):
//    public void setUserAlias(@NonNull String pid, @NonNull String alias) {
//
//        getThreadsDatabase().userDao().setAlias(pid, alias);
//    }
// --Commented out by Inspection STOP (4/28/2020 7:48 PM)


    public boolean getUserDialing(@NonNull String pid) {

        return getThreadsDatabase().userDao().getUserDialing(pid);
    }

    public boolean isUserConnected(@NonNull String pid) {

        return getThreadsDatabase().userDao().isConnected(pid);
    }


    public String getUserAlias(@NonNull String pid) {

        return getThreadsDatabase().userDao().getAlias(pid);
    }

    public void updateGroup(@NonNull Group group) {

        getThreadsDatabase().groupDao().updateGroups(group);
    }


    @Nullable
    public Group getGroupByIdx(long idx) {
        return getThreadsDatabase().groupDao().getGroupByIdx(idx);
    }

    @Nullable
    public Group getGroup(@NonNull Note note) {

        return getGroupByIdx(note.getGroup());
    }

    @Nullable
    public Note getNoteByIdx(long idx) {
        return getThreadsDatabase().noteDao().getNoteByIdx(idx);
    }


    public void blockUser(@NonNull PID user) {

        getThreadsDatabase().userDao().setBlocked(user.getPid(), true);
    }

    public void unblockUser(@NonNull PID user) {

        getThreadsDatabase().userDao().setBlocked(user.getPid(), false);
    }

    private Note createNote(long group,
                            @NonNull PID owner,
                            @NonNull String senderAuthor,
                            @NonNull NoteType noteType,
                            @Nullable CID cid,
                            @NonNull String mimeType,
                            @NonNull Date date) {
        Note note = Note.createNote(group, owner, senderAuthor,
                noteType, mimeType, date.getTime());
        note.setContent(cid);

        return note;
    }


    @NonNull
    private Group createGroup(@NonNull String uuid,
                              @NonNull PID owner,
                              @NonNull String sesKey,
                              @NonNull Date date) {
        return Group.createGroup(uuid, owner, sesKey, date.getTime());
    }


    public List<Note> getNewestSeedingNotes(int limit, @NonNull NoteType type) {
        return getThreadsDatabase().noteDao().getNewestSeedingNotes(limit, type);
    }


    public List<Note> getSeedingNotes(@NonNull NoteType type) {
        return getThreadsDatabase().noteDao().getSeedingNotes(type);
    }


    public List<Note> getSeedingNotesByQuery(String query, @NonNull NoteType type) {


        String searchQuery = query.trim();
        if (!searchQuery.startsWith("%")) {
            searchQuery = "%" + searchQuery;
        }
        if (!searchQuery.endsWith("%")) {
            searchQuery = searchQuery + "%";
        }
        return getThreadsDatabase().noteDao().getSeedingNotesByQuery(searchQuery, type);
    }

    @NonNull
    public List<Note> getNotesByContent(@NonNull CID cid) {

        return getThreadsDatabase().noteDao().getNotesByContent(cid);
    }


    @NonNull
    public List<String> getUsersPids() {
        return getThreadsDatabase().userDao().getUserPids();
    }

    @NonNull
    public List<Group> getGroupsByUuid(@NonNull String uuid) {

        return getThreadsDatabase().groupDao().getGroupsByUuid(uuid);
    }


    public void setNoteThumbnail(long idx, @NonNull CID image) {

        getThreadsDatabase().noteDao().setThumbnail(idx, image);
    }


    public void setNoteSeeding(long idx) {
        getThreadsDatabase().noteDao().setSeeding(idx);
    }

    public void setNoteLeaching(long idx) {
        getThreadsDatabase().noteDao().setLeaching(idx);
    }

    public void setNoteError(long idx) {
        getThreadsDatabase().noteDao().setError(idx);
    }

    public void resetNoteLeaching(long idx) {
        getThreadsDatabase().noteDao().resetLeaching(idx);
    }

    public void setNoteSize(long idx, long size) {
        getThreadsDatabase().noteDao().setSize(idx, size);
    }

    public String getNoteMimeType(long idx) {
        return getThreadsDatabase().noteDao().getMimeType(idx);
    }


    public String getNoteText(long idx) {
        return getThreadsDatabase().noteDao().getText(idx);
    }

    public void setNoteText(long idx, String text) {

        getThreadsDatabase().noteDao().setText(idx, text);
    }


// --Commented out by Inspection START (4/28/2020 7:48 PM):
//    public void setNoteName(long idx, @NonNull String name) {
//
//        getThreadsDatabase().noteDao().setName(idx, name);
//    }
// --Commented out by Inspection STOP (4/28/2020 7:48 PM)

// --Commented out by Inspection START (4/28/2020 7:43 PM):
//    public void setNoteMimeType(long idx, @NonNull String mimeType) {
//
//        getThreadsDatabase().noteDao().setMimeType(idx, mimeType);
//    }
// --Commented out by Inspection STOP (4/28/2020 7:43 PM)

    public void removeNote(long idx) {
        getThreadsDatabase().noteDao().removeNoteByIdx(idx);
    }

    public void setGroupImage(long idx, @NonNull CID image) {

        getThreadsDatabase().groupDao().setImage(idx, image);
    }

    public List<Group> getGroups() {
        return getThreadsDatabase().groupDao().getGroups();
    }

    public boolean isNoteLeaching(long idx) {
        return getThreadsDatabase().noteDao().isLeaching(idx);
    }

    public void resetNoteUri(long idx) {
        getThreadsDatabase().noteDao().resetUri(idx);
    }

    public void setNoteWork(long idx, UUID uuid) {
        getThreadsDatabase().noteDao().setWork(idx, uuid.toString());
    }

    public void resetNoteWork(long idx) {
        getThreadsDatabase().noteDao().resetWork(idx);
    }

    public boolean isNoteInit(long idx) {
        return getThreadsDatabase().noteDao().isInit(idx);
    }

    public void resetNoteInit(long idx) {
        getThreadsDatabase().noteDao().resetInit(idx);
    }

    public List<Group> getDeletedGroups() {
        return getThreadsDatabase().groupDao().getDeletedGroups();
    }

    public void removeGroup(@NonNull Group group) {
        getThreadsDatabase().groupDao().removeGroup(group);
    }

    public void setNoteDeleting(long idx) {
        getThreadsDatabase().noteDao().setDeleting(idx);
    }

    public List<Note> getDeletedNotes() {
        return getThreadsDatabase().noteDao().getDeletedNotes();
    }

    public void setGroupsDeleting(long[] idxs) {
        getThreadsDatabase().groupDao().setGroupsDeleting(idxs);
    }

    public List<Note> getNonSeedingNotesByOwner(@NonNull PID owner) {
        return getThreadsDatabase().noteDao().getNonSeedingNotesByOwner(owner);
    }

    public String getGroupSesKey(long idx) {
        return getThreadsDatabase().groupDao().getSesKey(idx);
    }

    public PID getGroupOwner(long idx) {
        return getThreadsDatabase().groupDao().getOwner(idx);
    }


    @Nullable
    public String getUserPublicKey(@NonNull String pid) {

        return getThreadsDatabase().userDao().getPublicKey(pid);
    }


    @Nullable
    public String getUserAgent(@NonNull String pid) {

        return getThreadsDatabase().userDao().getAgent(pid);
    }

    public void setUserPublicKey(@NonNull String pid, @NonNull String pkey) {
        getThreadsDatabase().userDao().setPublicKey(pid, pkey);
    }

    public void setUserAlias(@NonNull String pid, @NonNull String alias) {
        getThreadsDatabase().userDao().setAlias(pid, alias);
    }

    public void setUserToken(@NonNull String pid, @NonNull String token) {
        getThreadsDatabase().userDao().setToken(pid, token);
    }

    public void setUserImage(@NonNull String pid, @NonNull CID image) {
        getThreadsDatabase().userDao().setImage(pid, image);
    }

    public void setUserAgent(@NonNull String pid, @NonNull String agent) {
        getThreadsDatabase().userDao().setAgent(pid, agent);
    }

    @Nullable
    public CID getUserImage(@NonNull String pid) {
        return getThreadsDatabase().userDao().getImage(pid);
    }

    @Nullable
    private String getUserToken(@NonNull String pid) {
        return getThreadsDatabase().userDao().getToken(pid);
    }

    public boolean hasUserToken(@NonNull String pid) {
        return getUserToken(pid) != null;
    }

    public boolean hasUserPublicKey(@NonNull String pid) {
        return getUserPublicKey(pid) != null;
    }

    public void setUserTimestamp(@NonNull String pid, long timestamp) {
        getThreadsDatabase().userDao().setTimestamp(pid, timestamp);
    }

    public int getNumberNotes(@NonNull Group group) {
        return getThreadsDatabase().noteDao().getNotesCountByGroup(group.getIdx());
    }

    public boolean hasNotes(@NonNull Group group) {
        return getNumberNotes(group) > 0;
    }

    public void resetGroupRequest(long idx) {
        getThreadsDatabase().groupDao().resetGroupRequest(idx);
    }

    public void resetGroupUnread(long group) {
        getThreadsDatabase().noteDao().resetGroupUnread(group);
    }

    public void setGroupRequest(long idx) {
        getThreadsDatabase().groupDao().setGroupRequest(idx);
    }

    public PID getNoteOwner(long idx) {
        return getThreadsDatabase().noteDao().getOwner(idx);
    }
}
