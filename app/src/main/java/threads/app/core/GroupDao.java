package threads.app.core;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import java.util.List;

import threads.ipfs.CID;
import threads.ipfs.PID;

@Dao
public interface GroupDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertGroup(Group group);

    @Query("SELECT * FROM `Group`")
    LiveData<List<Group>> getLiveDataGroups();


    @Query("UPDATE `Group` SET deleting = 1  WHERE idx = :idx")
    void setDeleting(long idx);


    @Query("UPDATE `Group` SET date = :date  WHERE idx = :idx")
    void setDate(long idx, long date);

    @Query("SELECT * FROM `Group` WHERE uuid = :uuid")
    List<Group> getGroupsByUuid(String uuid);

    @Delete
    void removeGroup(Group group);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateGroups(Group... groups);

    @Query("UPDATE `Group` SET number = 0 WHERE idx = :idx")
    void resetNumber(long idx);

    @Query("SELECT COUNT(idx) FROM `Group` WHERE image =:cid")
    @TypeConverters({Converter.class})
    int references(CID cid);

    @Query("SELECT * FROM `Group` WHERE idx =:idx")
    Group getGroupByIdx(long idx);

    @Query("UPDATE `Group` SET mimeType =:mimeType  WHERE idx = :idx")
    void setMimeType(long idx, String mimeType);


    @Query("UPDATE `Group` SET content =:content  WHERE idx = :idx")
    void setContent(long idx, String content);

    @Query("UPDATE `Group` SET image = :image WHERE idx = :idx")
    @TypeConverters({Converter.class})
    void setImage(long idx, CID image);


    @Query("SELECT * FROM `Group`")
    List<Group> getGroups();

    @Query("SELECT * FROM `Group` WHERE deleting = 1")
    List<Group> getDeletedGroups();


    @Query("UPDATE `Group` SET deleting = 0 WHERE idx IN (:idxs)")
    void resetGroupsDeleting(long[] idxs);

    @Query("UPDATE `Group` SET deleting = 1 WHERE idx IN (:idxs)")
    void setGroupsDeleting(long[] idxs);

    @Query("SELECT sesKey FROM `Group` WHERE idx =:idx")
    String getSesKey(long idx);


    @Query("SELECT owner FROM `Group` WHERE idx =:idx")
    @TypeConverters({Converter.class})
    PID getOwner(long idx);

    @Query("UPDATE `Group` SET request = 0 WHERE idx = :idx")
    void resetGroupRequest(long idx);


    @Query("UPDATE `Group` SET request = 1 WHERE idx = :idx")
    void setGroupRequest(long idx);
}
