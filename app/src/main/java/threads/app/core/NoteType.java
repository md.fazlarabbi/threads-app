package threads.app.core;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

public enum NoteType {
    MESSAGE(1), INFO(2), VIDEO(3), IMAGE(4), AUDIO(5), LINK(6);

    @NonNull
    private final Integer code;

    NoteType(@NonNull Integer code) {
        this.code = code;
    }


    @TypeConverter
    public static NoteType toNoteType(@NonNull Integer type) {

        if (type.equals(NoteType.MESSAGE.getCode())) {
            return NoteType.MESSAGE;
        } else if (type.equals(NoteType.INFO.getCode())) {
            return NoteType.INFO;
        } else if (type.equals(NoteType.VIDEO.getCode())) {
            return NoteType.VIDEO;
        } else if (type.equals(NoteType.AUDIO.getCode())) {
            return NoteType.AUDIO;
        } else if (type.equals(NoteType.IMAGE.getCode())) {
            return NoteType.IMAGE;
        } else if (type.equals(NoteType.LINK.getCode())) {
            return NoteType.LINK;
        } else {
            throw new IllegalArgumentException("Could not recognize type");
        }
    }

    @TypeConverter
    public static Integer toInteger(@NonNull NoteType type) {

        return type.getCode();
    }

    @NonNull
    public Integer getCode() {
        return code;
    }

}
