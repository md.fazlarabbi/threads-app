package threads.app.core;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import java.util.List;

import threads.ipfs.CID;
import threads.ipfs.PID;

@Dao
public interface NoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertNote(Note note);

    @Query("UPDATE Note SET content = :cid WHERE idx = :idx")
    @TypeConverters({Converter.class})
    void setContent(long idx, CID cid);

    @Query("UPDATE Note SET alias = :alias  WHERE owner = :owner")
    @TypeConverters(Converter.class)
    void setOwnerAlias(PID owner, String alias);

// --Commented out by Inspection START (4/28/2020 7:46 PM):
//    @Query("UPDATE Note SET mimeType =:mimeType  WHERE idx = :idx")
//    void setMimeType(long idx, String mimeType);
// --Commented out by Inspection STOP (4/28/2020 7:46 PM)

    @Query("UPDATE Note SET uri = null  WHERE idx = :idx")
    void resetUri(long idx);

    @Query("SELECT * FROM Note WHERE content = :cid")
    @TypeConverters({Converter.class})
    List<Note> getNotesByContent(CID cid);

    @Query("Delete FROM Note WHERE idx =:idx")
    void removeNoteByIdx(long idx);

    @Query("SELECT * FROM Note WHERE idx =:idx")
    Note getNoteByIdx(long idx);

    @Query("SELECT * FROM Note WHERE `group` =:group")
    List<Note> getNotesByGroup(long group);

    @Query("SELECT COUNT(idx) FROM Note WHERE `group` =:group")
    int getNotesCountByGroup(long group);

    @Query("SELECT * FROM Note WHERE `group` =:group AND deleting = 0")
    LiveData<List<Note>> getLiveDataNotesByGroup(long group);

    @Query("SELECT mimeType FROM Note WHERE idx = :idx")
    String getMimeType(long idx);

    @Query("SELECT text FROM Note WHERE idx = :idx")
    String getText(long idx);

    @Query("SELECT * FROM Note WHERE expire < :date")
    List<Note> getExpiredNotes(long date);

    @Query("SELECT COUNT(idx) FROM Note WHERE content =:cid OR thumbnail =:cid")
    @TypeConverters({Converter.class})
    int references(CID cid);

    @Query("UPDATE Note SET thumbnail = :thumbnail WHERE idx = :idx")
    @TypeConverters({Converter.class})
    void setThumbnail(long idx, CID thumbnail);

    @Query("UPDATE Note SET deleting = 1 WHERE `group` =:group")
    void setGroupDeleting(long group);

    @Query("UPDATE Note SET seeding = 1, leaching = 0, progress = 0  WHERE idx = :idx")
    void setSeeding(long idx);

    @Query("UPDATE Note SET leaching = 1, progress = 0  WHERE idx = :idx")
    void setLeaching(long idx);

    @Query("UPDATE Note SET leaching = 0, progress = 0  WHERE idx = :idx")
    void resetLeaching(long idx);

    @Query("UPDATE Note SET error = 1 WHERE idx = :idx")
    void setError(long idx);

    @Query("UPDATE Note SET published = 1, progress = 0  WHERE idx = :idx")
    void setPublished(long idx);

    @Query("UPDATE Note SET progress = :progress WHERE idx = :idx")
    void setProgress(long idx, int progress);

    @Query("UPDATE Note SET size = :size WHERE idx = :idx")
    void setSize(long idx, long size);

// --Commented out by Inspection START (4/28/2020 7:49 PM):
//    @Query("UPDATE Note SET name = :name WHERE idx = :idx")
//    void setName(long idx, String name);
// --Commented out by Inspection STOP (4/28/2020 7:49 PM)

    @Query("SELECT * FROM Note WHERE seeding = 1 AND deleting = 0 AND noteType =:type ORDER BY date DESC LIMIT :limit")
    @TypeConverters({NoteType.class})
    List<Note> getNewestSeedingNotes(int limit, NoteType type);

    @Query("SELECT * FROM Note WHERE seeding = 1 AND deleting = 0 AND noteType =:type AND name LIKE :query")
    @TypeConverters({NoteType.class})
    List<Note> getSeedingNotesByQuery(String query, NoteType type);

    @Query("SELECT * FROM Note WHERE seeding = 1 AND deleting = 0 AND noteType =:type")
    @TypeConverters({NoteType.class})
    List<Note> getSeedingNotes(NoteType type);

    @Query("UPDATE Note SET text = :text WHERE idx = :idx")
    void setText(long idx, String text);

    @Query("SELECT leaching FROM Note WHERE idx =:idx")
    boolean isLeaching(long idx);

    @Query("UPDATE Note SET work = :work WHERE idx = :idx")
    void setWork(long idx, String work);

    @Query("UPDATE Note SET work = null WHERE idx = :idx")
    void resetWork(long idx);

    @Query("SELECT init FROM Note WHERE idx =:idx")
    boolean isInit(long idx);

    @Query("UPDATE Note SET init = 0 WHERE idx = :idx")
    void resetInit(long idx);

    @Query("UPDATE Note SET deleting = 1 WHERE idx = :idx")
    void setDeleting(long idx);

    @Query("SELECT * FROM Note WHERE deleting = 1")
    List<Note> getDeletedNotes();

    @Query("SELECT * FROM Note WHERE owner =:owner AND deleting = 0 AND seeding = 0")
    @TypeConverters(Converter.class)
    List<Note> getNonSeedingNotesByOwner(PID owner);

    @Query("UPDATE Note SET unread = 0 WHERE `group` = :group")
    void resetGroupUnread(long group);

    @Query("SELECT owner FROM Note WHERE idx =:idx")
    @TypeConverters(Converter.class)
    PID getOwner(long idx);
}
