package threads.app.core;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Event.class}, version = 38, exportSchema = false)
public abstract class EventsDatabase extends RoomDatabase {

    public abstract EventDao eventDao();
}
