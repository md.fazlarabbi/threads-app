package threads.app.core;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import java.util.List;

import threads.ipfs.CID;

@Dao
public interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUsers(User... users);

    @Query("SELECT pid FROM User")
    List<String> getUserPids();

    @Query("SELECT alias FROM User WHERE pid = :pid ")
    String getAlias(String pid);

    @Query("SELECT * FROM User WHERE pid = :pid")
    User getUserByPid(String pid);


    @Query("SELECT * FROM User WHERE visible = 1")
    LiveData<List<User>> getLiveDataUsers();

    @Query("SELECT * FROM User WHERE valid = 1 AND visible = 1")
    LiveData<List<User>> getLiveDataLiteUsers();

    @Query("UPDATE User SET dialing = :dialing WHERE pid = :pid")
    void setDialing(String pid, boolean dialing);

    @Query("UPDATE User SET valid = 1 WHERE pid = :pid")
    void setValid(String pid);

    @Query("SELECT valid FROM User WHERE pid = :pid")
    boolean isValid(String pid);

    @Query("UPDATE User SET visible = :visible WHERE pid = :pid")
    void setVisible(String pid, boolean visible);


    @Query("SELECT dialing FROM User WHERE pid = :pid")
    boolean getUserDialing(String pid);

    @Query("UPDATE User SET connected = :connected WHERE pid = :pid")
    void setConnected(String pid, boolean connected);

    @Query("SELECT connected FROM User WHERE pid = :pid ")
    boolean isConnected(String pid);

    @Query("UPDATE User SET blocked = :blocked WHERE pid = :pid")
    void setBlocked(String pid, boolean blocked);

    @Query("SELECT blocked FROM User WHERE pid = :pid ")
    boolean isBlocked(String pid);

    @Query("UPDATE User SET rating = rating + 1  WHERE pid = :pid")
    void incrementRating(String pid);

    @Query("UPDATE User SET rating = rating - 1  WHERE pid = :pid")
    void decrementRating(String pid);

    @Query("SELECT publicKey FROM User WHERE pid = :pid")
    String getPublicKey(String pid);

    @Query("SELECT agent FROM User WHERE pid = :pid")
    String getAgent(String pid);

    @Query("UPDATE User SET publicKey = :pkey WHERE pid = :pid")
    void setPublicKey(String pid, String pkey);


    @Query("UPDATE User SET agent = :agent WHERE pid = :pid")
    void setAgent(String pid, String agent);

    @Query("UPDATE User SET alias = :alias WHERE pid = :pid")
    void setAlias(String pid, String alias);

    @Query("UPDATE User SET token = :token WHERE pid = :pid")
    void setToken(String pid, String token);

    @Query("UPDATE User SET image = :image WHERE pid = :pid")
    @TypeConverters({Converter.class})
    void setImage(String pid, CID image);

    @Query("SELECT address FROM User WHERE pid = :pid")
    String getAddress(String pid);

    @Query("UPDATE User SET address = :address WHERE pid = :pid")
    void setAddress(String pid, String address);

    @Query("SELECT image FROM User WHERE pid = :pid")
    @TypeConverters({Converter.class})
    CID getImage(String pid);

    @Query("SELECT token FROM User WHERE pid = :pid")
    String getToken(String pid);

    @Query("UPDATE User SET timestamp = :timestamp WHERE pid = :pid")
    void setTimestamp(String pid, long timestamp);
}
