package threads.app;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import threads.app.core.Content;


public class ImageActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image);


        Intent intent = getIntent();

        String uri = intent.getStringExtra(Content.URI);

        ImageView image = findViewById(R.id.image);

        Glide.with(getApplicationContext()).
                load(Uri.parse(uri)).
                into(image);

    }

}
