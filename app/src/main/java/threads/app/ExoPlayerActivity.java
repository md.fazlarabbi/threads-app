package threads.app;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;

import java.io.IOException;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.utils.ContentDataSource;

import static com.google.android.exoplayer2.util.RepeatModeUtil.REPEAT_TOGGLE_MODE_NONE;


public class ExoPlayerActivity extends Activity {

    private static final String TAG = ExoPlayerActivity.class.getSimpleName();
    private PlayerView mPlayerView;
    private SimpleExoPlayer mPlayer;
    private long playbackPosition;
    private int currentWindow;
    private boolean playWhenReady = true;
    private String uri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exoplayer);

        Intent intent = getIntent();

        uri = intent.getStringExtra(Content.URI);

        mPlayerView = findViewById(R.id.video_preview);


        if (savedInstanceState == null) {
            playWhenReady = true;
            currentWindow = 0;
            playbackPosition = 0;
        } else {
            playWhenReady = savedInstanceState.getBoolean("playWhenReady");
            currentWindow = savedInstanceState.getInt("currentWindow");
            playbackPosition = savedInstanceState.getLong("playBackPosition");
        }

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean("playWhenReady", playWhenReady);
        outState.putInt("currentWindow", currentWindow);
        outState.putLong("playBackPosition", playbackPosition);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onStart() {
        super.onStart();
        initializePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPlayer == null) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
    }

    private void initializePlayer() {
        if (mPlayer == null) {
            mPlayer = new SimpleExoPlayer.Builder(getApplicationContext()).build();
            mPlayerView.setRepeatToggleModes(REPEAT_TOGGLE_MODE_NONE);
            mPlayerView.setPlayer(mPlayer);
            mPlayer.setPlayWhenReady(playWhenReady);
            mPlayer.seekTo(currentWindow, playbackPosition);
            mPlayerView.setUseController(true);
        }

        MediaSource mediaSource = buildMediaSource(Uri.parse(uri));

        mPlayer.prepare(mediaSource, false, false);
    }

    private void releasePlayer() {
        if (mPlayer != null) {
            playbackPosition = mPlayer.getCurrentPosition();
            currentWindow = mPlayer.getCurrentWindowIndex();
            playWhenReady = mPlayer.getPlayWhenReady();
            mPlayer.release();
            mPlayer = null;
        }
    }


    private MediaSource buildMediaSource(@NonNull Uri uri) {


        DataSpec dataSpec = new DataSpec(uri);
        final ContentDataSource fileDataSource = new ContentDataSource(getApplicationContext());
        try {
            fileDataSource.open(dataSpec);
        } catch (IOException e) {
            LogUtils.error(TAG, e);
        }

        DataSource.Factory factory = () -> fileDataSource;

        return new ProgressiveMediaSource.Factory(factory).createMediaSource(uri);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }


}
