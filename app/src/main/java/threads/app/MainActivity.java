package threads.app;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.List;
import java.util.Objects;

import de.psdev.licensesdialog.LicensesDialogFragment;
import threads.LogUtils;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.THREADS;
import threads.app.fragments.FabFragment;
import threads.app.fragments.GroupsFragment;
import threads.app.fragments.LoginDialogFragment;
import threads.app.fragments.NotesFragment;
import threads.app.fragments.SettingsDialogFragment;
import threads.app.fragments.ShowAccountDialogFragment;
import threads.app.fragments.UsersFragment;
import threads.app.fragments.WebViewDialogFragment;
import threads.app.model.EventViewModel;
import threads.app.model.SelectionViewModel;
import threads.app.services.DaemonService;
import threads.app.services.DiscoveryService;
import threads.app.services.GroupDelete;
import threads.app.services.IdentityService;
import threads.app.services.MimeTypeService;
import threads.app.services.MoinService;
import threads.app.services.RegistrationService;
import threads.app.utils.CustomViewPager;
import threads.app.utils.PermissionAction;
import threads.app.work.ConnectionWorker;
import threads.app.work.GroupDeleteWorker;
import threads.app.work.SwarmConnectWorker;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GroupsFragment.ActionListener,
        UsersFragment.ActionListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private NavigationView mNavigationView;
    private BottomNavigationView mNavigation;
    private DrawerLayout mDrawerLayout;
    private CustomViewPager mCustomViewPager;
    private NsdManager mNsdManager;
    private SelectionViewModel mSelectionViewModel;
    private boolean isTablet;
    private Toolbar mToolbar;
    private PID owner;
    private PID host;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notes_activity, menu);

        MenuItem actionInvite = menu.findItem(R.id.action_invite);
        actionInvite.setVisible(Objects.equals(host, owner) && isTablet);

        MenuItem actionMembers = menu.findItem(R.id.action_members);
        actionMembers.setVisible(isTablet);
        return true;
    }


    @Override
    public void setPagingEnabled(boolean enabled) {
        mCustomViewPager.setPagingEnabled(enabled);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.ThreadsTheme);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        isTablet = getResources().getBoolean(R.bool.isTablet);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if (!isTablet) {
            mToolbar.setSubtitle(R.string.sessions);
        }

        if (isTablet) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_notes_view, NotesFragment.newInstance())
                    .commit();
        }


        mSelectionViewModel = new ViewModelProvider(this).get(SelectionViewModel.class);

        mCustomViewPager = findViewById(R.id.customViewPager);
        mNavigation = findViewById(R.id.navigation);
        mNavigation.refreshDrawableState();
        mNavigation.setOnNavigationItemSelectedListener((item) -> {

            switch (item.getItemId()) {
                case R.id.navigation_groups:
                    mCustomViewPager.setCurrentItem(0);
                    if (!isTablet) {
                        mToolbar.setSubtitle(R.string.sessions);
                    }
                    return true;
                case R.id.navigation_friends:
                    mCustomViewPager.setCurrentItem(1);
                    if (!isTablet) {
                        mToolbar.setSubtitle(R.string.friends);
                    }
                    return true;
            }
            return false;

        });


        mCustomViewPager = findViewById(R.id.customViewPager);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        mCustomViewPager.setAdapter(adapter);

        mCustomViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private MenuItem prevMenuItem;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null)
                    prevMenuItem.setChecked(false);
                else
                    mNavigation.getMenu().getItem(0).setChecked(false);

                mNavigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = mNavigation.getMenu().getItem(position);

                switch (prevMenuItem.getItemId()) {
                    case R.id.navigation_groups:
                        if (!isTablet) {
                            mToolbar.setSubtitle(R.string.sessions);
                        }
                        break;
                    case R.id.navigation_friends:
                        if (!isTablet) {
                            mToolbar.setSubtitle(R.string.friends);
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        toggle.setToolbarNavigationClickListener((v) -> onBackPressed());

        handleIntent(getIntent());


        EventViewModel eventViewModel = new ViewModelProvider(this).get(EventViewModel.class);

        eventViewModel.getPermission().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.app_settings, new PermissionAction());
                        snackbar.setAnchorView(mNavigation);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                showFab(true);
                            }
                        });
                        showFab(false);
                        snackbar.show();

                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });


        eventViewModel.getDelete().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Gson gson = new Gson();
                        GroupDelete groupDelete = gson.fromJson(content, GroupDelete.class);


                        List<Long> entries = groupDelete.getGroups();
                        int size = entries.size();
                        long[] idxs = new long[size];
                        for (int i = 0; i < entries.size(); i++) {
                            idxs[i] = entries.get(i);
                        }

                        String message;
                        if (size == 1) {
                            message = getString(R.string.delete_group);
                        } else {
                            message = getString(R.string.delete_groups, "" + size);
                        }

                        Snackbar snackbar = Snackbar.make(mDrawerLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.setAction(getString(R.string.revert_operation), (view) -> {

                            try {
                                THREADS.getInstance(getApplicationContext()).resetGroupsDeleting(idxs);
                            } catch (Throwable e) {
                                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                            } finally {
                                snackbar.dismiss();
                            }

                        });
                        snackbar.setAnchorView(mNavigation);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                GroupDeleteWorker.delete(getApplicationContext(), idxs);
                                showFab(true);
                            }
                        });
                        showFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });

        eventViewModel.getError().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());
                        snackbar.setAnchorView(mNavigation);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                showFab(true);
                            }
                        });
                        showFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });

        eventViewModel.getWarning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Toast.makeText(getApplicationContext(), content, Toast.LENGTH_LONG).show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });


        try {
            host = IPFS.getPID(getApplicationContext());
            Objects.requireNonNull(host);
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            if (!ipfs.isDaemonRunning()) {
                ipfs.setDaemonListener((port) -> {
                    runOnUiThread(this::unregisterLogin);
                    registerService((int) port, host.getPid());
                });
            } else {
                unregisterLogin();
                registerService((int) ipfs.getSwarmPort(), host.getPid());
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        if (!InitApplication.isLogin(getApplicationContext())) {
            IdentityService.identity(getApplicationContext(), false);
        }
    }


    @Override
    public void setBottomNavigationEnabled(boolean enable) {
        for (int i = 0; i < mNavigation.getMenu().size(); i++) {
            mNavigation.getMenu().getItem(i).setEnabled(enable);
        }
    }

    private void unregisterLogin() {
        try {
            mNavigationView.getMenu().findItem(R.id.nav_alias).setVisible(false);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_alias: {
                LoginDialogFragment.newInstance(30).show(
                        getSupportFragmentManager(), LoginDialogFragment.TAG);
                break;
            }
            case R.id.nav_id: {
                try {
                    PID pid = IPFS.getPID(getApplicationContext());
                    Objects.requireNonNull(pid);
                    ShowAccountDialogFragment.newInstance(pid.getPid()).show(
                            getSupportFragmentManager(), ShowAccountDialogFragment.TAG);
                    ConnectionWorker.connect(getApplicationContext());
                } catch (Throwable e) {
                    LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                }
                break;
            }
            case R.id.nav_daemon: {
                DaemonService.invoke(getApplicationContext(), true);
                break;
            }
            case R.id.nav_settings: {
                try {
                    SettingsDialogFragment settingsDialogFragment = new SettingsDialogFragment();
                    settingsDialogFragment.show(getSupportFragmentManager(), SettingsDialogFragment.TAG);
                } catch (Throwable e) {
                    LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                }
                break;
            }
            case R.id.nav_licences: {
                try {
                    LicensesDialogFragment fragment = new LicensesDialogFragment.Builder(this)
                            .setNotices(R.raw.licenses)
                            .setShowFullLicenseText(false)
                            .setIncludeOwnLicense(true)
                            .build();

                    fragment.show(getSupportFragmentManager(), null);


                } catch (Throwable e) {
                    LogUtils.error(TAG, e);
                }
                break;
            }
            case R.id.nav_privacy_policy: {

                try {
                    String data;
                    if (MoinService.isNightNode(getApplicationContext())) {
                        data = MoinService.loadRawData(getApplicationContext(),
                                R.raw.privacy_policy_night);
                    } else {
                        data = MoinService.loadRawData(getApplicationContext(),
                                R.raw.privacy_policy);
                    }

                    WebViewDialogFragment.newInstance(data)
                            .show(getSupportFragmentManager(), WebViewDialogFragment.TAG);


                } catch (Throwable e) {
                    LogUtils.error(TAG, e);
                }
                break;
            }
            case R.id.nav_issues: {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://gitlab.com/remmer.wilts/threads-app/issues"));
                    startActivity(intent);
                } catch (Throwable e) {
                    LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                }
                break;
            }
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mNsdManager != null) {
                mNsdManager.unregisterService(RegistrationService.getInstance());
                mNsdManager.stopServiceDiscovery(DiscoveryService.getInstance());
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    private void registerService(int port, String serviceName) {
        try {
            String serviceType = "_ipfs-discovery._udp";
            NsdServiceInfo serviceInfo = new NsdServiceInfo();
            serviceInfo.setServiceName(serviceName);
            serviceInfo.setServiceType(serviceType);
            serviceInfo.setPort(port);
            mNsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
            Objects.requireNonNull(mNsdManager);
            mNsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD,
                    RegistrationService.getInstance());

            PID host = IPFS.getPID(getApplicationContext());

            DiscoveryService discovery = DiscoveryService.getInstance();
            discovery.setOnServiceFoundListener((info) -> mNsdManager.resolveService(info, new NsdManager.ResolveListener() {

                @Override
                public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                    LogUtils.error(TAG, "onResolveFailed : " + errorCode);
                }


                @Override
                public void onServiceResolved(NsdServiceInfo serviceInfo) {

                    try {
                        String serviceName = serviceInfo.getServiceName();
                        boolean connect = true;
                        if (host != null) {
                            connect = !Objects.equals(host.getPid(), serviceName);
                        }
                        if (connect) {
                            InetAddress inetAddress = serviceInfo.getHost();
                            SwarmConnectWorker.connect(getApplicationContext(),
                                    serviceName, serviceInfo.getHost().toString(),
                                    serviceInfo.getPort(), inetAddress instanceof Inet6Address);
                        }
                    } catch (Throwable e) {
                        LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                    }
                }
            }));
            mNsdManager.discoverServices(serviceType, NsdManager.PROTOCOL_DNS_SD, discovery);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doSearch(query);
        }
    }

    private void doSearch(String queryStr) {
        Toast.makeText(getApplicationContext(), queryStr, Toast.LENGTH_LONG).show();
    }


    @Override
    public void groupSelected(@NonNull Group group) {

        owner = group.getOwner();
        mSelectionViewModel.setGroup(group.getIdx());

        if (!isTablet) {
            Intent intent = new Intent(this, NotesActivity.class);
            intent.putExtra(Content.IDX, group.getIdx());
            intent.putExtra(Content.NAME, group.getName());
            intent.putExtra(Content.PID, group.getOwner().getPid());
            startActivity(intent);
        } else {
            String title = MimeTypeService.getCompactString(group.getName());
            if (Objects.equals(owner, host)) {
                mToolbar.setSubtitle(title + "  " + getString(R.string.owner_session,
                        InitApplication.getAlias(getApplicationContext())));
            } else {
                String alias = THREADS.getInstance(getApplicationContext()).getUserAlias(
                        owner.getPid());
                mToolbar.setSubtitle(title + "  " + getString(R.string.owner_session, alias));
            }


            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_notes_view, NotesFragment.newInstance());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    private void showFab(boolean visible) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof FabFragment) {
                FabFragment fabFragment = (FabFragment) fragment;
                fabFragment.showFab(visible);
            }
        }
    }


    private static class PagerAdapter extends FragmentStatePagerAdapter {


        PagerAdapter(FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }


        @Override
        @NonNull
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new GroupsFragment();
                case 1:
                    return new UsersFragment();
                default:
                    throw new RuntimeException("Not Supported position");
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
