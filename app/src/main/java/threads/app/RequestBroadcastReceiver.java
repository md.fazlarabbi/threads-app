package threads.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Objects;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.services.GroupService;


public class RequestBroadcastReceiver extends BroadcastReceiver {
    public static final String ACTION_REJECT = "threads.app.ACTION_REJECT";
    public static final String ACTION_ACCEPT = "threads.app.ACTION_ACCEPT";
    private static final String TAG = RequestBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        try {

            long idx = intent.getLongExtra(Content.IDX, -1);

            if (Objects.equals(intent.getAction(), ACTION_REJECT)) {
                GroupService.reject(context, idx);
            } else if (Objects.equals(intent.getAction(), ACTION_ACCEPT)) {
                GroupService.accept(context, idx);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }
}
