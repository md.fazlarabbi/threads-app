package threads.app;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

import threads.LogUtils;


public class NotificationFCMServer {
    private static final String TAG = NotificationFCMServer.class.getSimpleName();
    private static final String SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
    private static final String SERVICE = "https://fcm.googleapis.com/v1/projects/threads-app-basic/messages:send";
    private static final NotificationFCMServer SINGLETON = new NotificationFCMServer();

    private NotificationFCMServer() {
    }

    public static NotificationFCMServer getInstance() {
        return SINGLETON;
    }

    @Nullable
    private String getAccessToken(@NonNull Context context) {
        try {

            InputStream inputStream = context.getResources().
                    openRawResource(R.raw.threads_app_basic);

            GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream);

            if (credentials.createScopedRequired()) {
                credentials = credentials.createScoped(
                        Collections.singleton(SCOPE));
            }
            credentials.refreshIfExpired();
            return credentials.getAccessToken().getTokenValue();

        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            // ignore exception, can happen when no connection
        }
        return null;
    }


    public synchronized void sendNotification(@NonNull Context context,
                                              @NonNull String token,
                                              @NonNull Map<String, String> params) {

        String accessToken = getAccessToken(context);
        if (accessToken != null) {
            JsonObject jsonObject = new JsonObject();

            for (String key : params.keySet()) {
                String content = params.get(key);
                if (content != null) {
                    jsonObject.addProperty(key, content);
                }
            }


            JsonObject jsonObj = new JsonObject();
            jsonObj.addProperty("token", token);
            jsonObj.add("data", jsonObject);

            JsonObject msgObj = new JsonObject();
            msgObj.add("message", jsonObj);
            String text = msgObj.toString();

            LogUtils.error(TAG, "FCM message : " + text);
            sendMessageToFcm(accessToken, text);
        }

    }

    private void sendMessageToFcm(@NonNull String accessToken, @NonNull String postData) {
        HttpURLConnection httpConn = null;

        try {
            httpConn = getConnection(accessToken);

            DataOutputStream wr = new DataOutputStream(httpConn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();


            InputStream inputStream = httpConn.getInputStream();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(inputStream));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        } finally {
            if (httpConn != null) {
                httpConn.disconnect();
            }
        }

    }

    private HttpURLConnection getConnection(@NonNull String accessToken) throws Exception {

        URL url = new URL(SERVICE);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
        httpURLConnection.setRequestProperty("Content-Type", "application/json; UTF-8");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        return httpURLConnection;
    }

}
