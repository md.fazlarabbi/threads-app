package threads.app.provider;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.DocumentsContract.Document;
import android.provider.DocumentsProvider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.BuildConfig;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.ipfs.CID;
import threads.ipfs.IPFS;

import static android.os.ParcelFileDescriptor.MODE_READ_ONLY;


public class FileDocumentsProvider extends DocumentsProvider {
    private static final String TAG = FileDocumentsProvider.class.getSimpleName();
    private static final String HASH_ID = ".hid";
    private static final String IMAGE_ID = ".iid";
    private final static long SPLIT = (long) 1e+7;
    private final static String[] DEFAULT_ROOT_PROJECTION =
            new String[]{
                    DocumentsContract.Root.COLUMN_ROOT_ID,
                    DocumentsContract.Root.COLUMN_ICON,
                    DocumentsContract.Root.COLUMN_TITLE,
                    DocumentsContract.Root.COLUMN_FLAGS,
                    DocumentsContract.Root.COLUMN_DOCUMENT_ID,
            };
    private static final String[] DEFAULT_DOCUMENT_PROJECTION = new String[]{
            Document.COLUMN_DOCUMENT_ID,
            Document.COLUMN_MIME_TYPE,
            Document.COLUMN_DISPLAY_NAME,
            Document.COLUMN_LAST_MODIFIED,
            Document.COLUMN_FLAGS,
            Document.COLUMN_SIZE
    };
    private static final int QR_CODE_SIZE = 250;
    private String appName;
    private THREADS threads;
    private IPFS ipfs;


    private static String[] resolveRootProjection(String[] projection) {
        return projection != null ? projection : DEFAULT_ROOT_PROJECTION;
    }

    private static String[] resolveDocumentProjection(String[] projection) {
        return projection != null ? projection : DEFAULT_DOCUMENT_PROJECTION;
    }

    public static Uri getUriForNote(Note note) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("content")
                .authority(BuildConfig.DOCUMENTS_AUTHORITY)
                .appendPath("document")
                .appendPath("" + note.getContent());
        return builder.build();
    }

    public static Uri getUriForImage(String name) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("content")
                .authority(BuildConfig.DOCUMENTS_AUTHORITY)
                .appendPath("document")
                .appendPath(name + IMAGE_ID);
        return builder.build();
    }

    public static Uri getUriForBitmap(String hash) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("content")
                .authority(BuildConfig.DOCUMENTS_AUTHORITY)
                .appendPath("document")
                .appendPath(hash + HASH_ID);
        return builder.build();
    }

    public static Bitmap getBitmap(@NonNull String hash) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(hash,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public Cursor queryRoots(String[] projection) {
        MatrixCursor result = new MatrixCursor(resolveRootProjection(projection));

        String rootId = BuildConfig.DOCUMENTS_AUTHORITY;
        String rootDocumentId = "0";
        MatrixCursor.RowBuilder row = result.newRow();
        row.add(DocumentsContract.Root.COLUMN_ROOT_ID, rootId);
        row.add(DocumentsContract.Root.COLUMN_ICON, R.drawable.app_icon);
        row.add(DocumentsContract.Root.COLUMN_TITLE, appName);
        row.add(DocumentsContract.Root.COLUMN_FLAGS,
                DocumentsContract.Root.FLAG_LOCAL_ONLY |
                        DocumentsContract.Root.FLAG_SUPPORTS_RECENTS |
                        DocumentsContract.Root.FLAG_SUPPORTS_SEARCH);
        row.add(DocumentsContract.Root.COLUMN_DOCUMENT_ID, rootDocumentId);
        row.add(DocumentsContract.Root.COLUMN_MIME_TYPES, "*/*");
        row.add(DocumentsContract.Root.COLUMN_AVAILABLE_BYTES, ipfs.getFreeSpace());
        row.add(DocumentsContract.Root.COLUMN_CAPACITY_BYTES, ipfs.getTotalSpace());
        return result;
    }

    @Override
    public Cursor queryRecentDocuments(String rootId, String[] projection) {

        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));


        List<Note> entries = evaluateList(threads.getNewestSeedingNotes(5, NoteType.LINK));
        for (Note note : entries) {
            includeFile(result, note);
        }

        return result;
    }

    @Override
    public Cursor querySearchDocuments(String rootId, String query, String[] projection) {
        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));


        List<Note> entries = evaluateList(threads.getSeedingNotesByQuery(query, NoteType.LINK));
        for (Note note : entries) {
            includeFile(result, note);
        }

        return result;
    }


    @Override
    public AssetFileDescriptor openDocumentThumbnail(
            String documentId, Point sizeHint, CancellationSignal signal)
            throws FileNotFoundException {

        try {

            CID cid = CID.create(documentId);
            List<Note> files = threads.getNotesByContent(cid);
            if (files.isEmpty()) {
                throw new FileNotFoundException("File not found.");
            }
            Note file = files.get(0);

            CID thumbnail = file.getThumbnail();
            if (thumbnail == null) {
                throw new FileNotFoundException();
            }

            if (signal != null) {
                if (signal.isCanceled()) {
                    return null;
                }
            }

            if (file.getSize() < SPLIT) {
                ParcelFileDescriptor pfd = ParcelFileDescriptor.open(
                        getContentFile(thumbnail, ""), MODE_READ_ONLY);
                return new AssetFileDescriptor(pfd, 0, AssetFileDescriptor.UNKNOWN_LENGTH);
            } else {
                final ParcelFileDescriptor pfd = ParcelFileDescriptorUtil.pipeFrom(ipfs, thumbnail, "");
                return new AssetFileDescriptor(pfd, 0, AssetFileDescriptor.UNKNOWN_LENGTH);
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            throw new FileNotFoundException(e.getLocalizedMessage());
        }
    }

    @Override
    public Cursor queryDocument(String docId, String[] projection) throws FileNotFoundException {

        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));

        try {
            int flags = 0;
            if ("0".equals(docId)) {
                final MatrixCursor.RowBuilder row = result.newRow();
                row.add(Document.COLUMN_DOCUMENT_ID, docId);
                row.add(Document.COLUMN_DISPLAY_NAME, "ipfs");
                row.add(Document.COLUMN_SIZE, null);
                row.add(Document.COLUMN_MIME_TYPE, Document.MIME_TYPE_DIR);
                row.add(Document.COLUMN_LAST_MODIFIED, new Date());
                row.add(Document.COLUMN_FLAGS, flags);
            } else if (docId.endsWith(HASH_ID)) {
                final MatrixCursor.RowBuilder row = result.newRow();
                row.add(Document.COLUMN_DOCUMENT_ID, docId);
                row.add(Document.COLUMN_DISPLAY_NAME, docId);
                row.add(Document.COLUMN_SIZE, null);
                row.add(Document.COLUMN_MIME_TYPE, "image/png");
                row.add(Document.COLUMN_LAST_MODIFIED, new Date());
                row.add(Document.COLUMN_FLAGS, 0);
            } else {

                CID cid = CID.create(docId);
                List<Note> files = threads.getNotesByContent(cid);
                if (files.isEmpty()) {
                    throw new FileNotFoundException("File not found.");
                }
                Note file = files.get(0);
                includeFile(result, file);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new FileNotFoundException("" + throwable.getLocalizedMessage());
        }


        return result;

    }

    @Override
    public String getDocumentType(String documentId) throws FileNotFoundException {

        try {

            if ("0".equals(documentId)) {
                return Document.MIME_TYPE_DIR;
            } else if (documentId.endsWith(HASH_ID)) {
                return "image/png";
            } else {

                CID cid = CID.create(documentId);
                List<Note> files = threads.getNotesByContent(cid);
                if (files.isEmpty()) {
                    throw new FileNotFoundException("File not found.");
                }
                Note file = files.get(0);
                return file.getMimeType();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new FileNotFoundException("" + throwable.getLocalizedMessage());
        }
    }

    private void includeFile(MatrixCursor result, Note file) {
        int flags = 0;

        if (file.hasThumbnail() && file.getNoteType() == NoteType.LINK) {
            flags |= Document.FLAG_SUPPORTS_THUMBNAIL;
        }

        CID content = file.getContent();
        Objects.requireNonNull(content);
        final MatrixCursor.RowBuilder row = result.newRow();
        row.add(Document.COLUMN_DOCUMENT_ID, content.getCid());
        row.add(Document.COLUMN_DISPLAY_NAME, file.getName());
        row.add(Document.COLUMN_SIZE, file.getSize());
        row.add(Document.COLUMN_MIME_TYPE, file.getMimeType());
        row.add(Document.COLUMN_LAST_MODIFIED, file.getDate());
        row.add(Document.COLUMN_FLAGS, flags);

    }

    @Override
    public Cursor queryChildDocuments(String parentDocumentId, String[] projection, String sortOrder)
            throws FileNotFoundException {

        try {
            final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));

            if ("0".equals(parentDocumentId)) {
                List<Note> entries = evaluateList(threads.getSeedingNotes(NoteType.LINK));
                for (Note file : entries) {
                    includeFile(result, file);
                }
            }


            return result;

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            throw new FileNotFoundException("" + e.getLocalizedMessage());
        }
    }

    private List<Note> evaluateList(@NonNull List<Note> notes) {
        List<Note> allowedNotes = new ArrayList<>();
        HashSet<String> contents = new HashSet<>();
        for (Note note : notes) {

            CID cid = note.getContent();
            if (cid != null) {
                if (contents.add(cid.getCid())) {
                    allowedNotes.add(note);
                }
            }

        }

        return allowedNotes;
    }

    @Override
    public ParcelFileDescriptor openDocument(String documentId, String mode,
                                             @Nullable CancellationSignal signal) throws FileNotFoundException {

        LogUtils.info(TAG, "openDocument : " + documentId);
        final int accessMode = ParcelFileDescriptor.parseMode(mode);
        final boolean isWrite = (mode.indexOf('w') != -1);
        try {
            if (isWrite) {
                File impl = new File(ipfs.getCacheDir(), documentId);
                boolean success = impl.createNewFile();
                if (!success) {
                    LogUtils.error(TAG, "File couldn't be created " + impl.getName());
                }
                impl.deleteOnExit();
                return ParcelFileDescriptor.open(impl, accessMode);
            } else {
                if (documentId.endsWith(HASH_ID)) {
                    File impl = getBitmapFile(documentId);
                    return ParcelFileDescriptor.open(impl, accessMode);
                } else if (documentId.endsWith(IMAGE_ID)) {
                    File impl = new File(ipfs.getCacheDir(), documentId);
                    return ParcelFileDescriptor.open(impl, accessMode);
                } else {

                    CID cid = CID.create(documentId);
                    List<Note> files = threads.getNotesByContent(cid);
                    if (files.isEmpty()) {
                        throw new FileNotFoundException("File not found.");
                    }
                    Note file = files.get(0);

                    String sKey = threads.getGroupSesKey(file.getGroup());

                    if (file.getSize() < SPLIT) {
                        return ParcelFileDescriptor.open(getContentFile(cid, sKey), accessMode);
                    } else {
                        return ParcelFileDescriptorUtil.pipeFrom(ipfs, cid, sKey);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new FileNotFoundException("" + throwable.getLocalizedMessage());
        }
    }

    @NonNull
    private File getContentFile(@NonNull CID cid, @NonNull String sesKey) throws Exception {

        File file = new File(ipfs.getCacheDir(), cid.getCid());
        if (!file.exists()) {
            ipfs.storeToFile(file, cid, sesKey, 4096);
        }
        return file;
    }

    private File getBitmapFile(@NonNull String hash) throws IOException {
        Bitmap bitmap = getBitmap(hash);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        bitmap.recycle();
        File file = new File(ipfs.getCacheDir(), hash + ".png");
        if (!file.exists()) {
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                try (ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes)) {
                    IPFS.copy(inputStream, fileOutputStream);
                }
            }
        }
        return file;
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        Objects.requireNonNull(context);
        appName = context.getString(R.string.app_name);

        InitApplication.runUpdatesIfNecessary(context);

        ipfs = IPFS.getInstance(getContext());
        threads = THREADS.getInstance(context);

        return true;
    }


    private static class ParcelFileDescriptorUtil {

        static ParcelFileDescriptor pipeFrom(@NonNull IPFS ipfs, @NonNull CID cid, @NonNull String skey) throws Exception {
            final ParcelFileDescriptor[] pipe = ParcelFileDescriptor.createPipe();

            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> {
                try (OutputStream output =
                             new ParcelFileDescriptor.AutoCloseOutputStream(pipe[1])) {
                    ipfs.storeToOutputStream(output, cid, skey, 4096);
                } catch (Throwable e) {
                    LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                }
            });
            return pipe[0];
        }

    }
}