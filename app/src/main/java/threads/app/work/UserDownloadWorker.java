package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.List;
import java.util.Objects;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class UserDownloadWorker extends Worker {

    private static final String TAG = UserDownloadWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UserDownloadWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    public static OneTimeWorkRequest getWork(@NonNull PID pid) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid.getPid());

        return new OneTimeWorkRequest.Builder(UserDownloadWorker.class)
                .setInputData(data.build())
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();
    }


    @NonNull
    @Override
    public Result doWork() {

        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start connect [" + pid + "]...");

        try {
            THREADS threads = THREADS.getInstance(getApplicationContext());

            if (!threads.isUserBlocked(pid)) {

                IPFS ipfs = IPFS.getInstance(getApplicationContext());

                if (ipfs.isConnected(PID.create(pid))) {
                    List<Note> notes = threads.getNonSeedingNotesByOwner(PID.create(pid));
                    // now download all messages
                    for (Note note : notes) {
                        if (!note.isExpired()) {
                            if (note.getNoteType() == NoteType.MESSAGE) {
                                OneTimeWorkRequest work = DownloadMessageWorker.getWork(note.getIdx());
                                threads.setNoteWork(note.getIdx(), work.getId());

                                WorkManager.getInstance(getApplicationContext()).enqueue(work);

                            } else if (note.getNoteType() == NoteType.VIDEO
                                    || note.getNoteType() == NoteType.AUDIO ||
                                    note.getNoteType() == NoteType.IMAGE) {

                                OneTimeWorkRequest work = DownloadNoteWorker.getWork(note.getIdx(),
                                        DownloadNoteWorker.isTask(note.getSize()));
                                threads.setNoteWork(note.getIdx(), work.getId());

                                WorkManager.getInstance(getApplicationContext()).enqueue(work);

                            }
                        }
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }
}

