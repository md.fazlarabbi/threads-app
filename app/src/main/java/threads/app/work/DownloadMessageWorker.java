package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.utils.Network;
import threads.ipfs.CID;
import threads.ipfs.CloseableProgress;
import threads.ipfs.IPFS;

public class DownloadMessageWorker extends Worker {

    private static final String TAG = GroupImageWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public DownloadMessageWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    public static OneTimeWorkRequest getWork(long idx) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(DownloadMessageWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .build();
    }


    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);
        THREADS threads = THREADS.getInstance(getApplicationContext());

        long start = System.currentTimeMillis();

        boolean result = false;
        LogUtils.info(TAG, " start ... " + idx);

        int timeout = InitApplication.getDownloadTimeout(getApplicationContext());
        try {
            Note note = threads.getNoteByIdx(idx);
            Objects.requireNonNull(note);

            if (!Objects.equals(note.getWorkUUID(), getId())) {
                threads.setNoteWork(idx, getId());
            }


            threads.setNoteLeaching(idx);

            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            CID cid = note.getContent();
            Objects.requireNonNull(cid);

            Group group = threads.getGroup(note);
            Objects.requireNonNull(group);
            String sesKey = group.getSesKey();
            AtomicLong started = new AtomicLong(System.currentTimeMillis());
            String content = ipfs.loadText(cid, sesKey, new CloseableProgress() {
                @Override
                public boolean isClosed() {
                    long diff = System.currentTimeMillis() - started.get();
                    boolean abort = !Network.isConnected(getApplicationContext())
                            || (diff > (timeout * 1000));
                    return isStopped() || abort;
                }

                public void setProgress(int percent) {
                    started.set(System.currentTimeMillis());
                }
            });

            if (content != null) {
                result = true;
                threads.setNoteText(idx, content);
                threads.setNoteSeeding(idx);
            } else {
                String message = getApplicationContext().getString(
                        R.string.message_from_failed, note.getAlias());
                threads.setNoteText(idx, message);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            threads.resetNoteLeaching(idx);
            threads.resetNoteWork(idx);
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        if (result) {
            return Result.success();
        } else {
            return Result.failure();
        }
    }


}
