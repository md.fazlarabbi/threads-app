package threads.app.work;


import android.content.Context;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Objects;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.NotificationFCMServer;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.utils.FcmContent;
import threads.ipfs.CID;
import threads.ipfs.Encryption;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupRequestWorker extends Worker {
    private static final String TAG = GroupRequestWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public GroupRequestWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    public static void request(@NonNull Context context, @NonNull PID user, long idx) {


        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, user.getPid());
        data.putLong(Content.IDX, idx);

        OneTimeWorkRequest syncWorkRequest =
                new OneTimeWorkRequest.Builder(GroupRequestWorker.class)
                        .addTag(TAG)
                        .setInputData(data.build())
                        .setConstraints(builder.build())
                        .build();

        WorkManager.getInstance(context).enqueueUniqueWork(
                user.getPid() + idx, ExistingWorkPolicy.KEEP, syncWorkRequest);
    }

    @NonNull
    @Override
    public Result doWork() {

        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);

        long idx = getInputData().getLong(Content.IDX, -1);

        try {
            THREADS threads = THREADS.getInstance(getApplicationContext());
            Group group = threads.getGroupByIdx(idx);
            Objects.requireNonNull(group);
            User user = threads.getUserByPid(pid);
            Objects.requireNonNull(user);

            String token = user.getToken();
            Objects.requireNonNull(token);
            String pkey = user.getPublicKey();
            Objects.requireNonNull(pkey);

            sendGroupRequestNotification(token, pkey, group);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        }
        return Result.success();

    }


    private void sendGroupRequestNotification(@NonNull String token,
                                              @NonNull String pkey,
                                              @NonNull Group group) throws Exception {

        PID host = IPFS.getPID(getApplicationContext());
        Objects.requireNonNull(host);

        NotificationFCMServer service = NotificationFCMServer.getInstance();

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Content.EST, FcmContent.REQUEST.name());
        hashMap.put(Content.PID, host.getPid());
        String alias = InitApplication.getAlias(getApplicationContext());
        alias = Base64.encodeToString(alias.getBytes(), Base64.DEFAULT);
        hashMap.put(Content.ALIAS, alias);
        hashMap.put(Content.FCM, InitApplication.getToken(getApplicationContext()));
        hashMap.put(Content.PKEY, IPFS.getPublicKey(getApplicationContext()));
        String name = Base64.encodeToString(group.getName().getBytes(), Base64.DEFAULT);
        hashMap.put(Content.NAME, name);
        if (group.isEncrypted()) {
            hashMap.put(Content.SKEY, Encryption.encryptRSA(group.getSesKey(), pkey));
        }
        hashMap.put(Content.UUID, group.getUuid());
        CID image = group.getImage();
        if (image != null) {
            hashMap.put(Content.CID, image.getCid());
        }

        service.sendNotification(getApplicationContext(), token, hashMap);

    }
}
