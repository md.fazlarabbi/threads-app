package threads.app.work;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.utils.Network;
import threads.ipfs.CID;
import threads.ipfs.CloseableProgress;
import threads.ipfs.IPFS;

public class GroupImageWorker extends Worker {
    private static final String TAG = GroupImageWorker.class.getSimpleName();

    public GroupImageWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);

    }

    public static void loadImage(@NonNull Context context, @NonNull String cid, long idx) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.CID, cid);
        data.putLong(Content.IDX, idx);

        OneTimeWorkRequest syncWorkRequest =
                new OneTimeWorkRequest.Builder(GroupImageWorker.class)
                        .addTag(TAG)
                        .setInputData(data.build())
                        .setConstraints(builder.build())
                        .build();

        WorkManager.getInstance(context).enqueueUniqueWork(
                cid, ExistingWorkPolicy.KEEP, syncWorkRequest);
    }

    @NonNull
    @Override
    public Result doWork() {

        String cid = getInputData().getString(Content.CID);
        Objects.requireNonNull(cid);

        long idx = getInputData().getLong(Content.IDX, -1);


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        int timeout = InitApplication.getDownloadTimeout(getApplicationContext());
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            THREADS threads = THREADS.getInstance(getApplicationContext());

            String key = ""; // ok, the group image is never encrypted
            CID image = CID.create(cid);
            AtomicLong started = new AtomicLong(System.currentTimeMillis());
            byte[] data = ipfs.loadData(image, key, new CloseableProgress() {
                @Override
                public boolean isClosed() {
                    long diff = System.currentTimeMillis() - started.get();
                    boolean abort = !Network.isConnected(getApplicationContext())
                            || (diff > (timeout * 1000));
                    return isStopped() || abort;
                }

                public void setProgress(int percent) {
                    started.set(System.currentTimeMillis());
                }
            });

            if (data != null) {
                threads.setGroupImage(idx, image);
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }

}
