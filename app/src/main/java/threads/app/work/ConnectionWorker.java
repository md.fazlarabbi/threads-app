package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.ArrayList;
import java.util.List;

import threads.LogUtils;
import threads.ipfs.DnsAddrResolver;
import threads.ipfs.IPFS;
import threads.ipfs.Peer;

public class ConnectionWorker extends Worker {

    private static final String TAG = ConnectionWorker.class.getSimpleName();
    private static final List<String> DNS_ADDRS = new ArrayList<>();
    private static final int MIN_PEERS = 10;

    @SuppressWarnings("WeakerAccess")
    public ConnectionWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    public static OneTimeWorkRequest getWork() {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);

        return new OneTimeWorkRequest.Builder(ConnectionWorker.class)
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();

    }

    public static void connect(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.KEEP, getWork());
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            List<Peer> peers = ipfs.swarmPeers();

            if (peers.size() < MIN_PEERS) {
                for (String address : IPFS.Bootstrap) {
                    boolean result = ipfs.swarmConnect(address, 10);
                    LogUtils.info(TAG, " \nBootstrap : " + address + " " + result);
                }
            }


            if (DNS_ADDRS.isEmpty()) {
                DNS_ADDRS.addAll(DnsAddrResolver.getMultiAddresses());
            }


            if (peers.size() < MIN_PEERS) {
                for (String address : DNS_ADDRS) {
                    boolean result = ipfs.swarmConnect(address, 10);
                    LogUtils.info(TAG, " \nBootstrap : " + address + " " + result);
                }
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }
}

