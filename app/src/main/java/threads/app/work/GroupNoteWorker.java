package threads.app.work;

import android.content.Context;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Objects;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.NotificationFCMServer;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.ipfs.CID;
import threads.ipfs.Encryption;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupNoteWorker extends Worker {
    private static final String WID = "PNW";
    private static final String TAG = GroupNoteWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public GroupNoteWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }


    public static OneTimeWorkRequest getSharedWork() {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);

        return new OneTimeWorkRequest.Builder(GroupNoteWorker.class)
                .addTag(GroupNoteWorker.TAG)
                .setConstraints(builder.build())
                .build();
    }

    public static String getUniqueId(long idx) {
        return WID + idx;
    }

    public static OneTimeWorkRequest getWork(long idx) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(GroupNoteWorker.class)
                .addTag(GroupNoteWorker.TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .build();
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        long idx = getInputData().getLong(Content.IDX, -1);

        LogUtils.info(TAG, " start [" + idx + "]...");
        try {

            THREADS threads = THREADS.getInstance(getApplicationContext());
            Note note = threads.getNoteByIdx(idx);
            Objects.requireNonNull(note);
            Group group = threads.getGroup(note);
            Objects.requireNonNull(group);


            HashMap<String, String> hashMap = new HashMap<>();
            fillContent(note, group.getUuid(), group.getSesKey(), hashMap);

            NotificationFCMServer service = NotificationFCMServer.getInstance();

            for (String member : group.getMembers()) {
                User user = threads.getUserByPid(member);
                if (user != null && user.hasToken() && !user.isBlocked()) {
                    String token = user.getToken();
                    Objects.requireNonNull(token);
                    service.sendNotification(getApplicationContext(), token, hashMap);
                }
            }

            if (group.hasMembers()) {
                threads.setNotePublished(idx);
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }

    private void fillContent(@NonNull Note note, @NonNull String uuid, @NonNull String sesKey,
                             @NonNull HashMap<String, String> content) throws Exception {


        PID host = IPFS.getPID(getApplicationContext());
        Objects.requireNonNull(host);

        // NOT ENCRYPTED
        NoteType type = note.getNoteType();
        content.put(Content.EST, String.valueOf(type.getCode()));

        // NOT ENCRYPTED
        content.put(Content.UUID, uuid);

        // NOT ENCRYPTED
        String alias = InitApplication.getAlias(getApplicationContext());
        alias = Base64.encodeToString(alias.getBytes(), Base64.DEFAULT);
        content.put(Content.ALIAS, alias);
        content.put(Content.PKEY, IPFS.getPublicKey(getApplicationContext()));
        content.put(Content.FCM, InitApplication.getToken(getApplicationContext()));
        content.put(Content.PID, host.getPid());

        // NOT ENCRYPTED
        content.put(Content.DATE, String.valueOf(note.getDate()));

        // NOT ENCRYPTED
        CID cid = note.getContent();
        if (cid != null) {
            content.put(Content.CID, cid.getCid());
        }

        // ENCRYPTED
        content.put(Content.MIME_TYPE, Encryption.encrypt(note.getMimeType(), sesKey));

        // ENCRYPTED
        content.put(Content.FILENAME, Encryption.encrypt(note.getName(), sesKey));

        // NOT ENCRYPTED
        content.put(Content.FILESIZE, "" + note.getSize());
    }

}
