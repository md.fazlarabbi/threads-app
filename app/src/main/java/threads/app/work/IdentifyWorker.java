package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.gson.Gson;

import java.util.Objects;

import threads.LogUtils;
import threads.app.BuildConfig;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;
import threads.ipfs.PeerInfo;
import threads.ipfs.TimeoutProgress;

public class IdentifyWorker extends Worker {

    private static final String WID = "IW";
    private static final String TAG = IdentifyWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public IdentifyWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static String getUniqueId(@NonNull String pid) {
        return WID + pid;
    }

    public static OneTimeWorkRequest getWork(@NonNull String pid) {
        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid);

        return new OneTimeWorkRequest.Builder(IdentifyWorker.class)
                .setInputData(data.build())
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();
    }

    public static void check(@NonNull Context context, @NonNull String pid) {

        WorkManager.getInstance(context).enqueueUniqueWork(
                getUniqueId(pid), ExistingWorkPolicy.KEEP, getWork(pid));
    }

    @NonNull
    @Override
    public Result doWork() {

        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start connect [" + pid + "]...");

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            THREADS threads = THREADS.getInstance(getApplicationContext());


            User user = threads.getUserByPid(pid);
            Objects.requireNonNull(user);
            PeerInfo pInfo = ipfs.id(PID.create(pid), 30);
            if (pInfo != null) {
                String publicKey = pInfo.getPublicKey();
                Objects.requireNonNull(publicKey);
                if (!publicKey.isEmpty()) {
                    if (!Objects.equals(user.getPublicKey(), publicKey)) {
                        threads.setUserPublicKey(pid, publicKey);
                    }
                }
                String agentVersion = pInfo.getAgentVersion();
                Objects.requireNonNull(agentVersion);
                if (!agentVersion.isEmpty() &&
                        !Objects.equals(agentVersion, user.getAgent())) {
                    try {

                        if (ipfs.isValidCID(agentVersion)) {

                            threads.setUserAgent(pid, agentVersion);
                            CID cid = CID.create(agentVersion);

                            Content content = getContent(ipfs, cid);

                            if (content != null) {

                                String token = content.get(Content.FCM);
                                Objects.requireNonNull(token);
                                if (!Objects.equals(user.getToken(), token)) {
                                    threads.setUserToken(pid, token);
                                    threads.setUserTimestamp(pid, System.currentTimeMillis());
                                }

                                String alias = content.get(Content.ALIAS);
                                Objects.requireNonNull(alias);
                                if (!Objects.equals(user.getAlias(), alias)) {
                                    threads.setUserAlias(pid, alias);
                                    threads.setUserTimestamp(pid, System.currentTimeMillis());

                                    threads.setNoteOwnerAlias(PID.create(pid), alias);
                                }


                                String img = content.get(Content.IMG);
                                if (img != null) {
                                    if (ipfs.isValidCID(img)) {
                                        CID image = CID.create(img);
                                        if (!Objects.equals(user.getImage(), image)) {
                                            if (loadImage(ipfs, image)) {
                                                threads.setUserImage(pid, image);
                                                threads.setUserTimestamp(pid,
                                                        System.currentTimeMillis());
                                            }
                                        }
                                    }
                                }


                                if (!threads.isUserValid(pid)) {
                                    if (threads.hasUserToken(pid) && threads.hasUserPublicKey(pid)) {
                                        threads.setUserValid(pid);
                                        threads.setUserTimestamp(pid, System.currentTimeMillis());
                                    }
                                }


                            }
                        }
                    } catch (Throwable e) {
                        LogUtils.error(TAG, e);
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }


    private Content getContent(@NonNull IPFS ipfs, @NonNull CID cid) {
        Gson gson = new Gson();

        Content content = null;
        try {
            String text = ipfs.loadText(cid, BuildConfig.ApiAesKey, new TimeoutProgress(30));
            content = gson.fromJson(text, Content.class);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        // TODO remove someday in the future
        if (content == null) {
            try {
                String text = ipfs.loadText(cid, "", new TimeoutProgress(30));
                content = gson.fromJson(text, Content.class);
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }

        return content;
    }


    private boolean loadImage(@NonNull IPFS ipfs, @NonNull CID cid) {
        try {
            // image is not encrypted
            byte[] bytes = ipfs.loadData(cid, "", new TimeoutProgress(30));
            if (bytes != null && bytes.length > 0) {
                return true;
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return false;
    }
}

