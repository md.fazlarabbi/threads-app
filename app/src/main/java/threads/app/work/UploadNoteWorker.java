package threads.app.work;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.MainActivity;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.services.GroupService;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.Progress;

public class UploadNoteWorker extends Worker {

    private static final String TAG = UploadNoteWorker.class.getSimpleName();
    private final NotificationManager mNotificationManager;

    @SuppressWarnings("WeakerAccess")
    public UploadNoteWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    public static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadNoteWorker.class)
                .addTag(UploadNoteWorker.TAG)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static UUID upload(@NonNull Context context, long idx) {
        OneTimeWorkRequest request = getWork(idx);
        WorkManager.getInstance(context).enqueue(request);
        return request.getId();
    }

    private void createChannel(@NonNull Context context) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {
                CharSequence name = context.getString(R.string.upload_channel_name);
                String description = context.getString(R.string.upload_channel_description);

                NotificationChannel mChannel = new NotificationChannel(TAG, name,
                        NotificationManager.IMPORTANCE_HIGH);
                mChannel.setDescription(description);

                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(mChannel);
                }


            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            THREADS threads = THREADS.getInstance(getApplicationContext());


            Note note = threads.getNoteByIdx(idx);
            Objects.requireNonNull(note);


            final long size = note.getSize();

            boolean showProgress = size > 100 * 1000 * 1000; // 100 MB

            if (showProgress) {
                if (!threads.isNoteLeaching(idx)) {
                    threads.setNoteLeaching(idx);
                }
                if (threads.isNoteInit(idx)) {
                    threads.resetNoteInit(idx);
                }
            }

            if (!Objects.equals(note.getWorkUUID(), getId())) {
                threads.setNoteWork(idx, getId());
            }

            String uri = note.getUri();
            Objects.requireNonNull(uri);
            Group group = threads.getGroup(note);
            Objects.requireNonNull(group);

            try (InputStream inputStream = getApplicationContext().getContentResolver().
                    openInputStream(Uri.parse(uri))) {

                Objects.requireNonNull(inputStream);


                CID cid = ipfs.storeInputStream(inputStream, group.getSesKey(), new Progress() {
                    @Override
                    public boolean isClosed() {
                        return isStopped();
                    }

                    @Override
                    public long getSize() {
                        if (showProgress) {
                            return size;
                        } else {
                            return 0;
                        }
                    }

                    @Override
                    public void setProgress(int percent) {
                        threads.setNoteProgress(idx, percent);
                    }
                });

                Objects.requireNonNull(cid);
                if (!isStopped()) {

                    threads.setNoteContent(idx, cid);
                    threads.setNoteSeeding(idx);
                    threads.resetNoteUri(idx);

                    GroupService.updateGroupInfo(getApplicationContext(), group.getIdx(), idx);

                    Data.Builder data = new Data.Builder();
                    data.putLong(Content.IDX, idx);
                    return Result.success(data.build());

                } else {
                    return Result.failure();
                }
            } catch (Throwable e) {

                if (!isStopped()) {
                    threads.setNoteError(idx);
                    buildNotification((int) idx, note.getName());
                }

                throw e;
            } finally {
                if (threads.isNoteLeaching(idx)) {
                    threads.resetNoteLeaching(idx);
                }
                threads.resetNoteWork(idx);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }


    }

    private void buildNotification(int idx, @NonNull String name) {

        createChannel(getApplicationContext());
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getApplicationContext(), TAG);

        builder.setContentTitle(getApplicationContext().getString(R.string.upload_failed, name));
        builder.setSmallIcon(R.drawable.upload);
        builder.setPriority(NotificationManager.IMPORTANCE_MAX);
        builder.setCategory(NotificationCompat.CATEGORY_ERROR);
        Intent defaultIntent = new Intent(getApplicationContext(), MainActivity.class);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent defaultPendingIntent = PendingIntent.getActivity(
                getApplicationContext(), requestID, defaultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(defaultPendingIntent);
        Notification notification = builder.build();


        if (mNotificationManager != null) {
            mNotificationManager.notify(idx, notification);
        }
    }
}
