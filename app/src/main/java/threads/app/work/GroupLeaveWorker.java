package threads.app.work;


import android.content.Context;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Objects;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.NotificationFCMServer;
import threads.app.core.Content;
import threads.app.utils.FcmContent;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

@SuppressWarnings("WeakerAccess")
public class GroupLeaveWorker extends Worker {
    private static final String TAG = GroupLeaveWorker.class.getSimpleName();
    private static final String WID = "GLW";

    @SuppressWarnings("WeakerAccess")
    public GroupLeaveWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    static void leave(@NonNull Context context, @NonNull String token, @NonNull String uuid) {


        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.FCM, token);
        data.putString(Content.UUID, uuid);

        OneTimeWorkRequest syncWorkRequest =
                new OneTimeWorkRequest.Builder(GroupLeaveWorker.class)
                        .addTag(TAG)
                        .setInputData(data.build())
                        .setConstraints(builder.build())
                        .build();

        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + token + uuid, ExistingWorkPolicy.KEEP, syncWorkRequest);
    }

    @NonNull
    @Override
    public Result doWork() {


        String uuid = getInputData().getString(Content.UUID);
        Objects.requireNonNull(uuid);
        String token = getInputData().getString(Content.FCM);
        Objects.requireNonNull(token);
        try {

            NotificationFCMServer service = NotificationFCMServer.getInstance();
            PID host = IPFS.getPID(getApplicationContext());
            Objects.requireNonNull(host);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Content.EST, FcmContent.LEAVE.name());
            hashMap.put(Content.UUID, uuid);
            hashMap.put(Content.PID, host.getPid());
            String alias = InitApplication.getAlias(getApplicationContext());
            alias = Base64.encodeToString(alias.getBytes(), Base64.DEFAULT);
            hashMap.put(Content.ALIAS, alias);


            service.sendNotification(getApplicationContext(), token, hashMap);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        }
        return Result.success();

    }
}
