package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.List;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.ipfs.CID;
import threads.ipfs.IPFS;

import static com.google.common.base.Preconditions.checkNotNull;

public class CleanupWorker extends Worker {

    private static final String TAG = CleanupWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public CleanupWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static PeriodicWorkRequest getWork() {
        Constraints.Builder builder = new Constraints.Builder()
                .setRequiresCharging(true);

        return new PeriodicWorkRequest.Builder(CleanupWorker.class, 6, TimeUnit.HOURS)
                .addTag(TAG)
                .setInitialDelay(1, TimeUnit.SECONDS)
                .setConstraints(builder.build())
                .build();

    }

    public static void cleanup(@NonNull Context context) {

        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
                TAG, ExistingPeriodicWorkPolicy.KEEP, getWork());

    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");

        try {


            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            checkNotNull(ipfs, "IPFS not valid");

            THREADS threads = THREADS.getInstance(getApplicationContext());

            List<Group> groups = threads.getDeletedGroups();
            for (Group group : groups) {
                removeGroup(ipfs, threads, group);
            }

            List<Note> notes = threads.getDeletedNotes();
            for (Note note : notes) {
                removeNote(ipfs, threads, note);
            }

            if (InitApplication.isAutoDelete(getApplicationContext())) {

                List<Note> expired = threads.getExpiredNotes();
                for (Note note : expired) {
                    removeNote(ipfs, threads, note);
                }
            }

            ipfs.gc();


            return Result.success();

        } catch (Throwable e) {
            LogUtils.error(TAG, e);

            return Result.failure();

        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
    }


    private void removeGroup(@NonNull IPFS ipfs, @NonNull THREADS threads, @NonNull Group group) {


        // delete groups notes children
        removeNotes(ipfs, threads, group);

        threads.removeGroup(group);


        unpin(ipfs, threads, group.getImage());


    }

    private void unpin(@NonNull IPFS ipfs, @NonNull THREADS threads, @Nullable CID cid) {
        try {
            if (cid != null) {
                if (!threads.isReferenced(cid)) {
                    ipfs.rm(cid);
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void removeNotes(@NonNull IPFS ipfs, @NonNull THREADS threads, @NonNull Group group) {

        List<Note> notes = threads.getNotes(group);
        for (Note note : notes) {
            removeNote(ipfs, threads, note);
        }
    }


    private void removeNote(@NonNull IPFS ipfs, @NonNull THREADS threads, @NonNull Note note) {

        threads.removeNote(note.getIdx());

        unpin(ipfs, threads, note.getContent());
        unpin(ipfs, threads, note.getThumbnail());

    }


}

