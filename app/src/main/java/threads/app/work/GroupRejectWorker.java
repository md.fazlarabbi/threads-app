package threads.app.work;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Objects;

import threads.LogUtils;
import threads.app.NotificationFCMServer;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.utils.FcmContent;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupRejectWorker extends Worker {

    private static final String TAG = GroupRejectWorker.class.getSimpleName();
    private static final String WID = "GRW";

    @SuppressWarnings("WeakerAccess")
    public GroupRejectWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    private static OneTimeWorkRequest getWork(@NonNull String pid, @NonNull String uuid) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid);
        data.putString(Content.UUID, uuid);

        return new OneTimeWorkRequest.Builder(GroupRejectWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .build();
    }

    public static void reject(@NonNull Context context, @NonNull String pid, @NonNull String uuid) {


        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + pid + uuid, ExistingWorkPolicy.KEEP,
                getWork(pid, uuid));

    }

    @NonNull
    @Override
    public Result doWork() {

        String uuid = getInputData().getString(Content.UUID);
        Objects.requireNonNull(uuid);
        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        try {
            NotificationFCMServer service = NotificationFCMServer.getInstance();
            PID host = IPFS.getPID(getApplicationContext());
            Objects.requireNonNull(host);


            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Content.EST, FcmContent.REJECT.name());
            hashMap.put(Content.UUID, uuid);
            hashMap.put(Content.PID, host.getPid());

            User user = THREADS.getInstance(getApplicationContext()).getUserByPid(pid);
            Objects.requireNonNull(user);
            String token = user.getToken();
            Objects.requireNonNull(token);

            service.sendNotification(getApplicationContext(), token, hashMap);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        }
        return Result.success();

    }
}
