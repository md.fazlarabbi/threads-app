package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.utils.MimeType;

public class GroupNotifyWorker extends Worker {


    private static final String TAG = GroupNotifyWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public GroupNotifyWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    public static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(GroupNotifyWorker.class)
                .addTag(TAG)
                .setInitialDelay(1, TimeUnit.MICROSECONDS)
                .setInputData(data.build())
                .build();
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        long idx = getInputData().getLong(Content.IDX, -1);


        try {
            THREADS threads = THREADS.getInstance(getApplicationContext());
            Note note = threads.getNoteByIdx(idx);
            Objects.requireNonNull(note);

            Group group = threads.getGroup(note);
            Objects.requireNonNull(group);

            String mimeType = note.getMimeType();

            String text = note.getText();
            if (text.isEmpty()) {
                if (mimeType.startsWith(MimeType.AUDIO)) {
                    text = getApplicationContext().getString(R.string.audio);
                } else if (mimeType.startsWith(MimeType.VIDEO)) {
                    text = getApplicationContext().getString(R.string.video);
                } else if (mimeType.startsWith(MimeType.IMAGE)) {
                    text = getApplicationContext().getString(R.string.image);
                } else {
                    text = "";
                }
            }
            String alias = note.getAlias();

            group.setMimeType(mimeType);
            group.setContent(alias.concat(": ").concat(text));
            group.setDate(System.currentTimeMillis());
            group.setNumber(group.getNumber() + 1);
            threads.updateGroup(group);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }
}

