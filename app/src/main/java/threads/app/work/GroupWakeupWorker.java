package threads.app.work;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Objects;

import threads.LogUtils;
import threads.app.NotificationFCMServer;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.utils.FcmContent;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupWakeupWorker extends Worker {
    private static final String TAG = GroupWakeupWorker.class.getSimpleName();
    private static final String WID = "GWW";

    @SuppressWarnings("WeakerAccess")
    public GroupWakeupWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    public static void wakeup(@NonNull Context context, @NonNull String pid) {


        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid);

        OneTimeWorkRequest syncWorkRequest =
                new OneTimeWorkRequest.Builder(GroupWakeupWorker.class)
                        .addTag(TAG)
                        .setInputData(data.build())
                        .setConstraints(builder.build())
                        .build();

        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + pid, ExistingWorkPolicy.KEEP, syncWorkRequest);
    }

    @NonNull
    @Override
    public Result doWork() {


        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        try {
            THREADS threads = THREADS.getInstance(getApplicationContext());
            User user = threads.getUserByPid(pid);
            Objects.requireNonNull(user);

            NotificationFCMServer service = NotificationFCMServer.getInstance();
            PID host = IPFS.getPID(getApplicationContext());
            Objects.requireNonNull(host);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Content.EST, FcmContent.WAKEUP.name());
            hashMap.put(Content.PID, host.getPid());

            String token = user.getToken();
            Objects.requireNonNull(token);

            service.sendNotification(getApplicationContext(), token, hashMap);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        }
        return Result.success();

    }
}
