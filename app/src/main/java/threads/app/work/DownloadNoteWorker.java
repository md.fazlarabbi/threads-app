package threads.app.work;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.MainActivity;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.app.services.ThumbnailService;
import threads.app.utils.Network;
import threads.codec.MetadataEditor;
import threads.codec.boxes.MetaValue;
import threads.ipfs.CID;
import threads.ipfs.CloseableProgress;
import threads.ipfs.Encryption;
import threads.ipfs.IPFS;

public class DownloadNoteWorker extends Worker {

    private static final String TAG = DownloadNoteWorker.class.getSimpleName();
    private static final String CHANNEL_PROGRESS_ID = "CHANNEL_PROGRESS_ID";
    private static final long MIN_PROGRESS_SIZE = 1000 * 1000 * 250; // 250 MB
    private final NotificationManager mNotificationManager;

    @SuppressWarnings("WeakerAccess")
    public DownloadNoteWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static boolean isTask(long size) {
        if (size <= 0) {
            return true;
        }
        return size > MIN_PROGRESS_SIZE;
    }

    public static OneTimeWorkRequest getWork(long idx, boolean foreground) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);
        data.putBoolean(Content.TASK, foreground);

        return new OneTimeWorkRequest.Builder(DownloadNoteWorker.class)
                .addTag(DownloadNoteWorker.TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .build();
    }


    private void createChannel() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {

                CharSequence name = getApplicationContext().getString(R.string.progress_channel_name);
                String description = getApplicationContext().getString(R.string.progress_channel_description);
                int importance = NotificationManager.IMPORTANCE_LOW;
                NotificationChannel mChannel = new NotificationChannel(
                        CHANNEL_PROGRESS_ID, name, importance);
                mChannel.setDescription(description);

                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(mChannel);
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }


    private String getFileInfo(@NonNull Note note) {
        String fileName = note.getName();

        String fileSize;
        long size = note.getSize();

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return getApplicationContext().getString(R.string.link_format, fileName, fileSize);
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return getApplicationContext().getString(R.string.link_format_kb, fileName, fileSize);
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return getApplicationContext().getString(R.string.link_format_mb, fileName, fileSize);
        }
    }


    private void reportProgress(long idx, @NonNull String title, int percent) {

        Notification notification = createNotification(title, percent);

        if (mNotificationManager != null) {
            mNotificationManager.notify((int) idx, notification);
        }

    }

    private Notification createNotification(@NonNull String content, int progress) {

        PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent main = new Intent(getApplicationContext(), MainActivity.class);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                main, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext(), CHANNEL_PROGRESS_ID)
                .setContentTitle(content)
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(R.drawable.pause, cancel, intent)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent))
                .setCategory(NotificationCompat.CATEGORY_PROGRESS)
                .setOngoing(true);

        return mBuilder.build();
    }

    @NonNull
    private ForegroundInfo createForegroundInfo(long idx, @NonNull String title, int progress) {
        Notification notification = createNotification(title, progress);
        return new ForegroundInfo((int) idx, notification);
    }

    private void closeNotification(long idx) {
        if (mNotificationManager != null) {
            mNotificationManager.cancel((int) idx);
        }
    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);

        boolean task = getInputData().getBoolean(Content.TASK, false);
        boolean success = false;

        long start = System.currentTimeMillis();

        THREADS threads = THREADS.getInstance(getApplicationContext());
        int timeout = InitApplication.getDownloadTimeout(getApplicationContext());

        LogUtils.info(TAG, " start ... " + idx);
        try {

            createChannel();

            IPFS ipfs = IPFS.getInstance(getApplicationContext());


            Note note = threads.getNoteByIdx(idx);
            Objects.requireNonNull(note);

            if (!Objects.equals(note.getWorkUUID(), getId())) {
                threads.setNoteWork(idx, getId());
            }

            String sesKey = threads.getGroupSesKey(note.getGroup());


            CID cid = note.getContent();
            Objects.requireNonNull(cid);
            threads.setNoteLeaching(idx);


            String content = getFileInfo(note);
            long size = note.getSize();
            File file = ipfs.createCacheFile(cid);

            if (task) {
                ForegroundInfo foregroundInfo = createForegroundInfo(
                        idx, content, note.getProgress());
                setForegroundAsync(foregroundInfo);
            }


            AtomicLong started = new AtomicLong(System.currentTimeMillis());
            success = ipfs.loadToFile(file, cid, sesKey, new CloseableProgress() {
                @Override
                public boolean isClosed() {

                    long diff = System.currentTimeMillis() - started.get();
                    boolean abort = !Network.isConnected(getApplicationContext())
                            || (diff > (timeout * 1000));
                    return isStopped() || abort;
                }

                public void setProgress(int percent) {
                    if (!isStopped()) {
                        threads.setNoteProgress(idx, percent);
                        reportProgress(idx, content, percent);
                        started.set(System.currentTimeMillis());
                    }
                }

            });


            if (success) {

                if (note.getNoteType() == NoteType.IMAGE) {
                    try {
                        Group group = threads.getGroup(note);
                        Objects.requireNonNull(group);
                        ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                        String text = exif.getAttribute(ExifInterface.TAG_IMAGE_DESCRIPTION);
                        if (text != null) {
                            String key = group.getSesKey();
                            String decText;
                            if (!key.isEmpty()) {
                                decText = Encryption.decrypt(text, key);
                            } else {
                                decText = new String(Base64.decode(text, Base64.DEFAULT));
                            }
                            threads.setNoteText(idx, decText);
                        }
                    } catch (Throwable e) {
                        LogUtils.error(TAG, e);
                    }
                }
                if (note.getNoteType() == NoteType.VIDEO) {
                    try {
                        Group group = threads.getGroup(note);
                        Objects.requireNonNull(group);
                        MetadataEditor mediaMeta = MetadataEditor.createFrom(file);
                        Map<String, MetaValue> keyedMeta = mediaMeta.getKeyedMeta();
                        if (keyedMeta != null) {
                            MetaValue metaValue = keyedMeta.get(Content.SYNOPSIS);
                            if (metaValue != null) {
                                String decText = Encryption.decrypt(
                                        metaValue.getString(), group.getSesKey());
                                threads.setNoteText(idx, decText);
                            }
                        }
                    } catch (Throwable e) {
                        LogUtils.error(TAG, e);
                    }
                }

                threads.setNoteSeeding(idx);

                if (size != file.length()) {
                    threads.setNoteSize(idx, file.length());
                }
                if (note.getThumbnail() == null) {
                    String mimeType = threads.getNoteMimeType(idx);
                    CID image = ThumbnailService.getThumbnail(
                            getApplicationContext(), file, mimeType);
                    if (image != null) {
                        threads.setNoteThumbnail(idx, image);
                    }
                }
            } else {
                if (file.exists()) {
                    boolean result = file.delete();
                    if (!result) {
                        LogUtils.error(TAG, "File is not deleted");
                    }
                }
            }
            threads.resetNoteLeaching(idx);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            closeNotification(idx);
            threads.resetNoteWork(idx);
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        if (success) {
            return Result.success();
        } else {
            return Result.failure();
        }
    }

}
