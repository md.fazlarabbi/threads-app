package threads.app.work;


import android.content.Context;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Objects;

import threads.LogUtils;
import threads.app.NotificationFCMServer;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.utils.FcmContent;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupJoinedWorker extends Worker {
    private static final String TAG = GroupJoinedWorker.class.getSimpleName();
    private static final String WID = "GJW";

    @SuppressWarnings("WeakerAccess")
    public GroupJoinedWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    public static void joined(@NonNull Context context, @NonNull String token,
                              @NonNull String user, @NonNull String uuid) {


        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.FCM, token);
        data.putString(Content.UUID, uuid);
        data.putString(Content.PID, user);
        OneTimeWorkRequest syncWorkRequest =
                new OneTimeWorkRequest.Builder(GroupJoinedWorker.class)
                        .addTag(TAG)
                        .setInputData(data.build())
                        .setConstraints(builder.build())
                        .build();

        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + user + uuid, ExistingWorkPolicy.KEEP, syncWorkRequest);
    }

    @NonNull
    @Override
    public Result doWork() {


        String uuid = getInputData().getString(Content.UUID);
        Objects.requireNonNull(uuid);
        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        String token = getInputData().getString(Content.FCM);
        Objects.requireNonNull(token);

        try {
            NotificationFCMServer service = NotificationFCMServer.getInstance();
            THREADS threads = THREADS.getInstance(getApplicationContext());
            User user = threads.getUserByPid(pid);
            Objects.requireNonNull(user);


            PID host = IPFS.getPID(getApplicationContext());
            Objects.requireNonNull(host);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Content.EST, FcmContent.JOINED.name());
            hashMap.put(Content.UUID, uuid);
            hashMap.put(Content.PID, pid);
            String alias = user.getAlias();
            alias = Base64.encodeToString(alias.getBytes(), Base64.DEFAULT);
            hashMap.put(Content.ALIAS, alias);
            String fcm = user.getToken();
            Objects.requireNonNull(fcm);
            hashMap.put(Content.FCM, fcm);
            String pkey = user.getPublicKey();
            Objects.requireNonNull(pkey);
            hashMap.put(Content.PKEY, pkey);


            service.sendNotification(getApplicationContext(), token, hashMap);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        }
        return Result.success();

    }
}
