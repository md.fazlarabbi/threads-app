package threads.app.work;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Objects;

import threads.LogUtils;
import threads.app.NotificationFCMServer;
import threads.app.core.Content;
import threads.app.utils.FcmContent;

public class GroupRemovedWorker extends Worker {
    private static final String TAG = GroupRemovedWorker.class.getSimpleName();
    private static final String WID = "GRW";

    @SuppressWarnings("WeakerAccess")
    public GroupRemovedWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    public static void removed(@NonNull Context context, @NonNull String token,
                               @NonNull String pid, @NonNull String uuid) {


        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.FCM, token);
        data.putString(Content.PID, pid);
        data.putString(Content.UUID, uuid);

        OneTimeWorkRequest syncWorkRequest =
                new OneTimeWorkRequest.Builder(GroupRemovedWorker.class)
                        .addTag(TAG)
                        .setInputData(data.build())
                        .setConstraints(builder.build())
                        .build();

        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + pid + uuid, ExistingWorkPolicy.KEEP, syncWorkRequest);
    }

    @NonNull
    @Override
    public Result doWork() {


        String uuid = getInputData().getString(Content.UUID);
        Objects.requireNonNull(uuid);
        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        String token = getInputData().getString(Content.FCM);
        Objects.requireNonNull(token);
        try {

            NotificationFCMServer service = NotificationFCMServer.getInstance();

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Content.EST, FcmContent.REMOVED.name());
            hashMap.put(Content.UUID, uuid);
            hashMap.put(Content.PID, pid);


            service.sendNotification(getApplicationContext(), token, hashMap);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        }
        return Result.success();

    }
}
