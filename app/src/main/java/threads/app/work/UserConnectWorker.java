package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.services.ConnectService;

public class UserConnectWorker extends Worker {

    private static final String WID = "UCW";
    private static final String TAG = UserConnectWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UserConnectWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    public static String getUniqueId(@NonNull String pid) {
        return WID + pid;
    }

    public static OneTimeWorkRequest getWork(@NonNull String pid,
                                             int timeout, boolean checkIdentity) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid);
        data.putInt(Content.TIME, timeout);
        data.putBoolean(Content.CHECK, checkIdentity);

        return new OneTimeWorkRequest.Builder(UserConnectWorker.class)
                .setInputData(data.build())
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();
    }

    public static void connect(@NonNull Context context, @NonNull String pid,
                               int timeout, boolean checkIdentity) {

        WorkManager.getInstance(context).enqueueUniqueWork(
                getUniqueId(pid), ExistingWorkPolicy.KEEP, getWork(pid, timeout, checkIdentity));
    }


    @NonNull
    @Override
    public Result doWork() {

        int timeout = getInputData().getInt(Content.TIME, 30);
        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        long start = System.currentTimeMillis();

        boolean checkIdentity = getInputData().getBoolean(Content.CHECK, false);
        LogUtils.info(TAG, " start connect [" + pid + "]...");

        try {
            ConnectService.connect(getApplicationContext(), pid, timeout, checkIdentity);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }
}

