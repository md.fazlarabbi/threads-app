package threads.app.work;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Members;
import threads.app.core.THREADS;
import threads.app.services.ConnectService;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GroupConnectWorker extends Worker {
    private static final String TAG = GroupConnectWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public GroupConnectWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    public static OneTimeWorkRequest getWork(@NonNull PID pid, long groupIdx) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, groupIdx);
        data.putString(Content.PID, pid.getPid());

        return new OneTimeWorkRequest.Builder(GroupConnectWorker.class)
                .addTag(GroupConnectWorker.TAG)
                .setConstraints(builder.build())
                .setInputData(data.build())
                .build();
    }


    @NonNull
    @Override
    public Result doWork() {


        long start = System.currentTimeMillis();

        long idx = getInputData().getLong(Content.IDX, -1);
        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);

        LogUtils.info(TAG, " start [" + idx + "]...");

        try {
            THREADS threads = THREADS.getInstance(getApplicationContext());
            PID host = IPFS.getPID(getApplicationContext());
            Objects.requireNonNull(host);
            Group group = threads.getGroupByIdx(idx);
            Objects.requireNonNull(group);


            Members members = group.getMembers();

            ExecutorService executor = Executors.newFixedThreadPool(Math.max(members.size(), 1));
            executor.submit(() -> ConnectService.connect(getApplicationContext(),
                    pid, 30, true));

            if (!members.isEmpty()) {

                for (String member : members) {
                    if (!threads.isUserBlocked(member)) {
                        if (!member.equals(pid)) {
                            executor.submit(() -> ConnectService.connect(getApplicationContext(),
                                    member, 20, true));
                        }
                    }
                }
            }
            shutdownAndAwaitTermination(executor, 45);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }

    private void shutdownAndAwaitTermination(@NonNull ExecutorService pool, int timeout) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(timeout, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(timeout, TimeUnit.SECONDS)) {
                    LogUtils.info(TAG, "Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }
}
