package threads.app.work;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.MainActivity;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.app.services.ThumbnailService;
import threads.app.utils.MimeType;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class GeneralNotifyWorker extends Worker {

    private static final String CHANNEL_NOTIFICATION_ID = "CHANNEL_NOTIFICATION_ID";
    private static final String TAG = GeneralNotifyWorker.class.getSimpleName();
    private final NotificationManager mNotificationManager;

    @SuppressWarnings("WeakerAccess")
    public GeneralNotifyWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(GeneralNotifyWorker.class)
                .addTag(TAG)
                .setInitialDelay(1, TimeUnit.MICROSECONDS)
                .setInputData(data.build())
                .build();
    }

    private static String getCompactString(@NonNull String title) {
        return title.replace("\n", " ");
    }

    private void createChannel() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {

                CharSequence name = getApplicationContext().getString(R.string.channel_name);
                String description = getApplicationContext().getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(
                        CHANNEL_NOTIFICATION_ID, name, importance);
                mChannel.setDescription(description);

                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(mChannel);
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private Bitmap getCircleBitmap(Bitmap bitmap) {
        Bitmap output;
        Rect srcRect, dstRect;
        float r;
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();

        if (width > height) {
            output = Bitmap.createBitmap(height, height, Bitmap.Config.ARGB_8888);
            int left = (width - height) / 2;
            int right = left + height;
            srcRect = new Rect(left, 0, right, height);
            dstRect = new Rect(0, 0, height, height);
            //noinspection IntegerDivisionInFloatingPointContext
            r = height / 2;
        } else {
            output = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            int top = (height - width) / 2;
            int bottom = top + width;
            srcRect = new Rect(0, top, width, bottom);
            dstRect = new Rect(0, 0, width, width);
            //noinspection IntegerDivisionInFloatingPointContext
            r = width / 2;
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, srcRect, dstRect, paint);

        bitmap.recycle();

        return output;
    }

    private Notification createNotification(long idx) {

        THREADS threads = THREADS.getInstance(getApplicationContext());
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        Note note = threads.getNoteByIdx(idx);
        Objects.requireNonNull(note);

        String mimeType = note.getMimeType();
        String text = note.getText();
        if (text.isEmpty()) {
            if (mimeType.startsWith(MimeType.AUDIO)) {
                text = getApplicationContext().getString(R.string.audio);
            } else if (mimeType.startsWith(MimeType.VIDEO)) {
                text = getApplicationContext().getString(R.string.video);
            } else if (mimeType.startsWith(MimeType.IMAGE)) {
                text = getApplicationContext().getString(R.string.image);
            } else {
                text = "";
            }
        }


        int res = R.drawable.forum_outline;

        if (note.getNoteType() == NoteType.AUDIO) {
            res = R.drawable.audio_notification;
        } else if (note.getNoteType() == NoteType.VIDEO) {
            res = R.drawable.video_notification;
        } else if (note.getNoteType() == NoteType.IMAGE) {
            res = R.drawable.image_notification;
        } else if (note.getNoteType() == NoteType.LINK) {
            res = R.drawable.file_notification;
        }


        String alias = note.getAlias();

        Bitmap bitmap = null;
        PID owner = note.getOwner();
        CID image = threads.getUserImage(owner.getPid());

        if (image != null) {
            try {
                bitmap = getCircleBitmap(
                        BitmapFactory.decodeStream(
                                ipfs.getInputStream(image, 4096)));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        if (bitmap == null) {
            bitmap = ThumbnailService.getNameImage(alias);
        }


        Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getApplicationContext(), CHANNEL_NOTIFICATION_ID);

        builder.setContentTitle(getCompactString(alias))
                .setContentIntent(pendingIntent)
                .setContentText(getCompactString(text))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setSmallIcon(res);

        return builder.build();
    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        long idx = getInputData().getLong(Content.IDX, -1);

        try {
            createChannel();
            showNotification(idx);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }

    private void showNotification(long idx) {

        try {
            Notification notification = createNotification(idx);
            mNotificationManager.notify((int) idx, notification);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }
}

