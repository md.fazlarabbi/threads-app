package threads.app.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.ipfs.IPFS;
import threads.ipfs.PID;
import threads.ipfs.Peer;
import threads.ipfs.PeerInfo;

public class UserWaitWorker extends Worker {


    private static final String TAG = UserWaitWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UserWaitWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    public static OneTimeWorkRequest getWork(@NonNull String pid) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid);

        return new OneTimeWorkRequest.Builder(UserWaitWorker.class)
                .setInputData(data.build())
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();
    }


    @NonNull
    @Override
    public Result doWork() {


        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        long start = System.currentTimeMillis();


        LogUtils.info(TAG, " start connect [" + pid + "]...");

        try {
            THREADS threads = THREADS.getInstance(getApplicationContext());
            boolean success = false;
            if (!threads.isUserBlocked(pid)) {
                success = connect(PID.create(pid));
            }

            threads.setUserDialing(pid, false);

            if (success) {
                threads.incrementRating(pid);
            } else {
                threads.decrementRating(pid);
            }

            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            if (success) {

                Peer peerInfo = ipfs.swarmPeer(PID.create(pid));

                String multiAddress = "";
                if (peerInfo != null) {
                    multiAddress = peerInfo.getMultiAddress();

                    boolean isRelay = peerInfo.isRelay();

                    if (isRelay) {
                        threads.incrementRating(pid);
                    }
                }

                if (!multiAddress.isEmpty() && !multiAddress.contains("p2p-circuit")) {

                    if (!Objects.equals(threads.getUserAddress(pid), multiAddress)) {
                        threads.setUserAddress(pid, multiAddress);
                    }
                }


                do {
                    Thread.sleep(20000);
                } while (!isStopped() && ipfs.isConnected(PID.create(pid)));

            }


        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }

    private boolean connect(@NonNull PID pid) {

        IPFS ipfs = IPFS.getInstance(getApplicationContext());

        if (ipfs.isConnected(pid)) {
            return true;
        }
        if (!isStopped()) {
            // now check old address
            THREADS peers = THREADS.getInstance(getApplicationContext());
            User user = peers.getUserByPid(pid.getPid());
            Objects.requireNonNull(user);


            String address = user.getAddress();
            if (!address.isEmpty() && !address.contains("p2p-circuit")) {
                String multiAddress = address.concat("/p2p/" + pid.getPid());

                if (ipfs.swarmConnect(multiAddress, 60)) {
                    return true;
                }
            }
        }

        if (!isStopped()) {
            if (ipfs.swarmConnect(pid, 60)) {
                return true;
            }
        }

        if (!isStopped()) {
            PeerInfo pInfo = ipfs.id(pid, 60);
            if (pInfo != null) {
                return ipfs.isConnected(pid);
            }
        }
        return false;
    }
}

