package threads.app.work;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.Progress;

public class UploadFileWorker extends Worker {
    private static final String WID = "UFW";
    private static final String TAG = UploadFileWorker.class.getSimpleName();
    private final NotificationManager mNotificationManager;

    @SuppressWarnings("WeakerAccess")
    public UploadFileWorker(@NonNull Context context,
                            @NonNull WorkerParameters params) {
        super(context, params);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private static OneTimeWorkRequest getWork(@NonNull Uri uri, long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);
        data.putString(Content.URI, uri.toString());

        return new OneTimeWorkRequest.Builder(UploadFileWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void upload(@NonNull Context context, @NonNull Uri uri, long idx) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + idx, ExistingWorkPolicy.KEEP, getWork(uri, idx));

    }

    private void closeNotification(long idx) {
        if (mNotificationManager != null) {
            mNotificationManager.cancel((int) idx);
        }
    }

    private void reportProgress(long idx, @NonNull String title, int percent) {

        Notification notification = createNotification(title, percent);

        if (mNotificationManager != null) {
            mNotificationManager.notify((int) idx, notification);
        }

    }

    private void createChannel(@NonNull Context context) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            try {
                CharSequence name = context.getString(R.string.file_channel_name);
                String description = context.getString(R.string.file_channel_description);

                NotificationChannel mChannel = new NotificationChannel(TAG, name,
                        NotificationManager.IMPORTANCE_HIGH);
                mChannel.setDescription(description);

                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(mChannel);
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }

    private Notification createNotification(@NonNull String content, int progress) {

        createChannel(getApplicationContext());

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext(), TAG)
                .setContentText(content)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setTicker(content)
                .setSmallIcon(R.drawable.upload)
                .setCategory(NotificationCompat.CATEGORY_PROGRESS)
                .setOngoing(true);

        return mBuilder.build();
    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);
        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            THREADS threads = THREADS.getInstance(getApplicationContext());


            Note note = threads.getNoteByIdx(idx);
            Objects.requireNonNull(note);

            CID cid = note.getContent();
            Objects.requireNonNull(cid);

            String skey = threads.getGroupSesKey(note.getGroup());

            long size = note.getSize();

            boolean showProgress = size > 100 * 1000 * 1000; // 100 MB

            OutputStream os = getApplicationContext().getContentResolver().
                    openOutputStream(Uri.parse(uri));
            if (os != null) {
                try {
                    ipfs.storeToOutputStream(os, new Progress() {
                        @Override
                        public long getSize() {
                            return size;
                        }

                        @Override
                        public void setProgress(int percent) {
                            if (showProgress) {
                                reportProgress(idx, note.getName(), percent);
                            }
                        }

                        @Override
                        public boolean isClosed() {
                            return false;
                        }
                    }, cid, skey, 4096);
                } catch (Throwable e) {
                    LogUtils.error(TAG, e);
                } finally {
                    os.close();
                }
                closeNotification(idx);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }
}
