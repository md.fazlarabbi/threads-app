package threads.app.work;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.ipfs.CID;
import threads.ipfs.IPFS;

public class GroupDeleteWorker extends Worker {

    private static final String TAG = GroupDeleteWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public GroupDeleteWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    private static OneTimeWorkRequest getWork(long... idxs) {

        Data.Builder data = new Data.Builder();
        data.putLongArray(Content.IDXS, idxs);

        return new OneTimeWorkRequest.Builder(GroupDeleteWorker.class)
                .addTag(TAG)
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .setInputData(data.build())
                .build();
    }

    public static void delete(@NonNull Context context, long... idxs) {
        WorkManager.getInstance(context).enqueue(getWork(idxs));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        long[] idxs = getInputData().getLongArray(Content.IDXS);
        Objects.requireNonNull(idxs);
        try {
            THREADS threads = THREADS.getInstance(getApplicationContext());
            List<Group> groups = new ArrayList<>();
            for (long idx : idxs) {
                Group group = threads.getGroupByIdx(idx);
                Objects.requireNonNull(group);
                if (group.isDeleting()) {
                    threads.setGroupDeleting(group);
                    groups.add(group);
                }
            }

            for (Group group : groups) {
                for (String member : group.getMembers()) {
                    User user = threads.getUserByPid(member);
                    if (user != null && user.hasToken() && !user.isBlocked()) {
                        String token = user.getToken();
                        Objects.requireNonNull(token);
                        GroupLeaveWorker.leave(getApplicationContext(), token, group.getUuid());
                    }
                }
                removeGroup(group);
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }


    private void removeGroup(@NonNull Group group) {

        THREADS threads = THREADS.getInstance(getApplicationContext());
        IPFS ipfs = IPFS.getInstance(getApplicationContext());

        threads.removeGroup(group);


        try {
            CID cid = group.getImage();
            if (cid != null) {
                if (!threads.isReferenced(cid)) {
                    ipfs.rm(cid);
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }


    }


}
