package threads.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import java.util.Objects;

import threads.LogUtils;
import threads.app.core.Content;
import threads.app.core.THREADS;
import threads.app.fragments.NotesFragment;
import threads.app.model.SelectionViewModel;
import threads.app.services.MimeTypeService;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class NotesActivity extends AppCompatActivity {

    private static final String TAG = NotesActivity.class.getSimpleName();

    private PID owner;
    private PID host;
    private long groupIdx;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notes_activity, menu);

        MenuItem actionInvite = menu.findItem(R.id.action_invite);
        actionInvite.setVisible(Objects.equals(host, owner));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();


        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notes);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        SelectionViewModel mSelectionViewModel =
                new ViewModelProvider(this).get(SelectionViewModel.class);


        Intent intent = getIntent();

        host = IPFS.getPID(getApplicationContext());
        String title = intent.getStringExtra(Content.NAME);
        Objects.requireNonNull(title);
        groupIdx = intent.getLongExtra(Content.IDX, 0);
        mSelectionViewModel.setGroup(groupIdx);

        String ownerPid = intent.getStringExtra(Content.PID);
        Objects.requireNonNull(ownerPid);
        owner = PID.create(ownerPid);

        setTitle(MimeTypeService.getCompactString(title));


        if (Objects.equals(owner, host)) {
            toolbar.setSubtitle(getString(R.string.owner_session,
                    InitApplication.getAlias(getApplicationContext())));
        } else {
            String alias = THREADS.getInstance(getApplicationContext()).getUserAlias(ownerPid);
            toolbar.setSubtitle(getString(R.string.owner_session, alias));
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(
                    R.id.fragment_notes_view, NotesFragment.newInstance()).commit();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            THREADS.getInstance(getApplicationContext()).resetGroupsNumber(groupIdx);
            THREADS.getInstance(getApplicationContext()).resetGroupUnread(groupIdx);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }
}
