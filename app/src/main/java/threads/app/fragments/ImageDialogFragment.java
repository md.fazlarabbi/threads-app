package threads.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;

import java.util.Objects;

import threads.app.R;
import threads.app.core.Content;
import threads.app.utils.IPFSData;
import threads.ipfs.CID;
import threads.ipfs.IPFS;

public class ImageDialogFragment extends DialogFragment {
    public static final String TAG = ImageDialogFragment.class.getSimpleName();

    private Context mContext;
    private FragmentActivity mActivity;

    public static ImageDialogFragment newInstance(@NonNull String cid) {


        Bundle bundle = new Bundle();

        bundle.putString(Content.CID, cid);

        ImageDialogFragment fragment = new ImageDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }


    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        String cid = args.getString(Content.CID);
        Objects.requireNonNull(cid);

        LayoutInflater inflater = mActivity.getLayoutInflater();


        @SuppressWarnings("all")
        View view = inflater.inflate(R.layout.image_view, null);

        ImageView mImageView = view.findViewById(R.id.image_view);
        IPFS ipfs = IPFS.getInstance(mContext);

        IPFSData data = IPFSData.create(ipfs, CID.create(cid));
        Glide.with(mContext).load(data).into(mImageView);


        builder.setView(view);


        Dialog dialog = builder.create();

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        Window window = dialog.getWindow();
        if (window != null) {
            window.getAttributes().windowAnimations = R.style.DialogTopAnimation;
            window.getAttributes().gravity = Gravity.TOP | Gravity.CENTER;
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        handleFullRatio(mImageView);


        return dialog;
    }

    private void handleFullRatio(@NonNull ImageView imageView) {


        DisplayMetrics metrics = new DisplayMetrics();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);


            Configuration config = getResources().getConfiguration();
            int screenHeight = metrics.heightPixels;
            if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
                screenHeight = (screenHeight * 6) / 10;
            }

            int screenWidth = metrics.widthPixels;

            ViewGroup.LayoutParams lp = imageView.getLayoutParams();

            lp.width = screenWidth;
            lp.height = screenHeight;

            imageView.setLayoutParams(lp);
        }
    }

}
