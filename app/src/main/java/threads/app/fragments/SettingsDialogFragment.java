package threads.app.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import threads.app.InitApplication;
import threads.app.R;


public class SettingsDialogFragment extends DialogFragment {

    public static final String TAG = SettingsDialogFragment.class.getSimpleName();


    private Context mContext;
    private FragmentActivity mActivity;

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
    }


    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        LayoutInflater inflater = mActivity.getLayoutInflater();


        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.settings_view, null);

        TextView automatic_delete_mode_text = view.findViewById(R.id.automatic_delete_mode_text);


        String auto_delete_html = getString(R.string.automatic_delete_mode_text);
        automatic_delete_mode_text.setTextAppearance(android.R.style.TextAppearance_Small);
        automatic_delete_mode_text.setText(Html.fromHtml(auto_delete_html, Html.FROM_HTML_MODE_LEGACY));

        Switch automatic_delete_mode = view.findViewById(R.id.automatic_delete_mode);
        automatic_delete_mode.setChecked(InitApplication.isAutoDelete(mContext));
        automatic_delete_mode.setOnCheckedChangeListener((buttonView, isChecked) ->
                InitApplication.setAutoDelete(mContext, isChecked)
        );

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(view)
                .setCancelable(false)
                .create();
        Dialog dialog = builder.create();
        Window window = dialog.getWindow();
        if (window != null) {
            window.getAttributes().windowAnimations = R.style.DialogRightAnimation;
            window.getAttributes().gravity = Gravity.CENTER;
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        return dialog;
    }
}
