package threads.app.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.ByteArrayOutputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import threads.LogUtils;
import threads.app.R;
import threads.app.model.SelectionViewModel;
import threads.app.services.NoteService;
import threads.app.utils.IPFSData;
import threads.ipfs.CID;
import threads.ipfs.IPFS;

@SuppressWarnings("WeakerAccess")
public class PreviewDialogFragment extends BottomSheetDialogFragment {
    public static final String TAG = PreviewDialogFragment.class.getSimpleName();
    private static final int CLICK_OFFSET = 500;
    private Context mContext;
    private long mLastClickTime = 0;
    private TextView mChatBox;
    private ImageView mImageView;
    private FragmentActivity mActivity;
    private CID image;

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.preview, container, false);

        mImageView = view.findViewById(R.id.image_view);
        mChatBox = view.findViewById(R.id.chat_box);
        ImageView image_video = view.findViewById(R.id.image_video);
        TextView sendAction = view.findViewById(R.id.chat_action);

        TextView abortAction = view.findViewById(R.id.abort_action);

        AtomicBoolean video = new AtomicBoolean(false);
        AtomicReference<String> mimeType = new AtomicReference<>(null);
        AtomicReference<Uri> uri = new AtomicReference<>(null);
        AtomicReference<String> name = new AtomicReference<>(null);
        SelectionViewModel mSelectionViewModel = new ViewModelProvider(mActivity).get(SelectionViewModel.class);


        mSelectionViewModel.getPreviewData().observe(getViewLifecycleOwner(), (data) -> {
            if (data != null) {
                uri.set(data.getUri());
                video.set(data.isVideo());
                mimeType.set(data.getMimeType());
                name.set(data.getName());


                if (video.get()) {
                    image_video.setImageResource(R.drawable.video_white);
                } else {
                    image_video.setImageResource(R.drawable.camera_white);
                }

                IPFS ipfs = IPFS.getInstance(mContext);


                Glide.with(mContext)
                        .asBitmap()
                        .load(uri.get())
                        .into(new CustomTarget<Bitmap>(200, 200) {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                resource.compress(Bitmap.CompressFormat.WEBP, 100, stream);

                                image = ipfs.storeData(stream.toByteArray(), "");
                                sendAction.setEnabled(true);
                                mActivity.runOnUiThread(() -> loadImage());
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            }
        });

        abortAction.setOnClickListener((v) -> dismiss());

        sendAction.setEnabled(false);
        sendAction.setOnClickListener((v) -> {
            try {
                if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                removeKeyboards();

                CharSequence text = mChatBox.getText();

                MutableLiveData<Long> group = mSelectionViewModel.getGroup();
                Long groupIdx = group.getValue();
                Objects.requireNonNull(groupIdx);

                if (video.get()) {
                    if (text != null) {
                        NoteService.storeVideo(mContext, uri.get(), image, text.toString(),
                                name.get(), mimeType.get(), groupIdx);
                    } else {
                        NoteService.storeVideo(mContext, uri.get(), image,
                                "", name.get(), mimeType.get(), groupIdx);
                    }
                } else {
                    if (text != null) {
                        NoteService.storeImage(mContext, uri.get(), image, text.toString(),
                                name.get(), mimeType.get(), groupIdx);
                    } else {
                        NoteService.storeImage(mContext, uri.get(), image,
                                "", name.get(), mimeType.get(), groupIdx);
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                dismiss();
            }
        });

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.setCanceledOnTouchOutside(false);
        }

        return view;
    }

    private void loadImage() {

        try {
            //noinspection unchecked
            MultiTransformation transform = new MultiTransformation(
                    new CenterCrop(), new RoundedCorners(10));
            Objects.requireNonNull(transform);

            IPFS ipfs = IPFS.getInstance(mContext);
            IPFSData imageData = IPFSData.create(ipfs, image);
            //noinspection unchecked
            Glide.with(mContext).
                    load(imageData).
                    apply(new RequestOptions().transform(transform)).
                    into(mImageView);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void removeKeyboards() {
        try {
            InputMethodManager imm = (InputMethodManager)
                    mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mChatBox.getWindowToken(), 0);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }


}
