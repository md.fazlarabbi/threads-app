package threads.app.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.EVENTS;
import threads.app.services.ContactService;
import threads.app.utils.Network;
import threads.ipfs.IPFS;
import threads.ipfs.PID;


public class EditPeerDialogFragment extends DialogFragment {
    public static final String TAG = EditPeerDialogFragment.class.getSimpleName();
    private static final int REQUEST_CAMERA_CAPTURE = 1;
    private final AtomicBoolean notPrintErrorMessages = new AtomicBoolean(false);
    private long mLastClickTime = 0;
    private TextInputLayout edit_multihash_layout;
    private TextInputEditText mMultihash;
    private Context mContext;
    private FragmentActivity mActivity;
    private boolean hasCamera;

    public static EditPeerDialogFragment newInstance() {
        return new EditPeerDialogFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
        PackageManager pm = mContext.getPackageManager();
        hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }


    private void isValidMultihash(Dialog dialog) {
        if (dialog instanceof AlertDialog) {
            AlertDialog alertDialog = (AlertDialog) dialog;
            Editable text = mMultihash.getText();
            Objects.requireNonNull(text);
            String multi = text.toString();


            boolean result = !multi.isEmpty();

            if (result) {
                IPFS ipfs = IPFS.getInstance(mContext);
                result = ipfs.isValidPID(multi);
            }


            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(result);


            if (!notPrintErrorMessages.get()) {

                if (multi.isEmpty()) {
                    edit_multihash_layout.setError(getString(R.string.account_identification_not_valid));
                } else {
                    edit_multihash_layout.setError(null);
                }

            } else {
                edit_multihash_layout.setError(null);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult
            (int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_CAPTURE) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    EVENTS.getInstance(mContext).postPermission(
                            getString(R.string.permission_camera_denied));
                }
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    invokeScan();
                }
            }
        }

    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);


        LayoutInflater inflater = mActivity.getLayoutInflater();


        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.peer_view, null);
        edit_multihash_layout = view.findViewById(R.id.edit_account_layout);
        mMultihash = view.findViewById(R.id.multihash);
        mMultihash.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isValidMultihash(getDialog());
            }
        });

        TextView mScanAccount = view.findViewById(R.id.scan_account);
        if (!hasCamera) {
            mScanAccount.setVisibility(View.GONE);
        } else {
            mScanAccount.setOnClickListener((v) -> clickInvokeScan());
        }

        builder.setView(view)
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    removeKeyboards();


                    Editable text = mMultihash.getText();
                    Objects.requireNonNull(text);
                    String hash = text.toString();

                    clickConnectPeer(hash);


                })
                .setTitle(getString(R.string.add_new_account));


        Dialog dialog = builder.create();

        Window window = dialog.getWindow();
        if (window != null) {
            window.getAttributes().windowAnimations = R.style.DialogTopAnimation;
            window.getAttributes().gravity = Gravity.TOP | Gravity.CENTER;
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents() != null) {
                    String content = result.getContents();
                    mMultihash.setText(content);
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

    }

    private void clickInvokeScan() {

        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_CAPTURE);
            return;
        }

        invokeScan();
    }

    private void invokeScan() {
        try {
            PackageManager pm = mContext.getPackageManager();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
                integrator.setOrientationLocked(false);
                integrator.initiateScan();
            } else {
                EVENTS.getInstance(mContext).postError(getString(R.string.feature_camera_required));
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }


    private void removeKeyboards() {

        try {
            InputMethodManager imm = (InputMethodManager)
                    mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mMultihash.getWindowToken(), 0);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        removeKeyboards();
    }

    @Override
    public void onResume() {
        super.onResume();
        notPrintErrorMessages.set(true);
        isValidMultihash(getDialog());
        notPrintErrorMessages.set(false);
    }


    private void clickConnectPeer(@NonNull String pid) {
        try {
            // CHECKED if pid is valid
            IPFS ipfs = IPFS.getInstance(mContext);
            if (!ipfs.isValidPID(pid)) {
                EVENTS.getInstance(mContext).postError(getString(R.string.account_identification_not_valid));
                return;
            }

            // CHECKED
            PID host = IPFS.getPID(mContext);
            PID user = PID.create(pid);

            if (user.equals(host)) {
                EVENTS.getInstance(mContext).postError(getString(R.string.same_pid_like_host));
                return;
            }

            // CHECKED
            if (!Network.isConnected(mContext)) {
                EVENTS.getInstance(mContext).postWarning(getString(R.string.offline_mode));
            }


            try {
                ContactService.contact(mContext, pid);
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        } finally {
            dismiss();
        }
    }

}

