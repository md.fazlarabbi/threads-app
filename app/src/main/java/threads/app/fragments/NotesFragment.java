package threads.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ShareCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.ExoPlayerActivity;
import threads.app.ImageActivity;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.EVENTS;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.NoteType;
import threads.app.core.THREADS;
import threads.app.model.EventViewModel;
import threads.app.model.GroupNotesViewModel;
import threads.app.model.SelectionViewModel;
import threads.app.provider.FileDocumentsProvider;
import threads.app.services.NoteService;
import threads.app.utils.Network;
import threads.app.utils.NotesViewAdapter;
import threads.app.utils.PermissionAction;
import threads.app.work.ConnectionWorker;
import threads.app.work.DownloadMessageWorker;
import threads.app.work.DownloadNoteWorker;
import threads.app.work.GroupConnectWorker;
import threads.app.work.GroupNoteWorker;
import threads.app.work.GroupWakeupWorker;
import threads.app.work.UploadFileWorker;
import threads.app.work.UploadNoteWorker;
import threads.ipfs.CID;
import threads.ipfs.IPFS;

public class NotesFragment extends Fragment implements NotesViewAdapter.NotesViewAdapterListener {


    private static final String TAG = NotesFragment.class.getSimpleName();


    private static final int FILE_EXPORT_REQUEST_CODE = 0;
    private static final int CLICK_OFFSET = 500;

    private long mLastClickTime = 0;
    private RecyclerView mRecyclerView;
    private NotesViewAdapter mNotesViewAdapter;
    private EditText mChatBox;
    private ImageView mChatAction;
    private long noteIdx;
    private LinearLayout mChatBoxContainer;
    private FragmentActivity mActivity;
    private Context mContext;
    private SelectionViewModel mSelectionViewModel;

    public static NotesFragment newInstance() {
        return new NotesFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notes_view, container, false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_invite) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return true;
            }
            mLastClickTime = SystemClock.elapsedRealtime();


            inviteUsersDialog();

            return true;

        } else if (id == R.id.action_members) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return true;
            }
            mLastClickTime = SystemClock.elapsedRealtime();


            membersDialog();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        LinearLayout mNotesLayout = view.findViewById(R.id.notes_layout);


        mSelectionViewModel = new ViewModelProvider(mActivity).get(SelectionViewModel.class);


        mSelectionViewModel.getPreviewData().observe(getViewLifecycleOwner(), (data) -> {
            if (data != null) {
                PreviewDialogFragment dialogFragment = new PreviewDialogFragment();
                dialogFragment.show(getChildFragmentManager(), PreviewDialogFragment.TAG);
            }
        });

        EventViewModel eventViewModel = new ViewModelProvider(mActivity).get(EventViewModel.class);


        eventViewModel.getError().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mNotesLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (v) -> snackbar.dismiss());
                        snackbar.setAnchorView(mChatBox);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });


        eventViewModel.getWarning().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Toast.makeText(mContext, content, Toast.LENGTH_LONG).show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });


        eventViewModel.getPermission().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mNotesLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.app_settings, new PermissionAction());
                        snackbar.setAnchorView(mChatBoxContainer);
                        snackbar.show();

                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });

        mChatBoxContainer = view.findViewById(R.id.chat_box_container);
        mChatAction = view.findViewById(R.id.chat_action);
        mChatBox = view.findViewById(R.id.chat_box);

        mChatBoxContainer.setVisibility(View.VISIBLE);


        mChatBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    mChatAction.setImageResource(R.drawable.send);
                } else {
                    mChatAction.setImageResource(R.drawable.paperclip);
                }


            }
        });

        mRecyclerView = view.findViewById(R.id.recycler_view_messages);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setSmoothScrollbarEnabled(true);
        linearLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mNotesViewAdapter = new NotesViewAdapter(mContext, this);
        mRecyclerView.setAdapter(mNotesViewAdapter);

        mChatAction.setOnClickListener((v) -> {


            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            removeKeyboards();


            String text = mChatBox.getText().toString();
            if (!text.isEmpty()) {
                createMessage(text);
            } else {
                mNotesViewAdapter.stopAudioHolder();
                NotesActionDialogFragment.newInstance().show(
                        getChildFragmentManager(), NotesActionDialogFragment.TAG);

            }

        });


        GroupNotesViewModel notificationsViewModel =
                new ViewModelProvider(this).get(GroupNotesViewModel.class);

        mSelectionViewModel.getGroup().observe(getViewLifecycleOwner(), (value) -> {
            if (value != null) {
                if (value == 0) {
                    mChatBoxContainer.setVisibility(View.GONE);
                    mNotesViewAdapter.updateNotes(new ArrayList<>());
                } else {
                    mChatBoxContainer.setVisibility(View.VISIBLE);


                    notificationsViewModel.getNotes(value).observe(getViewLifecycleOwner(), (notifications) -> {
                        if (notifications != null) {


                            notifications.sort((o1, o2) -> {

                                int val = Objects.compare(o1, o2,
                                        Comparator.comparing(Note::isUnread));

                                if (val == 0) {
                                    return Objects.compare(o1, o2,
                                            Comparator.comparingLong(Note::getDate));
                                }
                                return val;

                            });

                            int before = mNotesViewAdapter.getItemCount();

                            mNotesViewAdapter.updateNotes(notifications);
                            int position = mNotesViewAdapter.getItemCount();

                            if (position > before) {
                                mRecyclerView.scrollToPosition(position - 1);
                            }
                        }

                    });
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            MutableLiveData<Long> group = mSelectionViewModel.getGroup();
            Objects.requireNonNull(group);
            Long groupIdx = group.getValue();
            Objects.requireNonNull(groupIdx);
            THREADS.getInstance(mContext).resetGroupsNumber(groupIdx);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mNotesViewAdapter.onDestroy();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void removeKeyboards() {
        try {
            InputMethodManager imm = (InputMethodManager)
                    mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mChatBox.getWindowToken(), 0);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void createMessage(final @NonNull String text) {

        try {
            MutableLiveData<Long> group = mSelectionViewModel.getGroup();
            Objects.requireNonNull(group);
            Long groupIdx = group.getValue();
            Objects.requireNonNull(groupIdx);

            NoteService.storeText(mContext, groupIdx, text);
            mChatBox.setText("");
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void inviteUsersDialog() {

        try {
            ContactsDialogFragment dialogFragment = new ContactsDialogFragment();

            MutableLiveData<Long> group = mSelectionViewModel.getGroup();
            Objects.requireNonNull(group);
            Long groupIdx = group.getValue();
            Objects.requireNonNull(groupIdx);

            Bundle bundle = new Bundle();
            bundle.putLong(Content.IDX, groupIdx);
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getChildFragmentManager(), ContactsDialogFragment.TAG);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void membersDialog() {

        try {
            MembersDialogFragment dialogFragment = new MembersDialogFragment();

            MutableLiveData<Long> group = mSelectionViewModel.getGroup();
            Objects.requireNonNull(group);
            Long groupIdx = group.getValue();
            Objects.requireNonNull(groupIdx);

            Bundle bundle = new Bundle();
            bundle.putLong(Content.IDX, groupIdx);
            dialogFragment.setArguments(bundle);
            dialogFragment.show(getChildFragmentManager(), MembersDialogFragment.TAG);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    @Override
    public void invokeCopyTo(@NonNull Note note) {
        noteIdx = note.getIdx();

        try {

            String name = note.getName();

            Intent intent = ShareCompat.IntentBuilder.from(mActivity).getIntent();
            intent.setType(note.getMimeType());
            intent.setAction(Intent.ACTION_CREATE_DOCUMENT);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(Intent.EXTRA_TITLE, name);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);


            if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                startActivityForResult(intent, FILE_EXPORT_REQUEST_CODE);
            } else {
                EVENTS.getInstance(mContext).postError(
                        getString(R.string.no_activity_found_to_handle_uri));
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        try {
            if (requestCode == FILE_EXPORT_REQUEST_CODE) {
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        UploadFileWorker.upload(mContext, uri, noteIdx);
                    }
                }
                return;
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void invokeAction(@NonNull Note note) {

        try {

            if (note.getNoteType() == NoteType.LINK) {
                CID cid = note.getContent();
                Objects.requireNonNull(cid, getString(R.string.content_not_available));

                String mimeType = note.getMimeType();
                String name = note.getName();

                Group group = THREADS.getInstance(mContext).getGroup(note);
                Objects.requireNonNull(group);
                Uri uri = FileDocumentsProvider.getUriForNote(note);


                if (MimeTypes.isVideo(mimeType) && !group.isEncrypted()) {
                    Intent intent = new Intent(mContext, ExoPlayerActivity.class);
                    intent.putExtra(Content.URI, uri.toString());
                    startActivity(intent);
                } else {
                    Intent intent = ShareCompat.IntentBuilder.from(mActivity)
                            .setStream(uri)
                            .setType(mimeType)
                            .getIntent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.putExtra(Intent.EXTRA_TITLE, name);
                    intent.setData(uri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                    if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                        startActivity(intent);
                    } else {
                        EVENTS.getInstance(mContext).postError(
                                getString(R.string.no_activity_found_to_handle_uri));
                    }
                }
            } else {
                Uri uri = FileDocumentsProvider.getUriForNote(note);

                if (note.getNoteType() == NoteType.VIDEO) {
                    Intent intent = new Intent(mContext, ExoPlayerActivity.class);
                    intent.putExtra(Content.URI, uri.toString());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, ImageActivity.class);
                    intent.putExtra(Content.URI, uri.toString());
                    startActivity(intent);
                }
            }
        } catch (Throwable e) {
            EVENTS.getInstance(mContext).postError("" + e.getLocalizedMessage());
        } finally {
            mNotesViewAdapter.stopAudioHolder();
        }

    }


    @Override
    public void onPause() {
        super.onPause();
        removeKeyboards();
    }


    @Override
    public void invokeDownloadAction(@NonNull Note note) {


        if (!Network.isConnected(mContext)) {
            EVENTS.getInstance(mContext).postWarning(
                    getString(R.string.offline_mode));
        } else {


            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> THREADS.getInstance(mContext).setNoteLeaching(note.getIdx()));

            GroupWakeupWorker.wakeup(mContext, note.getOwner().getPid());

            THREADS threads = THREADS.getInstance(mContext);
            IPFS ipfs = IPFS.getInstance(mContext);
            if (ipfs.isDaemonRunning()) {
                if (ipfs.isConnected(note.getOwner())) {
                    if (note.getNoteType() == NoteType.MESSAGE) {
                        OneTimeWorkRequest work = DownloadMessageWorker.getWork(note.getIdx());
                        threads.setNoteWork(note.getIdx(), work.getId());

                        WorkManager.getInstance(mContext).enqueue(work);
                    } else {

                        OneTimeWorkRequest work = DownloadNoteWorker.getWork(note.getIdx(),
                                DownloadNoteWorker.isTask(note.getSize()));
                        threads.setNoteWork(note.getIdx(), work.getId());

                        WorkManager.getInstance(mContext).enqueue(work);

                    }
                    return;
                }
            }

            if (note.getNoteType() == NoteType.MESSAGE) {
                OneTimeWorkRequest work = DownloadMessageWorker.getWork(note.getIdx());
                threads.setNoteWork(note.getIdx(), work.getId());

                WorkManager.getInstance(mContext).beginWith(ConnectionWorker.getWork())
                        .then(GroupConnectWorker.getWork(note.getOwner(), note.getGroup()))
                        .then(work).enqueue();

            } else {
                OneTimeWorkRequest work = DownloadNoteWorker.getWork(note.getIdx(),
                        DownloadNoteWorker.isTask(note.getSize()));
                threads.setNoteWork(note.getIdx(), work.getId());

                WorkManager.getInstance(mContext).beginWith(ConnectionWorker.getWork())
                        .then(GroupConnectWorker.getWork(note.getOwner(), note.getGroup()))
                        .then(work)
                        .enqueue();
            }
        }
    }

    @Override
    public void invokePauseAction(@NonNull Note note) {

        if (!Objects.equals(note.getOwner(), IPFS.getPID(mContext))) {
            if (!Network.isConnected(mContext)) {
                EVENTS.getInstance(mContext).postWarning(
                        getString(R.string.offline_mode));
            } else {

                Executors.newSingleThreadExecutor().submit(() ->
                        THREADS.getInstance(mContext).resetNoteLeaching(note.getIdx()));

                UUID uuid = note.getWorkUUID();
                if (uuid != null) {
                    WorkManager.getInstance(mContext).cancelWorkById(uuid);
                }
            }
        } else {

            UUID uuid = note.getWorkUUID();
            if (uuid != null) {
                WorkManager.getInstance(mContext).cancelWorkById(uuid);
            }

            Executors.newSingleThreadExecutor().submit(() -> {
                THREADS threads = THREADS.getInstance(mContext);
                threads.resetNoteLeaching(note.getIdx());
            });
        }
    }

    @Override
    public void invokeUploadAction(@NonNull Note note) {

        THREADS threads = THREADS.getInstance(mContext);
        Group group = threads.getGroup(note);
        Objects.requireNonNull(group);
        if (group.hasMembers()) {
            OneTimeWorkRequest request = UploadNoteWorker.getWork(note.getIdx());
            UUID uuid = request.getId();
            WorkManager.getInstance(mContext).beginWith(request)
                    .then(GroupNoteWorker.getWork(note.getIdx()))
                    .then(ConnectionWorker.getWork())
                    .then(GroupConnectWorker.getWork(note.getOwner(), note.getGroup()))
                    .enqueue();

            Executors.newSingleThreadExecutor().submit(() -> {
                threads.setNoteLeaching(note.getIdx());
                threads.setNoteWork(note.getIdx(), uuid);
            });

            if (!Network.isConnected(mContext)) {
                EVENTS.getInstance(mContext).warning(
                        getString(R.string.offline_mode));
            }
        } else {

            UUID uuid = UploadNoteWorker.upload(mContext, note.getIdx());

            Executors.newSingleThreadExecutor().submit(() -> {
                threads.setNoteLeaching(note.getIdx());
                threads.setNoteWork(note.getIdx(), uuid);
            });
        }
    }

    @Override
    public void invokeDeleteAction(@NonNull Note note) {
        Executors.newSingleThreadExecutor().submit(() ->
                THREADS.getInstance(mContext).setNoteDeleting(note.getIdx()));
    }


}
