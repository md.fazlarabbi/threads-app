package threads.app.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.EVENTS;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.model.UsersViewModel;
import threads.app.services.UserService;
import threads.app.utils.Network;
import threads.app.utils.UserItemDetailsLookup;
import threads.app.utils.UserItemKeyProvider;
import threads.app.utils.UserViewAdapter;
import threads.app.work.ConnectionWorker;
import threads.app.work.UserConnectWorker;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;


public class UsersFragment extends Fragment implements FabFragment,
        SwipeRefreshLayout.OnRefreshListener, UserViewAdapter.UsersViewAdapterListener {

    private static final String TAG = UsersFragment.class.getSimpleName();
    private static final int CLICK_OFFSET = 500;
    @NonNull
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private final AtomicBoolean run = new AtomicBoolean(false);
    private long mLastClickTime = 0;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private UserViewAdapter mUsersViewAdapter;
    private Context mContext;
    private SelectionTracker<String> mSelectionTracker;
    private ActionMode mActionMode;
    private UsersFragment.ActionListener mListener;
    private FragmentActivity mActivity;
    private boolean isTablet;
    private FloatingActionButton mMainFab;

    private static void checkPeers(@NonNull Context context) {

        try {

            THREADS threads = THREADS.getInstance(context);
            IPFS ipfs = IPFS.getInstance(context);
            List<String> users = threads.getUsersPids();

            for (String user : users) {
                if (!threads.isUserBlocked(user) && !threads.getUserDialing(user)) {

                    try {
                        boolean value = ipfs.isConnected(PID.create(user));

                        boolean preValue = threads.isUserConnected(user);
                        if (preValue != value) {
                            threads.setUserConnected(user, value);
                        }

                    } catch (Throwable e) {
                        LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                        threads.setUserConnected(user, false);
                    }
                }
            }


        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        run.set(false);
        mContext = null;
        mListener = null;
        mActivity = null;

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
        mListener = (UsersFragment.ActionListener) mActivity;
        isTablet = getResources().getBoolean(R.bool.isTablet);
        run.set(true);
        peersOnlineStatus();
    }

    // todo remove and change to event mechanism
    private void peersOnlineStatus() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                while (run.get()) {
                    checkPeers(mContext);
                    java.lang.Thread.sleep(1000);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mSelectionTracker.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_peers_fragment, menu);

        MenuItem actionEditPid = menu.findItem(R.id.action_add_pid);
        actionEditPid.setVisible(isTablet);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_add_pid) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return true;
            }

            mLastClickTime = SystemClock.elapsedRealtime();

            clickAddContact();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.peers_view, container, false);


        mMainFab = view.findViewById(R.id.fab_user);
        if (isTablet) {
            mMainFab.setVisibility(View.GONE);
        }
        mMainFab.setOnClickListener((v) -> {

            // mis-clicking prevention, using threshold of 1000 ms
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            clickAddContact();
        });


        RecyclerView mRecyclerView = view.findViewById(R.id.recycler_users);


        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean hasSelection = mSelectionTracker.hasSelection();
                if (!isTablet) {
                    if (dy > 0 && !hasSelection) {
                        showFab(false);
                    } else if (dy < 0 && !hasSelection) {
                        showFab(true);
                    }
                }
            }
        });


        LinearLayoutManager linearLayout = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayout);
        mUsersViewAdapter = new UserViewAdapter(mContext, this);
        mRecyclerView.setAdapter(mUsersViewAdapter);

        final PID host = IPFS.getPID(mContext);


        mSelectionTracker = new SelectionTracker.Builder<>(
                "user-selection",//unique id
                mRecyclerView,
                new UserItemKeyProvider(mUsersViewAdapter),
                new UserItemDetailsLookup(mRecyclerView),
                StorageStrategy.createStringStorage())
                .build();


        mSelectionTracker.addObserver(new SelectionTracker.SelectionObserver<String>() {
            @Override
            public void onSelectionChanged() {
                if (!mSelectionTracker.hasSelection()) {
                    if (mActionMode != null) {
                        mActionMode.finish();
                    }
                } else {
                    if (mActionMode == null) {
                        mActionMode = ((AppCompatActivity) mActivity).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (mActionMode != null) {
                    mActionMode.setTitle("" + mSelectionTracker.getSelection().size());
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!mSelectionTracker.hasSelection()) {
                    if (mActionMode != null) {
                        mActionMode.finish();
                    }
                } else {
                    if (mActionMode == null) {
                        mActionMode = ((AppCompatActivity) mActivity).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (mActionMode != null) {
                    mActionMode.setTitle("" + mSelectionTracker.getSelection().size());
                }
                super.onSelectionRestored();
            }
        });

        mUsersViewAdapter.setSelectionTracker(mSelectionTracker);


        if (savedInstanceState != null) {
            mSelectionTracker.onRestoreInstanceState(savedInstanceState);
        }


        UsersViewModel messagesViewModel = new ViewModelProvider(this).get(UsersViewModel.class);
        messagesViewModel.getVisibleUsers().observe(getViewLifecycleOwner(), (users) -> {

            try {
                if (users != null) {
                    List<User> peers = new ArrayList<>();
                    for (User user : users) {
                        if (!user.getPID().equals(host)) {
                            peers.add(user);
                        }

                    }

                    try {

                        peers.sort(Comparator.comparing(User::getTimestamp));

                        mUsersViewAdapter.updateData(peers);
                    } catch (Throwable e) {
                        LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });


        return view;
    }


    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_users_action_mode, menu);

                mListener.setBottomNavigationEnabled(false);
                mListener.setPagingEnabled(false);
                showFab(false);
                mHandler.post(() -> mUsersViewAdapter.notifyDataSetChanged());

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_mode_connect: {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                            break;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();


                        try {
                            Selection<String> entries = mSelectionTracker.getSelection();

                            if (!Network.isConnected(mContext)) {
                                EVENTS.getInstance(mContext).postWarning(
                                        getString(R.string.offline_mode));
                            } else {
                                connectUsers(convert(entries));
                            }

                            mSelectionTracker.clearSelection();

                        } catch (Throwable e) {
                            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                        }
                    }
                    case R.id.action_mode_delete: {

                        if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                            break;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        try {
                            Selection<String> entries = mSelectionTracker.getSelection();

                            deleteUser(convert(entries));

                        } catch (Throwable e) {
                            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                        }

                        return true;
                    }

                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                mSelectionTracker.clearSelection();

                mListener.setBottomNavigationEnabled(true);
                mListener.setPagingEnabled(true);
                showFab(true);

                if (mActionMode != null) {
                    mActionMode = null;
                }
                mHandler.post(() -> mUsersViewAdapter.notifyDataSetChanged());

            }
        };

    }

    private String[] convert(List<String> entries) {
        int i = 0;

        String[] basic = new String[entries.size()];
        for (String entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    private String[] convert(Selection<String> entries) {
        int i = 0;

        String[] basic = new String[entries.size()];
        for (String entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    @Override
    public void showFab(boolean visible) {
        if (!isTablet) {
            if (visible) {
                mMainFab.show();
            } else {
                mMainFab.hide();
            }
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);

        try {

            if (Network.isConnected(mContext)) {

                ExecutorService executor = Executors.newSingleThreadExecutor();
                executor.submit(() -> {
                    THREADS peers = THREADS.getInstance(mContext);
                    List<String> users = peers.getUsersPids();
                    if (!users.isEmpty()) {
                        connectUsers(convert(users));
                    }
                });

            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        } finally {
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void invokeViewAction(@NonNull User user) {

        try {
            CID image = user.getImage();
            Objects.requireNonNull(image);
            ImageDialogFragment.newInstance(image.getCid()).show(
                    getChildFragmentManager(), ImageDialogFragment.TAG);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void invokeGeneralAction(@NonNull User user, @NonNull View view) {


        try {
            boolean senderBlocked = user.isBlocked();
            boolean isConnected = user.isConnected();

            PopupMenu menu = new PopupMenu(mContext, view);
            menu.inflate(R.menu.popup_peers_menu);
            menu.getMenu().findItem(R.id.popup_block).setVisible(!senderBlocked);
            menu.getMenu().findItem(R.id.popup_unblock).setVisible(senderBlocked);
            menu.getMenu().findItem(R.id.popup_connect).setVisible(!isConnected);

            menu.setOnMenuItemClickListener((item) -> {


                if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                    return true;
                }

                mLastClickTime = SystemClock.elapsedRealtime();


                if (item.getItemId() == R.id.popup_connect) {
                    if (!Network.isConnected(mContext)) {
                        EVENTS.getInstance(mContext).postWarning(getString(R.string.offline_mode));
                    } else if (THREADS.getInstance(mContext).isUserBlocked(user.getPid())) {
                        EVENTS.getInstance(mContext).postWarning(mContext.getString(R.string.user_is_blocked));
                    } else {
                        connectUsers(user.getPid());
                    }
                    return true;
                } else if (item.getItemId() == R.id.popup_delete) {
                    deleteUser(user.getPid());
                    return true;
                } else if (item.getItemId() == R.id.popup_block) {
                    clickUserBlock(user.getPid(), true);
                    return true;
                } else if (item.getItemId() == R.id.popup_unblock) {
                    clickUserBlock(user.getPid(), false);
                    return true;
                }
                return false;

            });

            MenuPopupHelper menuHelper = new MenuPopupHelper(
                    mContext, (MenuBuilder) menu.getMenu(), view);
            menuHelper.setForceShowIcon(true);
            menuHelper.show();


        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

    }


    private void clickUserBlock(@NonNull String pid, boolean value) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                THREADS peers = THREADS.getInstance(mContext);
                if (!value) {
                    peers.unblockUser(PID.create(pid));
                } else {
                    peers.blockUser(PID.create(pid));
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        });

    }

    private void clickAddContact() {
        try {
            EditPeerDialogFragment.newInstance().show(
                    getChildFragmentManager(), EditPeerDialogFragment.TAG);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void connectUsers(@NonNull String... pids) {


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            THREADS threads = THREADS.getInstance(mContext);

            List<OneTimeWorkRequest> prio = new ArrayList<>();
            List<OneTimeWorkRequest> requests = new ArrayList<>();

            for (String pid : pids) {
                if (!threads.isUserBlocked(pid)) {

                    if (Network.isConnected(mContext)) {
                        THREADS.getInstance(mContext).setUserDialing(pid, true);
                    }
                    prio.add(UserConnectWorker.getWork(pid, 30, true));
                }
            }

            String hash = "UF" + Arrays.hashCode(pids);
            WorkManager.getInstance(mContext).beginUniqueWork(
                    hash, ExistingWorkPolicy.KEEP,
                    ConnectionWorker.getWork())
                    .then(prio)
                    .then(requests)
                    .enqueue();


        });


    }

    @Override
    public void invokeAbortDialing(@NonNull User user) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> THREADS.getInstance(mContext).setUserDialing(user.getPid(), false));

        WorkManager.getInstance(mContext).cancelUniqueWork(
                UserConnectWorker.getUniqueId(user.getPid()));

    }


    private void deleteUser(String... pids) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(R.string.delete_operation);
        alertDialogBuilder.setIcon(R.drawable.delete);
        alertDialogBuilder.setMessage(R.string.action_delete_message);
        alertDialogBuilder.setPositiveButton(android.R.string.yes,
                (arg0, arg1) -> UserService.deleteUser(mContext, pids));

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnDismissListener((dialog) -> mSelectionTracker.clearSelection());
        alertDialog.show();
    }


    public interface ActionListener {

        void setBottomNavigationEnabled(boolean enable);

        void setPagingEnabled(boolean enable);
    }

}
