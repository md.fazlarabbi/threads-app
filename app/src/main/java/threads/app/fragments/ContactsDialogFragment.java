package threads.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.model.AccountsViewModel;
import threads.app.utils.AccountsAdapter;
import threads.app.utils.ContactsViewAdapter;
import threads.app.work.GroupRequestWorker;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class ContactsDialogFragment extends BottomSheetDialogFragment implements ContactsViewAdapter.ValidateListener {
    public static final String TAG = ContactsDialogFragment.class.getSimpleName();

    private static final int CLICK_OFFSET = 500;
    private long mLastClickTime = 0;
    private AccountsAdapter mAdapter;
    private Context mContext;
    private TextView mSendTo;
    private TextView mNoPeers;

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contacts_view, container, false);


        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);
        final long idx = bundle.getLong(Content.IDX);

        RecyclerView recycler_view_contact_list = view.findViewById(R.id.send_contact_list);

        mNoPeers = view.findViewById(R.id.no_peers);
        mNoPeers.setVisibility(View.GONE);
        mSendTo = view.findViewById(R.id.send_to);
        recycler_view_contact_list.setLayoutManager(new LinearLayoutManager(getContext()));
        ContactsViewAdapter contactsViewAdapter = new ContactsViewAdapter(mContext, this);
        recycler_view_contact_list.setAdapter(contactsViewAdapter);
        mAdapter = contactsViewAdapter;

        mSendTo.setOnClickListener((v) -> {

            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return;
            }

            mLastClickTime = SystemClock.elapsedRealtime();

            List<User> accounts = mAdapter.getSelectedAccounts();

            inviteUsers(accounts, idx);

        });
        PID host = IPFS.getPID(mContext);
        AccountsViewModel accountsViewModel = new ViewModelProvider(this).
                get(AccountsViewModel.class);
        accountsViewModel.getLiteUsers().observe(getViewLifecycleOwner(), (peers) -> {

            try {
                if (peers != null) {
                    try {
                        List<User> contacts = new ArrayList<>();
                        for (User peer : peers) {
                            if (!Objects.equals(host, peer.getPID())) {
                                contacts.add(peer);
                            }
                        }
                        if (contacts.isEmpty()) {
                            mNoPeers.setVisibility(View.VISIBLE);
                        } else {
                            mNoPeers.setVisibility(View.GONE);
                        }
                        contacts.sort(Comparator.comparing(User::getRating));

                        mAdapter.setAccounts(contacts);
                    } catch (Throwable e) {
                        LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });
        return view;
    }


    @Override
    public void validate() {
        mSendTo.setEnabled(!mAdapter.getSelectedAccounts().isEmpty());
    }


    private void inviteUsers(@NonNull List<User> accounts, long idx) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {
                THREADS threads = THREADS.getInstance(mContext);
                PID host = IPFS.getPID(mContext);
                Objects.requireNonNull(host);
                String hostAlias = InitApplication.getAlias(mContext);
                Group group = threads.getGroupByIdx(idx);
                Objects.requireNonNull(group);


                for (User account : accounts) {

                    if (group.hasMember(account.getPid())) {
                        String info = getString(R.string.already_member_of_group,
                                account.getAlias());
                        Note note = threads.createInfoNote(idx, host, hostAlias, info, new Date());

                        threads.storeNote(note);
                        continue;
                    }

                    String info = getString(R.string.notify_member_of_group,
                            account.getAlias());
                    Note note = threads.createInfoNote(group.getIdx(), host, hostAlias, info, new Date());
                    threads.storeNote(note);

                    GroupRequestWorker.request(mContext, account.getPID(), group.getIdx());

                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                dismiss();
            }
        });
    }


}
