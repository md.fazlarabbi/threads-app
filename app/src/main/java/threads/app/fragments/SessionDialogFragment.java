package threads.app.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.Group;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.services.MoinService;
import threads.app.utils.IPFSData;
import threads.ipfs.CID;
import threads.ipfs.Encryption;
import threads.ipfs.IPFS;
import threads.ipfs.PID;


public class SessionDialogFragment extends DialogFragment {
    public static final String TAG = SessionDialogFragment.class.getSimpleName();

    private static final int SELECT_IMAGE_FILE = 1;
    private final AtomicBoolean notPrintErrorMessages = new AtomicBoolean(false);
    private long mLastClickTime = 0;

    private TextInputLayout mLayout;
    private TextInputEditText mSessionName;
    private ImageView mImageView;
    private CID mImage;
    private Context mContext;
    private FragmentActivity mActivity;
    private Switch mEncryptionEnable;

    public static SessionDialogFragment newInstance() {
        return new SessionDialogFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        LayoutInflater inflater = mActivity.getLayoutInflater();


        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.session_dialog_view, null);

        mEncryptionEnable = view.findViewById(R.id.encryption_enable);
        mEncryptionEnable.setChecked(false);

        mLayout = view.findViewById(R.id.thread_name_layout);
        mLayout.setCounterEnabled(true);
        mLayout.setCounterMaxLength(MoinService.NUM_LETTERS);


        mSessionName = view.findViewById(R.id.thread_name);

        InputFilter[] filterTitle = new InputFilter[1];
        filterTitle[0] = new InputFilter.LengthFilter(MoinService.NUM_LETTERS);
        mSessionName.setFilters(filterTitle);
        mSessionName.setOnEditorActionListener(new DoneOnEditorActionListener());

        mImageView = view.findViewById(R.id.add_image);

        mImageView.setOnClickListener((v) -> {
            try {
                Intent intent = new Intent();
                intent.setType("*/*");

                String[] mimeTypes = {"image/*"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(
                        Intent.createChooser(intent, getString(R.string.select_image)),
                        SELECT_IMAGE_FILE);
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        });

        mSessionName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isValidInput(getDialog());
            }
        });

        builder.setView(view)
                // Add action buttons
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {

                    // mis-clicking prevention, using threshold of 1000 ms
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    removeKeyboards();

                    Editable text = mSessionName.getText();
                    Objects.requireNonNull(text);
                    String title = text.toString();
                    boolean enableEncryption = mEncryptionEnable.isChecked();
                    createGroup(mContext, title, mImage, enableEncryption);

                })
                .setTitle(getString(R.string.thread_create));


        Dialog dialog = builder.create();
        Window window = dialog.getWindow();
        if (window != null) {
            window.getAttributes().windowAnimations = R.style.DialogTopAnimation;
            window.getAttributes().gravity = Gravity.TOP | Gravity.CENTER;
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return dialog;
    }

    private void isValidInput(Dialog dialog) {

        if (dialog instanceof AlertDialog) {
            AlertDialog alertDialog = (AlertDialog) dialog;
            Editable text = mSessionName.getText();
            Objects.requireNonNull(text);
            String displayName = text.toString();


            boolean result = !displayName.isEmpty();

            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(result);


            if (!notPrintErrorMessages.get()) {
                if (displayName.isEmpty()) {
                    mLayout.setError(getString(R.string.thread_name_error));
                } else {
                    mLayout.setError(null);
                }
            } else {
                mLayout.setError(null);
            }
        }
    }

    private void removeKeyboards() {
        if (mActivity != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mSessionName.getWindowToken(), 0);
            }
        }

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        removeKeyboards();

        super.onDismiss(dialog);
    }

    @Override
    public void onResume() {
        super.onResume();

        notPrintErrorMessages.set(true);
        isValidInput(getDialog());
        notPrintErrorMessages.set(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == SELECT_IMAGE_FILE && resultCode == Activity.RESULT_OK
                    && data != null && data.getData() != null) {
                Uri uri = data.getData();

                Glide.with(mContext)
                        .asBitmap()
                        .load(uri)
                        .into(new CustomTarget<Bitmap>(256, 256) {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource,
                                                        @Nullable Transition<? super Bitmap> transition) {

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                resource.compress(Bitmap.CompressFormat.WEBP, 100, stream);
                                IPFS ipfs = IPFS.getInstance(mContext);
                                mImage = ipfs.storeData(stream.toByteArray(), "");
                                resource.recycle();

                                mActivity.runOnUiThread(() -> loadImage());
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });


            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void loadImage() {
        try {
            IPFS ipfs = IPFS.getInstance(mContext);
            IPFSData imageData = IPFSData.create(ipfs, mImage);
            Glide.with(mContext).
                    load(imageData).
                    apply(RequestOptions.circleCropTransform()).
                    into(mImageView);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void createGroup(@NonNull Context context, @NonNull String title,
                             @Nullable CID image, boolean enableEncryption) {


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {
                THREADS threads = THREADS.getInstance(context);

                PID host = IPFS.getPID(context);
                Objects.requireNonNull(host);

                String hostAlias = InitApplication.getAlias(context);

                String message = context.getString(R.string.thread_creator, hostAlias);

                String uuid = UUID.randomUUID().toString().replace("-", "");

                String skey = "";
                if (enableEncryption) {
                    skey = Encryption.generateAESKey();
                }

                Group group = threads.createGroup(uuid, host, skey);

                group.setName(title);
                group.setContent(message);

                group.setImage(image);

                long groupIdx = threads.storeGroup(group);


                // insert a pre-define message for internal view
                Note note = threads.createInfoNote(groupIdx, host, hostAlias, message, new Date());

                threads.storeNote(note);

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                dismiss();
            }
        });

    }

    static class DoneOnEditorActionListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager)
                        v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
            return false;
        }
    }
}
