package threads.app.fragments;

public interface FabFragment {
    void showFab(boolean visible);
}
