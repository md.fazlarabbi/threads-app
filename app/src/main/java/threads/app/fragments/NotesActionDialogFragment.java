package threads.app.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.DateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.EVENTS;
import threads.app.model.PreviewData;
import threads.app.model.SelectionViewModel;
import threads.app.provider.FileDocumentsProvider;
import threads.app.services.ThumbnailService;
import threads.app.services.UploadService;
import threads.app.utils.MimeType;


public class NotesActionDialogFragment extends BottomSheetDialogFragment {
    public static final String TAG = NotesActionDialogFragment.class.getSimpleName();
    private static final int REQUEST_IMAGE_CAPTURE = 1000;
    private static final int REQUEST_VIDEO_CAPTURE = 2000;
    private static final int REQUEST_AUDIO_CAPTURE = 3000;
    private static final int REQUEST_SELECT_FILES = 4000;
    private SelectionViewModel mSelectionViewModel;

    private long mLastClickTime = 0;
    private Context mContext;
    private FragmentActivity mActivity;
    private Uri mUri;

    public static NotesActionDialogFragment newInstance() {
        return new NotesActionDialogFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.action_notes_view, container, false);

        if (savedInstanceState != null) {
            String uri = savedInstanceState.getString(Content.URI);
            if (uri != null) {
                mUri = Uri.parse(uri);
            }
        }


        PackageManager pm = mContext.getPackageManager();
        boolean hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);

        boolean hasMic = pm.hasSystemFeature(PackageManager.FEATURE_MICROPHONE);


        TextView menu_camera = view.findViewById(R.id.menu_camera);
        if (!hasCamera) {
            menu_camera.setVisibility(View.GONE);
        } else {
            menu_camera.setOnClickListener((v) -> {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                clickMenuImage();
            });
        }

        TextView menu_video = view.findViewById(R.id.menu_video);
        if (!hasCamera) {
            menu_video.setVisibility(View.GONE);
        } else {
            menu_video.setOnClickListener((v) -> {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                clickMenuVideo();
            });
        }

        TextView menu_micro = view.findViewById(R.id.menu_micro);
        if (!hasMic) {
            menu_micro.setVisibility(View.GONE);
        } else {
            menu_micro.setOnClickListener((v) -> {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                clickMenuMicro();
            });
        }


        TextView menu_upload = view.findViewById(R.id.menu_files);

        menu_upload.setOnClickListener((v) -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            clickMenuUpload();
        });


        mSelectionViewModel = new ViewModelProvider(mActivity).get(SelectionViewModel.class);

        return view;
    }

    private long getGroup() {
        MutableLiveData<Long> group = mSelectionViewModel.getGroup();
        Objects.requireNonNull(group);
        Long groupIdx = group.getValue();
        Objects.requireNonNull(groupIdx);
        return groupIdx;
    }

    private void clickMenuUpload() {
        try {
            Intent intent = ShareCompat.IntentBuilder.from(mActivity).getIntent();
            intent.setType("*/*");

            String[] mimeTypes = {"*/*"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, false);
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);


            if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                startActivityForResult(Intent.createChooser(intent,
                        getString(R.string.select_files)), REQUEST_SELECT_FILES);
            } else {
                EVENTS.getInstance(mContext).postError(
                        getString(R.string.no_activity_found_to_handle_uri));
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void dispatchTakeAudioIntent() {
        try {
            VoiceDialogFragment.newInstance(getGroup()).show(
                    getParentFragmentManager(), VoiceDialogFragment.TAG);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        } finally {
            dismiss();
        }
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(mContext.getPackageManager()) != null) {
            takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 60);
            takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_SELECT_FILES) {
            try {
                Objects.requireNonNull(data);
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    int items = mClipData.getItemCount();

                    for (int i = 0; i < items; i++) {
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        boolean last = i == (items - 1);
                        UploadService.upload(mContext, uri, getGroup(), last);
                    }

                } else if (data.getData() != null) {
                    Uri uri = data.getData();
                    UploadService.upload(mContext, uri, getGroup(), true);
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                dismiss();
            }
        } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
            try {

                dismiss();
                Uri uri = data.getData();
                Objects.requireNonNull(uri);

                ThumbnailService.FileDetails details = ThumbnailService.getFileDetails(
                        mContext, uri);
                Objects.requireNonNull(details);
                String mimeType = details.getMimeType();
                String fileName = details.getFileName();


                PreviewData previewData = new PreviewData();
                previewData.setMimeType(mimeType);
                previewData.setName(fileName);
                previewData.setUri(uri);
                previewData.setVideo(true);
                mSelectionViewModel.setPreviewData(previewData);

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE) {

            try {
                dismiss();
                Objects.requireNonNull(mUri);
                String timeStamp = DateFormat.getDateTimeInstance().
                        format(new Date()).
                        replace(":", "").
                        replace(".", "_").
                        replace("/", "_").
                        replace(" ", "_");

                String name = "IMG_" + timeStamp;
                PreviewData previewData = new PreviewData();
                previewData.setMimeType(MimeType.JPEG_MIME_TYPE);
                previewData.setName(name);
                previewData.setUri(mUri);
                previewData.setVideo(false);
                mSelectionViewModel.setPreviewData(previewData);

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }


        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (mUri != null) {
            bundle.putString(Content.URI, mUri.toString());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE: {
                for (int i = 0, len = permissions.length; i < len; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        EVENTS.getInstance(mContext).postPermission(
                                getString(R.string.permission_camera_denied));
                        dismiss();
                    }
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        dispatchTakePictureIntent();
                    }
                }
                break;
            }
            case REQUEST_VIDEO_CAPTURE: {
                for (int i = 0, len = permissions.length; i < len; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        EVENTS.getInstance(mContext).postPermission(
                                getString(R.string.permission_camera_denied));
                        dismiss();
                    }
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        dispatchTakeVideoIntent();
                    }
                }
                break;
            }
            case REQUEST_AUDIO_CAPTURE: {
                for (int i = 0, len = permissions.length; i < len; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        EVENTS.getInstance(mContext).postPermission(
                                getString(R.string.permission_audio_denied));
                        dismiss();
                    }
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        dispatchTakeAudioIntent();
                    }
                }
                break;
            }
        }
    }

    private void dispatchTakePictureIntent() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            mUri = FileDocumentsProvider.getUriForImage(UUID.randomUUID().toString());

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void clickMenuImage() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_IMAGE_CAPTURE);
        } else {
            dispatchTakePictureIntent();
        }
    }

    private void clickMenuMicro() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_AUDIO_CAPTURE);
        } else {
            dispatchTakeAudioIntent();
        }
    }

    private void clickMenuVideo() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_VIDEO_CAPTURE);
        } else {
            dispatchTakeVideoIntent();
        }
    }

}
