package threads.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.Content;
import threads.app.core.Group;
import threads.app.core.Members;
import threads.app.core.Note;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.utils.AccountsAdapter;
import threads.app.utils.ContactsViewAdapter;
import threads.app.utils.MembersViewAdapter;
import threads.app.work.GroupRemovedWorker;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class MembersDialogFragment extends BottomSheetDialogFragment implements ContactsViewAdapter.ValidateListener {
    public static final String TAG = MembersDialogFragment.class.getSimpleName();
    private static final int CLICK_OFFSET = 500;
    private Context mContext;
    private long mLastClickTime = 0;
    private PID host;
    private TextView mDeleteMembers;
    private AccountsAdapter mAdapter;

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.members_view, container, false);


        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);
        final long idx = bundle.getLong(Content.IDX);

        host = IPFS.getPID(mContext);
        Objects.requireNonNull(host);


        TextView noPeers = view.findViewById(R.id.no_peers);
        RecyclerView recyclerView = view.findViewById(R.id.send_contact_list);
        mDeleteMembers = view.findViewById(R.id.delete_members);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        PID owner = THREADS.getInstance(mContext).getGroupOwner(idx);
        if (!Objects.equals(host, owner)) {
            mDeleteMembers.setVisibility(View.GONE);
            MembersViewAdapter membersViewAdapter = new MembersViewAdapter(mContext);
            recyclerView.setAdapter(membersViewAdapter);
            mAdapter = membersViewAdapter;
        } else {
            ContactsViewAdapter contactsViewAdapter = new ContactsViewAdapter(mContext, this);
            recyclerView.setAdapter(contactsViewAdapter);
            mAdapter = contactsViewAdapter;
        }

        mDeleteMembers.setOnClickListener((v) -> {

            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return;
            }

            mLastClickTime = SystemClock.elapsedRealtime();

            List<User> accounts = mAdapter.getSelectedAccounts();

            removeUsers(accounts, idx);

        });

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {
                THREADS threads = THREADS.getInstance(mContext);
                Group group = threads.getGroupByIdx(idx);

                if (group != null) {
                    try {
                        List<User> users = new ArrayList<>();

                        for (String pid : group.getMembers()) {
                            User user = threads.getUserByPid(pid);
                            if (user != null && !Objects.equals(host, user.getPID())) {
                                if (user.isVisible()) {
                                    users.add(user);
                                }
                            }
                        }

                        if (users.isEmpty()) {
                            noPeers.setVisibility(View.VISIBLE);
                        } else {
                            noPeers.setVisibility(View.GONE);
                        }

                        users.sort(Comparator.comparing(User::getRating));

                        mAdapter.setAccounts(users);
                    } catch (Throwable e) {
                        LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });
        return view;
    }

    @Override
    public void validate() {
        mDeleteMembers.setEnabled(!mAdapter.getSelectedAccounts().isEmpty());
    }

    private void removeUsers(@NonNull List<User> users, long idx) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            THREADS threads = THREADS.getInstance(mContext);
            try {
                Group group = threads.getGroupByIdx(idx);
                Objects.requireNonNull(group);
                Members members = (Members) group.getMembers().clone();
                Objects.requireNonNull(members);
                for (User user : users) {
                    group.removeMember(user.getPid());

                    String message = getString(R.string.user_removed_thread, user.getAlias());
                    Note note = threads.createInfoNote(group.getIdx(), group.getOwner(),
                            user.getAlias(), message, new Date());

                    threads.storeNote(note);

                }
                threads.updateGroup(group);

                for (User user : users) {
                    for (String member : members) {
                        User userMember = threads.getUserByPid(member);
                        if (userMember != null && userMember.hasToken() && !userMember.isBlocked()) {
                            String token = userMember.getToken();
                            Objects.requireNonNull(token);
                            GroupRemovedWorker.removed(mContext, token, user.getPid(), group.getUuid());
                        }
                    }
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            } finally {
                dismiss();
            }
        });


    }
}
