package threads.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.LogUtils;
import threads.app.R;
import threads.app.core.Group;
import threads.app.core.THREADS;
import threads.app.core.User;
import threads.app.model.GroupsViewModel;
import threads.app.model.SelectionViewModel;
import threads.app.services.GroupService;
import threads.app.utils.GroupItemDetailsLookup;
import threads.app.utils.GroupItemKeyProvider;
import threads.app.utils.GroupViewAdapter;
import threads.app.utils.Network;
import threads.app.work.UserConnectWorker;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;


public class GroupsFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener, FabFragment, GroupViewAdapter.GroupViewAdapterListener {

    private static final String TAG = GroupsFragment.class.getSimpleName();
    private static final int CLICK_OFFSET = 500;
    @NonNull
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private ActionMode mActionMode;
    private SelectionTracker<Long> mSelectionTracker;
    private ActionListener mListener;
    private RecyclerView mRecyclerView;
    private GroupViewAdapter mGroupViewAdapter;
    private long mLastClickTime = 0;
    private Context mContext;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isTablet;
    private FragmentActivity mActivity;
    private FloatingActionButton mFabAddGroup;
    private SelectionViewModel mSelectionViewModel;


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mContext = null;
        mActivity = null;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
        try {
            mListener = (ActionListener) getActivity();
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
        isTablet = getResources().getBoolean(R.bool.isTablet);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_groups_fragment, menu);

        MenuItem actionCreateGroup = menu.findItem(R.id.action_create_group);
        actionCreateGroup.setVisible(isTablet);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_create_group) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return true;
            }

            mLastClickTime = SystemClock.elapsedRealtime();

            try {
                SessionDialogFragment.newInstance().show(
                        getChildFragmentManager(), SessionDialogFragment.TAG);
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.groups_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mSelectionViewModel = new ViewModelProvider(mActivity).get(SelectionViewModel.class);

        mRecyclerView = view.findViewById(R.id.recycler_view_message_list);
        LinearLayoutManager linearLayout = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(linearLayout);
        mGroupViewAdapter = new GroupViewAdapter(mContext, this);
        mRecyclerView.setAdapter(mGroupViewAdapter);


        mFabAddGroup = view.findViewById(R.id.fab_add_group);
        if (isTablet) {
            mFabAddGroup.setVisibility(View.GONE);
        }

        mFabAddGroup.setOnClickListener((v) -> {

            if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            try {
                SessionDialogFragment.newInstance().show(
                        getChildFragmentManager(), SessionDialogFragment.TAG);
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }


        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isTablet) {
                    boolean hasSelection = mSelectionTracker.hasSelection();
                    if (dy > 0 && !hasSelection) {
                        showFab(false);
                    } else if (dy < 0 && !hasSelection) {
                        showFab(true);
                    }
                }
            }
        });

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);


        GroupsViewModel threadsViewModel =
                new ViewModelProvider(this).get(GroupsViewModel.class);
        threadsViewModel.getGroups().observe(getViewLifecycleOwner(), (threads) -> {
            try {
                if (threads != null) {
                    threads.sort(Comparator.comparing(Group::getDate).reversed());
                    mGroupViewAdapter.updateData(threads);

                    mRecyclerView.scrollToPosition(0);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }

        });


        PID pid = IPFS.getPID(mContext);
        Objects.requireNonNull(pid);

        mSelectionTracker = new SelectionTracker.Builder<>(
                "groups-selection",//unique id
                mRecyclerView,
                new GroupItemKeyProvider(mGroupViewAdapter),
                new GroupItemDetailsLookup(mRecyclerView),
                StorageStrategy.createLongStorage())
                .build();


        mSelectionTracker.addObserver(new SelectionTracker.SelectionObserver<String>() {
            @Override
            public void onSelectionChanged() {
                if (!mSelectionTracker.hasSelection()) {
                    if (mActionMode != null) {
                        mActionMode.finish();
                    }
                } else {
                    if (mActionMode == null) {
                        mActionMode = ((AppCompatActivity)
                                mActivity).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (mActionMode != null) {
                    mActionMode.setTitle("" + mSelectionTracker.getSelection().size());
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!mSelectionTracker.hasSelection()) {
                    if (mActionMode != null) {
                        mActionMode.finish();
                    }
                } else {
                    if (mActionMode == null) {
                        mActionMode = ((AppCompatActivity)
                                mActivity).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (mActionMode != null) {
                    mActionMode.setTitle("" + mSelectionTracker.getSelection().size());
                }
                super.onSelectionRestored();
            }
        });

        mGroupViewAdapter.setSelectionTracker(mSelectionTracker);


        if (savedInstanceState != null) {
            mSelectionTracker.onRestoreInstanceState(savedInstanceState);
        }
    }

    private long[] convert(Selection<Long> entries) {
        int i = 0;

        long[] basic = new long[entries.size()];
        for (Long entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mSelectionTracker != null) {
            mSelectionTracker.onSaveInstanceState(outState);
        }

    }

    @Override
    public void showFab(boolean visible) {
        if (!isTablet) {
            if (visible) {
                mFabAddGroup.show();
            } else {
                mFabAddGroup.hide();
            }
        }
    }

    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_group_action_mode, menu);


                mListener.setBottomNavigationEnabled(false);
                mListener.setPagingEnabled(false);
                showFab(false);

                mHandler.post(() -> mGroupViewAdapter.notifyDataSetChanged());

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.action_mode_delete) {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                        return true;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    deleteAction();

                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                mSelectionTracker.clearSelection();

                mListener.setBottomNavigationEnabled(true);
                mListener.setPagingEnabled(true);
                showFab(true);
                if (mActionMode != null) {
                    mActionMode = null;
                }
                mHandler.post(() -> mGroupViewAdapter.notifyDataSetChanged());

            }
        };

    }


    private void deleteAction() {

        try {
            long[] entries = convert(mSelectionTracker.getSelection());

            GroupService.deleteGroups(mContext, entries);

            mSelectionTracker.clearSelection();

            // reset group selection
            mSelectionViewModel.setGroup(0L);

        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }


    @Override
    public void onClick(@NonNull Group group) {

        try {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();


            if (!group.isRequest() && !group.isDeleting()) {
                mListener.groupSelected(group);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }


    @Override
    public void onRejectClick(@NonNull Group group) {

        try {
            GroupService.reject(mContext, group.getIdx());
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    @Override
    public void onAcceptClick(@NonNull Group group) {

        try {
            GroupService.accept(mContext, group.getIdx());
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

    }

    @Override
    public void invokeViewAction(@NonNull Group group) {

        try {
            CID image = group.getImage();
            Objects.requireNonNull(image);
            ImageDialogFragment.newInstance(image.getCid()).show(
                    getChildFragmentManager(), ImageDialogFragment.TAG);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }


    @Override
    public void onRefresh() {
        loadGroups();
    }

    private void loadGroups() {

        mSwipeRefreshLayout.setRefreshing(true);

        if (Network.isConnected(mContext)) {

            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(() -> {
                try {
                    THREADS threads = THREADS.getInstance(mContext);
                    List<Group> groups = threads.getGroups();
                    for (Group group : groups) {
                        PID owner = group.getOwner();
                        User user = threads.getUserByPid(owner.getPid());
                        Objects.requireNonNull(user);
                        if (!user.isBlocked()) {
                            UserConnectWorker.connect(mContext, user.getPid(),
                                    15, true);
                        }
                    }
                } catch (Throwable e) {
                    LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
                }
            });

        }

        mSwipeRefreshLayout.setRefreshing(false);

    }


    public interface ActionListener {
        void groupSelected(@NonNull Group group);

        void setBottomNavigationEnabled(boolean enable);

        void setPagingEnabled(boolean enable);
    }
}
