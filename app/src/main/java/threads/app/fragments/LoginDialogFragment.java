package threads.app.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.LogUtils;
import threads.app.InitApplication;
import threads.app.R;
import threads.app.core.THREADS;
import threads.app.services.IdentityService;
import threads.app.utils.IPFSData;
import threads.app.work.ConnectionWorker;
import threads.ipfs.CID;
import threads.ipfs.IPFS;
import threads.ipfs.PID;

public class LoginDialogFragment extends DialogFragment {
    public static final String TAG = LoginDialogFragment.class.getSimpleName();
    private static final String NUM_LETTERS = "NUM_LETTERS";
    private static final int SELECT_IMAGE_FILE = 1;
    private final AtomicBoolean notPrintErrorMessages = new AtomicBoolean(false);
    private long mLastClickTime = 0;
    private TextInputLayout mLayout;
    private TextInputEditText mName;
    private Context mContext;
    private FragmentActivity mActivity;
    private ImageView mImageView;
    private CID mImage;

    public static LoginDialogFragment newInstance(int numLetters) {

        Bundle bundle = new Bundle();
        bundle.putInt(NUM_LETTERS, numLetters);

        LoginDialogFragment fragment = new LoginDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        int numLetters = args.getInt(NUM_LETTERS);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        LayoutInflater inflater = mActivity.getLayoutInflater();

        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.name_login_view, null);

        mImageView = view.findViewById(R.id.add_image);

        mImageView.setOnClickListener((v) -> {

            try {
                Intent intent = new Intent();
                intent.setType("*/*");

                String[] mimeTypes = {"image/*"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(
                        Intent.createChooser(intent, getString(R.string.select_image)),
                        SELECT_IMAGE_FILE);
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }

        });

        String image = InitApplication.getImage(mContext);
        if (!image.isEmpty()) {
            mImage = CID.create(image);
            loadImage();
        }

        mLayout = view.findViewById(R.id.text_name_layout);
        mLayout.setCounterEnabled(true);
        mLayout.setCounterMaxLength(numLetters);

        mName = view.findViewById(R.id.text);
        mName.setText(InitApplication.getAlias(mContext));
        InputFilter[] filterTitle = new InputFilter[1];
        filterTitle[0] = new InputFilter.LengthFilter(numLetters);
        mName.setFilters(filterTitle);

        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isValidInput(getDialog());
            }
        });


        builder.setView(view)
                // Add action buttons
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();
                    // This removes all input keyboards from user_alias edit views
                    removeKeyboards();


                    Editable text = mName.getText();
                    Objects.requireNonNull(text);
                    String alias = text.toString();
                    boolean publishIdentity = false;
                    if (!Objects.equals(alias, InitApplication.getAlias(mContext))) {
                        InitApplication.setAlias(mContext, alias);
                        publishIdentity = true;


                        PID host = IPFS.getPID(mContext);
                        Objects.requireNonNull(host);
                        THREADS.getInstance(mContext).setNoteOwnerAlias(host, alias);

                    }

                    if (mImage != null) {
                        if (!Objects.equals(mImage.getCid(), InitApplication.getImage(mContext))) {
                            InitApplication.setImage(mContext, mImage.getCid());
                            publishIdentity = true;
                        }
                    }

                    if (publishIdentity) {
                        IdentityService.identity(mContext, true);
                    } else {
                        ConnectionWorker.connect(mContext);
                    }

                    dismiss();

                })
                .setTitle(getString(R.string.account_login));


        Dialog dialog = builder.create();
        Window window = dialog.getWindow();
        if (window != null) {
            window.getAttributes().windowAnimations = R.style.DialogRightAnimation;
            window.getAttributes().gravity = Gravity.CENTER;
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        return dialog;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == SELECT_IMAGE_FILE && resultCode == Activity.RESULT_OK
                    && data != null && data.getData() != null) {
                Uri uri = data.getData();

                Glide.with(mContext)
                        .asBitmap()
                        .load(uri)
                        .into(new CustomTarget<Bitmap>(256, 256) {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource,
                                                        @Nullable Transition<? super Bitmap> transition) {

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                resource.compress(Bitmap.CompressFormat.WEBP, 100, stream);
                                IPFS ipfs = IPFS.getInstance(mContext);
                                // image is not encrypted
                                mImage = ipfs.storeData(stream.toByteArray(), "");
                                resource.recycle();

                                mActivity.runOnUiThread(() -> loadImage());
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });


            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void loadImage() {
        try {
            IPFS ipfs = IPFS.getInstance(mContext);
            IPFSData imageData = IPFSData.create(ipfs, mImage);
            Glide.with(mContext).
                    load(imageData).
                    apply(RequestOptions.circleCropTransform()).
                    into(mImageView);
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }
    }

    private void isValidInput(Dialog dialog) {
        if (dialog instanceof AlertDialog) {
            AlertDialog alertDialog = (AlertDialog) dialog;
            Editable text = mName.getText();
            Objects.requireNonNull(text);
            String displayName = text.toString();

            boolean result = !displayName.isEmpty();

            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(result);


            if (!notPrintErrorMessages.get()) {

                if (displayName.isEmpty()) {
                    mLayout.setError(getString(R.string.login_name_error));
                } else {
                    mLayout.setError(null);
                }

            } else {
                mLayout.setError(null);
            }
        }
    }


    private void removeKeyboards() {
        try {

            InputMethodManager imm = (InputMethodManager)
                    mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mName.getWindowToken(), 0);
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
        }

    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        removeKeyboards();
    }

    @Override
    public void onResume() {
        super.onResume();
        notPrintErrorMessages.set(true);
        isValidInput(getDialog());
        notPrintErrorMessages.set(false);
    }

}
