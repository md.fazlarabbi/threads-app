package threads.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import threads.LogUtils;
import threads.app.utils.GroupRequestChannel;
import threads.app.work.CleanupWorker;
import threads.ipfs.IPFS;

public class InitApplication extends Application {
    private static final String APP_KEY = "AppKey";
    private static final String UPDATE = "UPDATE";
    private static final String ALIAS_KEY = "aliasKey";
    private static final String IMAGE_KEY = "imageKey";

    private static final String AUTO_DELETE_KEY = "autoDeleteKey";
    private static final String LOGIN_KEY = "loginKey";
    private static final String TAG = InitApplication.class.getSimpleName();


    private static final String PREF_KEY = "prefKey";
    private static final String TOKEN_KEY = "tokenKey";
    private static final String DOWN_TIMEOUT_KEY = "downTimeoutKey";


    @NonNull
    public static String getImage(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(IMAGE_KEY, "");
    }

    public static void setImage(@NonNull Context context, @NonNull String image) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(IMAGE_KEY, image);
        editor.apply();
    }

    @NonNull
    public static String getToken(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(TOKEN_KEY, "");
    }

    public static void setToken(@NonNull Context context, @NonNull String token) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
    }

    public static int getDownloadTimeout(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(DOWN_TIMEOUT_KEY, 60);
    }

    private static void setDownloadTimeout(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(DOWN_TIMEOUT_KEY, 120);
        editor.apply();
    }

    public static boolean isAutoDelete(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(AUTO_DELETE_KEY, false);

    }

    public static void setAutoDelete(@NonNull Context context, boolean enable) {

        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(AUTO_DELETE_KEY, enable);
        editor.apply();
    }


    public static void setAlias(@NonNull Context context, @NonNull String alias) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ALIAS_KEY, alias);
        editor.apply();
    }

    @NonNull
    public static String getAlias(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(ALIAS_KEY, IPFS.getDeviceName());
    }

    public static void setLogin(Context context, boolean login) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(LOGIN_KEY, login);
        editor.apply();
    }

    public static boolean isLogin(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(LOGIN_KEY, false);

    }

    public static void runUpdatesIfNecessary(@NonNull Context context) {

        try {
            int versionCode = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionCode;
            SharedPreferences prefs = context.getSharedPreferences(
                    APP_KEY, Context.MODE_PRIVATE);
            if (prefs.getInt(UPDATE, 0) != versionCode) {


                InitApplication.setDownloadTimeout(context);

                setLogin(context, false);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(UPDATE, versionCode);
                editor.apply();
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();


        runUpdatesIfNecessary(getApplicationContext());


        try {
            GroupRequestChannel.createThreadRequestChannel(getApplicationContext());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        CleanupWorker.cleanup(getApplicationContext());

        if (LogUtils.isDebug()) {
            IPFS.logCacheDir(getApplicationContext());
            IPFS.logBaseDir(getApplicationContext());
        }
    }
}
