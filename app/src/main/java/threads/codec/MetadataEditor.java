package threads.codec;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import threads.codec.MP4Util.Movie;
import threads.codec.boxes.MetaBox;
import threads.codec.boxes.MetaValue;
import threads.codec.boxes.NodeBox;
import threads.codec.boxes.UdtaBox;
import threads.codec.boxes.UdtaMetaBox;
import threads.codec.common.Format;
import threads.codec.common.JCodecUtil;

public class MetadataEditor {
    private final File source;
    private final MovieEditor movieEditor;

    private MetadataEditor(File source, MovieEditor movieEditor) {
        this.source = source;
        this.movieEditor = movieEditor;
    }

    public static MetadataEditor createFrom(File f) throws IOException {
        String format = JCodecUtil.detectFormat(f);
        if (!Objects.equals(format, Format.MOV)) {
            throw new IllegalArgumentException("Unsupported format: " + format);
        }

        Movie movie = MP4Util.parseFullMovie(f);
        return new MetadataEditor(f, MovieEditor.createFromMovie(movie.getMoov()));
    }

    public void save(boolean fast) throws IOException {

        MP4Edit edit = movieEditor::apply;
        if (fast) {
            new RelocateEditor().modifyOrRelocate(source, edit);
        } else {
            new ReplaceEditor().modifyOrReplace(source, edit);
        }
    }

    public Map<String, MetaValue> getKeyedMeta() {
        return movieEditor.getKeyedMeta();
    }


    static class MovieEditor {
        private final Map<String, MetaValue> keyedMeta;
        private final Map<Integer, MetaValue> itunesMeta;
        private final Map<Integer, MetaValue> udata;

        MovieEditor(Map<String, MetaValue> keyedMeta, Map<Integer, MetaValue> itunesMeta,
                    Map<Integer, MetaValue> udata) {
            this.keyedMeta = keyedMeta;
            this.itunesMeta = itunesMeta;
            this.udata = udata;
        }

        static MovieEditor createFromMovie(MovieBox moov) {
            MetaBox keyedMeta = NodeBox.findFirst(moov, MetaBox.class, MetaBox.fourcc());
            MetaBox itunesMeta = NodeBox.findFirstPath(moov, MetaBox.class, new String[]{"udta", MetaBox.fourcc()});

            UdtaBox udtaBox = NodeBox.findFirst(moov, UdtaBox.class, "udta");

            return new MovieEditor(keyedMeta == null ? new HashMap<>() : keyedMeta.getKeyedMeta(),
                    itunesMeta == null ? new HashMap<>() : itunesMeta.getItunesMeta(),
                    udtaBox == null ? new HashMap<>() : udtaBox.getMetadata());
        }

        void apply(MovieBox movie) {
            MetaBox meta1 = NodeBox.findFirst(movie, MetaBox.class, MetaBox.fourcc());
            MetaBox meta2 = NodeBox.findFirstPath(movie, MetaBox.class, new String[]{"udta", MetaBox.fourcc()});
            if (keyedMeta != null && keyedMeta.size() > 0) {
                if (meta1 == null) {
                    meta1 = MetaBox.createMetaBox();
                    movie.add(meta1);
                }
                meta1.setKeyedMeta(keyedMeta);
            }

            if (itunesMeta != null && itunesMeta.size() > 0) {
                UdtaBox udta = NodeBox.findFirst(movie, UdtaBox.class, "udta");
                if (meta2 == null) {
                    meta2 = UdtaMetaBox.createUdtaMetaBox();
                    if (udta == null) {
                        udta = UdtaBox.createUdtaBox();
                        movie.add(udta);
                    }
                    udta.add(meta2);
                }
                meta2.setItunesMeta(itunesMeta);
                udta.setMetadata(udata);
            }
        }


        Map<String, MetaValue> getKeyedMeta() {
            return keyedMeta;
        }

    }
}
