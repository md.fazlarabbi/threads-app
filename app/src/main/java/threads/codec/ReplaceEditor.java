package threads.codec;

import java.io.File;
import java.io.IOException;

import threads.LogUtils;
import threads.codec.MP4Util.Movie;

class ReplaceEditor {

    void modifyOrReplace(File src, MP4Edit edit) throws IOException {
        boolean modify = new InPlaceEditor().modify(src, edit);
        if (!modify)
            replace(src, edit);
    }

    private void replace(File src, MP4Edit edit) throws IOException {
        File tmp = new File(src.getParentFile(), "." + src.getName());
        copy(src, tmp, edit);
        boolean result = tmp.renameTo(src);
        if (!result) {
            LogUtils.error(LogUtils.TAG, "File not renamed");
        }
    }

    private void copy(File src, File dst, MP4Edit edit) throws IOException {
        final Movie movie = MP4Util.createRefFullMovieFromFile(src);
        edit.apply(movie.getMoov());
        Flatten fl = new Flatten();

        fl.flatten(movie, dst);
    }
}