package threads.codec;


import threads.codec.boxes.Header;
import threads.codec.boxes.MovieHeaderBox;
import threads.codec.boxes.NodeBox;
import threads.codec.boxes.TrakBox;

class MovieBox extends NodeBox {

    public MovieBox(Header atom) {
        super(atom);
    }


    TrakBox[] getTracks() {
        return NodeBox.findAll(this, TrakBox.class, "trak");
    }


    int getTimescale() {
        return getMovieHeader().getTimescale();
    }


    long rescale(long tv, long ts) {
        return (tv * getTimescale()) / ts;
    }


    private MovieHeaderBox getMovieHeader() {
        return NodeBox.findFirst(this, MovieHeaderBox.class, "mvhd");
    }


    boolean isPureRefMovie() {
        boolean pureRef = true;
        TrakBox[] tracks = getTracks();
        for (TrakBox trakBox : tracks) {
            pureRef &= trakBox.isPureRef();
        }
        return pureRef;
    }


}