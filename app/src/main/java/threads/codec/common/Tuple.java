package threads.codec.common;


public class Tuple {


    public static <T0, T1> _2<T0, T1> pair(T0 v0, T1 v1) {
        return new _2<>(v0, v1);
    }


    public static class _2<T0, T1> {
        public final T0 v0;
        public final T1 v1;

        _2(T0 v0, T1 v1) {
            this.v0 = v0;
            this.v1 = v1;
        }
    }

}
