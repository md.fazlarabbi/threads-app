package threads.codec.common;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import threads.codec.Platform;

import static threads.codec.Platform.stringFromBytes;


public class NIOUtils {

    public static ByteBuffer read(ByteBuffer buffer, int count) {
        ByteBuffer slice = buffer.duplicate();
        int limit = buffer.position() + count;
        slice.limit(limit);
        buffer.position(limit);
        return slice;
    }


    public static ByteBuffer fetchFromChannel(ReadableByteChannel ch, int size) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(size);
        NIOUtils.readFromChannel(ch, buf);
        buf.flip();
        return buf;
    }


    static ByteBuffer fetchFromFileL(File file) throws IOException {
        FileChannel is = null;
        try {
            is = new FileInputStream(file).getChannel();
            return fetchFromChannel(is, 204800);
        } finally {
            closeQuietly(is);
        }
    }


    public static byte[] toArray(ByteBuffer buffer) {
        byte[] result = new byte[buffer.remaining()];
        buffer.duplicate().get(result);
        return result;
    }

    private static byte[] toArrayL(ByteBuffer buffer, int count) {
        byte[] result = new byte[Math.min(buffer.remaining(), count)];
        buffer.duplicate().get(result);
        return result;
    }


    private static void readFromChannel(ReadableByteChannel channel, ByteBuffer buffer) throws IOException {
        buffer.position();
        //noinspection StatementWithEmptyBody
        while (channel.read(buffer) != -1 && buffer.hasRemaining())
            ;
        buffer.position();
    }

    public static void write(ByteBuffer to, ByteBuffer from) {
        if (from.hasArray()) {
            to.put(from.array(), from.arrayOffset() + from.position(), Math.min(to.remaining(), from.remaining()));
        } else {
            to.put(toArrayL(from, to.remaining()));
        }
    }

    public static void skip(ByteBuffer buffer, int count) {
        int toSkip = Math.min(buffer.remaining(), count);
        buffer.position(buffer.position() + toSkip);
    }


    public static String readString(ByteBuffer buffer, int len) {
        return stringFromBytes(toArray(read(buffer, len)));
    }

    public static String readNullTermStringCharset(ByteBuffer buffer, String charset) {
        ByteBuffer fork = buffer.duplicate();
        //noinspection StatementWithEmptyBody
        while (buffer.hasRemaining() && buffer.get() != 0) {
        }
        fork.limit(buffer.position() - 1);
        return Platform.stringFromCharset(toArray(fork), charset);
    }

    public static ByteBuffer readBuf(ByteBuffer buffer) {
        ByteBuffer result = buffer.duplicate();
        buffer.position(buffer.limit());
        return result;
    }


    public static void closeQuietly(Closeable channel) {
        if (channel == null)
            return;
        try {
            channel.close();
        } catch (IOException ignored) {
        }
    }


    public static FileChannelWrapper readableChannel(File file) throws FileNotFoundException {
        return new FileChannelWrapper(new FileInputStream(file).getChannel());
    }

    public static FileChannelWrapper writableChannel(File file) throws FileNotFoundException {
        return new FileChannelWrapper(new FileOutputStream(file).getChannel());
    }

    public static FileChannelWrapper rwChannel(File file) throws FileNotFoundException {
        return new FileChannelWrapper(new RandomAccessFile(file, "rw").getChannel());
    }

    public static ByteBuffer duplicate(ByteBuffer bb) {
        ByteBuffer out = ByteBuffer.allocate(bb.remaining());
        out.put(bb.duplicate());
        out.flip();
        return out;
    }


}