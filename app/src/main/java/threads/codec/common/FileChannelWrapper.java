package threads.codec.common;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelWrapper implements SeekableByteChannel {

    private final FileChannel ch;

    FileChannelWrapper(FileChannel ch) {
        this.ch = ch;
    }

    @Override
    public int read(ByteBuffer arg0) throws IOException {
        return ch.read(arg0);
    }

    @Override
    public void close() throws IOException {
        ch.close();
    }

    @Override
    public boolean isOpen() {
        return ch.isOpen();
    }

    @Override
    public int write(ByteBuffer arg0) throws IOException {
        return ch.write(arg0);
    }

    @Override
    public long position() throws IOException {
        return ch.position();
    }

    @Override
    public void setPosition(long newPosition) throws IOException {
        ch.position(newPosition);
    }

    @Override
    public long size() throws IOException {
        return ch.size();
    }


}