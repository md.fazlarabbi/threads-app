package threads.codec.common;


import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import threads.codec.MP4Demuxer;
import threads.codec.Platform;

import static threads.codec.common.Format.MOV;

public class JCodecUtil {

    public static byte[] asciiString(String fourcc) {
        return Platform.getBytes(fourcc);
    }

    public static String detectFormat(File f) throws IOException {
        return detectFormatBuffer(NIOUtils.fetchFromFileL(f));
    }

    @Nullable
    private static String detectFormatBuffer(ByteBuffer b) {
        int maxScore = 0;
        String selected = null;

        int score = MP4Demuxer.probe(b);
        if (score > maxScore) {
            selected = MOV;
        }

        return selected;
    }
}