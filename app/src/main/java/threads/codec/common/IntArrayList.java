package threads.codec.common;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.arraycopy;

public class IntArrayList {
    private static final int DEFAULT_GROW_AMOUNT = 1 << 8;
    private final List<int[]> chunks;
    private final int growAmount;
    private final int _start = 0;
    private int[] storage;
    private int _size;

    private IntArrayList() {
        this.chunks = new ArrayList<>();
        this.growAmount = IntArrayList.DEFAULT_GROW_AMOUNT;
        this.storage = new int[IntArrayList.DEFAULT_GROW_AMOUNT];
    }

    public static IntArrayList createIntArrayList() {
        return new IntArrayList();
    }

    public int[] toArray() {
        int[] result = new int[_size + chunks.size() * growAmount - _start];
        int off = 0;
        for (int i = 0; i < chunks.size(); i++) {
            int[] chunk = chunks.get(i);
            int aoff = i == 0 ? _start : 0;
            arraycopy(chunk, aoff, result, off, growAmount - aoff);
            off += growAmount;
        }
        int aoff = chunks.size() == 0 ? _start : 0;
        arraycopy(storage, aoff, result, off, _size - aoff);
        return result;
    }


    public int size() {
        return chunks.size() * growAmount + _size - _start;
    }

    public void addAll(int[] other) {
        int otherOff = 0;
        while (otherOff < other.length) {
            int copyAmount = Math.min(other.length - otherOff, growAmount - _size);
            if (copyAmount < 32) {
                for (int i = 0; i < copyAmount; i++)
                    storage[_size++] = other[otherOff++];
            } else {
                arraycopy(other, otherOff, storage, _size, copyAmount);
                _size += copyAmount;
                otherOff += copyAmount;
            }
            if (otherOff < other.length) {
                chunks.add(storage);
                storage = new int[growAmount];
                _size = 0;
            }
        }
    }


}