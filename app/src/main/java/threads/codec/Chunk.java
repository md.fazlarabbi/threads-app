package threads.codec;

import java.nio.ByteBuffer;


class Chunk {
    static final int UNEQUAL_DUR = -1;
    static final int UNEQUAL_SIZES = -1;
    private final long startTv;
    private final int sampleSize;
    private final int sampleDur;
    private final int entry;
    private final long offset;
    private final int sampleCount;
    private final int[] sampleDurs;
    private final int[] sampleSizes;
    private ByteBuffer data;

    Chunk(long offset, long startTv, int sampleCount, int sampleSize, int[] sampleSizes, int sampleDur,
          int[] sampleDurs, int entry) {
        this.offset = offset;
        this.startTv = startTv;
        this.sampleCount = sampleCount;
        this.sampleSize = sampleSize;
        this.sampleSizes = sampleSizes;
        this.sampleDur = sampleDur;
        this.sampleDurs = sampleDurs;
        this.entry = entry;
    }


    long getOffset() {
        return offset;
    }

    long getStartTv() {
        return startTv;
    }

    int getSampleCount() {
        return sampleCount;
    }

    int getSampleSize() {
        return sampleSize;
    }

    int[] getSampleSizes() {
        return sampleSizes;
    }


    public int getEntry() {
        return entry;
    }

    public int getDuration() {
        if (sampleDur != UNEQUAL_DUR)
            return sampleDur * sampleCount;
        int sum = 0;
        for (int i : sampleDurs) {
            sum += i;
        }
        return sum;
    }

    public long getSize() {
        if (sampleSize != UNEQUAL_SIZES)
            return sampleSize * sampleCount;
        long sum = 0;
        for (int i : sampleSizes) {
            sum += i;
        }
        return sum;
    }

    public ByteBuffer getData() {
        return data;
    }

    public void setData(ByteBuffer data) {
        this.data = data;
    }


}