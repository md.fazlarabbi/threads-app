package threads.codec.boxes;

import java.nio.ByteBuffer;

import threads.LogUtils;
import threads.codec.Platform;
import threads.codec.common.JCodecUtil;
import threads.codec.common.NIOUtils;

public class Header {

    public static final byte[] FOURCC_FREE = new byte[]{'f', 'r', 'e', 'e'};
    private static final long MAX_UNSIGNED_INT = 0x100000000L;
    private final String fourcc;
    private long size;
    private boolean lng;

    public Header(String fourcc) {
        this.fourcc = fourcc;
    }

    public static Header createHeader(String fourcc, long size) {
        Header header = new Header(fourcc);
        header.size = size;
        return header;
    }

    private static Header newHeader(String fourcc, long size, boolean lng) {
        Header header = new Header(fourcc);
        header.size = size;
        header.lng = lng;
        return header;
    }

    public static Header read(ByteBuffer input) {
        long size = 0;
        //noinspection StatementWithEmptyBody
        while (input.remaining() >= 4 && (size = Platform.unsignedInt(input.getInt())) == 0)
            ;
        if (input.remaining() < 4 || size < 8 && size != 1) {
            LogUtils.error(LogUtils.TAG, "Broken atom of size " + size);
            return null;
        }

        String fourcc = NIOUtils.readString(input, 4);
        boolean lng = false;
        if (size == 1) {
            if (input.remaining() >= 8) {
                lng = true;
                size = input.getLong();
            } else {
                LogUtils.error(LogUtils.TAG, "Broken atom of size " + size);
                return null;
            }
        }

        return newHeader(fourcc, size, lng);
    }


    public long headerSize() {
        return lng || (size > MAX_UNSIGNED_INT) ? 16 : 8;
    }


    public String getFourcc() {
        return fourcc;
    }

    public long getBodySize() {
        return size - headerSize();
    }

    void setBodySize(int length) {
        size = length + headerSize();
    }

    public void write(ByteBuffer out) {
        if (size > MAX_UNSIGNED_INT)
            out.putInt(1);
        else
            out.putInt((int) size);
        byte[] bt = JCodecUtil.asciiString(fourcc);
        if (bt != null && bt.length == 4)
            out.put(bt);
        else
            out.put(FOURCC_FREE);
        if (size > MAX_UNSIGNED_INT) {
            out.putLong(size);
        }
    }


    public long getSize() {
        return size;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fourcc == null) ? 0 : fourcc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Header other = (Header) obj;
        if (fourcc == null) {
            return other.fourcc == null;
        } else return fourcc.equals(other.fourcc);
    }
}