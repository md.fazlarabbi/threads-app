package threads.codec.boxes;

import java.nio.ByteBuffer;

import threads.codec.common.NIOUtils;

import static threads.codec.common.JCodecUtil.asciiString;

public class HandlerBox extends FullBox {
    private String componentType;
    private String componentSubType;
    private String componentManufacturer;
    private int componentFlags;
    private int componentFlagsMask;
    private String componentName;

    public HandlerBox(Header atom) {
        super(atom);
    }


    static HandlerBox createHandlerBox() {
        HandlerBox hdlr = new HandlerBox(new Header("hdlr"));
        hdlr.componentType = null;
        hdlr.componentSubType = "mdir";
        hdlr.componentManufacturer = "appl";
        hdlr.componentFlags = 0;
        hdlr.componentFlagsMask = 0;
        hdlr.componentName = "";
        return hdlr;
    }

    public void parse(ByteBuffer input) {
        super.parse(input);

        componentType = NIOUtils.readString(input, 4);
        componentSubType = NIOUtils.readString(input, 4);
        componentManufacturer = NIOUtils.readString(input, 4);

        componentFlags = input.getInt();
        componentFlagsMask = input.getInt();
        componentName = NIOUtils.readString(input, input.remaining());
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);

        out.put(fourcc(componentType));
        out.put(fourcc(componentSubType));
        out.put(fourcc(componentManufacturer));

        out.putInt(componentFlags);
        out.putInt(componentFlagsMask);
        if (componentName != null) {
            out.put(fourcc(componentName));
        }
    }

    private byte[] fourcc(String fourcc) {
        byte[] dst = new byte[4];
        if (fourcc != null) {
            byte[] tmp = asciiString(fourcc);
            System.arraycopy(tmp, 0, dst, 0, Math.min(tmp.length, 4));
        }
        return dst;
    }

    @Override
    public int estimateSize() {
        return 32 + (componentName != null ? 4 : 0);
    }


    String getComponentSubType() {
        return componentSubType;
    }

}