package threads.codec.boxes;

import java.nio.ByteBuffer;

public class SampleDescriptionBox extends NodeBox {

    public SampleDescriptionBox(Header header) {
        super(header);
    }


    @Override
    public void parse(ByteBuffer input) {
        input.getInt();
        input.getInt();
        super.parse(input);
    }

    @Override
    public void doWrite(ByteBuffer out) {
        out.putInt(0);
        //even if there is no sample descriptors entry count can not be less than 1
        out.putInt(Math.max(1, boxes.size()));
        super.doWrite(out);
    }

    @Override
    public int estimateSize() {
        return 8 + super.estimateSize();
    }
}