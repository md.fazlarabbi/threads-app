package threads.codec.boxes;

public class MediaBox extends NodeBox {

    public MediaBox(Header atom) {
        super(atom);
    }


    MediaInfoBox getMinf() {
        return NodeBox.findFirst(this, MediaInfoBox.class, "minf");
    }
}
