package threads.codec.boxes;

public class DataInfoBox extends NodeBox {

    public DataInfoBox(Header atom) {
        super(atom);
    }


    static DataInfoBox createDataInfoBox() {
        return new DataInfoBox(new Header("dinf"));
    }

    DataRefBox getDref() {
        return DataRefBox.createDataRefBox();
    }
}
