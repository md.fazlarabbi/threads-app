package threads.codec.boxes;

import java.nio.ByteBuffer;

import threads.codec.Platform;


public class ChunkOffsetsBox extends FullBox {

    private long[] chunkOffsets;

    public ChunkOffsetsBox(Header atom) {
        super(atom);
    }


    public void parse(ByteBuffer input) {
        super.parse(input);
        int length = input.getInt();
        chunkOffsets = new long[length];
        for (int i = 0; i < length; i++) {
            chunkOffsets[i] = Platform.unsignedInt(input.getInt());
        }
    }

    @Override
    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(chunkOffsets.length);
        for (long offset : chunkOffsets) {
            out.putInt((int) offset);
        }
    }

    @Override
    public int estimateSize() {
        return 12 + 4 + chunkOffsets.length * 4;
    }

    public long[] getChunkOffsets() {
        return chunkOffsets;
    }

}
