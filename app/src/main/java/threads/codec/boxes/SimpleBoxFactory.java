package threads.codec.boxes;

import threads.codec.Boxes;
import threads.codec.IBoxFactory;
import threads.codec.Platform;

public class SimpleBoxFactory implements IBoxFactory {
    private final Boxes boxes;

    public SimpleBoxFactory(Boxes boxes) {
        this.boxes = boxes;
    }

    @Override
    public Box newBox(Header header) {
        Class<? extends Box> claz = boxes.toClass(header.getFourcc());
        if (claz == null)
            return new Box.LeafBox(header);
        return Platform.newInstance(claz, new Object[]{header});
    }

}
