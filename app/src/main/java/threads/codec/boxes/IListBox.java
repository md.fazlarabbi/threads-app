package threads.codec.boxes;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import threads.codec.Boxes;
import threads.codec.IBoxFactory;
import threads.codec.common.NIOUtils;

public class IListBox extends Box {

    private final IBoxFactory factory;
    private Map<Integer, List<Box>> values;

    public IListBox(Header atom) {
        super(atom);
        factory = new SimpleBoxFactory(new LocalBoxes());
        values = new LinkedHashMap<>();
    }

    static IListBox createIListBox(Map<Integer, List<Box>> values) {
        IListBox box = new IListBox(Header.createHeader("ilst", 0));
        box.values = values;
        return box;
    }

    public void parse(ByteBuffer input) {
        while (input.remaining() >= 4) {
            int size = input.getInt();
            ByteBuffer local = NIOUtils.read(input, size - 4);
            int index = local.getInt();
            List<Box> children = new ArrayList<>();
            values.put(index, children);
            while (local.hasRemaining()) {
                Header childAtom = Header.read(local);
                if (childAtom != null && local.remaining() >= childAtom.getBodySize()) {
                    Box box = Box.parseBox(NIOUtils.read(local, (int) childAtom.getBodySize()), childAtom, factory);
                    children.add(box);
                }
            }
        }
    }

    Map<Integer, List<Box>> getValues() {
        return values;
    }

    protected void doWrite(ByteBuffer out) {
        for (Entry<Integer, List<Box>> entry : values.entrySet()) {
            ByteBuffer fork = out.duplicate();
            out.putInt(0);
            out.putInt(entry.getKey());
            for (Box box : entry.getValue()) {
                box.write(out);
            }
            fork.putInt(out.position() - fork.position());
        }
    }

    @Override
    public int estimateSize() {
        int sz = 8;
        for (Entry<Integer, List<Box>> entry : values.entrySet()) {
            for (Box box : entry.getValue()) {
                sz += 8 + box.estimateSize();
            }
        }
        return sz;
    }

    private static class LocalBoxes extends Boxes {
        //Initializing blocks are not supported by Javascript.
        LocalBoxes() {
            super();
            mappings.put(DataBox.fourcc(), DataBox.class);
        }
    }
}
