package threads.codec.boxes;

import java.nio.ByteBuffer;

public abstract class FullBox extends Box {

    byte version;
    int flags;

    FullBox(Header atom) {
        super(atom);
    }

    public void parse(ByteBuffer input) {
        int vf = input.getInt();
        version = (byte) ((vf >> 24) & 0xff);
        flags = vf & 0xffffff;
    }

    protected void doWrite(ByteBuffer out) {
        out.putInt((version << 24) | (flags & 0xffffff));
    }


    int getFlags() {
        return flags;
    }

}