package threads.codec.boxes;

import java.util.ListIterator;
import java.util.Objects;

public class TrakBox extends NodeBox {

    public TrakBox(Header atom) {
        super(atom);
    }


    public void setDataRef(String url) {
        MediaInfoBox minf = getMdia().getMinf();
        DataInfoBox dinf = minf.getDinf();
        if (dinf == null) {
            dinf = DataInfoBox.createDataInfoBox();
            minf.add(dinf);
        }
        DataRefBox dref = dinf.getDref();
        UrlBox urlBox = UrlBox.createUrlBox(url);

        dinf.add(dref);
        dref.add(urlBox);

        ListIterator<Box> lit = dref.boxes.listIterator();
        while (lit.hasNext()) {
            FullBox box = (FullBox) lit.next();
            if ((box.getFlags() & 0x1) != 0)
                lit.set(urlBox);
        }

    }

    private MediaBox getMdia() {
        return NodeBox.findFirst(this, MediaBox.class, "mdia");
    }


    public boolean isVideo() {
        return "vide".equals(getHandlerType());
    }


    private String getHandlerType() {
        HandlerBox handlerBox = NodeBox.findFirstPath(this, HandlerBox.class, Box.path("mdia.hdlr"));
        if (handlerBox == null)
            return null;
        return handlerBox.getComponentSubType();
    }


    public int getTimescale() {
        MediaHeaderBox headerBox = NodeBox.findFirstPath(this,
                MediaHeaderBox.class, Box.path("mdia.mdhd"));
        Objects.requireNonNull(headerBox);
        return headerBox.getTimescale();
    }


    public boolean isPureRef() {
        MediaInfoBox minf = getMdia().getMinf();
        DataInfoBox dinf = minf.getDinf();
        if (dinf == null) {
            return false;
        }
        DataRefBox dref = dinf.getDref();
        if (dref == null)
            return false;

        for (Box box : dref.boxes) {
            if ((((FullBox) box).getFlags() & 0x1) != 0)
                return false;
        }
        return true;
    }


    public SampleEntry[] getSampleEntries() {
        return NodeBox.findAllPath(this, SampleEntry.class, new String[]{"mdia", "minf", "stbl", "stsd", null});
    }


    public TimeToSampleBox getStts() {
        return NodeBox.findFirstPath(this, TimeToSampleBox.class, Box.path("mdia.minf.stbl.stts"));
    }

    public ChunkOffsetsBox getStco() {
        return NodeBox.findFirstPath(this, ChunkOffsetsBox.class, Box.path("mdia.minf.stbl.stco"));
    }

    public ChunkOffsets64Box getCo64() {
        return NodeBox.findFirstPath(this, ChunkOffsets64Box.class, Box.path("mdia.minf.stbl.co64"));
    }

    public SampleSizesBox getStsz() {
        return NodeBox.findFirstPath(this, SampleSizesBox.class, Box.path("mdia.minf.stbl.stsz"));
    }

    public SampleToChunkBox getStsc() {
        return NodeBox.findFirstPath(this, SampleToChunkBox.class, Box.path("mdia.minf.stbl.stsc"));
    }

    @SuppressWarnings("UnusedReturnValue")
    public SampleDescriptionBox getStsd() {
        return NodeBox.findFirstPath(this, SampleDescriptionBox.class, Box.path("mdia.minf.stbl.stsd"));
    }


}