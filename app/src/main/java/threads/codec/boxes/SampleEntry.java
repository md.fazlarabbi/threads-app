package threads.codec.boxes;

import java.nio.ByteBuffer;

public class SampleEntry extends NodeBox {

    private short drefInd;

    public SampleEntry(Header header) {
        super(header);
    }

    public void parse(ByteBuffer input) {
        input.getInt();
        input.getShort();

        drefInd = input.getShort();
    }


    protected void doWrite(ByteBuffer out) {
        out.put(new byte[]{0, 0, 0, 0, 0, 0});
        out.putShort(drefInd); // data ref index
    }


    public short getDrefInd() {
        return drefInd;
    }


    @Override
    public int estimateSize() {
        return 8 + super.estimateSize();
    }
}
