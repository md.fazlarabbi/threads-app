package threads.codec.boxes;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import threads.codec.IBoxFactory;
import threads.codec.common.NIOUtils;

public class UdtaBox extends NodeBox {
    private static final String META_GPS = "©xyz";

    private static final Map<String, Integer> knownMetadata = new HashMap<>();

    static {
        knownMetadata.put(META_GPS, MetaValue.TYPE_STRING_UTF8);
    }

    public UdtaBox(Header atom) {
        super(atom);
    }

    public static UdtaBox createUdtaBox() {
        return new UdtaBox(Header.createHeader("udta", 0));
    }


    @Override
    public void setFactory(final IBoxFactory _factory) {
        factory = header -> {
            if (header.getFourcc().equals(UdtaMetaBox.fourcc())) {
                UdtaMetaBox box = new UdtaMetaBox(header);
                box.setFactory(_factory);
                return box;
            }

            return _factory.newBox(header);
        };
    }


    private List<MetaValue> parseStringData(ByteBuffer bb) {
        List<MetaValue> result = new ArrayList<>();
        while (bb.remaining() >= 4) {
            int lang = 0;
            int sz = NIOUtils.duplicate(bb).getInt();
            if (4 + sz > bb.remaining()) {
                sz = bb.getShort();
                if (2 + sz > bb.remaining())
                    break;
                lang = bb.getShort();
            }
            if (sz != 0) {
                byte[] bytes = new byte[sz];
                bb.get(bytes);
                result.add(MetaValue.createStringWithLocale(new String(bytes), lang));
            }
        }
        return result;
    }

    private ByteBuffer serializeStringData(List<MetaValue> data) {
        int totalLen = 0;
        for (MetaValue mv : data) {
            String string = mv.getString();
            if (string != null)
                totalLen += string.getBytes().length + 4;
        }
        if (totalLen == 0)
            return null;
        byte[] bytes = new byte[totalLen];
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        for (MetaValue mv : data) {
            String string = mv.getString();
            if (string != null) {
                bb.putShort((short) string.length());
                // en_US
                bb.putShort((short) mv.getLocale());
                bb.put(string.getBytes());
            }
        }
        bb.flip();
        return bb;
    }


    public Map<Integer, MetaValue> getMetadata() {
        Map<Integer, MetaValue> result = new HashMap<>();
        List<Box> boxes = getBoxes();
        for (Box box : boxes) {
            String fourcc = box.getFourcc();
            if (knownMetadata.containsKey(fourcc) && (box instanceof LeafBox)) {
                Integer object = knownMetadata.get(fourcc);
                Objects.requireNonNull(object);
                int type = object;
                ByteBuffer data = ((LeafBox) box).getData();

                // only string for now :(
                if (type != MetaValue.TYPE_STRING_UTF8)
                    continue;
                List<MetaValue> value = parseStringData(data);
                byte[] bytes;
                try {
                    bytes = box.getFourcc().getBytes("iso8859-1");
                } catch (UnsupportedEncodingException e) {
                    bytes = null;
                }
                if (bytes != null && !value.isEmpty())
                    result.put(ByteBuffer.wrap(bytes).getInt(), value.get(0));
            }
        }

        return result;
    }

    public void setMetadata(Map<Integer, MetaValue> udata) {
        Set<Entry<Integer, MetaValue>> entrySet = udata.entrySet();
        for (Entry<Integer, MetaValue> entry : entrySet) {
            List<MetaValue> lst = new LinkedList<>();
            lst.add(entry.getValue());
            ByteBuffer bb = serializeStringData(lst);
            byte[] bytes = new byte[4];
            ByteBuffer.wrap(bytes).putInt(entry.getKey());
            LeafBox box;
            try {
                Objects.requireNonNull(bb);
                box = LeafBox.createLeafBox(Header.createHeader(
                        new String(bytes, "iso8859-1"), bb.remaining() + 4),
                        bb);
                replaceBox(box);
            } catch (UnsupportedEncodingException ignored) {
            }
        }
    }
}
