package threads.codec.boxes;

import java.nio.ByteBuffer;


public class TimeToSampleBox extends FullBox {

    private TimeToSampleEntry[] entries;

    public TimeToSampleBox(Header atom) {
        super(atom);
    }


    public void parse(ByteBuffer input) {
        super.parse(input);
        int foo = input.getInt();
        entries = new TimeToSampleEntry[foo];
        for (int i = 0; i < foo; i++) {
            entries[i] = new TimeToSampleEntry(input.getInt(), input.getInt());
        }
    }

    public TimeToSampleEntry[] getEntries() {
        return entries;
    }

    @Override
    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(entries.length);
        for (TimeToSampleEntry timeToSampleEntry : entries) {
            out.putInt(timeToSampleEntry.getSampleCount());
            out.putInt(timeToSampleEntry.getSampleDuration());
        }
    }

    @Override
    public int estimateSize() {
        return 16 + entries.length * 8;
    }

    public static class TimeToSampleEntry {
        final int sampleCount;
        final int sampleDuration;

        TimeToSampleEntry(int sampleCount, int sampleDuration) {
            this.sampleCount = sampleCount;
            this.sampleDuration = sampleDuration;
        }

        public int getSampleCount() {
            return sampleCount;
        }


        public int getSampleDuration() {
            return sampleDuration;
        }

    }
}