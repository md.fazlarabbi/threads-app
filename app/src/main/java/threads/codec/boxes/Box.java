package threads.codec.boxes;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

import threads.codec.IBoxFactory;
import threads.codec.Platform;
import threads.codec.common.NIOUtils;


public abstract class Box {
    public static final int MAX_BOX_SIZE = 128 * 1024 * 1024;
    final Header header;

    Box(Header header) {
        this.header = header;
    }


    public static String[] path(String path) {
        return path.split("\\.");
    }

    static LeafBox createLeafBox(Header atom, ByteBuffer data) {
        LeafBox leaf = new LeafBox(atom);
        leaf.data = data;
        return leaf;
    }

    static Box parseBox(ByteBuffer input, Header childAtom, IBoxFactory factory) {
        Box box = factory.newBox(childAtom);

        if (childAtom.getBodySize() < Box.MAX_BOX_SIZE) {
            box.parse(input);
            return box;
        } else {
            return new LeafBox(Header.createHeader("free", 8));
        }
    }

    static <T extends Box> T asBox(Class<T> class1, Box box) {
        try {
            T res = Platform.newInstance(class1, new Object[]{box.getHeader()});
            ByteBuffer buffer = ByteBuffer.allocate((int) box.getHeader().getBodySize());
            box.doWrite(buffer);
            buffer.flip();
            res.parse(buffer);
            return res;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void checkState(boolean expression) {
        if (!expression) {
            throw new IllegalStateException();
        }
    }

    public Header getHeader() {
        return header;
    }

    public abstract void parse(ByteBuffer buf);

    public void write(ByteBuffer buf) {
        ByteBuffer dup = buf.duplicate();
        NIOUtils.skip(buf, 8);
        doWrite(buf);

        header.setBodySize(buf.position() - dup.position() - 8);
        checkState(header.headerSize() == (long) 8);
        header.write(dup);
    }

    protected abstract void doWrite(ByteBuffer out);

    protected abstract int estimateSize();

    String getFourcc() {
        return header.getFourcc();
    }

    @NonNull
    public String toString() {
        StringBuilder sb = new StringBuilder();
        dump(sb);
        return sb.toString();

    }

    void dump(StringBuilder sb) {
        sb.append("{\"tag\":\"").append(header.getFourcc()).append("\"}");
    }

    public static class LeafBox extends Box {
        ByteBuffer data;

        public LeafBox(Header atom) {
            super(atom);
        }

        public void parse(ByteBuffer input) {
            data = NIOUtils.read(input, (int) header.getBodySize());
        }

        ByteBuffer getData() {
            return data.duplicate();
        }

        @Override
        protected void doWrite(ByteBuffer out) {
            NIOUtils.write(out, data);
        }

        @Override
        public int estimateSize() {
            return data.remaining() + 8;
        }
    }

}