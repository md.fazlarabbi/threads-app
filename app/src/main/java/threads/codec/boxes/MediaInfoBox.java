package threads.codec.boxes;

public class MediaInfoBox extends NodeBox {

    public MediaInfoBox(Header atom) {
        super(atom);
    }


    DataInfoBox getDinf() {
        return NodeBox.findFirst(this, DataInfoBox.class, "dinf");
    }

}
