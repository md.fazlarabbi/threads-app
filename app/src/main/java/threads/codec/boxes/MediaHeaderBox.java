package threads.codec.boxes;

import java.nio.ByteBuffer;

import static threads.codec.TimeUtil.fromMovTime;
import static threads.codec.TimeUtil.toMovTime;

public class MediaHeaderBox extends FullBox {
    private long created;
    private long modified;
    private int timescale;
    private long duration;
    private int language;
    private int quality;

    public MediaHeaderBox(Header atom) {
        super(atom);
    }


    int getTimescale() {
        return timescale;
    }


    public void parse(ByteBuffer input) {
        super.parse(input);
        if (version == 0) {
            created = fromMovTime(input.getInt());
            modified = fromMovTime(input.getInt());
            timescale = input.getInt();
            duration = input.getInt();
        } else if (version == 1) {
            created = fromMovTime((int) input.getLong());
            modified = fromMovTime((int) input.getLong());
            timescale = input.getInt();
            duration = input.getLong();
        } else {
            throw new RuntimeException("Unsupported version");
        }
        language = input.getShort();
        quality = input.getShort();
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        if (version == 0) {
            out.putInt(toMovTime(created));
            out.putInt(toMovTime(modified));
            out.putInt(timescale);
            out.putInt((int) duration);
        } else if (version == 1) {
            out.putLong(toMovTime(created));
            out.putLong(toMovTime(modified));
            out.putInt(timescale);
            out.putLong((int) duration);
        }
        out.putShort((short) language);
        out.putShort((short) quality);
    }

    @Override
    public int estimateSize() {
        return 32;
    }
}