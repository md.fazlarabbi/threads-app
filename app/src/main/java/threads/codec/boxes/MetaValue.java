package threads.codec.boxes;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import threads.codec.Platform;


public class MetaValue {
    static final int TYPE_STRING_UTF8 = 1;
    // All data types below are Big Endian
    private static final int TYPE_STRING_UTF16 = 2;
    private static final int TYPE_FLOAT_64 = 24;
    private static final int TYPE_FLOAT_32 = 23;
    private static final int TYPE_INT_32 = 67;
    private static final int TYPE_INT_16 = 66;
    private static final int TYPE_INT_8 = 65;
    private static final int TYPE_INT_V = 22;
    private static final int TYPE_UINT_V = 21;

    private final int type;
    private final int locale;
    private final byte[] data;

    private MetaValue(int type, int locale, byte[] data) {
        this.type = type;
        this.locale = locale;
        this.data = data;
    }


    public static MetaValue createString(String value) {
        return new MetaValue(1, 0, Platform.getBytesForCharset(value, Platform.UTF_8));
    }

    static MetaValue createStringWithLocale(String value, int locale) {
        return new MetaValue(1, locale, Platform.getBytesForCharset(value, Platform.UTF_8));
    }


    static MetaValue createOtherWithLocale(int type, int locale, byte[] data) {
        return new MetaValue(type, locale, data);
    }

    private int getInt() {
        if (type == TYPE_UINT_V || type == TYPE_INT_V) {
            switch (data.length) {
                case 1:
                    return data[0];
                case 2:
                    return toInt16(data);
                case 3:
                    return toInt24(data);
                case 4:
                    return toInt32(data);
            }
        }
        if (type == TYPE_INT_8)
            return data[0];
        if (type == TYPE_INT_16)
            return toInt16(data);
        if (type == TYPE_INT_32)
            return toInt32(data);
        return 0;
    }

    private double getFloat() {
        if (type == TYPE_FLOAT_32)
            return toFloat(data);
        if (type == TYPE_FLOAT_64)
            return toDouble(data);
        return 0;
    }

    public String getString() {
        if (type == TYPE_STRING_UTF8)
            return Platform.stringFromCharset(data, Platform.UTF_8);
        if (type == TYPE_STRING_UTF16) {
            return Platform.stringFromCharset(data, Platform.UTF_16BE);
        }
        return null;
    }

    private boolean isInt() {
        return type == TYPE_UINT_V || type == TYPE_INT_V || type == TYPE_INT_8 || type == TYPE_INT_16 || type == TYPE_INT_32;
    }

    private boolean isString() {
        return type == TYPE_STRING_UTF8 || type == TYPE_STRING_UTF16;
    }

    private boolean isFloat() {
        return type == TYPE_FLOAT_32 || type == TYPE_FLOAT_64;
    }

    @NonNull
    public String toString() {
        if (isInt())
            return String.valueOf(getInt());
        else if (isFloat())
            return String.valueOf(getFloat());
        else if (isString())
            return String.valueOf(getString());
        else
            return "BLOB";
    }

    public int getType() {
        return type;
    }

    int getLocale() {
        return locale;
    }

    public byte[] getData() {
        return data;
    }

    private int toInt16(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.BIG_ENDIAN);
        return bb.getShort();
    }

    private int toInt24(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.BIG_ENDIAN);
        return ((bb.getShort() & 0xffff) << 8) | (bb.get() & 0xff);
    }

    private int toInt32(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.BIG_ENDIAN);
        return bb.getInt();
    }

    private float toFloat(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.BIG_ENDIAN);
        return bb.getFloat();
    }

    private double toDouble(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.BIG_ENDIAN);
        return bb.getDouble();
    }

}