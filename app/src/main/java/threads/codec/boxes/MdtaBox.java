package threads.codec.boxes;

import java.nio.ByteBuffer;

import threads.codec.Platform;
import threads.codec.common.NIOUtils;


public class MdtaBox extends Box {

    private String key;

    public MdtaBox(Header header) {
        super(header);
    }

    static MdtaBox createMdtaBox(String key) {
        MdtaBox box = new MdtaBox(Header.createHeader("mdta", 0));
        box.key = key;
        return box;
    }

    @Override
    public void parse(ByteBuffer buf) {
        key = Platform.stringFromBytes(NIOUtils.toArray(NIOUtils.readBuf(buf)));
    }

    public String getKey() {
        return key;
    }

    @Override
    protected void doWrite(ByteBuffer out) {
        out.put(key.getBytes());
    }

    @Override
    public int estimateSize() {
        return key.getBytes().length;
    }
}
