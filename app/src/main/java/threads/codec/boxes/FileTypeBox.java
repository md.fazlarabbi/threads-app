package threads.codec.boxes;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedList;

import threads.codec.common.JCodecUtil;
import threads.codec.common.NIOUtils;

public class FileTypeBox extends Box {
    private final Collection<String> compBrands;
    private String majorBrand;
    private int minorVersion;

    public FileTypeBox(Header header) {
        super(header);
        this.compBrands = new LinkedList<>();
    }

    public void parse(ByteBuffer input) {
        majorBrand = NIOUtils.readString(input, 4);
        minorVersion = input.getInt();

        String brand;
        while (input.hasRemaining() && (brand = NIOUtils.readString(input, 4)) != null) {
            compBrands.add(brand);
        }
    }

    public void doWrite(ByteBuffer out) {
        out.put(JCodecUtil.asciiString(majorBrand));
        out.putInt(minorVersion);

        for (String string : compBrands) {
            out.put(JCodecUtil.asciiString(string));
        }
    }

    @Override
    public int estimateSize() {
        int size = 5 + 8;

        for (String string : compBrands) {
            size += JCodecUtil.asciiString(string).length;
        }

        return size;
    }
}