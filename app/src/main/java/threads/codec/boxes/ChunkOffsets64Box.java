package threads.codec.boxes;

import java.nio.ByteBuffer;

public class ChunkOffsets64Box extends FullBox {
    private long[] chunkOffsets;

    public ChunkOffsets64Box(Header atom) {
        super(atom);
    }


    public static ChunkOffsets64Box createChunkOffsets64Box(long[] offsets) {
        ChunkOffsets64Box co64 = new ChunkOffsets64Box(Header.createHeader("co64", 0));
        co64.chunkOffsets = offsets;
        return co64;
    }

    public void parse(ByteBuffer input) {
        super.parse(input);
        int length = input.getInt();
        chunkOffsets = new long[length];
        for (int i = 0; i < length; i++) {
            chunkOffsets[i] = input.getLong();
        }
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(chunkOffsets.length);
        for (long offset : chunkOffsets) {
            out.putLong(offset);
        }
    }

    @Override
    public int estimateSize() {
        return 12 + 4 + chunkOffsets.length * 8;
    }

    public long[] getChunkOffsets() {
        return chunkOffsets;
    }

}
