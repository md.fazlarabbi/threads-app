package threads.codec.boxes;

import java.nio.ByteBuffer;

public class DataRefBox extends NodeBox {

    public DataRefBox(Header atom) {
        super(atom);
    }


    static DataRefBox createDataRefBox() {
        return new DataRefBox(new Header("dref"));
    }

    @Override
    public void parse(ByteBuffer input) {
        input.getInt();
        input.getInt();
        super.parse(input);
    }

    @Override
    public void doWrite(ByteBuffer out) {
        out.putInt(0);
        out.putInt(boxes.size());
        super.doWrite(out);
    }

    @Override
    public int estimateSize() {
        return 8 + super.estimateSize();
    }
}
