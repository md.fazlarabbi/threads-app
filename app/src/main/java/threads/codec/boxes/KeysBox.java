package threads.codec.boxes;

import java.nio.ByteBuffer;

import threads.codec.Boxes;

public class KeysBox extends NodeBox {
    private static final String FOURCC = "keys";

    public KeysBox(Header atom) {
        super(atom);
        factory = new SimpleBoxFactory(new LocalBoxes());
    }

    static KeysBox createKeysBox() {
        return new KeysBox(Header.createHeader(FOURCC, 0));
    }

    public static String fourcc() {
        return FOURCC;
    }

    public void parse(ByteBuffer input) {
        input.getInt();
        input.getInt();
        super.parse(input);
    }

    protected void doWrite(ByteBuffer out) {
        out.putInt(0);
        out.putInt(boxes.size());
        super.doWrite(out);
    }

    @Override
    public int estimateSize() {
        return 8 + super.estimateSize();
    }

    private static class LocalBoxes extends Boxes {
        //Initializing blocks are not supported by Javascript.
        LocalBoxes() {
            super();
            mappings.put("mdta", MdtaBox.class);
        }
    }
}
