package threads.codec.boxes;

import java.nio.ByteBuffer;


public class SampleToChunkBox extends FullBox {

    private SampleToChunkEntry[] sampleToChunk;

    public SampleToChunkBox(Header atom) {
        super(atom);
    }


    public void parse(ByteBuffer input) {
        super.parse(input);
        int size = input.getInt();

        sampleToChunk = new SampleToChunkEntry[size];
        for (int i = 0; i < size; i++) {
            sampleToChunk[i] = new SampleToChunkEntry(input.getInt(), input.getInt(),
                    input.getInt());
        }
    }

    public SampleToChunkEntry[] getSampleToChunk() {
        return sampleToChunk;
    }


    @Override
    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(sampleToChunk.length);

        for (SampleToChunkEntry stc : sampleToChunk) {
            out.putInt((int) stc.getFirst());
            out.putInt(stc.getCount());
            out.putInt(stc.getEntry());
        }
    }

    @Override
    public int estimateSize() {
        return 16 + sampleToChunk.length * 12;
    }

    public static class SampleToChunkEntry {
        private final long first;
        private final int count;
        private final int entry;

        SampleToChunkEntry(long first, int count, int entry) {
            this.first = first;
            this.count = count;
            this.entry = entry;
        }

        public long getFirst() {
            return first;
        }


        public int getCount() {
            return count;
        }


        public int getEntry() {
            return entry;
        }

    }
}