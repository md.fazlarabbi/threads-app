package threads.codec.boxes;

import java.nio.ByteBuffer;

public class SampleSizesBox extends FullBox {
    private int defaultSize;
    private int count;
    private int[] sizes;

    public SampleSizesBox(Header atom) {
        super(atom);
    }

    public static SampleSizesBox createSampleSizesBox(int defaultSize, int count) {
        SampleSizesBox stsz = new SampleSizesBox(new Header("stsz"));
        stsz.defaultSize = defaultSize;
        stsz.count = count;
        return stsz;
    }

    public static SampleSizesBox createSampleSizesBox2(int[] sizes) {
        SampleSizesBox stsz = new SampleSizesBox(new Header("stsz"));
        stsz.sizes = sizes;
        stsz.count = sizes.length;
        return stsz;
    }

    public void parse(ByteBuffer input) {
        super.parse(input);
        defaultSize = input.getInt();
        count = input.getInt();

        if (defaultSize == 0) {
            sizes = new int[count];
            for (int i = 0; i < count; i++) {
                sizes[i] = input.getInt();
            }
        }
    }

    public int getDefaultSize() {
        return defaultSize;
    }

    public int[] getSizes() {
        return sizes;
    }


    @Override
    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(defaultSize);

        if (defaultSize == 0) {
            out.putInt(count);
            for (long size : sizes) {
                out.putInt((int) size);
            }
        } else {
            out.putInt(count);
        }
    }

    @Override
    public int estimateSize() {
        return (defaultSize == 0 ? sizes.length * 4 : 0) + 20;
    }
}