package threads.codec.boxes;

import java.nio.ByteBuffer;

import threads.codec.Platform;
import threads.codec.common.NIOUtils;

public class UrlBox extends FullBox {

    private String url;

    private UrlBox(Header atom) {
        super(atom);
    }


    static UrlBox createUrlBox(String url) {
        UrlBox urlBox = new UrlBox(new Header("url "));
        urlBox.url = url;
        return urlBox;
    }

    @Override
    public void parse(ByteBuffer input) {
        super.parse(input);
        if ((flags & 0x1) != 0)
            return;
        url = NIOUtils.readNullTermStringCharset(input, Platform.UTF_8);
    }

    @Override
    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);

        if (url != null) {
            NIOUtils.write(out, ByteBuffer.wrap(Platform.getBytesForCharset(url, Platform.UTF_8)));
            out.put((byte) 0);
        }
    }

    @Override
    public int estimateSize() {
        int sz = 13;

        if (url != null) {
            sz += Platform.getBytesForCharset(url, Platform.UTF_8).length;
        }
        return sz;
    }

    public String getUrl() {
        return url;
    }
}
