package threads.codec.boxes;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import threads.LogUtils;
import threads.codec.IBoxFactory;
import threads.codec.Platform;
import threads.codec.common.NIOUtils;

public class NodeBox extends Box {
    final List<Box> boxes;
    IBoxFactory factory;

    public NodeBox(Header atom) {
        super(atom);
        this.boxes = new LinkedList<>();
    }

    private static Box parseChildBox(ByteBuffer input, IBoxFactory factory) {
        ByteBuffer fork = input.duplicate();
        while (input.remaining() >= 4 && fork.getInt() == 0)
            input.getInt();
        if (input.remaining() < 4)
            return null;

        Box ret = null;
        Header childAtom = Header.read(input);
        if (childAtom != null && input.remaining() >= childAtom.getBodySize()) {
            ret = Box.parseBox(NIOUtils.read(input, (int) childAtom.getBodySize()), childAtom, factory);
        }
        return ret;
    }


    public static <T extends Box> T[] findAll(Box box, Class<T> class1, String path) {
        return findAllPath(box, class1, new String[]{path});
    }

    public static <T extends Box> T findFirst(NodeBox box, Class<T> clazz, String path) {
        return findFirstPath(box, clazz, new String[]{path});
    }

    public static <T extends Box> T findFirstPath(NodeBox box, Class<T> clazz, String[] path) {
        T[] result = findAllPath(box, clazz, path);
        return result.length > 0 ? result[0] : null;
    }

    static <T extends Box> T[] findAllPath(Box box, Class<T> class1, String[] path) {
        List<Box> result = new LinkedList<>();
        findBox(box, new ArrayList<>(Arrays.asList(path)), result);

        for (ListIterator<Box> it = result.listIterator(); it.hasNext(); ) {
            Box next = it.next();
            if (next == null) {
                it.remove();
            } else if (!Platform.isAssignableFrom(class1, next.getClass())) {
                // Trying to reinterpret one box as the other
                try {
                    it.set(Box.asBox(class1, next));
                } catch (Exception e) {
                    LogUtils.error(LogUtils.TAG, "Failed to reinterpret box: " + next.getFourcc() + " as: " + class1.getName() + "."
                            + e.getMessage());
                    it.remove();
                }
            }
        }
        return result.toArray((T[]) Array.newInstance(class1, 0));
    }

    private static void findBox(Box root, List<String> path, Collection<Box> result) {

        if (path.size() > 0) {
            String head = path.remove(0);
            if (root instanceof NodeBox) {
                NodeBox nb = (NodeBox) root;
                for (Box candidate : nb.getBoxes()) {
                    if (head == null || head.equals(candidate.header.getFourcc())) {
                        findBox(candidate, path, result);
                    }
                }
            }
            path.add(0, head);
        } else {
            result.add(root);
        }
    }

    public void setFactory(IBoxFactory factory) {
        this.factory = factory;
    }

    public void parse(ByteBuffer input) {

        while (input.remaining() >= 8) {
            Box child = parseChildBox(input, factory);
            if (child != null)
                boxes.add(child);
        }
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public void add(Box box) {
        boxes.add(box);
    }

    protected void doWrite(ByteBuffer out) {
        for (Box box : boxes) {
            box.write(out);
        }
    }

    @Override
    public int estimateSize() {
        int total = 0;
        for (Box box : boxes) {
            total += box.estimateSize();
        }
        return total + 8;
    }


    public void replaceBox(Box box) {
        removeChildren(new String[]{box.getFourcc()});
        add(box);
    }

    protected void dump(StringBuilder sb) {
        sb.append("{\"tag\":\"").append(header.getFourcc()).append("\",");
        sb.append("\"boxes\": [");
        dumpBoxes(sb);
        sb.append("]");
        sb.append("}");
    }

    private void dumpBoxes(StringBuilder sb) {
        for (int i = 0; i < boxes.size(); i++) {
            boxes.get(i).dump(sb);
            if (i < boxes.size() - 1)
                sb.append(",");
        }
    }

    public void removeChildren(String[] fourcc) {
        for (Iterator<Box> it = boxes.iterator(); it.hasNext(); ) {
            Box box = it.next();
            String fcc = box.getFourcc();
            for (String cand : fourcc) {
                if (cand.equals(fcc)) {
                    it.remove();
                    break;
                }
            }
        }
    }
}