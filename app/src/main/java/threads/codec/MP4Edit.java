package threads.codec;

interface MP4Edit {

    void apply(MovieBox mov);
}
