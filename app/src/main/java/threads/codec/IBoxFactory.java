package threads.codec;

import threads.codec.boxes.Box;
import threads.codec.boxes.Header;

public interface IBoxFactory {

    Box newBox(Header header);
}