package threads.codec;

import java.nio.ByteBuffer;

import threads.codec.common.NIOUtils;

import static threads.codec.common.Fourcc.free;
import static threads.codec.common.Fourcc.ftyp;
import static threads.codec.common.Fourcc.mdat;
import static threads.codec.common.Fourcc.moov;
import static threads.codec.common.Fourcc.wide;

public class MP4Demuxer {


    public static int probe(final ByteBuffer b) {
        ByteBuffer fork = b.duplicate();
        int success = 0;
        int total = 0;
        while (fork.remaining() >= 8) {
            long len = Platform.unsignedInt(fork.getInt());
            int fcc = fork.getInt();
            int hdrLen = 8;
            if (len == 1) {
                len = fork.getLong();
                hdrLen = 16;
            } else if (len < 8)
                break;
            if (fcc == ftyp && len < 64 || fcc == moov && len < 100 * 1024 * 1024 || fcc == free || fcc == mdat
                    || fcc == wide)
                success++;
            total++;
            if (len >= Integer.MAX_VALUE)
                break;
            NIOUtils.skip(fork, (int) (len - hdrLen));
        }

        return total == 0 ? 0 : success * 100 / total;
    }

}