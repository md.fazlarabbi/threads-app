package threads.codec;

import java.io.IOException;

import threads.codec.boxes.ChunkOffsets64Box;
import threads.codec.boxes.ChunkOffsetsBox;
import threads.codec.boxes.SampleEntry;
import threads.codec.boxes.SampleSizesBox;
import threads.codec.boxes.SampleToChunkBox;
import threads.codec.boxes.SampleToChunkBox.SampleToChunkEntry;
import threads.codec.boxes.TimeToSampleBox;
import threads.codec.boxes.TimeToSampleBox.TimeToSampleEntry;
import threads.codec.boxes.TrakBox;
import threads.codec.common.NIOUtils;
import threads.codec.common.SeekableByteChannel;


class ChunkReader {
    private final long[] chunkOffsets;
    private final SampleToChunkEntry[] sampleToChunk;
    private final SampleSizesBox stsz;
    private final TimeToSampleEntry[] tts;
    private final SeekableByteChannel[] inputs;
    private final SampleEntry[] entries;
    private int curChunk;
    private int sampleNo;
    private int s2cIndex;
    private int ttsInd = 0;
    private int ttsSubInd = 0;
    private long chunkTv = 0;

    ChunkReader(TrakBox trakBox, SeekableByteChannel[] inputs) {
        TimeToSampleBox stts = trakBox.getStts();
        tts = stts.getEntries();
        ChunkOffsetsBox stco = trakBox.getStco();
        ChunkOffsets64Box co64 = trakBox.getCo64();
        stsz = trakBox.getStsz();
        SampleToChunkBox stsc = trakBox.getStsc();

        if (stco != null)
            chunkOffsets = stco.getChunkOffsets();
        else
            chunkOffsets = co64.getChunkOffsets();
        sampleToChunk = stsc.getSampleToChunk();
        trakBox.getStsd();
        entries = trakBox.getSampleEntries();
        this.inputs = inputs;
    }


    Chunk next() throws IOException {
        if (curChunk >= chunkOffsets.length)
            return null;

        if (s2cIndex + 1 < sampleToChunk.length && curChunk + 1 == sampleToChunk[s2cIndex + 1].getFirst())
            s2cIndex++;
        int sampleCount = sampleToChunk[s2cIndex].getCount();

        int[] samplesDur = null;
        int sampleDur = Chunk.UNEQUAL_DUR;
        if (ttsSubInd + sampleCount <= tts[ttsInd].getSampleCount()) {
            sampleDur = tts[ttsInd].getSampleDuration();
            ttsSubInd += sampleCount;
        } else {
            samplesDur = new int[sampleCount];
            for (int i = 0; i < sampleCount; i++) {
                if (ttsSubInd >= tts[ttsInd].getSampleCount() && ttsInd < tts.length - 1) {
                    ttsSubInd = 0;
                    ++ttsInd;
                }
                samplesDur[i] = tts[ttsInd].getSampleDuration();
                ++ttsSubInd;
            }
        }

        int size = Chunk.UNEQUAL_SIZES;
        int[] sizes = null;
        if (stsz.getDefaultSize() > 0) {
            size = getFrameSize();
        } else {
            sizes = Platform.copyOfRangeI(stsz.getSizes(), sampleNo, sampleNo + sampleCount);
        }

        int dref = sampleToChunk[s2cIndex].getEntry();
        Chunk chunk = new Chunk(chunkOffsets[curChunk], chunkTv, sampleCount, size, sizes, sampleDur, samplesDur, dref);

        chunkTv += chunk.getDuration();
        sampleNo += sampleCount;
        ++curChunk;

        if (inputs != null) {
            SeekableByteChannel input = getInput(chunk);
            input.setPosition(chunk.getOffset());
            chunk.setData(NIOUtils.fetchFromChannel(input, (int) chunk.getSize()));
        }
        return chunk;
    }

    private SeekableByteChannel getInput(Chunk chunk) {
        SampleEntry se = entries[chunk.getEntry() - 1];
        return inputs[se.getDrefInd() - 1];
    }

    private int getFrameSize() {
        return stsz.getDefaultSize();
    }

}