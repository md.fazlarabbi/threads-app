package threads.codec;

import java.util.HashMap;
import java.util.Map;

import threads.codec.boxes.Box;

public abstract class Boxes {
    protected final Map<String, Class<? extends Box>> mappings;

    protected Boxes() {
        this.mappings = new HashMap<>();
    }

    public Class<? extends Box> toClass(String fourcc) {
        return mappings.get(fourcc);
    }


}
