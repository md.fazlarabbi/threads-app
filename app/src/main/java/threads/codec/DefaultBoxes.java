package threads.codec;

import threads.codec.boxes.Box;
import threads.codec.boxes.Box.LeafBox;
import threads.codec.boxes.ChunkOffsets64Box;
import threads.codec.boxes.ChunkOffsetsBox;
import threads.codec.boxes.DataInfoBox;
import threads.codec.boxes.FileTypeBox;
import threads.codec.boxes.HandlerBox;
import threads.codec.boxes.IListBox;
import threads.codec.boxes.KeysBox;
import threads.codec.boxes.MediaBox;
import threads.codec.boxes.MediaHeaderBox;
import threads.codec.boxes.MediaInfoBox;
import threads.codec.boxes.MetaBox;
import threads.codec.boxes.MovieHeaderBox;
import threads.codec.boxes.NodeBox;
import threads.codec.boxes.SampleDescriptionBox;
import threads.codec.boxes.SampleSizesBox;
import threads.codec.boxes.SampleToChunkBox;
import threads.codec.boxes.TimeToSampleBox;
import threads.codec.boxes.TrakBox;
import threads.codec.boxes.UdtaBox;

class DefaultBoxes extends Boxes {
    DefaultBoxes() {
        super();


        mappings.put("ftyp", FileTypeBox.class);
        mappings.put("moov", MovieBox.class);
        mappings.put("mvhd", MovieHeaderBox.class);
        mappings.put("trak", TrakBox.class);

        mappings.put("edts", NodeBox.class);

        mappings.put("mdia", MediaBox.class);
        mappings.put("mdhd", MediaHeaderBox.class);
        mappings.put("minf", MediaInfoBox.class);
        mappings.put("hdlr", HandlerBox.class);
        mappings.put("dinf", DataInfoBox.class);
        mappings.put("stbl", NodeBox.class);
        mappings.put("stsd", SampleDescriptionBox.class);
        mappings.put("stts", TimeToSampleBox.class);

        mappings.put("stsc", SampleToChunkBox.class);
        mappings.put("stsz", SampleSizesBox.class);
        mappings.put("stco", ChunkOffsetsBox.class);
        mappings.put("keys", KeysBox.class);
        mappings.put("ilst", IListBox.class);
        mappings.put("mvex", NodeBox.class);
        mappings.put("moof", NodeBox.class);
        mappings.put("traf", NodeBox.class);
        mappings.put("mfra", NodeBox.class);
        mappings.put("skip", NodeBox.class);
        mappings.put("meta", MetaBox.class);
        mappings.put("ipro", NodeBox.class);
        mappings.put("sinf", NodeBox.class);
        mappings.put("co64", ChunkOffsets64Box.class);
        mappings.put("clip", NodeBox.class);

        mappings.put("tapt", NodeBox.class);
        mappings.put("gmhd", NodeBox.class);
        mappings.put("tmcd", Box.LeafBox.class);
        mappings.put("tref", NodeBox.class);

        mappings.put("udta", UdtaBox.class);

        mappings.put("mdta", LeafBox.class);

    }

}
