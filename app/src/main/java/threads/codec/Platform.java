package threads.codec;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import threads.LogUtils;

public class Platform {

    public final static String UTF_8 = "UTF-8";
    public final static String UTF_16BE = "UTF-16BE";


    private final static Map<Class, Class> boxed2primitive = new HashMap<>();

    static {
        boxed2primitive.put(Void.class, void.class);
        boxed2primitive.put(Byte.class, byte.class);
        boxed2primitive.put(Short.class, short.class);
        boxed2primitive.put(Character.class, char.class);
        boxed2primitive.put(Integer.class, int.class);
        boxed2primitive.put(Long.class, long.class);
        boxed2primitive.put(Float.class, float.class);
        boxed2primitive.put(Double.class, double.class);
    }

    public static <T> T newInstance(Class<T> clazz, Object[] params) {
        try {
            return clazz.getConstructor(classes(params)).newInstance(params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Class[] classes(Object[] params) {
        Class[] classes = new Class[params.length];
        for (int i = 0; i < params.length; i++) {
            Class<?> cls = params[i].getClass();
            if (boxed2primitive.containsKey(cls)) {
                classes[i] = boxed2primitive.get(cls);
            } else {
                classes[i] = cls;
            }
        }
        return classes;
    }

    public static String stringFromCharset(byte[] data, String charset) {
        return new String(data, Charset.forName(charset));
    }

    public static byte[] getBytesForCharset(String url, String charset) {
        return url.getBytes(Charset.forName(charset));
    }


    static int[] copyOfRangeI(int[] original, int from, int to) {
        return Arrays.copyOfRange(original, from, to);
    }


    static void deleteFile(File file) {
        boolean result = file.delete();
        if (!result) {
            LogUtils.error(LogUtils.TAG, "File not renamed");
        }
    }

    public static byte[] getBytes(String fourcc) {
        try {
            return fourcc.getBytes("iso8859-1");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String stringFromBytes(byte[] bytes) {
        try {
            return new String(bytes, "iso8859-1");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static boolean isAssignableFrom(Class class1, Class class2) {
        if (class1 == class2) {
            return true;
        }
        return class1.isAssignableFrom(class2);
    }


    public static long unsignedInt(int signed) {
        return (long) signed & 0xffffffffL;
    }


}
