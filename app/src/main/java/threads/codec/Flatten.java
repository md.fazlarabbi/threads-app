package threads.codec;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import threads.codec.MP4Util.Movie;
import threads.codec.boxes.Box;
import threads.codec.boxes.ChunkOffsetsBox;
import threads.codec.boxes.DataRefBox;
import threads.codec.boxes.Header;
import threads.codec.boxes.NodeBox;
import threads.codec.boxes.TrakBox;
import threads.codec.boxes.UrlBox;
import threads.codec.common.SeekableByteChannel;

import static threads.codec.common.NIOUtils.readableChannel;
import static threads.codec.common.NIOUtils.writableChannel;

class Flatten {


    private void flattenChannel(Movie movie, SeekableByteChannel out) throws IOException {

        MovieBox moov = movie.getMoov();

        if (!moov.isPureRefMovie())
            throw new IllegalArgumentException("movie should be reference");
        out.setPosition(0);
        MP4Util.writeFullMovie(out, movie);

        int extraSpace = calcSpaceReq(moov);
        ByteBuffer buf = ByteBuffer.allocate(extraSpace);
        out.write(buf);

        long mdatOff = out.position();
        writeHeader(Header.createHeader("mdat", 0x100000001L), out);

        SeekableByteChannel[][] inputs = getInputs(moov);

        TrakBox[] tracks = moov.getTracks();
        ChunkReader[] readers = new ChunkReader[tracks.length];
        ChunkWriter[] writers = new ChunkWriter[tracks.length];
        Chunk[] head = new Chunk[tracks.length];

        long[] off = new long[tracks.length];
        for (int i = 0; i < tracks.length; i++) {
            readers[i] = new ChunkReader(tracks[i], inputs[i]);

            writers[i] = new ChunkWriter(tracks[i], inputs[i], out);
            head[i] = readers[i].next();
            if (tracks[i].isVideo())
                off[i] = 2 * moov.getTimescale();
        }

        while (true) {
            int min = -1;
            for (int i = 0; i < readers.length; i++) {
                if (head[i] == null)
                    continue;

                if (min == -1)
                    min = i;
                else {
                    long iTv = moov.rescale(head[i].getStartTv(), tracks[i].getTimescale()) + off[i];
                    long minTv = moov.rescale(head[min].getStartTv(), tracks[min].getTimescale()) + off[min];
                    if (iTv < minTv)
                        min = i;
                }
            }
            if (min == -1)
                break;

            writers[min].write(head[min]);

            head[min] = readers[min].next();

        }

        for (int i = 0; i < tracks.length; i++) {
            writers[i].apply();
        }
        long mdatSize = out.position() - mdatOff;

        out.setPosition(0);
        MP4Util.writeFullMovie(out, movie);

        long extra = mdatOff - out.position();
        if (extra < 0)
            throw new RuntimeException("Not enough space to write the header");
        writeHeader(Header.createHeader("free", extra), out);

        out.setPosition(mdatOff);
        writeHeader(Header.createHeader("mdat", mdatSize), out);
    }

    private void writeHeader(Header header, SeekableByteChannel out) throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(16);
        header.write(bb);
        bb.flip();
        out.write(bb);
    }


    private SeekableByteChannel[][] getInputs(MovieBox movie) throws IOException {
        TrakBox[] tracks = movie.getTracks();
        SeekableByteChannel[][] result = new SeekableByteChannel[tracks.length][];
        for (int i = 0; i < tracks.length; i++) {
            DataRefBox drefs = NodeBox.findFirstPath(tracks[i], DataRefBox.class, Box.path("mdia.minf.dinf.dref"));
            if (drefs == null) {
                throw new RuntimeException("No data references");
            }
            List<Box> entries = drefs.getBoxes();
            SeekableByteChannel[] e = new SeekableByteChannel[entries.size()];
            SeekableByteChannel[] inputs = new SeekableByteChannel[entries.size()];
            for (int j = 0; j < e.length; j++) {
                inputs[j] = resolveDataRef(entries.get(j));
            }
            result[i] = inputs;
        }
        return result;
    }

    private int calcSpaceReq(MovieBox movie) {
        int sum = 0;
        TrakBox[] tracks = movie.getTracks();
        for (TrakBox trakBox : tracks) {
            ChunkOffsetsBox stco = trakBox.getStco();
            if (stco != null)
                sum += stco.getChunkOffsets().length * 4;
        }
        return sum;
    }

    private SeekableByteChannel resolveDataRef(Box box) throws IOException {
        if (box instanceof UrlBox) {
            String url = ((UrlBox) box).getUrl();
            if (!url.startsWith("file://"))
                throw new RuntimeException("Only file:// urls are supported in data reference");
            return readableChannel(new File(url.substring(7)));
        } else {
            throw new RuntimeException(box.getHeader().getFourcc() + " dataref type is not supported");
        }
    }

    void flatten(Movie movie, File video) throws IOException {
        Platform.deleteFile(video);
        try (SeekableByteChannel out = writableChannel(video)) {
            flattenChannel(movie, out);
        }
    }


}