package threads.codec;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import threads.LogUtils;
import threads.codec.boxes.Box;
import threads.codec.boxes.FileTypeBox;
import threads.codec.boxes.Header;
import threads.codec.boxes.TrakBox;
import threads.codec.common.NIOUtils;
import threads.codec.common.SeekableByteChannel;

import static threads.codec.common.NIOUtils.readableChannel;


class MP4Util {

    private static Movie createRefFullMovie(SeekableByteChannel input, String url) throws IOException {
        Movie movie = parseFullMovieChannel(input);
        Objects.requireNonNull(movie);
        TrakBox[] tracks = movie.moov.getTracks();
        for (TrakBox trakBox : tracks) {
            trakBox.setDataRef(url);
        }
        return movie;
    }

    private static Movie parseFullMovieChannel(SeekableByteChannel input) throws IOException {
        FileTypeBox ftyp = null;
        for (Atom atom : getRootAtoms(input)) {
            if ("ftyp".equals(atom.getHeader().getFourcc())) {
                ftyp = (FileTypeBox) atom.parseBox(input);
            } else if ("moov".equals(atom.getHeader().getFourcc())) {
                return new Movie(ftyp, (MovieBox) atom.parseBox(input));
            }
        }
        return null;
    }

    static List<Atom> getRootAtoms(SeekableByteChannel input) throws IOException {
        input.setPosition(0);
        List<Atom> result = new ArrayList<>();
        long off = 0;
        Header atom;
        while (off < input.size()) {
            input.setPosition(off);
            atom = Header.read(NIOUtils.fetchFromChannel(input, 16));
            if (atom == null)
                break;
            result.add(new Atom(atom, off));
            off += atom.getSize();
        }

        return result;
    }


    static void writeMovie(SeekableByteChannel out, MovieBox movie) throws IOException {
        doWriteMovieToChannel(out, movie);
    }

    private static void doWriteMovieToChannel(SeekableByteChannel out, MovieBox movie)
            throws IOException {
        int sizeHint = estimateMoovBoxSize(movie);
        LogUtils.error(LogUtils.TAG, "Using " + sizeHint + " bytes for MOOV box");

        ByteBuffer buf = ByteBuffer.allocate(sizeHint * 4);
        movie.write(buf);
        buf.flip();
        out.write(buf);
    }

    static Movie parseFullMovie(File source) throws IOException {
        try (SeekableByteChannel input = readableChannel(source)) {
            return parseFullMovieChannel(input);
        }
    }

    static Movie createRefFullMovieFromFile(File source) throws IOException {
        try (SeekableByteChannel input = readableChannel(source)) {
            return createRefFullMovie(input, "file://" + source.getCanonicalPath());
        }
    }

    static void writeFullMovie(SeekableByteChannel out, Movie movie) throws IOException {
        doWriteFullMovieToChannel(out, movie);
    }

    private static void doWriteFullMovieToChannel(SeekableByteChannel out, Movie movie)
            throws IOException {
        int sizeHint = estimateMoovBoxSize(movie.getMoov());
        LogUtils.info(LogUtils.TAG, "Using " + sizeHint + " bytes for MOOV box");

        ByteBuffer buf = ByteBuffer.allocate(sizeHint + 128);
        movie.getFtyp().write(buf);
        movie.getMoov().write(buf);
        buf.flip();
        out.write(buf);
    }


    private static int estimateMoovBoxSize(MovieBox movie) {
        return movie.estimateSize() + (4 << 10);
    }


    static class Movie {
        private final FileTypeBox ftyp;
        private final MovieBox moov;

        Movie(FileTypeBox ftyp, MovieBox moov) {
            this.ftyp = ftyp;
            this.moov = moov;
        }

        FileTypeBox getFtyp() {
            return ftyp;
        }

        MovieBox getMoov() {
            return moov;
        }
    }

    public static class Atom {
        private final long offset;
        private final Header header;

        Atom(Header header, long offset) {
            this.header = header;
            this.offset = offset;
        }

        long getOffset() {
            return offset;
        }

        public Header getHeader() {
            return header;
        }

        Box parseBox(SeekableByteChannel input) throws IOException {
            input.setPosition(offset + header.headerSize());
            return BoxUtil.parseBox(NIOUtils.fetchFromChannel(input, (int) header.getBodySize()), header,
                    BoxFactory.getDefault());
        }

    }
}