package threads.codec;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import threads.codec.MP4Util.Atom;
import threads.codec.boxes.Box;
import threads.codec.boxes.Header;
import threads.codec.common.NIOUtils;
import threads.codec.common.SeekableByteChannel;

class RelocateEditor {

    void modifyOrRelocate(File src, MP4Edit edit) throws IOException {
        boolean modify = new InPlaceEditor().modify(src, edit);
        if (!modify)
            relocate(src, edit);
    }

    private void relocate(File src, MP4Edit edit) throws IOException {
        SeekableByteChannel f = null;
        try {
            f = NIOUtils.rwChannel(src);
            Atom moovAtom = getMoov(f);
            Objects.requireNonNull(moovAtom);
            ByteBuffer moovBuffer = fetchBox(f, moovAtom);
            MovieBox moovBox = (MovieBox) parseBox(moovBuffer);

            edit.apply(moovBox);

            if (moovAtom.getOffset() + moovAtom.getHeader().getSize() < f.size()) {
                f.setPosition(moovAtom.getOffset() + 4);
                f.write(ByteBuffer.wrap(Header.FOURCC_FREE));
                f.setPosition(f.size());
            } else {
                f.setPosition(moovAtom.getOffset());
            }
            MP4Util.writeMovie(f, moovBox);
        } finally {
            NIOUtils.closeQuietly(f);
        }
    }

    private ByteBuffer fetchBox(SeekableByteChannel fi, Atom moov) throws IOException {
        fi.setPosition(moov.getOffset());
        return NIOUtils.fetchFromChannel(fi, (int) moov.getHeader().getSize());
    }

    private Box parseBox(ByteBuffer oldMov) {
        Header header = Header.read(oldMov);
        return BoxUtil.parseBox(oldMov, header, BoxFactory.getDefault());
    }

    private Atom getMoov(SeekableByteChannel f) throws IOException {
        for (Atom atom : MP4Util.getRootAtoms(f)) {
            if ("moov".equals(atom.getHeader().getFourcc())) {
                return atom;
            }
        }
        return null;
    }
}
