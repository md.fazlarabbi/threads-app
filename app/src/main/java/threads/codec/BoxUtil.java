package threads.codec;

import java.nio.ByteBuffer;

import threads.codec.boxes.Box;
import threads.codec.boxes.Header;

class BoxUtil {

    static Box parseBox(ByteBuffer input, Header childAtom, IBoxFactory factory) {
        Box box = factory.newBox(childAtom);

        if (childAtom.getBodySize() < Box.MAX_BOX_SIZE) {
            box.parse(input);
            return box;
        } else {
            return new Box.LeafBox(Header.createHeader("free", 8));
        }
    }
}
