package threads.codec;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import threads.codec.boxes.Box;
import threads.codec.boxes.ChunkOffsets64Box;
import threads.codec.boxes.ChunkOffsetsBox;
import threads.codec.boxes.NodeBox;
import threads.codec.boxes.SampleEntry;
import threads.codec.boxes.SampleSizesBox;
import threads.codec.boxes.TrakBox;
import threads.codec.common.IntArrayList;
import threads.codec.common.NIOUtils;
import threads.codec.common.SeekableByteChannel;


class ChunkWriter {
    private final long[] offsets;
    private final SampleEntry[] entries;
    private final SeekableByteChannel[] inputs;
    private final SeekableByteChannel out;
    private final TrakBox trak;
    private final IntArrayList sampleSizes;
    private int curChunk;
    private int sampleSize;
    private int sampleCount;

    ChunkWriter(TrakBox trak, SeekableByteChannel[] inputs, SeekableByteChannel out) {
        entries = trak.getSampleEntries();
        ChunkOffsetsBox stco = trak.getStco();
        ChunkOffsets64Box co64 = trak.getCo64();
        int size;
        if (stco != null)
            size = stco.getChunkOffsets().length;
        else
            size = co64.getChunkOffsets().length;
        this.inputs = inputs;

        offsets = new long[size];
        this.out = out;
        this.trak = trak;
        this.sampleSizes = IntArrayList.createIntArrayList();
    }

    public void apply() {
        NodeBox stbl = NodeBox.findFirstPath(trak, NodeBox.class, Box.path("mdia.minf.stbl"));
        Objects.requireNonNull(stbl);
        stbl.removeChildren(new String[]{"stco", "co64"});

        stbl.add(ChunkOffsets64Box.createChunkOffsets64Box(offsets));

        SampleSizesBox stsz = sampleCount != 0 ? SampleSizesBox.createSampleSizesBox(sampleSize, sampleCount)
                : SampleSizesBox.createSampleSizesBox2(sampleSizes.toArray());
        stbl.replaceBox(stsz);
    }

    private SeekableByteChannel getInput(Chunk chunk) {
        SampleEntry se = entries[chunk.getEntry() - 1];
        return inputs[se.getDrefInd() - 1];
    }

    public void write(Chunk chunk) throws IOException {
        long pos = out.position();

        ByteBuffer chunkData = chunk.getData();
        if (chunkData == null) {
            SeekableByteChannel input = getInput(chunk);
            input.setPosition(chunk.getOffset());
            chunkData = NIOUtils.fetchFromChannel(input, (int) chunk.getSize());
        }

        out.write(chunkData);
        offsets[curChunk++] = pos;

        if (chunk.getSampleSize() == Chunk.UNEQUAL_SIZES) {
            if (sampleCount != 0)
                throw new RuntimeException("Mixed chunks unsupported 1.");
            sampleSizes.addAll(chunk.getSampleSizes());
        } else {
            if (sampleSizes.size() != 0)
                throw new RuntimeException("Mixed chunks unsupported 2.");
            if (sampleCount == 0) {
                sampleSize = chunk.getSampleSize();
            } else if (sampleSize != chunk.getSampleSize()) {
                throw new RuntimeException("Mismatching default sizes");
            }
            sampleCount += chunk.getSampleCount();
        }
    }
}