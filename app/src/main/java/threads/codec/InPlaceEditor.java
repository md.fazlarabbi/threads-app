package threads.codec;

import java.io.File;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import threads.codec.MP4Util.Atom;
import threads.codec.boxes.Box;
import threads.codec.boxes.Header;
import threads.codec.common.NIOUtils;
import threads.codec.common.SeekableByteChannel;
import threads.codec.common.Tuple;


class InPlaceEditor {


    boolean modify(File file, MP4Edit edit) throws IOException {
        SeekableByteChannel fi = null;
        try {
            fi = NIOUtils.rwChannel(file);

            List<Tuple._2<Atom, ByteBuffer>> fragments = doTheFix(fi, edit);
            if (fragments == null)
                return false;

            // If everything is clean, only then actually writing stuff to the
            // file
            for (Tuple._2<Atom, ByteBuffer> fragment : fragments) {
                replaceBox(fi, fragment.v0, fragment.v1);
            }

            return true;
        } finally {
            NIOUtils.closeQuietly(fi);
        }
    }


    private List<Tuple._2<Atom, ByteBuffer>> doTheFix(SeekableByteChannel fi, MP4Edit edit) throws IOException {
        Atom moovAtom = getMoov(fi);
        Objects.requireNonNull(moovAtom);

        ByteBuffer moovBuffer = fetchBox(fi, moovAtom);
        MovieBox moovBox = (MovieBox) parseBox(moovBuffer);

        List<Tuple._2<Atom, ByteBuffer>> fragments = new LinkedList<>();

        edit.apply(moovBox);

        if (!rewriteBox(moovBuffer, moovBox))
            return null;
        fragments.add(Tuple.pair(moovAtom, moovBuffer));
        return fragments;
    }

    private void replaceBox(SeekableByteChannel fi, Atom atom, ByteBuffer buffer) throws IOException {
        fi.setPosition(atom.getOffset());
        fi.write(buffer);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean rewriteBox(ByteBuffer buffer, Box box) {
        try {
            buffer.clear();
            box.write(buffer);
            if (buffer.hasRemaining()) {
                if (buffer.remaining() < 8)
                    return false;
                buffer.putInt(buffer.remaining());
                buffer.put(new byte[]{'f', 'r', 'e', 'e'});
            }
            buffer.flip();
            return true;
        } catch (BufferOverflowException e) {
            return false;
        }
    }

    private ByteBuffer fetchBox(SeekableByteChannel fi, Atom moov) throws IOException {
        fi.setPosition(moov.getOffset());
        return NIOUtils.fetchFromChannel(fi, (int) moov.getHeader().getSize());
    }

    private Box parseBox(ByteBuffer oldMov) {
        Header header = Header.read(oldMov);
        return BoxUtil.parseBox(oldMov, header, BoxFactory.getDefault());
    }

    private Atom getMoov(SeekableByteChannel f) throws IOException {
        for (Atom atom : MP4Util.getRootAtoms(f)) {
            if ("moov".equals(atom.getHeader().getFourcc())) {
                return atom;
            }
        }
        return null;
    }

}
