package threads.codec;

import threads.codec.boxes.Box;
import threads.codec.boxes.Header;
import threads.codec.boxes.NodeBox;

public class BoxFactory implements IBoxFactory {

    private static final IBoxFactory instance = new BoxFactory(new DefaultBoxes());
    private final Boxes boxes;

    private BoxFactory(Boxes boxes) {
        this.boxes = boxes;
    }

    public static IBoxFactory getDefault() {
        return instance;
    }

    @Override
    public Box newBox(Header header) {
        Class<? extends Box> claz = boxes.toClass(header.getFourcc());
        if (claz == null)
            return new Box.LeafBox(header);
        Box box = Platform.newInstance(claz, new Object[]{header});
        if (box instanceof NodeBox) {
            NodeBox nodebox = (NodeBox) box;
            nodebox.setFactory(this);
        }
        return box;
    }
}