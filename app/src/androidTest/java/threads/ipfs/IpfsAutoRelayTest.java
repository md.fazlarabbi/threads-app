package threads.ipfs;


import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import threads.LogUtils;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

@SuppressWarnings("SpellCheckingInspection")
@RunWith(AndroidJUnit4.class)
public class IpfsAutoRelayTest {

    private static final String TAG = IpfsAutoRelayTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void dummy() {
        assertNotNull(context);
    }

    //@Test
    public void testAutoRelay() throws Exception {


        IPFS ipfs = TestEnv.getTestInstance(context);


        AtomicInteger counter = new AtomicInteger(0);
        while (counter.incrementAndGet() < 40) {


            boolean result = ipfs.swarmConnect("/ip4/2.207.31.127/tcp/4001/ipfs/QmPdjjouL3gJHEAJxosF964QQYG6AXCsVoeXktaKkN6i51", 30000);
            assertTrue(result);


            Peer peer = ipfs.swarmPeer(PID.create("QmPdjjouL3gJHEAJxosF964QQYG6AXCsVoeXktaKkN6i51"));


            assertNotNull(peer);


            LogUtils.error(TAG, "" + peer.toString());


            ipfs.swarmConnect("/ip4/18.224.32.43/tcp/4001/ipfs/QmaXbbcs7LRFuEoQcxfXqziZATzS68WT5DgFjYFgn3YYLX", 10000);


            ipfs.swarmPeers();

            PeerInfo info = ipfs.id();

            assertNotNull(info);
            LogUtils.error(TAG, "" + info.toString());


            List<String> addresses = info.getMultiAddresses();
            for (String address : addresses) {

                if (address.contains("p2p-circuit")) {
                    LogUtils.error(TAG, "" + address);

                    String[] res = address.split("p2p-circuit");

                    String moin = res[1].replaceFirst("/ipfs/", "");
                    PeerInfo moinInfo = ipfs.id(PID.create(moin), 10);
                    if (moinInfo != null) {
                        LogUtils.error(TAG, moinInfo.toString());

                        PID pid = moinInfo.getPID();
                        assertNotNull(pid);
                        Peer pInfo = ipfs.swarmPeer(pid);
                        assertNotNull(pInfo);
                        LogUtils.error(TAG, "" + pInfo.toString());
                    }


                }

            }


            Thread.sleep(10000);
        }


    }
}
