package threads.ipfs;


import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import threads.LogUtils;

import static junit.framework.TestCase.assertNotNull;

@SuppressWarnings("SpellCheckingInspection")
@RunWith(AndroidJUnit4.class)
public class IpfsSwarmWriterTest {

    private static final String TAG = IpfsSwarmWriterTest.class.getSimpleName();

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    private static String getRandomString(int number) {
        return "" + RandomStringUtils.randomAscii(number);
    }


    @Test
    public void dummy() {
        assertNotNull(context);
    }

    //@Test
    public void write() throws Exception {
        PID RELAY_PID = PID.create("QmWFhiem9PnRAm9pBHQYvRqQcGAeJ2VfSFhD3JKdytiWKG");

        IPFS ipfs = TestEnv.getTestInstance(context);
        LogUtils.error(TAG, "Connecting to RELAY ...");
        boolean success = ipfs.swarmConnect(RELAY_PID, 10);
        LogUtils.error(TAG, "Connecting to RELAY done " + success);


        PeerInfo info = ipfs.id();
        assertNotNull(info);
        PID pid = info.getPID();
        assertNotNull(pid);
        LogUtils.error(TAG, "PID : " + pid.getPid());


        File file = createRandomFile();

        CID hash58Base = ipfs.storeFile(file);
        assertNotNull(hash58Base);
        LogUtils.error(TAG, "CID : " + hash58Base.getCid());


        try {

            info = ipfs.id();
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());
            Thread.sleep(60000);

        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage());
        }
    }

    private File createRandomFile() throws Exception {


        int packetSize = 1000;
        long maxData = 10;

        IPFS ipfs = TestEnv.getTestInstance(context);

        File inputFile = ipfs.createCacheFile();
        String randomString = getRandomString(packetSize);
        for (int i = 0; i < maxData; i++) {
            FileServer.insertRecord(inputFile, i, packetSize, randomString.getBytes());
        }

        return inputFile;
    }
}
