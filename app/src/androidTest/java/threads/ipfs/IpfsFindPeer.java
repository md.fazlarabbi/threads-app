package threads.ipfs;


import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import threads.LogUtils;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

@SuppressWarnings("SpellCheckingInspection")
@RunWith(AndroidJUnit4.class)
public class IpfsFindPeer {
    private static final String TAG = IpfsFindPeer.class.getSimpleName();
    private static final String RELAY_PID = "QmVLnkyetpt7JNpjLmZX21q9F8ZMnhBts3Q53RcAGxWH6U";
    private static final String DUMMY_PID = "QmVLnkyetpt7JNpjLmZX21q9F8ZMnhBts3Q53RcAGxWH6V";

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void dummy() {
        assertNotNull(context);
    }

    @Test
    public void no_id() {

        IPFS ipfs = TestEnv.getTestInstance(context);

        PeerInfo info = ipfs.id();
        assertNotNull(info);
        assertEquals(info.getProtocolVersion(), "ipfs/0.1.0");
        LogUtils.error(LogUtils.TAG, info.getPID().getPid());

        PID dummy = PID.create(DUMMY_PID);

        PeerInfo dummyInfo = ipfs.id(dummy, 5);
        assertNull(dummyInfo);

    }

    @Test
    public void swarm_connect() {

        IPFS ipfs = TestEnv.getTestInstance(context);
        PID pc = PID.create("QmRxoQNy1gNGMM1746Tw8UBNBF8axuyGkzcqb2LYFzwuXd");

        // TIMEOUT not working
        boolean result = ipfs.swarmConnect(pc, 6);
        assertFalse(result);

    }


    //libp@Test
    public void test_find_peer() {
        IPFS ipfs = TestEnv.getTestInstance(context);

        PID relay = PID.create(RELAY_PID);
        PID dummy = PID.create(DUMMY_PID);
        PeerInfo peerInfo = ipfs.id(relay, 5);
        assertNotNull(peerInfo);

        PeerInfo dummyInfo = ipfs.id(dummy, 5);
        assertNotNull(dummyInfo);

        ipfs.swarmConnect(relay, 10);


        ipfs.relay(relay, dummy, 10);

        List<Peer> peers = ipfs.swarmPeers();
        assertNotNull(peers);
        for (Peer peer : peers) {

            if (peer.getPid().equals(relay)) {
                LogUtils.error(LogUtils.TAG, "Connect with specificRelay PID");
            }

        }


    }

    //@Test
    public void test_local_peer() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        PID local = PID.create("Qmf5TsSK8dVm3btzuUrnvS8wfUW6e2vMxMRkzV9rsG6eDa");

        PeerInfo peerInfo = ipfs.id(local, 60);
        assertNotNull(peerInfo);

        boolean result = ipfs.swarmConnect(local, 60);

        assertTrue(result);

        while (ipfs.isConnected(local)) {
            LogUtils.error(TAG, "Peer conntected with : " + local.getPid());
            Thread.sleep(1000);
        }
    }

    //@Test
    public void test_fail_find_peer() {
        IPFS ipfs = TestEnv.getTestInstance(context);

        PID relay = PID.create(DUMMY_PID);
        PID dummy = PID.create(DUMMY_PID);
        PeerInfo peerInfo = ipfs.id(relay, 10);
        assertNull(peerInfo);

        LogUtils.error(TAG, "relay_connect");
        ipfs.swarmConnect(relay, 10);


        ipfs.relay(relay, dummy, 15);

        LogUtils.error(TAG, "swarmPeers");

        List<Peer> peers = ipfs.swarmPeers();
        assertNotNull(peers);
        for (Peer peer : peers) {
            LogUtils.error(TAG, peer.toString());

            if (peer.getPid().equals(relay)) {
                LogUtils.error(TAG, "Connect with specificRelay PID");
            }

        }

    }

    @Test
    public void test_swarm_connect() {
        IPFS ipfs = TestEnv.getTestInstance(context);

        PID relay = PID.create("QmchgNzyUFyf2wpfDMmpGxMKHA3PkC1f3H2wUgbs21vXoz");


        boolean connected = ipfs.isConnected(relay);
        assertFalse(connected);


        boolean result = ipfs.swarmConnect(relay, 1);
        assertFalse(result);


        relay = PID.create("QmchgNzyUFyf2wpfDMmpGxMKHA3PkC1f3H2wUgbs21vXoz");
        result = ipfs.swarmConnect(relay, 10);
        assertFalse(result);


        relay = PID.create(DUMMY_PID);
        result = ipfs.swarmConnect(relay, 10);
        assertFalse(result);

    }


    @Test
    public void test_find_swarm_peers() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);


        AtomicBoolean found = new AtomicBoolean(false);

        AtomicInteger atomicInteger = new AtomicInteger(0);


        while (!found.get() && atomicInteger.incrementAndGet() < 10) {
            List<Peer> peers = ipfs.swarmPeers();

            assertNotNull(peers);
            LogUtils.error(TAG, "Peers : " + peers.size());
            for (Peer peer : peers) {

                long time = System.currentTimeMillis();
                LogUtils.error(TAG, "isConnected : " + ipfs.isConnected(peer.getPid())
                        + " " + (System.currentTimeMillis() - time));

                LogUtils.error(TAG, peer.toString());

                PeerInfo peerInfo = ipfs.id(peer, 15);
                if (peerInfo != null) {
                    String publicKey = peerInfo.getPublicKey();
                    assertNotNull(publicKey);
                    LogUtils.error(TAG, peerInfo.toString());
                }


                if (peer.isRelay()) {

                    boolean result = ipfs.checkRelay(peer, 10);
                    LogUtils.error(TAG, "Relay : " + result);

                    if (result) {


                        boolean value = ipfs.swarmConnect(peer.getPid(), 10);
                        LogUtils.error(TAG, "Connect : " + value);

                        LogUtils.error(TAG, peer.toString());


                        found.set(true);
                    }

                    result = ipfs.checkRelay(peer, 5);
                    LogUtils.error(TAG, "Relay : " + result);
                }
            }


            Thread.sleep(10000);
        }

    }

}
